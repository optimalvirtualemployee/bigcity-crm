<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_campaign', function (Blueprint $table) {
            $table->id();
            $table->string('campaign_name');
            $table->string('campaign_mechanic');
            $table->string('campaign_type');
            $table->string('campaign_description');
            $table->enum('sms_required',['0','1']);
            $table->date('campaign_start');
            $table->date('campaign_end');
            $table->bigInteger('campaign_validity');
            $table->string('campaign_experiences');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_campaign');
    }
}
