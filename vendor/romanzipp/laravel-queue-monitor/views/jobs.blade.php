@extends('common.default')
@section('title', 'jobs List')
@section('content')
<style type="text/css">
    .text-green-800 {
    --text-opacity: 1;
    color: #276749;
    color: rgba(39,103,73,var(--text-opacity));
}
.px-2 {
    padding-left: .5rem;
    padding-right: .5rem;
}
.leading-5 {
    line-height: 1.25rem;
}
.text-xs {
    font-size: .75rem;
}
.font-medium {
    font-weight: 500;
}
.flex-1 {
    flex: 1 1 0%;
}
.inline-flex {
    display: inline-flex;
}
.rounded-full {
    border-radius: 9999px;
}
.bg-green-200 {
    --bg-opacity: 1;
    background-color: #c6f6d5;
    background-color: rgba(198,246,213,var(--bg-opacity));
}

.text-blue-800 {
    --text-opacity: 1;
    color: #2c5282;
    color: rgba(44,82,130,var(--text-opacity));
}
.px-2 {
    padding-left: .5rem;
    padding-right: .5rem;
}
.leading-5 {
    line-height: 1.25rem;
}
.text-xs {
    font-size: .75rem;
}
.font-medium {
    font-weight: 500;
}
.flex-1 {
    flex: 1 1 0%;
}
.inline-flex {
    display: inline-flex;
}
.rounded-full {
    border-radius: 9999px;
}
.bg-blue-200 {
    --bg-opacity: 1;
    background-color: #bee3f8;
    background-color: rgba(190,227,248,var(--bg-opacity));
}
.text-red-800 {
    --text-opacity: 1;
    color: #9b2c2c;
    color: rgba(155,44,44,var(--text-opacity));
}
.px-2 {
    padding-left: .5rem;
    padding-right: .5rem;
}
.leading-5 {
    line-height: 1.25rem;
}
.text-xs {
    font-size: .75rem;
}
.font-medium {
    font-weight: 500;
}
.flex-1 {
    flex: 1 1 0%;
}
.inline-flex {
    display: inline-flex;
}
.rounded-full {
    border-radius: 9999px;
}
.bg-red-200 {
    --bg-opacity: 1;
    background-color: #fed7d7;
    background-color: rgba(254,215,215,var(--bg-opacity));
}
.overflow-hidden {
    overflow: hidden;
}
.h-3 {
    height: .75rem;
}
.items-stretch {
    align-items: stretch;
}
.flex {
    display: flex;
}
.rounded-full {
    border-radius: 9999px;
}
.bg-gray-300 {
    --bg-opacity: 1;
    background-color: #e2e8f0;
    background-color: rgba(226,232,240,var(--bg-opacity));
}
.h-full {
    height: 100%;
}
.bg-green-500 {
    --bg-opacity: 1;
    background-color: #48bb78;
    background-color: rgba(72,187,120,var(--bg-opacity));
}

</style>
<!-- Main Content -->
<div id="content">
<div class="container-fluid" style="margin-top:15px;">
<div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
        
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Queue Monitor</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">

                        
                        <a  href="{{url('admin/generate_codes')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>

                         
                    
                </div>

                <form action="" method="get">

                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">

                    <input type="checkbox" name="only_failed" id="only-failed" @if($filters['onlyFailed']) checked @endif>

                    <label for="only-failed" class="text-sm ml-2 text-gray-900">
                        Only show failed jobs
                    </label>

                </div>

                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">

                    <button type="submit" class="btn btn-primary btn-sm  js-click-ripple-enabled">
                        Filter
                    </button>

                </div>

            </form>
            </div>
        </div>
    </div>
        
 
      <div class="card">
         
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >Status</th>
                        <th >Job</th>
                        <th>Details</th>
                        @if(config('queue-monitor.ui.show_custom_data'))

                        <th>Custom Data</th>

                        @endif
                        <th>Progress</th>
                        <th>Duration</th>
                        <th>Started</th>
                        <th>Error</th>
                        
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($jobs as $job)
                      
                    <tr>
                        <td>@if(!$job->isFinished())

                                <div class="inline-flex flex-1 px-2 text-xs font-medium leading-5 rounded-full bg-blue-200 text-blue-800">
                                    Running
                                </div>

                            @elseif($job->hasSucceeded())

                                <div class="inline-flex flex-1 px-2 text-xs font-medium leading-5 rounded-full bg-green-200 text-green-800">
                                    Success
                                </div>

                            @else

                                <div class="inline-flex flex-1 px-2 text-xs font-medium leading-5 rounded-full bg-red-200 text-red-800">
                                    Failed
                                </div>

                            @endif</td>
                        <td>{{ $job->getBaseName() }}

                            <span class="ml-1 text-xs text-gray-600">
                                #{{ $job->job_id }}
                            </span></td>
                        <td><div class="text-xs">
                                <span class="text-gray-600 font-medium">Queue:</span>
                                <span class="font-semibold">{{ $job->queue }}</span>
                            </div>

                            <div class="text-xs">
                                <span class="text-gray-600 font-medium">Attempt:</span>
                                <span class="font-semibold">{{ $job->attempt }}</span>
                            </div></td>
                        @if(config('queue-monitor.ui.show_custom_data'))
                        <td>
                            <textarea rows="4" class="w-64 text-xs p-1 border rounded" readonly>{{ json_encode($job->getData(), JSON_PRETTY_PRINT) }}
                            </textarea>
                        </td>
                        @endif

                        <td>@if($job->progress !== null)

                                <div class="w-32">

                                    <div class="flex items-stretch h-3 rounded-full bg-gray-300 overflow-hidden">
                                        <div class="h-full bg-green-500" style="width: {{ $job->progress }}%"></div>
                                    </div>

                                    <div class="flex justify-center mt-1 text-xs text-gray-800 font-semibold">
                                        {{ $job->progress }}%
                                    </div>

                                </div>

                            @else
                                -
                            @endif</td>
                        <td>{{ sprintf('%02.2f', (float) $job->time_elapsed) }} s</td>
                        <td>{{ $job->started_at->diffForHumans() }}</td>
                        <td>@if($job->hasFailed() && $job->exception_message !== null)

                                <textarea rows="4" class="w-64 text-xs p-1 border rounded" readonly>{{ $job->exception_message }}</textarea>

                            @else
                                -
                            @endif</td>    

                    </tr>
                    @empty
                    <tr>

                        <td colspan="100" class="">

                            <div class="my-6">

                                <div class="text-center">

                                    <div class="text-gray-500 text-lg">
                                        No Jobs
                                    </div>

                                </div>

                            </div>

                        </td>

                    </tr>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      
   </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
