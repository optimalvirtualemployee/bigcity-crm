<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-config', function() {
    Artisan::call('queue:flush');
    return "Config is cleared";
});

Route::get('/', function () {
    return view('auth.login');
    });
Route::post('/logins', 'LoginController@checklogin')->name('login');
Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('login');
});
Route::group(['middleware'=>['auth'],'prefix'=> 'admin/background-jobs',], function (){

    Route::queueMonitor();
    
});
//admin authenticate

Route::group(['middleware'=>['auth'],'prefix'=> 'admin/',], function (){

    Route::get('dashboard', 'HomeController@dashboard');
    //Get Category Resource
    Route::resource('categories', 'CategoryController');
    //Get Category dashboard
    Route::get('category/dashboard', 'CategoryController@dashboard');
    //Get Category status change
    Route::get('changeStatusCategory', 'CategoryController@changeCategoryStatus');
    //Download Category
    
    Route::get('category/download-category-excel', 'CategoryController@categoryDownload')->name('category.download-category-excel');
    
    
    // ---------------- Communication Template Type ---------------------------
    
    //Get Communication Template Type dashboard
    Route::get('communication_template_type/dashboard', 'CommunicationTemplateTypeController@dashboard');
    
    //Get Communication Template Type Resource
    Route::resource('communication_template_type', 'CommunicationTemplateTypeController');
    
    //Get Communication Template Type status change
    Route::get('changeStatusCommunicationTemplateType', 'CommunicationTemplateTypeController@changeCommunicationTemplateTypeStatus');
    
    //Download Communication Template Type
    
    Route::get('communication_template_type/download-communication_template_type-excel', 'CommunicationTemplateTypeController@communication_template_typeDownload')->name('communication_template_type.download-communication_template_type-excel');
    
    // ---------------- Communication Template Type ---------------------------
    
    
    Route::get('area/download-area-excel', 'AreaController@allAreaDownload')->name('area.download-area-excel');
    
    
    //Get Product Resource
    Route::resource('products', 'ProductController');
    //Get Product dashboard
    Route::get('product/dashboard', 'ProductController@dashboard');
    //Get Product status change
    Route::get('changeStatusProduct', 'ProductController@changeProductStatus');
    //Download Category
    
    Route::get('product/download-product-excel', 'ProductController@productDownload')->name('product.download-product-excel');
    
    //Get User Resource
    Route::resource('users', 'User\UserController');
    Route::get('user/download-user-excel', 'User\UserController@userDownload')->name('user.download-user-excel');
    Route::resource('roles','User\RoleController');
    //Get User dashboard
    Route::get('user/dashboard', 'User\UserController@dashboard');
    //Get User status change
    Route::get('changeStatusUser', 'User\UserController@changeUserStatus');
    
    //Get State Resource
    Route::resource('states', 'StateController');
    //Get State dashboard
    Route::get('state/dashboard', 'StateController@dashboard');
    //Download State
    
    Route::get('state/download-state-excel', 'StateController@stateDownload')->name('state.download-state-excel');
    //Get City Resource
    Route::resource('cities', 'CityController');
    //Get City dashboard
    Route::get('city/dashboard', 'CityController@dashboard');
    //Upload City
    Route::post('city/upload-city-excel', 'CityController@uploadCity')->name('city.upload-city-excel');
    //Download city
    Route::get('city/download-city-excel', 'CityController@cityDownload')->name('city.download-city-excel');
    Route::get('city/download-allcity-excel', 'CityController@allCityDownload')->name('allcity.download-allcity-excel');
    //Get Area Resource
    Route::resource('areas', 'AreaController');
    //Upload Area
    Route::post('area/upload-area-excel', 'AreaController@uploadArea')->name('area.upload-area-excel');
    //Get Area dashboard
    Route::get('area/dashboard', 'AreaController@dashboard');
    Route::get('get-city-list','AreaController@getCityList');
    Route::get('get-area-list','AreaController@getAreaList');
    
    //Get VoucherType Resource
    Route::resource('vouchertype', 'Campaign\VoucherTypeController');
    //Get VoucherType Dashboard
    Route::get('voucher-type/dashboard', 'Campaign\VoucherTypeController@dashboard');
    Route::get('voucher-type/download-voucher-type-excel', 'Campaign\VoucherTypeController@voucherTypeDownload')->name('voucher-type.download-voucher-type-excel');
    //Get VoucherType Resource
    Route::resource('voucherspecification', 'Campaign\VoucherSpecificationController');
    //Get VoucherType Dashboard
    Route::get('voucher-specification/dashboard', 'Campaign\VoucherSpecificationController@dashboard');
    Route::get('voucher-specification/download-voucher-specification-excel', 'Campaign\VoucherSpecificationController@voucherSpecificationDownload')->name('voucher-specification.download-voucher-specification-excel');
    //post ajax request for checking values exists or not
    Route::post('getVoucher_specification','Campaign\VoucherSpecificationController@getVoucher_specification');
    
    //Get Delivery Mechanic Type Resource
    Route::resource('deliverymechanictype','Campaign\DeliveryMechanicTypeController');
    //Get Delivery Mechanic Type Dashboard

    Route::get('deliverymechanic-type/dashboard', 'Campaign\DeliveryMechanicTypeController@dashboard');
    Route::get('deliverymechanic-type/download-deliverymechanic-type-excel', 'Campaign\DeliveryMechanicTypeController@deliveryMechanicTypeDownload')->name('deliverymechanic-type.download-deliverymechanic-type-excel');
    //Get Promo TypeResource
    Route::resource('promotype','Campaign\PromoTypeController');
    //Get Promo Type Dashboard
    Route::get('promo-type/dashboard', 'Campaign\PromoTypeController@dashboard');
    Route::get('promo-type/download-promo-type-excel', 'Campaign\PromoTypeController@promoTypeDownload')->name('promo-type.download-promo-type-excel');
    //Get Company Resource
    Route::resource('companies','Campaign\CompanyController');
    //Get Company Dashboard
    Route::get('company/dashboard', 'Campaign\CompanyController@dashboard');
    Route::get('company/download-company-excel', 'Campaign\CompanyController@companyDownload')->name('company.download-company-excel');
    //Get Campaign Type Resource
    Route::resource('campaigntype','Campaign\CampaignTypeController');
    //Get Campaign Type Dashboard
    Route::get('campaign-type/dashboard', 'Campaign\CampaignTypeController@dashboard');
    
    //Get Venue  Resource
    Route::resource('venues','Pd\VenueController');
    //Get Venue  Resource
    Route::resource('venue-qc','Pd\VenueQcController');
    Route::resource('qc-rejected-venue','Pd\QCRejectedVenueController');
    //Get Venue  Resource
    Route::resource('target-assign','Pd\TargetAssignController');
    //Get Venue  Dashboard
    Route::get('venue/dashboard', 'Pd\VenueController@dashboard');
    //Get Product List On Venue Module
    Route::get('get-product-list','Pd\VenueController@getProductList');
    //Upload Venue Location On Venue Module
    Route::post('venue/upload-venue-location-excel', 'Pd\VenueController@uploadVenueLocation')->name('venue.upload-venue-location-excel');
    
    //Get Campaign  Resource
    Route::resource('campaigns','Campaign\CampaignController');
    //Get Campaign Dashboard
    Route::get('campaign/dashboard','Campaign\CampaignController@dashboard');
    //Get Category Export Excel
    Route::get('export_category/excel', 'CategoryController@categoryExportExcel')->name('export_category.excel');
    //Get Product Rule  Resource
    Route::resource('productrules','ProductRuleController');
    Route::get('changingProduct', 'ProductRuleController@changingProduct');
    //Get Generate Code  Resource
    Route::resource('generate_codes','Campaign\GenerateCodesController');
    
    Route::get('cronjob','CronJobController@store');
    
    Route::get('generate_code/csv-download/{id}','Campaign\GenerateCodesController@csvDownload');
    
    Route::resource('mobile-usage-rules','Campaign\MobileUsageRuleController');
    //Get Code  Resource
    Route::get('generate_code/voucher_code/{id}','Campaign\GenerateCodesController@allVoucherCode')->name('generate_code.voucher_code');
    Route::get('generate_code/download-voucher-excel/{id}', 'Campaign\GenerateCodesController@voucherCodeDownload')->name('voucher.download-voucher-excel');
    // Get ProductList Dropdown on behalf of campaign ID
    Route::get('get_genproductList','Campaign\GenerateCodesController@get_genProductList');

    // Get Product NameList Dropdown on behalf of campaign ID
    Route::get('get_genproductName','Campaign\GenerateCodesController@get_genProductName');
    //Campaign Details Resource
    Route::resource('campaign_details','Campaign\CampaignDetailsController');
    // Get ProductRuleList for Campaign Detail Page Dropdown on behalf of product ID
    Route::get('get_ProductRuleList','Campaign\CampaignDetailsController@get_ProductRuleList');
    //Tag Campaign Venue Resource
    Route::resource('tag_campaign_venues','Campaign\TagCampaignVenueController');    
    Route::get('tag_campaign_venues/{campaign_id}/{product_id}','Campaign\TagCampaignVenueController@show')->name('tag_campaign_venues.show');
    Route::get('tag_campaign_venues/{campaign_id}/{product_id}/edit','Campaign\TagCampaignVenueController@edit')->name('tag_campaign_venues.edit');
    Route::PATCH('tag_campaign_venues/{campaign_id}/{product_id}','Campaign\TagCampaignVenueController@update')->name('tag_campaign_venues.update');
    
    // Get ProductList Dropdown on behalf of campaign ID
    Route::get('get_productList','Campaign\TagCampaignVenueController@get_productList');

    // Get Product NameList Dropdown on behalf of campaign ID
    Route::get('get_productName','Campaign\TagCampaignVenueController@get_productName');

    // Get Venue list on behalf of City ID
    Route::get('cityTag_venueList','Campaign\TagCampaignVenueController@get_venueList');
    
    
    //------------Language ------------------------------------------//

    //Get Language dashboard
    Route::get('language/dashboard', function () {
    return view('admin.primary_language.dashboard');
    });

    // Get primary language
    Route::resource('primary_language','PrimaryLanguageController');

    //Get Category status change
    Route::post('changeStatus_primaryLanguage', 'PrimaryLanguageController@change_PrimaryLanguage_Status');

    //Get primary language Export Excel
    Route::get('export_primarylanguage/excel', 'PrimaryLanguageController@primarylanguage_ExportExcel')->name('export_primarylanguage.excel');
    
    //Other Language Resource
    Route::resource('otherlanguage','OtherLanguageController');  
   
    Route::get('export_otherlanguage/excel', 'OtherLanguageController@otherLanguageDownload')->name('export_otherlanguage.excel');
    //------------Language ------------------------------------------//
    //Get Operation Inbound dashboard
    Route::get('operation-management/dashboard', 'BookingAssignController@dashboard');
    //Get customer Query status change
    Route::post('changeStatus_customerQuery', 'CustomerQueryController@changeCustomerQueryStatus');
    Route::resource('assign-type', 'AssignTypeController');
    Route::resource('booking-assign', 'BookingAssignController');
    Route::post('booking-assign/fetch_data', 'BookingAssignController@fetch_data')->name('booking-assign.fetch_data');
    // Get customer Query
    Route::resource('customerquery','CustomerQueryController');
    
    // Get customer Query
    Route::resource('operationinbound','OperationInboundController');
    
    //Get Customer Transaction History
    Route::get('customer-transaction-history','InboundModuleController@customerTransactionHistory');
    
    // ---------------- Operation Inbound Module starts here ------------------------------------//
    
        // Get Inbound Module form
        Route::get('inbound_module', function(){
            return view('admin.inbound_module.inbound_form');
        });

    Route::post('inbound_module/popup','InboundModuleController@popup');
    Route::get('inbound_module/booking_details','InboundModuleController@booking_details');
    Route::post('inbound_module/cancel_booking','InboundModuleController@cancel_booking');
    Route::get('inbound_module/check_request','InboundModuleController@check_request');
    Route::post('inbound_module/cannot_rescheduled','InboundModuleController@cannot_rescheduled');
    Route::post('inbound_module/can_rescheduled','InboundModuleController@can_rescheduled');
    Route::post('inbound_module/send_email_microsite_issue','InboundModuleController@send_email_microsite_issue');
    Route::post('inbound_module/send_email_venue_related_issue','InboundModuleController@send_email_venue_related_issue');
    Route::post('inbound_module/booking_confirmation','InboundModuleController@booking_confirmation');
    Route::post('inbound_module/unable_to_register','InboundModuleController@unable_to_register');
    Route::post('inbound_module/validity_related','InboundModuleController@validity_related');
    Route::post('inbound_module/contest_related','InboundModuleController@contest_related');
    Route::post('inbound_module/voucher_offer_code_related','InboundModuleController@voucher_offer_code_related');
    Route::post('inbound_module/reward_offer_code_resend','InboundModuleController@reward_offer_code_resend');
    Route::post('inbound_module/queryDetails','InboundModuleController@queryDetails');    
    Route::post('inbound_module/queryName','InboundModuleController@queryName');    
    
    // ---------------- Operation Inbound Module ends here -------------------------------------//
    
    //Get Operation Inbound dashboard
    Route::get('winner-campaign/dashboard', 'Winner\WinnerCampaignController@dashboard');

    // Winner Management
    Route::resource('winnercampaign','Winner\WinnerCampaignController');
    Route::resource('gifttype','Winner\GiftTypeController');
    Route::resource('setgiftallocation','Winner\SetGiftAllocationController');
    Route::resource('set-gift-attribute','Winner\GiftAttributeController');
    Route::resource('holiday-attribute','Winner\HolidayAttributeController');
    Route::resource('set-winner-frequency','Winner\SetWinnerFrequencyController');
    Route::resource('winner-data-upload','Winner\WinnerDataUploadController');
    Route::resource('winner-data-operation','Operation\WinnerDataOperationController');
    Route::resource('winner-agent','Winner\WinnerAgentController');
    
    
    
   // Route::get('target-assign','HomeController@targetAssign');
    Route::PATCH('venue-detail/{id}','Pd\VenueQcController@updateVenueDetail');
    Route::PATCH('rejected-venue-detail/{id}','Pd\QCRejectedVenueController@updateRejectedVenueDetail');
    Route::GET('voucher/generate-coupons','Campaign\GenerateCodesController@generateCoupons');
    
    Route::Post('generate_codes/store', function(){
        
        \Illuminate\Support\Facades\Artisan::call('queue:work');
        
    });
    Route::resource('communication-template','CommunicationTemplateController');
    
    Route::get('total-booking-report','Report\ReportController@totalBookingReport');
    Route::get('re-booking-report','Report\ReportController@reBookingReport');
    Route::get('cancel-booking-report','Report\ReportController@cancelBookingReport');
    Route::get('confirm-booking-report','Report\ReportController@confirmBookingReport');
    Route::get('venue-wise-booking-report','Report\ReportController@venueWiseBookingReport');
    
    Route::get('report/dashboard','Report\ReportController@dashboard');
    
    
     
});

//admin authenticate
Route::group(['middleware'=>['auth','QC'],'prefix'=> 'QC/',], function (){

    Route::get('dashboard','QC\HomeController@dashboard');
    Route::get('venue-list','QC\HomeController@getVenueList');
    Route::get('venue-detail/{id}','QC\HomeController@venueDetail');
    Route::PATCH('venue-detail/{id}','QC\HomeController@updateVenueDetail');
});
//Auth authenticate.
Auth::routes();
// Route::group(['middleware'=>['auth'],'prefix'=> '/',], function (){

//     // Get booking Agent dashboard
//     Route::get('booking-champion/dashboard','BookingChampionController@dashboard');
//     Route::resource('booking-champion/bookings','BookingChampionController');
//     Route::post('booking-champion/bookings/fetch_data', 'BookingChampionController@fetch_data')->name('bookings.fetch_data');
// });

Route::group(['middleware'=>['auth'],'prefix'=> '/',], function (){

// Get booking Agent dashboard
Route::get('booking-champion/dashboard','BookingChampionController@dashboard');
Route::resource('booking-champion/bookings','BookingChampionController');
Route::post('booking-champion/bookings/fetch_data', 'bookingchampioncontroller@fetch_data')->name('bookings.fetch_data');
route::post('booking-champion/bookings/popup','bookingchampioncontroller@popup')->name('bookings.popup');
Route::post('booking-champion/bookings/validateBooking', 'bookingchampioncontroller@validateBooking')->name('bookings.validateBooking');

});
