<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WinnerCampaign extends Model
{
    protected $table = 'tbl_winner_campaign';
    protected $fillable = ['campaign_name','campaign_brand_id','bd_person_name_id','cs_person_name_id','is_auditor_required','start_date','end_date','total_no_of_winners','gift_type_id','gift_tax_applicable'];
    
    //Company function
    public function company()
    {
        return $this->belongsTo('App\Company','campaign_brand_id','id');
    }
    

    //BD User function
    public function bd_user()
    {
        return $this->belongsTo('App\User','bd_person_name_id','id');
    }

    //CS User function
    public function cs_user()
    {
        return $this->belongsTo('App\User','cs_person_name_id','id');
    }
    
    //CS User function
    public function gift_type()
    {
        return $this->belongsTo('App\GiftType','gift_type_id','id');
    }
}
