<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class Campaign extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_campaign';
    protected $fillable = ['campaign_name','campaign_brand_id','campaign_type_id','campaign_bd_person_name_id','campaign_cs_person_name_id','campaign_estimate_no','campaign_estimate_date','campaign_upload_signed_estimate_po','campaign_mechanic_type_id','campaign_gateway_details','sms_keyword','sms_long_code','web_microsite_name','campaign_voucher_type_id','voucher_spec_type_id','is_printing_bigcity','campaign_voucher_cost','campaign_cost_to_client','campaign_cost_to_bigcity'];
    
   
    //Company function
    public function company()
    {
        return $this->belongsTo('App\Company','campaign_brand_id','id');
    }

    //Campaign Type function
    public function campaign_type()
    {
        return $this->belongsTo('App\CampaignType','campaign_type_id','id');
    }

    //BD User function
    public function bd_user()
    {
        return $this->belongsTo('App\User','campaign_bd_person_name_id','id');
    }

    //CS User function
    public function cs_user()
    {
        return $this->belongsTo('App\User','campaign_cs_person_name_id','id');
    }

    //Delivery Mechanic Type function
    public function delivery_mechanic_type()
    {
        return $this->belongsTo('App\DeliveryMechanicType','campaign_mechanic_type_id','id');
    }

    //Voucher Type function
    public function voucher_type()
    {
        return $this->belongsTo('App\VoucherType','campaign_voucher_type_id','id');
    }

    //Voucher Specification function
    public function voucher_specification()
    {
        return $this->belongsTo('App\VoucherSpecification','voucher_spec_type_id','id');
    }
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }

}
