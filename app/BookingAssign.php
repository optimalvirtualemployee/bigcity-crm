<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class BookingAssign extends Model
{
    
    use UserStampsTrait;
    protected $table = 'tbl_booking_assign_agent';
    

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    public function booking(){
    	return $this->belongsTo('App\Booking','booking_id','id');
    }
    
}
