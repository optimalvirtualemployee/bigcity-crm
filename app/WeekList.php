<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class WeekList extends Model
{
    use UserStampsTrait;
    
    protected $table = 'tbl_week_list';
    protected $fillable = ['name'];

    
}
