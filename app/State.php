<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;


class State extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_states';
    protected $fillable = ['name','zone_id','primary_lang_id','other_lang_id'];

    
    //city  function
    public function city()
    {
    	return $this->hasMany('App\City','state_id','id');
    }
    
    //Primary  Language function
    public function primary_lang()
    {
    	return $this->belongsTo('App\PrimaryLanguage','primary_lang_id','id');
    }
    
    //Other Language  function
    public function other_lang()
    {
    	return $this->belongsTo('App\OtherLanguage','other_lang_id','id');
    }
    
    //Other Language  function
    public function state_zone()
    {
    	return $this->belongsTo('App\StateZone','zone_id','id');
    }
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
