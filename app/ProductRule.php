<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\UserStampsTrait;

class ProductRule extends Model
{
     use UserStampsTrait;
    protected $table = 'product_week_rule';
    protected $fillable = ['product_id','title','mon_from_time','tue_from_time','wed_from_time','thus_from_time','fri_from_time','sat_from_time','sun_from_time','mon_to_time','tue_to_time','wed_to_time',
    'thus_to_time','fri_to_time','sat_to_time','sun_to_time'];


    //Product  function
    public function products()
    {
        return $this->belongsTo('App\Product','product_id');
    }
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
