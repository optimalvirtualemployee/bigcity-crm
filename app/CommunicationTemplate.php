<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class CommunicationTemplate extends Model
{
	use UserStampsTrait;
	
    protected $table = 'tbl_communication_template';
    protected $fillable = ['campaign_id','product_id','subject','sms','template_content'];
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
    //Campaign function
    public function campaign()
    {
        return $this->belongsTo('App\Campaign','campaign_id','id');
    }

    //Product function
    public function products()
    {
        return $this->belongsTo('App\Product','product_id','id');
    }
    
    

    
}
