<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueProduct extends Model
{
    //outlet table
    protected $table = 'product_venue';
    protected $fillable = [
        'venue_id', 'product_id','description',
    ];
    //venue  function
    public function venue()
    {
        return $this->belongsTo('App\Venue','venue_id');
    }
    //Product  function
    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }
    

}
