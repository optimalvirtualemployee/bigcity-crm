<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagCityVenue extends Model
{
    protected $table = 'tbl_tag_city_venue';
    protected $fillable = ['tbl_campaign_city_id','city_id','venue_id'];

    public function tagged_city_venue(){
    	return $this->belongsTo('App\TagCityVenue','tbl_campaign_city_id');
    }
}
