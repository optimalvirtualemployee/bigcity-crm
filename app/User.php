<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\UserStampsTrait;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use UserStampsTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //User  function
    public function venue()
    {
    	return $this->hasMany('App\Venue','user_id','id');
    }

    //Outlet  function
    public function user_role()
    {
        return $this->belongsTo('App\UserRole','role');
    }
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
    
}
