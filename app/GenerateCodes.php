<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class GenerateCodes extends Model
{
    use UserStampsTrait;
    protected $table = "tbl_generate_codes";
    protected $fillable = ['creation_type','gc_campaign_id','gc_product_id','length_of_code_required','gc_types_of_order_id','gc_estimate_no','gc_estimate_date','gc_po_no','gc_no_of_codes_as_per_estimate_po','gc_campaign_value','gc_campaign_cost','gc_codes_type','gc_otp_required','gc_voucher_code_batch_name','gc_no_of_codes_required','gc_no_of_test_codes_required','gc_no_of_client_test_codes_required','gc_last_date_to_register_date','gc_offer_valid_till_date'];


    //Campaign function
    public function campaign()
    {
        return $this->belongsTo('App\Campaign','gc_campaign_id','id');
    }

    //Products function
    public function products()
    {
        return $this->belongsTo('App\Product','gc_product_id','id');
    }
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
