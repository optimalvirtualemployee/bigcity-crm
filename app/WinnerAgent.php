<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WinnerAgent extends Model
{
    protected $table = 'tbl_set_gift_allocation';
    protected $fillable = ['campaign_id','gift_type_id','qty'];
 
    //Winner Campaign  function
    public function winner_campaign()
    {
        return $this->belongsTo('App\WinnerCampaign','campaign_id');
    }
    
    //Gift Type  function
    public function Gift_type()
    {
        return $this->belongsTo('App\GiftType','gift_type_id');
    }
    
}
