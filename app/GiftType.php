<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiftType extends Model
{
    protected $table = 'tbl_gift_type';
    protected $fillable = ['name'];
}
