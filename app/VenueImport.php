<?php
namespace App;
use App\Venue;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class VenueImport implements ToModel,WithHeadingRow
{
	/**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Venue([
            'address'     => $row['address'],
            'latitude'    => $row['latitude'],
            'longitude'    => $row['longitude'],
        ]);
    }
}