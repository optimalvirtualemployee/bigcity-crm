<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    
    
    protected $table = 'tbl_booking_data';
    

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    public function campaign(){
    	return $this->belongsTo('App\Campaign','campaign_id','id');
    }

    public function products(){
    	return $this->belongsTo('App\Product','product_id','id');
    }

}
