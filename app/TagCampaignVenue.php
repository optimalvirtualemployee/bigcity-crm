<?php

namespace App;
use App\UserStampsTrait;
use Illuminate\Database\Eloquent\Model;

class TagCampaignVenue extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_tag_campaign_venue';
    protected $fillable = ['campaign_id','product_id'];

    //Campaign function
    public function campaign()
    {
        return $this->belongsTo('App\Campaign','campaign_id','id');
    }

    //Product function
    public function products()
    {
        return $this->belongsTo('App\Product','product_id','id');
    }
    
    public function tagged_campaign_city(){
    	return $this->hasMany('App\TaggedCityVenue','tagged_city_id');
    }
    public function cities(){
    	return $this->belongsToMany('App\City','city_id');
    }
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
    
    
    
}
