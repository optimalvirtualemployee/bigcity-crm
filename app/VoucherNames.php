<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherNames extends Model
{
    protected $table = "tbl_campaign_detail_voucher_names";
    protected $fillable = ['campaign_detail_id','campaign_id','product_id','voucher_name'];
}
