<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class TargetAssign extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_target_assign';
    protected $fillable = ['product_id','pd_lead_id','comment','target_mrp','target_bcp_price','target_closure_date','target_no'];

    //Product  function
    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }

    //User  function
    public function user()
    {
        return $this->belongsTo('App\User','pd_lead_id');
    }
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
