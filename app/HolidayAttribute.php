<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HolidayAttribute extends Model
{
    protected $table = 'tbl_holiday_attribute';
    protected $fillable = ['winner_city_id','destination_city_id','travel_date','no_of_cotraveler','win_name','win_dob','win_passport_no','win_passport_expiry_date','win_meal_preference','co_name','co_dob','co_passport_no','co_passport_expiry_date','co_meal_preference','win_upload_pass','co_upload_pass','gift_tax_required','gift_tax_amount'];


    //city  function
    public function win_city()
    {
    	return $this->belongsTo('App\City','winner_city_id');
    }

    //city  function
    public function des_city()
    {
    	return $this->belongsTo('App\City','destination_city_id');
    }
}
