<?php
 
namespace App\Console\Commands;
 
use Illuminate\Console\Command;
use DB;
use GuzzleHttp\Client;


class BookingApiData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:data';
     
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Booking Data';
     
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
     
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $client = new Client();
        $response = $client->request('GET','http://middlewaremicrositetocrm.optimaltechnology.in/api/bookingalldata', [
            'auth' => [
                'bcMicrtoCRM', 
                'S53PscF!g#4(G#y8q~R%<Kr-4{}kyH~CJf'
            ]
            
        ]);

        $booking = $response->getBody()->getContents();
        $finalData = json_decode($booking, true);

        if(!empty($finalData['data'])){

            foreach($finalData['data'] as $data){

                $url = 'http://middlewaremicrositetocrm.optimaltechnology.in/api/updatestatusincrm/'.$data['id'];
                $bookingData = DB::table('tbl_booking_data')->insert(

                array('campaign_id'        => $data['campaign_id'], 
                      'product_id'         => $data['crm_offer_id'],
                      'voucher_code'       => $data['voucher_code'],
                      'cname'              => $data['cname'],
                      'customer_mob_no'    => $data['cmobile'],
                      'customer_email_id'  => $data['cemail'],
                      'crm_city_id'        => $data['crm_city_id'],
                      'crm_venue_id_1'     => $data['crm_venue_id_1'],
                      'crm_venue_id_2'     => $data['crm_venue_id_2'],
                      'crm_prod_id_1'      => $data['crm_prod_id_1'],
                      'crm_prod_id_2'      => $data['crm_prod_id_2'],
                      'prefereddate1'      => $data['prefereddate1'],
                      'prefereddate2'      => $data['prefereddate2'],
                      'preferedTime1'      => $data['preferedTime1'],
                      'preferedTime2'      => $data['preferedTime2'],
                      'fetch_data_status'  => $data['status'],
                      'reward_code'        => $data['reward_code'],
                      'reward_code_status' => $data['reward_code_status'],
                      'created_on'         => $data['created_on']
                ));


                $response = $client->post($url, [
                'auth' => [
                    'bcMicrtoCRM', 
                    'S53PscF!g#4(G#y8q~R%<Kr-4{}kyH~CJf'
                ],
                
                ]);
            $this->info('Booking Data');

            }


        }else{

            $this->info('Booking Data Not Found');
        }
        
              
        
    }
    
    
}