<?php
 
namespace App\Console\Commands;
 
use Illuminate\Console\Command;
use App\GenerateCodes;
use App\Campaign;
use App\Product;
use Illuminate\Support\Str;
use DB;
use Excel;
use League\Csv\Writer;
use App\CronJob;

class GenerateCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:code';
     
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Code';
     
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
     
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cron_job = CronJob::where('flag','0')->first();
        
        if($cron_job){
            
            $cron_job->flag = '1';
            if($cron_job->save()){
               $campaign = Campaign::where('id',$cron_job['gc_campaign_id'])->first();
            $product = Product::where('id',$cron_job['gc_product_id'])->first();
            /*$generate_codes = GenerateCodes::create($validatedData);*/
        
            $count = $cron_job['gc_no_of_codes_required'];
            $length = $cron_job['length_of_code_required'];
                
            
            
                    //$prefix = substr($campaign->campaign_name,0,3);  
                    $counter = 1;
                    $test_data = array();
                    
                    for ($i = 0; $i < $count; $i++){
                        
                        $test_data[$i]['SN'] = $counter;
                        $test_data[$i]['Camapign_id'] = $cron_job['gc_campaign_id'];
                        $test_data[$i]['campaign_name'] = $campaign['campaign_name'];
                        $test_data[$i]['product_id'] = $cron_job['gc_product_id'];
                        $test_data[$i]['product_name'] = $product->name;
                        
                        $generate_code = GenerateCodes::select('code_prefix')->latest()->first();
                        
                        if($generate_code){
                            
                            $prefix = $generate_code->code_prefix + 1;
                            
                        }else{
                            
                            $prefix = 101;
                        }
                        if($cron_job['gc_codes_type'] == '1'){
                        
                            //numeric code    
                            
                            
                            $test_data[$i]['voucher_code'] = $prefix.substr(str_shuffle("0123456789"), 0, $length);
                            
                            
                        }else{
        
                            //alphanumeric code    
                            $test_data[$i]['voucher_code'] =  substr(str_shuffle($dynamicAlphanumeric), 0, $length);  
                        }
                        $counter++;
                        
        
                    }
                    
                    //Generate csv file for read vocher code
        
                    $this->createCsv($test_data,$cron_job,date('d-i'),$prefix);
            
            }
                                
                               
                            
                            
                            
        }
                
        $this->info('Generate code');
    }
    
    public function createCsv($modelCollection,$request,$created_at,$code_prefix){

        
        $folder_name = public_path('/assets/file/voucher-code/');
        $csv = Writer::createFromPath($folder_name.'campaign_'.$request['gc_campaign_id'].'_'.$created_at.'.csv', 'w');
        
        // This creates header columns in the CSV file - probably not needed in some cases.
        $csv->insertOne(['SN','Campaign Id','Campaign Name','Product Id','Product Name','voucher Code']);
        
        if($csv->insertAll($modelCollection)){
            
            $voucher_code = new GenerateCodes;
        
            $voucher_code->voucher_code_file = 'campaign_'.$request['gc_campaign_id'].'_'.$created_at.'.csv';
            $voucher_code->gc_campaign_id = $request['gc_campaign_id'];
            $voucher_code->gc_codes_type = $request['gc_codes_type'];
            $voucher_code->gc_product_id = $request['gc_product_id'];
            $voucher_code->length_of_code_required = $request['length_of_code_required'];
            $voucher_code->gc_voucher_code_batch_name = $request['gc_voucher_code_batch_name'];
            $voucher_code->gc_last_date_to_register_date = $request['gc_last_date_to_register_date'];
            $voucher_code->gc_offer_valid_till_date = $request['gc_offer_valid_till_date'];
            $voucher_code->code_prefix = $code_prefix;
            
            
            $voucher_code->save();
            
        
        }
        
    }
}