<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WinnerDataUpload extends Model
{
    protected $table = 'tbl_winner_data_upload';
    protected $fillable = ['campaign_id','upload_date','gift_type_id','winner_data','winner_type'];
 
    //Winner Campaign  function
    public function winner_campaign()
    {
        return $this->belongsTo('App\WinnerCampaign','campaign_id');
    }
    
    //Gift Type  function
    public function Gift_type()
    {
        return $this->belongsTo('App\GiftType','gift_type_id');
    }
    
}
