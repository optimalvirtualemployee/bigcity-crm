<?php

namespace App;
use App\UserStampsTrait;

use Illuminate\Database\Eloquent\Model;

class OtherLanguage extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_other_language';
    protected $fillable = ['name'];
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }

}
