<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagCampaignCity extends Model
{
    protected $table = 'tbl_tag_campaign_city';
    protected $fillable = ['tbl_campaign_venue_id','campaign_id','city_id'];

    public function tagged_campaign_venue(){
    	return $this->belongsTo('App\TagCampaignVenue','tbl_campaign_venue_id','id');
    }
}
