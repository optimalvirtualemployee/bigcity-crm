<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
   //user role table
   protected $table = 'tbl_user_role';

   //User role  function
    public function user()
    {
    	return $this->hasMany('App\User','role','id');
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }
}
