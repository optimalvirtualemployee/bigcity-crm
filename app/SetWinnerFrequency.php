<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class SetWinnerFrequency extends Model
{
    use UserStampsTrait;
    
    protected $table = 'tbl_set_winner_frequency';
    protected $fillable = ['campaign_id','winner_frequency','backup_winner_frequency','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
    
    //Winner Campaign  function
    public function winner_campaign()
    {
        return $this->belongsTo('App\WinnerCampaign','campaign_id');
    }
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
