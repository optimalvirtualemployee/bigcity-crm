<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueBillingContact extends Model
{
    //outlet table
    protected $table = 'tbl_venue_billing_contact';
    protected $fillable = [
        'venue_id','name','email','phone','designation'
    ];
    //Venue  function
    public function Venue()
    {
        return $this->belongsTo('App\Venue','venue_id');
    }

   
    

}
