<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class Area extends Model
{
    use UserStampsTrait;
    
    protected $table = 'tbl_areas';
    protected $fillable = ['area_name','state_id','city_id','zone_id','pin_code','lat','lng'];

    //state  function
    public function state()
    {
        return $this->belongsTo('App\State','state_id');
    }
    
    //state  function
    public function area_zone()
    {
        return $this->belongsTo('App\AreaZone','zone_id');
    }

    //city  function
    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
