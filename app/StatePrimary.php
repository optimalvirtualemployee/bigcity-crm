<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;


class StatePrimary extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_state_primary_lang';
    protected $fillable = ['name','primary_lang_id','state_id'];

    
    
    
    //Primary  Language function
    public function state()
    {
    	return $this->belongsTo('App\State','state_id','id');
    }
    
    //Other Language  function
    public function primary_lang()
    {
    	return $this->belongsTo('App\PrimaryLanguage','primary_lang_id','id');
    }
    
}
