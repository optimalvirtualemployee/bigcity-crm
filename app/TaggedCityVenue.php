<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaggedCityVenue extends Model
{
    protected $table = "tbl_tagged_city_venue";
    protected $fillable = ['tagged_city_id','tagged_venue_id'];


    public function city(){
    	return $this->BelongsTo('App\City','tagged_city_id');
    }

    public function venue(){
    	return $this->BelongsTo('App\Venue','tagged_venue_id');
    }
}
