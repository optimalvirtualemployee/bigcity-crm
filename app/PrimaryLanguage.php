<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class PrimaryLanguage extends Model
{
    use UserStampsTrait;
    
    protected $table = 'tbl_primary_language';
    protected $fillable = ['name'];
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }

}
