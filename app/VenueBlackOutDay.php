<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueBlackOutDay extends Model
{
    protected $table = 'tbl_venue_black_out_day';
    protected $fillable = ['id','venue_id','day_name'];


    
}
