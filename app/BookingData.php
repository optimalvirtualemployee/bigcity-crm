<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingData extends Model
{
    protected $table = 'tbl_booking_data';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function campaign(){
        return $this->belongsTo('App\Campaign','campaign_id','id');
    }

    public function products(){
        return $this->belongsTo('App\Product','product_id','id');
    }

    public function venues(){
        return $this->belongsTo('App\Venue','crm_venue_id_1','id');
    }

    public function cities(){
        return $this->belongsTo('App\City','crm_city_id','id');
    }

    public function booking_assign_agent(){
        return $this->hasMany('App\BookingAssignAgent','booking_id','id');
    }

}