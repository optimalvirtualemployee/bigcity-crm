<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiftAttribute extends Model
{
    protected $table = 'tbl_gift_attribute';
    protected $fillable = ['name'];
}
