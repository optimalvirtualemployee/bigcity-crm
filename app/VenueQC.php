<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueQC extends Model
{
    protected $table = 'tbl_venue_QC';
    protected $fillable = ['comment','venue_id','status'];

}
