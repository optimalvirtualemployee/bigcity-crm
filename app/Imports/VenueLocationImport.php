<?php

  

namespace App\Imports;

use App\Venue;
use App\VenueProduct;
use App\VenueBlackOutDay;
use App\VenueBillingContact;
use App\VenueBookingContact;
use App\VenueListType;
use App\City;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use DB;
use Illuminate\Support\Facades\Validator;

class VenueLocationImport implements ToCollection, WithStartRow, WithValidation, SkipsOnFailure
{
    use Importable, SkipsFailures;
    protected $pd_lead_id;
 
    protected $chain_name;
    private $errors = []; // array to accumulate errors

    function __construct($pd_lead_id,$chain_name) {

            
            $this->pd_lead_id = $pd_lead_id;
            $this->chain_name = $chain_name;
    }
    public function startRow(): int
    {
        return 2;
    }
    
    public function collection(Collection $rows)
    {
        
       
        $rows = $rows->toArray();
        
        foreach ($rows as $row) 
        {
            


            $validator = Validator::make($row, $this->rules(), $this->validationMessages($row));
            $city = City::where("id", $row[2])->first();
            $billing_term = DB::table('tbl_billing_term')->where("id",$row[10])->first();
            $booking_alert_required = DB::table('tbl_booking_alert_required')->where("id",$row[8])->first();
            $category = DB::table('tbl_categories')->where("id",$row[24])->first();
                
                
            if ($validator->fails()) {

                foreach ($validator->errors()->messages() as $messages) {

                    foreach ($messages as $error) {

                        // accumulating errors:
                        $this->errors[] = $error;
                    }
                }
                
            }elseif(!$city){
                
                $this->errors[] = "This city is not exist in city table";
                    
            }elseif(!$billing_term){
                
                $this->errors[] = "This billing term is not exist in billing term table";
                    
            }elseif(!$booking_alert_required){
                
                $this->errors[] = "This booking alert required is not exist in booking alert required table";
                    
            }elseif(!$category){
                
                $this->errors[] = "This category is not exist in category table";
                    
            }else {
               
                $venue = Venue::create([
                    
                    'venue_type'             => '1',
                    'comment_status'         => 'Send For QC',
                    'pd_lead_id'             => $this->pd_lead_id,
                    'chain_name'             => $this->chain_name,
                    'venue_name'             => $row[0],
                    'state_id'               => $row[1],
                    'city_id'                => $city['id'],
                    'area_id'                => $row[3],
                    'latitude'               => $row[4],
                    'longitude'              => $row[5],
                    'rating'                 => $row[6],
                    'booking_alert_required' => $booking_alert_required->id,
                    'address'                => $row[9],
                    'billing_terms'          => $billing_term->id,
                    'category_id'            => $category->id,
                    
                    
                ]);
                
                //Add List type data
                foreach(explode(',',$row[7]) as $listType) {
                    
                   //$listTypeData = DB::table('tbl_list_type')->where("id",$listType)->first();
                  
                    $list_type = new VenueListType;
                    $list_type->venue_id = $venue->id;
                    $list_type->list_type_id = $listType;
                    $list_type->save();
                    
                
                }
                switch ($billing_term->id) {
                    case '1':
                        $venue->bank_name = $row[11];
                        $venue->account_number = $row[12];
                        $venue->ifsc_code = $row[13];
                        $venue->name_on_account = $row[14];
                        $venue->save();
                        break;
                    case '2':
            
                        $venue->bank_name = $row[11];
                        $venue->account_number = $row[12];
                        $venue->ifsc_code = $row[13];
                        $venue->name_on_account = $row[14];
                        $venue->save();
                        break;
                        
                    case '4':
            
                        $venue->other_value = $row[15];
                        $venue->save();
                        break;
                    
                }
                
                //Add Pertner Billing data
                $billing_email = explode(',',$row[17]);
                $billing_phone = explode(',',$row[18]);
                $designation = explode(',',$row[19]);
                
                foreach(explode(',',$row[16]) as $i => $billing_name) {
                   
                   VenueBillingContact::create([
                    'venue_id' => $venue->id,
                    'name' => $billing_name,
                    'email' => $billing_email[$i],
                    'phone' => $billing_phone[$i],
                    'designation' => $designation[$i],
                    
                ]); 
                }
                
                //Add Pertner Booking data
                $booking_email = explode(',',$row[21]);
                $booking_phone = explode(',',$row[22]);
                $designation = explode(',',$row[23]);
                foreach(explode(',',$row[20]) as $i => $booking_name) {
                   
                   VenueBookingContact::create([
                       
                    'venue_id'    => $venue->id,
                    'name'        => $booking_name,
                    'email'       => $booking_email[$i],
                    'phone'       => $booking_phone[$i],
                    'designation' => $designation[$i],
                    
                ]); 
                }
                //Add multiple product with description,MRP and cost
                $mon_mrp =explode(',',$row[27]);
                $tue_mrp =explode(',',$row[28]);
                $wed_mrp =explode(',',$row[29]);
                $thus_mrp=explode(',',$row[30]);
                $fri_mrp =explode(',',$row[31]);
                $sat_mrp =explode(',',$row[32]);
                $sun_mrp =explode(',',$row[33]);
                
                $mon_cost = explode(',',$row[34]);
                $tue_cost = explode(',',$row[35]);
                $wed_cost = explode(',',$row[36]);
               
                $thus_cost= explode(',',$row[37]);
                $fri_cost = explode(',',$row[38]);
                $sat_cost = explode(',',$row[39]);
                $sun_cost = explode(',',$row[40]);

                $description = explode(',',$row[26]);
                
                foreach(explode(',',$row[25]) as $i => $product) {
                   $products = DB::table('tbl_products')->where("id",$product)->first();
                   
                    $product = new VenueProduct;
                       
                    $product->venue_id    = $venue->id;
                    $product->product_id  = $products->id;
                    $product->category_id = $products->category_id;
                    $product->description = $description[$i];
                    $product->mon_mrp     = @$mon_mrp[$i];
                    $product->tue_mrp     = @$tue_mrp[$i];
                    $product->wed_mrp     = @$wed_mrp[$i];
                    $product->thus_mrp    = @$thus_mrp[$i];
                    $product->fri_mrp     = @$fri_mrp[$i];
                    $product->sat_mrp     = @$sat_mrp[$i];
                    $product->sun_mrp     = @$sun_mrp[$i];
                    $product->mon_cost    = @$mon_cost[$i];
                    $product->tue_cost    = @$tue_cost[$i];
                    $product->wed_cost    = @$wed_cost[$i];
                    $product->thus_cost   = @$thus_cost[$i];
                    $product->fri_cost    = @$fri_cost[$i];
                    $product->sat_cost    = @$sat_cost[$i];
                    $product->sun_cost    = @$sun_cost[$i];
                    $product->save();
                    
                    
                
                }
                //Add Black out dates
                
                foreach(explode(',',$row[41]) as $i => $black_out_dates) {
                   
                    $black_out = new VenueBlackOutDay;
                    
                    $black_out->venue_id = $venue->id;
                    $black_out->date = $black_out_dates;
                    $black_out->save();
                    
                
                }
                
            }
            
            
            
            
        }
        
        
    }
    // this function returns all validation errors after import:
    public function getErrors()
    {

        return $this->errors;
    }

    public function rules(): array
    {
        

        return [
            '0' => 'required',
            '1' => 'required',
            '2' => 'required',
            '3' => 'required',
            '4' => 'required',
            '5' => 'required',
            '6' => 'required',
            '7' => 'required',
            '8' => 'required',
            '9' => 'required',
            '10' => 'required',
            /*'11' => 'required',*/
            
            /*'12' => 'required',
            '13' => 'required',
            '14' => 'required',
            '15' => 'required|numeric',
            '16' => 'required|numeric',
            '17' => 'required',
            '18' => 'required',
            '19' => 'required|email',
            '20' => 'required|numeric',
            '21' => 'required',
            '22' => 'required',
            '23' => 'required|email',
            '24' => 'required|numeric',
            '25' => 'required',*/
        ];
    }

    public function validationMessages($row)
    {
        /*$billing_term = DB::table('tbl_billing_term')->where("name", "like", "%" . $row[6] . "%")->first();*/
        
        return [
            '0.required' => 'Venue name feild is required',
            '1.required' => 'State name feild is required',
            '2.required' => 'City feild is required',
            '3.required' => 'Area feild is required',
            '4.required' => 'Latitude feild is required',
            '5.required' => 'Longitude feild is required',
            '6.required' => 'Rating feild is required',
            '7.required' => 'Venue List feild is required',
            '8.required' => 'Booking Alert Required feild is required',
            '9.required' => 'Address feild is required',
            '10.required' => 'Billing Term feild is required',
            
            /*'12.required' => 'Booking Alert feild is required',
            '13.required' => 'Category name feild is required',
            '14.required' => 'Product name feild is required',
            '15.required' => 'MRP of product feild is required',
            '15.numeric' => 'MRP of product must be a number',
            '16.required' => 'Cost of product feild is required',
            '16.numeric' => 'Cost of product must be a number',
            '17.required' => 'Product description must be a number',
            '18.required' => 'Billing SPOC name feild is required',
            '19.required'    => 'Billing SPOC email feild is required',
            '19.email'    => 'Billing SPOC must be a valid email address',
            '20.required' => 'Billing SPOC phone feild is required',
            '20.numeric' => 'Billing SPOC phone must be a number',
            '21.required' => 'Billing SPOC designation feild is required',
            '22.required' => 'Billing SPOC name feild is required',

            '23.required' => 'Billing SPOC email feild is required',
            '23.email'    => 'Billing SPOC must be a valid email address',
            '24.required' => 'Billing SPOC phone feild is required',
            '24.numeric' => 'Billing SPOC phone must be a number',
            '25.required' => 'Billing SPOC designation feild is required',*/
        ];
        /*switch ($billing_term->id) {

            case '1':
            return [
                '7.required' => 'Bank Account name feild is required',
                '8.required' => 'Bank name feild is required',
                '9.required' => 'Account Number feild is required',
                '10.required' => 'IFSC code feild is required',
                
                
            ];
            break;
            case '2':
    
                return [
                '7.required' => 'Bank Account name feild is required',
                '8.required' => 'Bank name feild is required',
                '9.required' => 'Account Number feild is required',
                '10.required' => 'IFSC code feild is required',
                
                
            ];
                break;
                
            case '4':
    
                return [
                    '11.required' => 'Other number feild is required',
                ];
                break;
            
        }*/
    }

}