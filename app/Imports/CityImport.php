<?php

  

namespace App\Imports;

use App\City;
use Maatwebsite\Excel\Concerns\WithStartRow;

use Maatwebsite\Excel\Concerns\ToModel;

  

class CityImport implements ToModel,WithStartRow

{
    protected $id;

    function __construct($id) {

            $this->id = $id;
    }
    public function startRow(): int
    {
        return 2;
    }
    /**

    * @param array $row

    *

    * @return \Illuminate\Database\Eloquent\Model|null

    */

    public function model(array $row)

    {

        return new City([
            'state_id' =>$this->id,
            'name'     => $row[0],

        ]);

    }

}