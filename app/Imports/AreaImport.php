<?php

  

namespace App\Imports;

use App\Area;

use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\ToModel;

  

class AreaImport implements ToModel,WithStartRow

{
    protected $state_id;
    protected $city_id;

    function __construct($state_id,$city_id) {

            $this->city_id = $city_id;
            $this->state_id = $state_id;
    }
    public function startRow(): int
    {
        return 2;
    }
    /**

    * @param array $row

    *

    * @return \Illuminate\Database\Eloquent\Model|null

    */

    public function model(array $row)

    {

        return new Area([
            'state_id'    =>$this->state_id,
            'city_id'     =>$this->city_id,
            'area_name'   => $row[0],
            'zone'        => $row[1],
            'pin_code'    => $row[2],
            'lat'         => $row[3],
            'lng'         => $row[4],

        ]);

    }

}