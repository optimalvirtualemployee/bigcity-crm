<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PrimaryLanguage;
use App\Exports\PrimaryLanguageExport;
use Excel;

class PrimaryLanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $primarylanguage = PrimaryLanguage::get();

        return view('admin.primary_language.index')->with([
        'primarylanguage'  => $primarylanguage
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.primary_language.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
        ]);
        
        $pl = new PrimaryLanguage;
        $pl->name = $validatedData['name'];
        $pl->created_by = Auth::user()->id;

        $pl->save();

      return redirect('admin/primary_language')->withSuccess('Primary Language added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $primarylanguage = PrimaryLanguage::find($id);
        return view('admin.primary_language.show', compact('primarylanguage'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $primarylanguage = PrimaryLanguage::find($id);
        return view('admin.primary_language.edit', compact('primarylanguage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);        
        $pl = PrimaryLanguage::find($id);
        $pl->name =  $request->get('name');
        $pl->save();        
        return redirect('/admin/primary_language')->with('success', 'Primary Language updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function primarylanguage_ExportExcel()
    {
        $primaryLanguage_data = PrimaryLanguage::with('created_user','updated_user')->get()->toArray();
        
        $primaryLanguage_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($primaryLanguage_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Primary Language ID'           => $cat['id'],
                   'Primary Language Name'         => $cat['name'],
                   'Created By'            => isset($cat['created_user']['name']) ? $cat['created_user']['name'] : 'NA',
                   'Updated By'            => isset($cat['updated_user']['name']) ? $cat['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new PrimaryLanguageExport($data), 'primary_language_excelsheet.xlsx');
    }
}
