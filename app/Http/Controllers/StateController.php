<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exports\StateExport;
use App\State;
use App\PrimaryLanguage;
use App\OtherLanguage;
use Excel;
use App\StateZone;
use App\StatePrimary;
use App\StateOther;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $states = State::get();

        return view('admin.state.index')->with([
        'states'  => $states
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $primary_language = PrimaryLanguage::get();
        $other_language   = OtherLanguage::get();
        $state_zone       = StateZone::get();
        return view('admin.state.create',compact('primary_language','other_language','state_zone'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'              => 'required',
            'zone_id'              => 'required|not_in:0',
            /*'primary_lang_id'      => 'required|not_in:0',
            'other_lang_id'       => 'required|not_in:0',*/
      ]);
      
      
      $state = State::create($validatedData);
      if($state){
          
        foreach($request->primary_lang_id as $i => $value){
            $venue_booking_contact = new StatePrimary;
            $venue_booking_contact->state_id = $state->id;
            $venue_booking_contact->primary_lang_id = $value;
            $venue_booking_contact->save();
        }
        
        foreach($request->other_lang_id as $i => $value){
            $venue_booking_contact = new StateOther;
            $venue_booking_contact->state_id = $state->id;
            $venue_booking_contact->other_lang_id = $value;
            $venue_booking_contact->save();
        }
      }
      
                

      return redirect('/admin/states')->withSuccess('You have successfully created a State!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $states = State::find($id);
        
        return view('admin.state.show',compact('states'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $states = State::find($id);
        $primary_language = PrimaryLanguage::get();
        $other_language = OtherLanguage::get();
         $state_zone = StateZone::get();
        return view('admin.state.edit', compact('states','primary_language','other_language','state_zone'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'              => 'required',
            'zone_id'              => 'required|not_in:0',
            'primary_lang_id'      => 'required|not_in:0',
            'other_lang_id'       => 'required|not_in:0',
            
        ]);        
        $state = State::find($id);
        $state->name =  $request->get('name');
        $state->zone_id =  $request->get('zone_id');
        $state->primary_lang_id =  $request->get('primary_lang_id');
        $state->other_lang_id =  $request->get('other_lang_id');
        $state->save();        
        return redirect('/admin/states')->with('success', 'State updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function dashboard(Request $request)
    {
  
        return view('admin.state.dashboard');
    }
    
    //State Download

    public function stateDownload()
    {

        $state_data = State::with('primary_lang','other_lang','city','state_zone')->get()->toArray();
        $state_array[] = array('SN','ID','State Name','Zone','Primary Langauge','Other Language','Created By','Created Date & Time','Status');
             
        foreach($state_data as $index=>$cat)
        {
            $data[] = array(
                    
                'SN'                    => ++$index,
                'State ID'              => $cat['id'],
                'State Name'            => $cat['name'],
                'Zone Id'               => $cat['state_zone']['id'],
                'Zone Name'             => $cat['state_zone']['name'],
                'Primary Id'            => $cat['primary_lang']['id'],
                'Primary Langauge'      => $cat['primary_lang']['name'],
                'Other Id'              => $cat['other_lang']['id'],
                'Other Language'        => $cat['other_lang']['name'],
                'Created By'            => isset($cat->created_user['name']) ? $cat->created_user['name'] : 'NA',
                'Updated By'            => isset($cat->updated_user['name']) ? $cat->updated_user['name'] : 'NA',
                'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new StateExport($data), 'state.xlsx');
    }
}
