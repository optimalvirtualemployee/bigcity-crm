<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\OtherLanguage;
use App\Exports\OtherLanguageExport;
use Excel;

class OtherLanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $other_language = OtherLanguage::get();

        return view('admin.other_language.index')->with([
        'other_language'  => $other_language
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.other_language.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            
      ]);

      OtherLanguage::create($validatedData);

      return redirect('admin/otherlanguage')->withSuccess('Other language has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $other_language = OtherLanguage::find($id);
        return view('admin.other_language.show',compact('other_language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $other_language = OtherLanguage::find($id);
        return view('admin.other_language.edit', compact('other_language'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $other_language = OtherLanguage::find($id);
        $other_language->name =  $request->get('name');
        $other_language->save();        
        return redirect('/admin/otherlanguage')->with('success', 'Other language updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function otherLanguageDownload()
    {
        
        $otherLanguage_data = OtherLanguage::with('created_user','updated_user')->get()->toArray();
        
        $otherLanguage_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($otherLanguage_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Other Language ID'           => $cat['id'],
                   'Other Language Name'         => $cat['name'],
                   'Created By'            => isset($cat['created_user']['name']) ? $cat['created_user']['name'] : 'NA',
                   'Updated By'            => isset($cat['updated_user']['name']) ? $cat['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new OtherLanguageExport($data), 'OtherLanguagesheet.xlsx');
    }
    
    

    
    
}
