<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\CustomerQuery;

class CustomerQueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $customer_query = CustomerQuery::get();

        return view('admin.customer_query.index')->with([
        'customer_query'  => $customer_query
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer_query.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required|min:3|regex:/^[a-zA-Z\s]*$/',
            
      ]);

      CustomerQuery::create($validatedData);

      return redirect('admin/customerquery')->withSuccess('Customer Query has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer_query = CustomerQuery::find($id);
        return view('admin.customer_query.show',compact('customer_query'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer_query = CustomerQuery::find($id);
        return view('admin.customer_query.edit', compact('customer_query'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|regex:/^[a-zA-Z\s]*$/',
            
        ]);        
        $customer_query = CustomerQuery::find($id);
        $customer_query->name =  $request->get('name');
        $customer_query->save();        
        return redirect('/admin/customerquery')->with('success', 'Customer query updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function changeCustomerQueryStatus(Request $request)
    {
        $customer_query = CustomerQuery::find($request->id);
        $customer_query->status = $request->status;
        $customer_query->save();
  
        return response()->json(['success'=>'Customer Query status change successfully.']);
    }
    
    
    
    
}
