<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Otp;
use App\User;
use App\Http\Controllers\Controller;
use App\Venue;
use App\Category;
use App\Product;
use App\TargetAssign;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
            //Admin  User
            if(Auth::user()->role==1){
                return redirect('/admin/dashboard');
            }
            
        
    }
    public function dashboard()
    {
        $totalVenue = Venue::get();
        $totalVenuePending = Venue::get();
        $totalVenueQC = Venue::get();
        $totalCategory = Category::where('status','1')->get();
        $totalTargetAssign = TargetAssign::get();
        $number_blocks = [
            [
                'title' => 'Users Logged In Today',
                'number' => User::whereDate('last_login_at', today())->count()
            ],
            [
                'title' => 'Users Logged In Last 7 Days',
                'number' => User::whereDate('last_login_at', '>', today()->subDays(7))->count()
            ],
            [
                'title' => 'Users Logged In Last 30 Days',
                'number' => User::whereDate('last_login_at', '>', today()->subDays(30))->count()
            ],
        ];

        $list_blocks = [
            [
                'title' => 'Last Logged In Users',
                'entries' => User::orderBy('last_login_at', 'desc')
                    ->take(5)
                    ->get(),
            ],
            [
                'title' => 'Users Not Logged In For 30 Days',
                'entries' => User::where('last_login_at', '<', today()->subDays(30))
                    ->orwhere('last_login_at', null)
                    ->orderBy('last_login_at', 'desc')
                    ->take(5)
                    ->get()
            ],
        ];

        /*$chart_settings = [
            'chart_title'        => 'Users By Months',
            'chart_type'         => 'line',
            'report_type'        => 'group_by_date',
            'model'              => 'App\\User',
            'group_by_field'     => 'last_login_at',
            'group_by_period'    => 'month',
            'aggregate_function' => 'count',
            'filter_field'       => 'last_login_at',
            'column_class'       => 'col-md-12',
            'entries_number'     => '5',
        ];
        $chart = new LaravelChart($chart_settings);*/

        
        return view('admin.dashboard.dashboard',compact('totalVenueQC','totalVenuePending','totalVenue','totalCategory','number_blocks', 'list_blocks','totalTargetAssign'));
            
        
    }
    
    public function targetAssign()
    {
        
        $products = Product::where('status','1')->get();

        return view('admin.target-assign',compact('products'));
            
        
    }
    
    public function resendOtp()
    {
        dd('fff');
        $uid = Auth::user()->id;
        $otp = DB::table('users')
                    ->select("*")
                    ->where("id",$uid)
                    ->get();
         $phone = $otp[0]->phone; 
              
         $otp = new otp();
         $pin = rand(100000,999999);
         $otp->phone  = $phone;
         $otp->otp        = $pin;
         $otp->save();
   } 
    //check otp
    public function checkOtp(Request $request)
    {
        $phone = Auth::user()->phone;
        $otp = $request->input('otp');
        $otpdata = new Otp;
        $data = $otpdata->getOtp($phone, $otp);
        //print_r($data);die;
        if(count($data) != 0){
            if(Auth::user()->usertype==1){
                return redirect('/admin/dashboard');
            }
            if (Auth::user()->usertype==2) {
               return redirect('/vendor/dashboard');
            }
        } else {
            return redirect('/admin/otp')->with('success_msg','OTP does not match');
        }
    }
}
