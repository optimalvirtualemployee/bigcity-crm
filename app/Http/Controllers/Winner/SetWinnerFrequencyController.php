<?php

namespace App\Http\Controllers\Winner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SetWinnerFrequency;
use App\Campaign;

class SetWinnerFrequencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $set_winner_frequency = SetWinnerFrequency::get();

        return view('admin.set_winner_frequency.index')->with([
        'set_winner_frequency'  => $set_winner_frequency
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $winner_campaign = Campaign::where('status','1')->get();
        return view('admin.set_winner_frequency.create',compact('winner_campaign'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'campaign_id'      => 'required|not_in:0',
            'winner_frequency'      => 'required|not_in:0',
            'backup_winner_frequency'      => 'required|not_in:0',
            
      ]);

      SetWinnerFrequency::create($validatedData);

      return redirect('admin/set-winner-frequency')->withSuccess('Set Winner Frequency has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $set_winner_frequency = SetWinnerFrequency::find($id);
        return view('admin.set_winner_frequency.show',compact('set_winner_frequency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $set_winner_frequency = SetWinnerFrequency::find($id);
        $winner_campaign = Campaign::where('status','1')->get();
        return view('admin.set_winner_frequency.edit', compact('set_winner_frequency','winner_campaign'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'campaign_id'=>'required|not_in:0',
            
        ]);        
        $set_winner_frequency = SetWinnerFrequency::find($id);
        $set_winner_frequency->campaign_id =  $request->get('campaign_id');
        $set_winner_frequency->winner_frequency =  $request->get('winner_frequency');
        $set_winner_frequency->backup_winner_frequency =  $request->get('backup_winner_frequency');
        $set_winner_frequency->save();        
        return redirect('/admin/set-winner-frequency')->with('success', 'Winner Frequency updated!');
    }

    

    
    
    
}
