<?php

namespace App\Http\Controllers\Winner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\WinnerCampaign;
use App\Company;
use App\User;
use App\GiftType;

class WinnerCampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $winner_campaign = WinnerCampaign::get();

        return view('admin.winner_campaign.index')->with([
        'winner_campaign'  => $winner_campaign
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies      = Company::get();
        $bd_person_name =  User::where('role',2)->get();
        $cs_person_name =  User::where('role',3)->get();
        $gift_type =  GiftType::get();
        return view('admin.winner_campaign.create',compact('companies','bd_person_name','cs_person_name','gift_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'campaign_name'        =>'required|alpha',
            'campaign_brand_id'    =>'required|not_in:0',
            'bd_person_name_id'    =>'required|not_in:0',
            'cs_person_name_id'    =>'required|not_in:0',
            'is_auditor_required'  =>'required|not_in:0',
            'start_date'           =>'required|before_or_equal:end_date',
            'end_date'             =>'required|after_or_equal:start_date',
            'total_no_of_winners'  =>'required|numeric',
            'gift_type_id'         =>'required|not_in:0',
            'gift_tax_applicable'  =>'required|not_in:0',
            
        ]);

      WinnerCampaign::create($validatedData);

      return redirect('admin/winnercampaign')->withSuccess('Winner Campaign has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $winner_campaign = WinnerCampaign::find($id);
        $companies       = Company::get();
        $bd_person_name  =  User::where('role',2)->get();
        $cs_person_name  =  User::where('role',3)->get();
        $gift_type       =  GiftType::get();
        return view('admin.winner_campaign.show',compact('winner_campaign','companies','bd_person_name','cs_person_name','gift_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $winner_campaign = WinnerCampaign::find($id);
        $companies       = Company::get();
        $bd_person_name  = User::where('role',2)->get();
        $cs_person_name  = User::where('role',3)->get();
        $gift_type       = GiftType::get();
        return view('admin.winner_campaign.edit',compact('winner_campaign','companies','bd_person_name','cs_person_name','gift_type'));    
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'campaign_name'        =>'required|alpha',
            'campaign_brand_id'    =>'required|not_in:0',
            'bd_person_name_id'    =>'required|not_in:0',
            'cs_person_name_id'    =>'required|not_in:0',
            'is_auditor_required'  =>'required|not_in:0',
            'start_date'           =>'required|before_or_equal:end_date',
            'end_date'             =>'required|after_or_equal:start_date',
            'total_no_of_winners'  =>'required|numeric',
            'gift_type_id'         =>'required|not_in:0',
            'gift_tax_applicable'  =>'required|not_in:0',
            
        ]);        
        $winner_campaign = WinnerCampaign::find($id);
        $winner_campaign->campaign_name       =  $request->get('campaign_name');
        $winner_campaign->campaign_brand_id   =  $request->get('campaign_brand_id');
        $winner_campaign->bd_person_name_id   =  $request->get('bd_person_name_id');
        $winner_campaign->cs_person_name_id   =  $request->get('cs_person_name_id');
        $winner_campaign->is_auditor_required =  $request->get('is_auditor_required');
        $winner_campaign->start_date          =  $request->get('start_date');
        $winner_campaign->end_date            =  $request->get('end_date');
        $winner_campaign->total_no_of_winners =  $request->get('total_no_of_winners');
        $winner_campaign->gift_type_id        =  $request->get('gift_type_id');
        $winner_campaign->gift_tax_applicable =  $request->get('gift_tax_applicable');
        $winner_campaign->save();        
        return redirect('/admin/winnercampaign')->with('success', 'Winner Campaign updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $winner_campaign = WinnerCampaign::find($id);
        $winner_campaign->delete();        
        return redirect('/admin/winnercampaign')->with('success', 'Winner Campaign deleted!');
    }
    
    public function dashboard(Request $request)
    {
        $winner_campaign = WinnerCampaign::get();
        return view('admin.winner_campaign.dashboard',compact('winner_campaign'));
    }
    
    
}
