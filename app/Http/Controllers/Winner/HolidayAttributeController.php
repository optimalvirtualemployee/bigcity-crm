<?php

namespace App\Http\Controllers\Winner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\HolidayAttribute;
use App\City;

class HolidayAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $holiday_attribute = HolidayAttribute::get();

        return view('admin.holiday_attribute.index')->with([
        'holiday_attribute'  => $holiday_attribute
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::get();

        return view('admin.holiday_attribute.create',compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'winner_city_id'           => 'required|not_in:0',
            'destination_city_id'      => 'required|not_in:0',
            'travel_date'              => 'required',
            'no_of_cotraveler'         => 'required|numeric',
            'win_name'                 => 'required',
            'win_dob'                  => 'required',
            'win_passport_no'          => 'required',
            'win_passport_expiry_date' => 'required',
            'win_meal_preference'      => 'required',
            'co_name'                  => 'required',
            'co_dob'                   => 'required',
            'co_passport_no'           => 'required',
            'co_passport_expiry_date'  => 'required',
            'co_meal_preference'       => 'required',
            /*'win_upload_pass'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'co_upload_pass'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',*/
            'gift_tax_required'        => 'required',
            'gift_tax_amount'          => 'required|numeric',
            
            
      ]);

        $holiday_attribute = HolidayAttribute::create($validatedData);
        if($request->win_upload_pass || $request->co_upload_pass){

              $win_upload_pass = time().'.'.$request->win_upload_pass->extension();
              $con_upload_pass = time().'.'.$request->con_upload_pass->extension();
          
              $holiday_attribute->win_upload_pass = $win_upload_pass;
              $holiday_attribute->con_upload_pass = $con_upload_pass;
              $holiday_attribute->save();
              $request->con_upload_pass->move(public_path('/assets/img/winner-passport-images/'), $con_upload_pass);
              $request->win_upload_pass->move(public_path('/assets/img/co-passport-images/'), $win_upload_pass);
          }
      return redirect('admin/holiday-attribute')->withSuccess('Holiday attribute been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $holiday_attribute = HolidayAttribute::find($id);
        return view('admin.holiday_attribute.show',compact('holiday_attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $holiday_attribute = HolidayAttribute::find($id);
        return view('admin.holiday_attribute.edit', compact('holiday_attribute'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $holiday_attribute = HolidayAttribute::find($id);
        $holiday_attribute->name =  $request->get('name');
        $holiday_attribute->save();        
        return redirect('/admin/holiday-attribute')->with('success', 'Holiday Attribute updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $holiday_attribute = HolidayAttribute::find($id);
        $holiday_attribute->delete();        
        return redirect('/admin/holiday-attribute')->with('success', 'Holiday Attribute deleted!');
    }
    
}
