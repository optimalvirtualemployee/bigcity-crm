<?php

namespace App\Http\Controllers\Winner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\SetGiftAllocation;
use App\WinnerCampaign;
use App\GiftType;

class SetGiftAllocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $set_gift_allocation = SetGiftAllocation::get();

        return view('admin.set_gift_allocation.index')->with([
        'set_gift_allocation'  => $set_gift_allocation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $winner_campaign = WinnerCampaign::get();
        $gift_type = GiftType::get();
        return view('admin.set_gift_allocation.create',compact('winner_campaign','gift_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'qty'      => 'required',
            'campaign_id'      => 'required|not_in:0',
            'gift_type_id'      => 'required|not_in:0',
            
      ]);

      SetGiftAllocation::create($validatedData);

      return redirect('admin/setgiftallocation')->withSuccess('Set Gift Allocation has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $winner_campaign = WinnerCampaign::get();
        $gift_type = GiftType::get();
        
        $set_gift_allocation = SetGiftAllocation::find($id);
        return view('admin.set_gift_allocation.show',compact('set_gift_allocation','winner_campaign','gift_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $winner_campaign = WinnerCampaign::get();
        $gift_type = GiftType::get();
        
        $set_gift_allocation = SetGiftAllocation::find($id);
        return view('admin.set_gift_allocation.edit',compact('set_gift_allocation','winner_campaign','gift_type'));     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'qty'=>'required|regex:/^[a-zA-Z\s]*$/',
            
        ]);        
        $set_gift_allocation = SetGiftAllocation::find($id);
        $set_gift_allocation->qty =  $request->get('qty');
        $set_gift_allocation->campaign_id =  $request->get('campaign_id');
        $set_gift_allocation->gift_type_id =  $request->get('gift_type_id');
        $set_gift_allocation->save();        
        return redirect('/admin/setgiftallocation')->with('success', 'Set Gift Allocation updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $set_gift_allocation = SetGiftAllocation::find($id);
        $set_gift_allocation->delete();        
        return redirect('/admin/setgiftallocation')->with('success', 'Set Gift Allocation deleted!');
    }

    
    
    
}
