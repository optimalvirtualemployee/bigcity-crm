<?php

namespace App\Http\Controllers\Winner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\GiftAttribute;

class GiftAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $gift_attribute = GiftAttribute::get();

        return view('admin.gift_attribute.index')->with([
        'gift_attribute'  => $gift_attribute
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gift_attribute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            
      ]);

      GiftAttribute::create($validatedData);

      return redirect('admin/set-gift-attribute')->withSuccess('Gift attribute been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gift_attribute = GiftAttribute::find($id);
        return view('admin.gift_attribute.show',compact('gift_attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gift_attribute = GiftAttribute::find($id);
        return view('admin.gift_attribute.edit', compact('gift_attribute'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $gift_attribute = GiftAttribute::find($id);
        $gift_attribute->name =  $request->get('name');
        $gift_attribute->save();        
        return redirect('/admin/set-gift-attribute')->with('success', 'Gift Attribute updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gift_attribute = GiftAttribute::find($id);
        $gift_attribute->delete();        
        return redirect('/admin/categories')->with('success', 'Gift Attribute deleted!');
    }
    
}
