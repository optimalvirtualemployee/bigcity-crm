<?php

namespace App\Http\Controllers\Winner;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\WinnerDataUpload;
use App\WinnerCampaign;
use App\GiftType;

class WinnerDataUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $winner_data_upload = WinnerDataUpload::get();

        return view('admin.winner_data_upload.index')->with([
        'winner_data_upload'  => $winner_data_upload
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $winner_campaign = WinnerCampaign::get();
        $gift_type = GiftType::get();
        return view('admin.winner_data_upload.create',compact('winner_campaign','gift_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            
            'campaign_id'    => 'required|not_in:0',
            'gift_type_id'   => 'required|not_in:0',
            'winner_type'    => 'required|not_in:0',
            'winner_data'    =>  'required',
            'upload_date'    =>  'required',
            
      ]);

      WinnerDataUpload::create($validatedData);

      return redirect('admin/winner-data-upload')->withSuccess('Winner Data has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $winner_data_upload = WinnerDataUpload::find($id);
        return view('admin.winner_data_upload.show',compact('winner_data_upload'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $winner_campaign = WinnerCampaign::get();
        $gift_type = GiftType::get();
        $winner_data_upload = WinnerCampaign::find($id);
        return view('admin.winner_data_upload.edit',compact('winner_campaign','gift_type','winner_data_upload'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'campaign_id'    => 'required|not_in:0',
            'gift_type_id'   => 'required|not_in:0',
            'winner_type'    => 'required|not_in:0',
            'winner_data'    =>  'required',
            'upload_date'    =>  'required',
            
            
        ]);        
        $winner_data_upload = WinnerCampaign::find($id);
        $winner_data_upload->winner_data =  $request->get('winner_data');
        $winner_data_upload->campaign_id =  $request->get('campaign_id');
        $winner_data_upload->gift_type_id =  $request->get('gift_type_id');
        $winner_data_upload->winner_type =  $request->get('winner_type');
        $winner_data_upload->upload_date =  $request->get('upload_date');
        $winner_data_upload->save();        
        return redirect('/admin/winner-data-upload')->with('success', 'Winner Data updated!');
    }

    

    
    
    
}
