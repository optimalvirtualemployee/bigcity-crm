<?php

namespace App\Http\Controllers\Winner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\GiftType;

class GiftTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $gift_type = GiftType::get();

        return view('admin.gift_type.index')->with([
        'gift_type'  => $gift_type
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gift_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required|min:3|alpha',
            
      ]);

      GiftType::create($validatedData);

      return redirect('admin/gifttype')->withSuccess('Gift Type has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gift_type = GiftType::find($id);
        return view('admin.gift_type.show',compact('gift_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gift_type = GiftType::find($id);
        return view('admin.gift_type.edit', compact('gift_type'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|regex:/^[a-zA-Z\s]*$/',
            
        ]);        
        $gift_type = GiftType::find($id);
        $gift_type->name =  $request->get('name');
        $gift_type->save();        
        return redirect('/admin/gifttype')->with('success', 'Gift Type updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gift_type = GiftType::find($id);
        $gift_type->delete();        
        return redirect('/admin/gift_type')->with('success', 'Gift Type deleted!');
    }

    
    
    
}
