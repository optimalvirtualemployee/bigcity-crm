<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class LoginController extends Controller
{
    public function home() {
        return view('login');
    }

    function checklogin(Request $request) {
    
		$this->validate($request, [
			'email'   => 'required|email',
			'password'  => 'required|min:3'
		]);
        
		$user_data = array(
			'email'  => $request->get('email'),
			'password' => $request->get('password'),
            
		);
        
		if(Auth::attempt($user_data)){
		    
		    $user = User::where('email',$user_data['email'])->first();
		    $user->last_login_at = now();
            $user->save();
            $qc_user = User::select('users.*')->where('users.status','1')->where('users.id',$user->id)->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '4')->first();
            $super_admin_user = User::select('users.*')->where('users.status','1')->where('users.id',$user->id)->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '3')->first();
            $pd_user = User::select('users.*')->where('users.status','1')->where('users.id',$user->id)->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '5')->first();
            $booking_agent = User::select('users.*')->where('users.status','1')->where('users.id',$user->id)->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                         		 ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        		 ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '8')->first();            
            
		    if($super_admin_user){
		        
		        return redirect('/admin/dashboard');
		        
		    }elseif($qc_user || $pd_user){
		        
		        return redirect('/admin/venue/dashboard');
		        
		    }elseif($booking_agent){
		        
		        return redirect('/booking-champion/dashboard');
		        
		    }else{
		        
		        return back()->withErrors(['error' => ['This user can not be login.']]);
		    }
		    

            
            
        } else {
            return back()->withErrors(['error' => ['Wrong Login Details.']]);
		}

    }

    function successlogin()  {
     	return view('dashbord');
    }
    
        
    function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login_at' => now(),
        ]);
    }

    function logout()  {
	    Auth::logout();
	    return redirect('main');
    }
}
