<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Imports\CityImport;
use App\Exports\CityExport;
use App\Exports\AllCityExport;
use App\City;
use App\State;
use Excel;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $cities = City::orderBy('name', 'ASC')->get();

        return view('admin.city.index')->with([
        'cities'  => $cities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::get();
        return view('admin.city.create',compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        $validatedData = $this->validate($request, [
        'name'       => 'required',
        'state_id'   => 'required|not_in:0',
        ]);
        
        
      
        $city = City::create($validatedData);

        return redirect('/admin/cities')->withSuccess('You have successfully created a City!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cities = City::find($id);
        return view('admin.city.show',compact('cities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cities = City::find($id);
        $states = State::get();
        return view('admin.city.edit', compact('cities','states'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $city = City::find($id);
        $city->name =  $request->get('name');
        $city->state_id =  $request->get('state_id');
        $city->save();        
        return redirect('/admin/cities')->with('success', 'City updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function dashboard(Request $request)
    {
  
        return view('admin.city.dashboard');
    }

    //Upload mulitple city through excel file
    public function uploadCity(Request $request)
    {   
        $id = $request->state_id;

        try {
            
           Excel::import(new CityImport($id),$request->file);

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            
            return back()->withError($e->getMessage())->withInput();
        }
        return redirect('/admin/cities')->withSuccess('You have successfully created a City!');
    }
    
    //City Download by state id

    public function cityDownload(Request $request)
    {
        
        //$state_id = $request->state_id;
        $city_data = City::where('state_id',$request->state_id)->with('state','created_user','updated_user')->get()->toArray();
        
        $city_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($city_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'City ID'           => $cat['id'],
                   'City Name'         => $cat['name'],
                   'state ID'           => $cat['state']['id'],
                   'state Zone ID'           => $cat['state']['zone_id'],
                   'state Primary lang ID'           => $cat['state']['primary_lang_id'],
                    'state Other lang ID'           => $cat['state']['other_lang_id'],
                   'State Name'         => $cat['state']['name'],
                   'Created By'            => isset($cat['created_user']['name']) ? $cat['created_user']['name'] : 'NA',
                   'Updated By'            => isset($cat['updated_user']['name']) ? $cat['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new CityExport($data), 'city.xlsx');
    }
    
    public function allCityDownload(Request $request)
    {
        
        
        $city_data = City::with('state','created_user','updated_user')->get()->toArray();
        
        $city_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($city_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'City ID'           => $cat['id'],
                   'City Name'         => $cat['name'],
                   'state ID'           => $cat['state']['id'],
                   'state Zone ID'           => $cat['state']['zone_id'],
                   'state Primary lang ID'           => $cat['state']['primary_lang_id'],
                    'state Other lang ID'           => $cat['state']['other_lang_id'],
                   'State Name'         => $cat['state']['name'],
                   'Created By'            => isset($cat->created_user['name']) ? $cat->created_user['name'] : 'NA',
                   'Updated By'            => isset($cat->updated_user['name']) ? $cat->updated_user['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new AllCityExport($data), 'Allcity.xlsx');
    }
}
