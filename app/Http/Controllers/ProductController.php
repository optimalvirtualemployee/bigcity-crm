<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exports\ProductExport;
use Excel;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $products = Product::with('category')->get();

        return view('admin.product.index')->with([
        'products'  => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status','1')->get();
        return view('admin.product.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            'description'=>'required',
            'category_id'=> 'required|not_in:0',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            
      ]);
      
      $product = Product::create($validatedData);
      if($request->image){
          $imageName = time().'.'.$request->image->extension();
      
          $product->image = $imageName;
          $product->save();
          $request->image->move(public_path('/assets/img/product-images/'), $imageName);
      }
      

      return redirect('/admin/products')->withSuccess('You have successfully created a Product!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::find($id);
        return view('admin.product.show',compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::find($id);
        $categories = Category::where('status','1')->get();
        return view('admin.product.edit', compact('products','categories'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'description'=>'required',
            'category_id'=> 'required|not_in:0',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            
        ]);        
        $product = Product::find($id);
        $product->name =  $request->get('name');
        $product->description =  $request->get('description');
        $product->category_id =  $request->get('category_id');
        if($request->image){
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/assets/img/product-images/'), $imageName);
            $product->image = $imageName;
        }
        
        $product->save();        
        return redirect('/admin/products')->with('success', 'Product updated!');
    }
    
    public function dashboard(Request $request)
    {
  
        return view('admin.product.dashboard');
    }
    
    //Export Excel

    public function productDownload()
    {
        $product_data = Product::with('category','created_user','updated_user')->get()->toArray();
        $product_array[] = array('SN','ID','Catgeory ID','Catgeory Name','Product Name','Created By','Created Date & Time','Status');
             
            foreach($product_data as $index=>$pro)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Product ID'                    => $pro['id'],
                   'Category ID'           => $pro['category']['id'],
                   'Catgeory Name'         => $pro['category']['name'],
                   'Product Name'          => $pro['name'],
                   'Created By'            => isset($pro['created_user']['name']) ? $pro['created_user']['name'] : 'NA',
                   'Updated By'            => isset($pro['updated_user']['name']) ? $pro['updated_user']['name'] : 'NA',
                   'Created Date & Time'   => date("Y-m-d", strtotime($pro['created_at'])).'&'.date("H:i", strtotime($pro['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($pro['updated_at'])).'&'.date("H:i", strtotime($pro['updated_at'])),
                   'Status'                => $pro['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new ProductExport($data), 'productsheet.xlsx');
    }
    
}
