<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VoucherCodes;
use App\CustomerQuery;
use Illuminate\Support\Facades\DB;
use App\Booking;
use App\BookingData;
use App\InboundQuery;
use Carbon\Carbon;

class InboundModuleController extends Controller
{
    public function index()
    {
    	return view('admin.inbound_module.inbound_form');
    }
    
    public function customerTransactionHistory()
    {
        
      $booking_data = InboundQuery::get();
    	return view('admin.inbound_module.customer_transaction_history',compact('booking_data'));
    }
//-----------------------------------------------------------------------------------------------------------//

    // popup method for opening dialogue containing campaign and voucher details.
    public function popup(Request $request){ 
        
       	$voucher_code = $request->input('voucher_code');// voucher code from post request
       	$mobile_no = $request->input('mobile_no');// mobile no from post request
    	
        $Get_voucher_code = VoucherCodes::where('voucher_code',$voucher_code)->first(); 
        // get records from voucher code table where voucher code exists
        
        $Get_Booking_data = BookingData::where(['voucher_code'=>$voucher_code])->first(); 
        // getting mobile no from booking table where voucher code and mobile no both exists.
        
        if($Get_voucher_code){ // getting campaign and product name from mapped voucher code
          $campaign_name = $Get_voucher_code->genCode->campaign->campaign_name;
          $product_name = $Get_voucher_code->genCode->products->name;
        }else{
          $campaign_name = 'N/A';
          $product_name = 'N/A';
        }
        if($Get_Booking_data){ // getting campaign and product name from mapped voucher code
          $voucher_status = 'Booked';
        }else{
           $voucher_status = 'This voucher code is not booked';
        }
 
        $CustomerQuery = CustomerQuery::select('id','name')->where('status','1')->get();
        // customer query dropdown list content
    
        if(!empty($Get_voucher_code)){// voucher code is valid

          /*DB::table('tbl_booking_data')->where('voucher_code', $Get_voucher_code->voucher_code)->update(['customer_mob_no' => $mobile_no]);*/
          return response()->json(
            [
              'data'             => 1,
              'get_voucher_code' => $Get_voucher_code,
              'get_booking_data' => $Get_Booking_data,
              'campaign_name'    => $campaign_name,
              'product_name'     => $product_name,
              'CustomerQuery'    => $CustomerQuery,
              'VoucherStatus'    => $voucher_status
            ]);
        }else if(empty($Get_voucher_code) ){ // when voucher code is invalid only
          return response()->json(['data'=>3]);
        }
    }

//--------------------------------------------------------------------------------------------------------//

    // getting booking details on behalf of voucher code.
    public function booking_details(Request $request){
        
        $voucher_code = $request->post('voucher_code');
    
        $voucher_code_data  = BookingData::where(["voucher_code"=>$voucher_code])
                            ->whereIn('status',array(1,2,3))// status ( 1 => confirm ), ( 2 => pending )
                            ->get()->first();
        if($voucher_code_data){

          $voucher_code = $voucher_code_data;
          $d1           = $voucher_code->venues->venue_name;

        }else {

          $voucher_code = 'Not Booked';
          $d1           = '';
        }
        //dd($voucher_code->venues->venue_name);                    
        if($voucher_code){
          return response()->json(['d'=>$voucher_code,'d1'=>$d1]);
        }
    }

//-----------------------------------------------------------------------------------------------------------//

    // cancel event for booking request comes 24hrs prior to the bookinng time.
    public function cancel_booking(Request $request){
    
        $status_id = $request->post('id');// Passing id as '3' for Cancellation
        // id for changing status of pending or confirmed booking into cancel status
          
        $voucher_code = $request->post('voucher_code');
        $mobile_no    = $request->post('mobile_no');
    
        $query = new Booking;
    
        $get_booking_timing = $query->select('booking_time','customer_email_id')
                                    ->where(['voucher_code'=>$voucher_code,'customer_mob_no'=>$mobile_no])
                                    ->get()
                                    ->first();// get the booking timing against given voucher code.
    
        // get the booking timing and decreasing 24 hours from it and get the value for comparison with current datetime
        $booking_time = strtotime(date( "Y-m-d H:i:s", strtotime( "$get_booking_timing->booking_time -24 hours" )));
          
        // current date and time
        $current_date_time = strtotime(date('Y-m-d H:i:s'));
    
        if($booking_time > $current_date_time){
          $cancel_booking = DB::table('tbl_booking')
                                ->where('voucher_code',$voucher_code)
                                ->update(['status'=>$status_id]);
        }else{
          $cancel_booking = 0;
        }
          
        // check if cancel request is possible
        if($cancel_booking != 0){
           
          // Mobile notification section
          $mob_message = 'Hi there,';
          $mob_message .= 'Your booking is cancel against voucher code :'.$voucher_code;
          $this->sendsms($mobile_no,$mob_message);
          // Mobile notification section
    
          // Email notification Section
          $email = $get_booking_timing->customer_email_id;
          $subject = 'Booking Cancel Notification';
          $from = 'no-reply@bigcity.com';
          $name = 'There,';
          $email_message  = "<br> Your booking request for cancellation";
          $email_message .= "<br> is processed and done, against voucher code : ".$voucher_code;
          $email_message = $this->emailTemplate($name,$email_message);
          $this->send_Email($email, $subject, $email_message, $from);
          // Email notification Section  
    
          return response()->json(['data'=>$cancel_booking]);                      
        }else{
          // Mobile notification section
          $message = 'Hi there,';
          $message .= 'Your booking cancel request cannot be processed, due to request time out.';
          $this->sendsms($mobile_no,$message);
          // Mobile notification section
    
          // Email notification Section
          $email = $get_booking_timing->customer_email_id;
          $subject = 'Booking Cancel Notification';
          $from = 'no-reply@bigcity.com';
          $name = 'There,';
          $email_message  = "<br> Your booking request for cancellation";
          $email_message .= "<br> cannot be processed, due to request time out.";
          $email_message = $this->emailTemplate($name,$email_message);
          $this->send_Email($email, $subject, $email_message, $from);
          // Email notification Section  
    
          return response()->json(['data'=>$cancel_booking]);                      
        }
    }

//---------------------------------------------------------------------------------------------------------//
  
    // check reschedule request for "can be / cannot be" --------------------------
    public function check_request(Request $request){ 
        $voucher_code = $request->get('voucher_code');
        $mobile_no = $request->get('mobile_no');
        $title = $request->get('title');
    
        $query = new Booking;
    
        $get_booking_detail = $query->select('booking_time','customer_email_id')->where(['voucher_code'=>$voucher_code,'customer_mob_no'=>$mobile_no])->get()->first();
    
        // get the booking timing and decreasing 24 hours from it and get the value for comparison with current datetime
        $booking_time = strtotime(date( "Y-m-d H:i:s", strtotime( "$get_booking_detail->booking_time -24 hours" )));
          
        // current date and time
        $current_date_time = strtotime(date('Y-m-d H:i:s'));
    
        if($booking_time > $current_date_time){
          $cancel_booking = 1;
        }else{
          $cancel_booking = 0;
        }
    
        if($cancel_booking == 0){
          // Mobile notification section
          $message = 'Hi there,';
          $message .= 'Your request for '.$title.' cannot be processed, due to request time out.';
          $this->sendsms($mobile_no,$message);
          // Mobile notification section
    
          // Email notification Section
          $email = $get_booking_detail->customer_email_id;
          $subject = $title.' Notification';
          $from = 'no-reply@bigcity.com';
          $name = 'There,';
          $email_message  = "<br> Your request for <strong>".$title."</strong>";
          $email_message .= "<br> cannot be processed, due to request time out.";
          $email_message = $this->emailTemplate($name,$email_message);
          $this->send_Email($email, $subject, $email_message, $from);
          // Email notification Section
    
          return response()->json(['data'=>$cancel_booking]);                      
        }else{
          return response()->json(['data'=>$cancel_booking,'email_id'=>$get_booking_detail->customer_email_id,'booking_time'=>$get_booking_detail->booking_time]);
        }
    }
    // check reschedule request for "can be / cannot be" --------------------------

//----------------------------------------------------------------------------------------------------------//

    // Function for can rescheduled event
    public function can_rescheduled(Request $request){
        $voucher_code = $request->post('voucher_code');
        $date_time = array($request->post('date'),$request->post('time'));
        $email = $request->post('email');
        $mobile_no = $request->post('mobile_no');
    
        $date_time = implode(' ',$date_time);
    
        $can_rescheduled = DB::table('tbl_booking')
                                ->where('voucher_code',$voucher_code)
                                ->update(['booking_time'=>$date_time]);
        if($can_rescheduled){
    
          // Mobile notification section
          $message = 'Hi there,';
          $message .= 'Your booking rescheduled on date : '.$request->post('date').' and time : '.$request->post('time');
          $this->sendsms($mobile_no,$message);
          // Mobile notification section
    
          // Email notification Section
          $subject = 'Booking Rescheduled Notification';
          $from = 'no-reply@bigcity.com';
          $name = 'There,';
          $email_message  = "<br> Your booking request Rescheduled";
          $email_message .= "<br> On date : ".$request->post('date').' and time : '.$request->post('time');
          $email_message = $this->emailTemplate($name,$email_message);
          $this->send_Email($email, $subject, $email_message, $from);
          // Email notification Section  
    
          return response()->json(['message'=>'Booking Rescheduled successfully']);                      
        }else{
          return response()->json(['message'=>$can_rescheduled]);                      
        }
    }

//----------------------------------------------------------------------------------------------------------//

    // Function for cannot rescheduled event
    public function cannot_rescheduled(Request $request){
        $voucher_code = $request->post('voucher_code');
        $remarks = $request->post('remarks');
          
        $cant_rescheduled = DB::table('tbl_booking')
                                ->where('voucher_code',$voucher_code)
                                ->update(['remarks'=>$remarks]);
        if($cant_rescheduled){
          return response()->json(['message'=>'Remark saved successfully']);                      
        }
    }

//-------------------------------------------------------------------------------------------------------//

    // Send email notification for microsite issue.
    public function send_email_microsite_issue(Request $request){
        $message = $request->message;
        $name = 'Team';
        $to = 'pankaj@optimaldigitech.com';
        $from = 'no-reply@bigcity.com';
        $subject = 'Microsite Issue';
          
        $email_message = $this->emailTemplate($name,$message);
        $sendMail = $this->send_Email($to,$subject,$email_message,$from);
    
        if($sendMail){
          return response()->json(['data'=>1,'message'=>'Email Sent to microsite team.']);
        }else{
          return response()->json(['data'=>0,'message'=>'Something went wrong, please try again.']);
        }
    }

//-------------------------------------------------------------------------------------------------------//

    // Send email notification for Venue Related issue.
    public function send_email_venue_related_issue(Request $request){
      
        $voucher_code = $request->post('voucher_code');
        $mobile_no = $request->post('mobile_no');
          
        if($voucher_code){
          $campaign_ID = DB::table('tbl_booking_data')
                            ->select('campaign_id')
                            ->where(['voucher_code'=> $voucher_code,'customer_mob_no'=>$mobile_no])
                            ->get()->first();
        }
        
        if($campaign_ID){
          $venue_id = DB::table('tbl_tagged_city_venue')
                          ->select('tagged_venue_id')
                          ->where('tagged_campaign_id', $campaign_ID->campaign_id)
                          ->get();
        }                                

        if($venue_id){
          $venueID_list = array();
          foreach($venue_id as $key => $val){
            $venueID_list[$key] = $val->tagged_venue_id;
          }
          $pd_lead = DB::table('tbl_venues')
                          ->select('pd_lead_id','venue_name')
                          ->whereIn('id', $venueID_list)->get();
        }

        if($pd_lead){
          $email_list = array();
          foreach($pd_lead as $key => $val){
            $email_list[$key] = DB::table('users')
                                ->select('email','name')
                                ->where('id', $val->pd_lead_id)->get()->first(); 
          }
        }                       
    
        foreach($email_list as $index => $value){
          $to        = $value->email;
          $venueName = $pd_lead[$index]->venue_name;
          $message   = $request->post('message');
          $name      = $value->name;
          $from      = 'no-reply@bigcity.com';
          $subject   = 'Venue Related Issue for - '.$venueName;
              
          $email_message = $this->emailTemplate($name,$message);
          $sendMail = $this->send_Email($to,$subject,$email_message,$from);
          $sendMail = ++$index;
    
          if($sendMail){
            $EmailCounter = ++$index;
          }
        }
         
        if($EmailCounter > 1){
          return response()->json(['data'=>1,'message'=>'Email sent to all concern person successfully']);
        }else{
          return response()->json(['data'=>0,'message'=>'Something went wrong, please try again.']);
        }
    }

//-------------------------------------------------------------------------------------------------------//


    public function booking_confirmation(Request $request){
        
        $voucher_code = $request->post('voucher_code');
        $email = $request->post('email');
        $mobile_no = $request->post('mobile_no');
    
        // Mobile notification section
          $message = 'Hi there,';
          $message .= 'Your booking is confirmed on date : '.$request->post('booking_date').' and time : '.$request->post('booking_time');
          $message .= ' against voucher code : '.$voucher_code;
          $this->sendsms($mobile_no,$message);
          // Mobile notification section
    
          // Email notification Section
          $subject = 'Booking Confirmed Notification';
          $from = 'no-reply@bigcity.com';
          $name = 'There,';
          $email_message  = "<br> Your booking is confirmed";
          $email_message .= " on Date : <strong>".$request->post('booking_date').'</strong> and Time : <strong>'.$request->post('booking_time').'</strong>';
          $email_message .= "<br> against voucher code : <strong>".$voucher_code."</strong>";
          $email_message = $this->emailTemplate($name,$email_message);
          $this->send_Email($email, $subject, $email_message, $from);
        // Email notification Section  
    
           return response()->json(['message'=>'Booking Confirmation message sent successfully']); 
   }
// ---------------------------------------------------------------------------------------------------------------//

    // Function for cannot rescheduled event
    public function unable_to_register(Request $request){
        $voucher_code   = $request->post('voucher_code');
        $remarks        = $request->post('remarks');
        $voucher_status = $request->post('voucher_status');
        $query_id       = $request->post('query_id');
        $mobile_no      = $request->post('mobile_no');
        
        $result = new InboundQuery();
        $result->voucher_code    = $voucher_code;
        $result->customer_mob_no = $mobile_no;
        $result->remarks         = $remarks;
        $result->status          = $voucher_status;
        $result->query_id        = $query_id;
        
        $result->save();

        if($result){
          return response()->json(['message'=>'Remark saved successfully']);                      
        }
    }

//-------------------------------------------------------------------------------------------------------//

    // Function for cannot rescheduled event
    public function validity_related(Request $request){
          
        $voucher_code   = $request->post('voucher_code');
        $remarks        = $request->post('remarks');
        $voucher_status = $request->post('voucher_status');
        $query_id       = $request->post('query_id');
        $mobile_no      = $request->post('mobile_no');
        
        $result = new InboundQuery();
        $result->voucher_code    = $voucher_code;
        $result->customer_mob_no = $mobile_no;
        $result->remarks         = $remarks;
        $result->status          = $voucher_status;
        $result->query_id        = $query_id;
        
        $result->save();

        if($result){
          return response()->json(['message'=>'Remark saved successfully']);                      
        }
    }

//-------------------------------------------------------------------------------------------------------//

    // Function for cannot rescheduled event
    public function contest_related(Request $request){
        $voucher_code   = $request->post('voucher_code');
        $remarks        = $request->post('remarks');
        $voucher_status = $request->post('voucher_status');
        $query_id       = $request->post('query_id');
        $mobile_no      = $request->post('mobile_no');
        
        $result = new InboundQuery();
        $result->voucher_code    = $voucher_code;
        $result->customer_mob_no = $mobile_no;
        $result->remarks         = $remarks;
        $result->status          = $voucher_status;
        $result->query_id        = $query_id;
        
        $result->save();

        if($result){
          return response()->json(['message'=>'Remark saved successfully']);                      
        }
    }

//-------------------------------------------------------------------------------------------------------//

    // Function for cannot rescheduled event
    public function voucher_offer_code_related(Request $request){
        
        $voucher_code   = $request->post('voucher_code');
        $remarks        = $request->post('remarks');
        $voucher_status = $request->post('voucher_status');
        $query_id       = $request->post('query_id');
        $mobile_no      = $request->post('mobile_no');
        
        $result = new InboundQuery();
        $result->voucher_code    = $voucher_code;
        $result->customer_mob_no = $mobile_no;
        $result->remarks         = $remarks;
        $result->status          = $voucher_status;
        $result->query_id        = $query_id;
        
        $result->save();

        if($result){
          return response()->json(['message'=>'Remark saved successfully']);                      
        }
    }

//-------------------------------------------------------------------------------------------------------//

    public function reward_offer_code_resend(Request $request){
        
        $voucher_code = $request->post('voucher_code');
        $email = $request->post('email');
        $mobile_no = $request->post('mobile_no');
        $code = '12345';

        // Mobile notification section
          $message = 'Hi there,';
          $message .= 'Your booking Reward / Offer Code is : '.$code;
          $message .= ' against voucher code : '.$voucher_code;
          $this->sendsms($mobile_no,$message);
          // Mobile notification section
    
          // Email notification Section
          $subject = 'Booking Confirmed Notification';
          $from = 'no-reply@bigcity.com';
          $name = 'There,';
          $email_message  = "<br> Your booking Reward / Offer Code is : ".$code;
          $email_message .= "<br> against voucher code : <strong>".$voucher_code."</strong>";
          $email_message  = $this->emailTemplate($name,$email_message);
          $this->send_Email($email, $subject, $email_message, $from);
          // Email notification Section  
    
          return response()->json(['message'=>'Reward / Offer Code resend successfully']); 
    }
// ---------------------------------------------------------------------------------------------------------------//  

    // function for sending SMS on given mobile number
    public function sendsms($mobileNo, $message){
        
        $responseBody = file_get_contents("https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobileNo."&message=".$message."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3");
        if($responseBody){
          return true;
        }else{
            return false;
        }
    }
    // function for sending SMS on given mobile number

//-----------------------------------------------------------------------------------------------------//
    function queryDetails(Request $request){
      $voucherCode = $request->post('voucher_code');
      $result = InboundQuery::with('cust_query')->where('voucher_code',$voucherCode)->get();

      echo json_encode($result);
    }
//-----------------------------------------------------------------------------------------------------//
   
    //send email function ------- 
    public function send_Email($to, $subject, $message, $from) {   
      
        $email = new \SendGrid\Mail\Mail(); 
        $email->setfrom($from);
        $email->setSubject($subject);
        $email->addTo($to);
        $email->addContent( "text/html", $message );
        $sendgrid = new \SendGrid('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');
        
        if($sendgrid->send($email)){
            
          return true;
        }else{
            
          return false; 
        }  
    }
    //send email function ------- 

//--------------------------------------------------------------------------------------------------------//

    // Email template for sending message in well formed template over email----------
    public function emailTemplate($userName,$message){
    
        return '<!DOCTYPE html>
                  <html>
                    <head>
                      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                      <title>Two Step Authentication</title>
                      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    </head>
                    <body style="margin: 0; padding: 0;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
                        <tr>
                          <td style="padding: 10px 0 30px 0;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                              <tr>
                                <td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                                  <img src="http://crm.developersoptimal.com/assets/img/Bigcity_logo.png" alt="Two Step Authentication" width="300" height="230" style="display: block;" />
                                </td>
                              </tr>
                              <tr>
                                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                        <b>Hi! '.$userName.'</b>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">'.$message.'
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">&reg; All rights reserved to Big City &copy; '.date('Y').'<br/>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </body>
                  </html>';
    }
  // Email template for sending message in well formed template over email----------

//------------------------------------------------------------------------------------------------------------//

}
