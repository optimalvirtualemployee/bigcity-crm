<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\CommunicationTemplate;
use App\CommunicationTemplateTypeModel;
use App\Campaign;
use App\Product;

class CommunicationTemplateController extends Controller
{
    /** @var string */
    /*public $name;

    public function __construct(User $user)
    {
        $this->name = $user->name;
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
        
        $comm_temp = CommunicationTemplate::get();

        return view('admin.communication_template.index')->with([
        'comm_temp'  => $comm_temp
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaign = Campaign::where('status','1')->get();
        $communication_template_type = CommunicationTemplateTypeModel::where('status','1')->get();
        return view('admin.communication_template.create',compact('campaign','communication_template_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'subject'          => 'required',
            'campaign_id'      => 'required|not_in:0',
            'product_id'       => 'required|not_in:0',
            'communication_template_type_id'   => 'required|not_in:0',
            
            
        ]);
        
          
        CommunicationTemplate::create($validatedData);
        
        return redirect('admin/communication-template')->withSuccess('You have successfully created a Template!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $com_temp = CommunicationTemplate::find($id);
        return view('admin.communication_template.show',compact('com_temp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $com_temp = CommunicationTemplate::find($id);
        $campaign = Campaign::where('status','1')->get();
        $product  =  Product::where('status','1')->get();
        $communication_template_type = CommunicationTemplateTypeModel::where('status','1')->get();
        return view('admin.communication_template.edit', compact('com_temp','campaign','product','communication_template_type'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validate($request, [
            'subject'          => 'required',
            'campaign_id'      => 'required|not_in:0',
            'product_id'       => 'required|not_in:0',
            'communication_template_type_id'   => 'required|not_in:0'
            
        ]);     
        
        $com_temp = CommunicationTemplate::find($id);
        $com_temp->subject =  $request->get('subject');
        $com_temp->campaign_id =  $request->get('campaign_id');
        $com_temp->product_id =  $request->get('product_id');
        $com_temp->communication_template_type_id = $request->get('communication_template_type_id');
        $com_temp->sms =  $request->get('sms');
        $com_temp->template_content =  $request->get('template_content');
        $com_temp->save();        
        return redirect('/admin/communication-template')->with('success', 'Communication Template updated!');
    }
    
    
}
