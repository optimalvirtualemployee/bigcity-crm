<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exports\CategoryExport;
use App\AssignType;
use Excel;

class AssignTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $assign_type = AssignType::get();

        return view('admin.assign_type.index')->with([
        'assign_type'  => $assign_type
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.assign_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            
      ]);

      AssignType::create($validatedData);

      return redirect('admin/assign-type')->withSuccess('Assign Type has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assign_type = AssignType::find($id);
        return view('admin.assign_type.show',compact('assign_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assign_type = AssignType::find($id);
        return view('admin.assign_type.edit', compact('assign_type'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $assign_type = AssignType::find($id);
        $assign_type->name =  $request->get('name');
        $assign_type->touch();        
        return redirect('/admin/categories')->with('success', 'Assign Type updated!');
    }

    
    

    //Export Excel

    public function categoryDownload()
    {
        
        $category_data = Category::with('created_user','updated_user')->get()->toArray();
        
        $category_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($category_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Category ID'           => $cat['id'],
                   'Catgeory Name'         => $cat['name'],
                   'Created By'            => isset($cat['created_user']['name']) ? $cat['created_user']['name'] : 'NA',
                   'Updated By'            => isset($cat['updated_user']['name']) ? $cat['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new CategoryExport($data), 'categorysheet.xlsx');
    }
    

    
    
}
