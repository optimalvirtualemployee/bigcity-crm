<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GenerateCodes;
use App\Campaign;
use App\Product;
use App\VoucherCodes;
use App\Exports\AllVoucherCodeExport;
use Illuminate\Support\Str;
use League\Csv\Reader;
use League\Csv\Statement;
use DB;
use Excel;
use League\Csv\Writer;
use Schema;
use SplTempFileObject;
use App\Jobs\ProcessPodcast;
use App\CronJob;

class CronJobController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           
        $cron_job = CronJob::where('flag','0')->first();
        
        if($cron_job){
            
            $cron_job->flag = '1';
            $cron_job->save();
        }
        
        
        
    }
    
    public function createCsv($modelCollection,$request,$created_at){

        
        $folder_name = public_path('/assets/file/voucher-code/');
        $csv = Writer::createFromPath($folder_name.'campaign_'.$request['gc_campaign_id'].'_'.$created_at.'.csv', 'w');
        
        // This creates header columns in the CSV file - probably not needed in some cases.
        $csv->insertOne(['SN','Campaign Id','Campaign Name','Product Id','Product Name','voucher Code']);
        
        if($csv->insertAll($modelCollection)){
            
            $voucher_code = new GenerateCodes;
        
            $voucher_code->voucher_code_file = 'campaign_'.$request['gc_campaign_id'].'_'.$created_at.'.csv';
            $voucher_code->gc_campaign_id = $request['gc_campaign_id'];
            $voucher_code->gc_codes_type = $request['gc_codes_type'];
            $voucher_code->gc_product_id = $request['gc_product_id'];
            $voucher_code->length_of_code_required = $request['length_of_code_required'];
            $voucher_code->gc_voucher_code_batch_name = $request['gc_voucher_code_batch_name'];
            $voucher_code->gc_last_date_to_register_date = $request['gc_last_date_to_register_date'];
            $voucher_code->gc_offer_valid_till_date = $request['gc_offer_valid_till_date'];
            $voucher_code->gc_no_of_codes_required = $request['gc_no_of_codes_required'];
            
            
            $voucher_code->save();
            
        
        }
        
    }
            


    
}
