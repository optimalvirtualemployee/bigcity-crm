<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exports\CommunicationTemplateTypeExport;
use App\CommunicationTemplateTypeModel;
use Excel;

class CommunicationTemplateTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $communication_template_type = CommunicationTemplateTypeModel::get();

        return view('admin.communication_template_type.index')->with([
        'communication_template_type'  => $communication_template_type
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.communication_template_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            
      ]);

      CommunicationTemplateTypeModel::create($validatedData);

      return redirect('admin/communication_template_type')->withSuccess('User has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $communication_template_type = CommunicationTemplateTypeModel::find($id);
        return view('admin.communication_template_type.show',compact('communication_template_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $communication_template_type = CommunicationTemplateTypeModel::find($id);
        return view('admin.communication_template_type.edit', compact('communication_template_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $communication_template_type = CommunicationTemplateTypeModel::find($id);
        $communication_template_type->name =  $request->get('name');
        $communication_template_type->touch();        
        return redirect('/admin/communication_template_type')->with('success', 'Communication Template Type updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $communication_template_type = CommunicationTemplateTypeModel::find($id);
        $communication_template_type->delete();        
        return redirect('/admin/communication_template_type')->with('success', 'Communication Template Type deleted!');
    }


     //Export Excel

    public function communication_template_typeDownload()
    {
        
        $communication_template_type_data = CommunicationTemplateTypeModel::with('created_user','updated_user')->get()->toArray();
        dd($communication_template_type_data);
        
        $communication_template_type_array[] = array('SN','ID','Communication Template Type Name','Created By','Created Date & Time','Status');
             
            foreach($communication_template_type_data as $index=>$val)
            {
                $data[] = array(
                    
                   'SN'                               => ++$index,
                   'Communication Template Type ID'   => $val['id'],
                   'Communication Template Type Name' => $val['name'],
                   'Created By'            => isset($val['created_user']['name']) ? $val['created_user']['name'] : 'NA',
                   'Updated By'            => isset($val['updated_user']['name']) ? $val['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($val['created_at'])).'&'.date("H:i", strtotime($val['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($val['updated_at'])).'&'.date("H:i", strtotime($val['updated_at'])),
                   'Status'                => $val['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new CommunicationTemplateTypeExport($data), 'communicationTemplateTypesheet.xlsx');
    }

      public function changeCommunicationTemplateTypeStatus(Request $request)
    {
        $model = app("App\\$request->model_name")->where('id',$request->id)->first();
        $model->status = $request->status;
        $model->save();
        
        return response()->json(['success'=>'status change successfully.']);
    }

    public function dashboard(Request $request)
    {

        return view('admin.communication_template_type.dashboard');
    }
}
