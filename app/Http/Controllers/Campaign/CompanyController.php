<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\Exports\CompanyExport;
use Excel;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $companies = Company::get();

        return view('admin.company.index')->with([
        'companies'  => $companies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            'brand_name'      => 'required',
            
        ]);
        if($validatedData['name'] != $validatedData['brand_name']){
          
          Company::create($validatedData);
          
        }else{
            
          return redirect()->back()->withErrors('Company name and brand name should not be same');
        }
      
        return redirect('admin/companies')->withSuccess('You have successfully created a Company!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        return view('admin.company.show',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('admin.company.edit', compact('company'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $company = Company::find($id);
        $company->name =  $request->get('name');
        $company->brand_name =  $request->get('brand_name');
        $company->save();        
        return redirect('/admin/companies')->with('success', 'Company updated!');
    }

    
    public function dashboard(Request $request)
    {
  
        return view('admin.company.dashboard');
    }
    
    public function companyDownload()
    {
        
        $company_data = Company::with('created_user','updated_user')->get()->toArray();
        
        $company_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($company_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Company ID'           => $cat['id'],
                   'Company Name'         => $cat['name'],
                   'Company Brand'         => $cat['brand_name'],
                   
                   'Created By'            => isset($cat['created_user']['name']) ? $cat['created_user']['name'] : 'NA',
                   'Updated By'            => isset($cat['updated_user']['name']) ? $cat['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new CompanyExport($data), 'Companysheet.xlsx');
    }

    
}
