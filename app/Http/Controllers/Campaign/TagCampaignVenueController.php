<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TagCampaignVenue;
use App\Campaign;
use App\Product;
use App\City;
use App\Venue;
use App\CampaignDetails;
use DB;
use App\TaggedCityVenue;
use App\TagCampaignCity;

class TagCampaignVenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tag_campaign_venue = TagCampaignVenue::groupBy('product_id','campaign_id')->get();
        
        
        return view('admin.tag_campaign_venue.index')->with([
        'tag_campaign_venue'  => $tag_campaign_venue
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaign_detail = TagCampaignVenue::get();
        
        foreach($campaign_detail as $detail){
            $data[] = $detail->campaign_id;
            
        }
        /*if(!empty($data)){
            
            $campaign = Campaign::where('status','1')->whereNotIn('id',$data)->get();
            
        }else{*/
            
            $campaign = Campaign::where('status','1')->get();
       /* }*/
       
        $venue = Venue::where('status','1')->groupBy('city_id')->get();
        
        foreach($venue as $ven){
            
            $data1[] = $ven->city_id;
            
        }
        if(!empty($data1)){
            
            $cities = City::where('status','1')->whereIn('id',$data1)->get();
        }else{
            $cities = City::where('status','1')->get();
        }
        
        $venue = Venue::where('status','1')->get();
        return view('admin.tag_campaign_venue.create',compact('campaign','cities','venue'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $validator = $this->validate($request, [
            "campaign_id"   => 'required|not_in:0',
            "product_id"    => "required|not_in:0",
            "city_id"       => "required|not_in:0",
            "venue_id"      => "required"
        ]);

        $camp_id  = $request->campaign_id;
        $prod_id  = $request->product_id;
        
        foreach($request->city_id as $city_id){
            $city_id  = $city_id;
        
            $tag_campaign_venue = new TagCampaignVenue;
            $tag_campaign_venue->campaign_id = $camp_id;
            $tag_campaign_venue->product_id = $prod_id;
            $tag_campaign_venue->city_id = $city_id;
        
            $tag_campaign_venue->save();
        
                if($tag_campaign_venue->id){

                    foreach($request['venue_id'] as $i => $venue_id){
                         $venue = explode(',', $venue_id);
                          
                           if($venue[1] == $city_id){
                            
                            $tagCityVenue = new TaggedCityVenue;
                            
                            $tagCityVenue->tagged_campaign_id = $camp_id;
                            $tagCityVenue->tagged_product_id = $prod_id;
                            $tagCityVenue->tagged_venue_id = $venue[0];
                            $tagCityVenue->tagged_city_id = $venue[1];
                            
                            $tagCityVenue->save();
                         }
                    }
                    /*$tagged_city_id  = $request->input('city_id');
                    foreach($request->venue_id as $key => $venue_id){
                        
               
                        $tagged_city_venue = new TaggedCityVenue;
                        $tagged_city_venue->tagged_campaign_id = $camp_id;
                        $tagged_city_venue->tagged_city_id = $tagged_city_id[$key];
                        $tagged_city_venue->tagged_venue_id = $venue_id;
                        
                        $tagged_city_venue->save();
                    }*/
                }
        }

        return redirect('/admin/tag_campaign_venues')->withSuccess('You have successfully tagged Campaign with Venues!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($campaign_id,$product_id)
    {
        
        $tagCampaignVenue = TagCampaignVenue::where('campaign_id',$campaign_id)->where('product_id',$product_id)->first();
        $tagCityVenue = TaggedCityVenue::where('tagged_campaign_id',$campaign_id)->where('tagged_product_id',$product_id)->get();
        
        
        
        return view('admin.tag_campaign_venue.show',compact('tagCampaignVenue','tagCityVenue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($campaign_id,$product_id)
    {
        $campaign = Campaign::where('status','1')->get();
        
        $venues = Venue::where('status','1')->groupBy('city_id')->get();
        
        foreach($venues as $ven){
            
            $data1[] = $ven->city_id;
            
        }
        if(!empty($data1)){
            
            $cities = City::where('status','1')->whereIn('id',$data1)->get();
        }else{
            $cities = City::where('status','1')->get();
        }
        $venue = Venue::where('status','1')->get();
        
        $tagCampaignVenue = TagCampaignVenue::where('campaign_id',$campaign_id)->where('product_id',$product_id)->get();
        
        $products = CampaignDetails::where('campaign_id',$campaign_id)->with('products')->get();
        
        $product = Product::whereIn('id',explode(',',$products[0]['product_id']))->get(['name','id']);
        $tagCityVenue = TaggedCityVenue::where('tagged_campaign_id',$campaign_id)->where('tagged_product_id',$product_id)->groupBy('tagged_city_id')->get();
        $CityVenue = TaggedCityVenue::where('tagged_campaign_id',$campaign_id)->where('tagged_product_id',$product_id)->get();
        return view('admin.tag_campaign_venue.edit',compact('CityVenue','tagCampaignVenue','campaign','product','cities','venue','tagCityVenue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $campaign_id,$product_id)
    {
        
        $validator = $this->validate($request, [
            "campaign_id"   => 'required|not_in:0',
            "product_id"    => "required|not_in:0",
            "city_id"       => "required|not_in:0",
            "venue_id"      => "required"
        ]);
        $deletedTaggedCityRecords = TaggedCityVenue::where('tagged_campaign_id',$campaign_id)->where('tagged_product_id',$product_id)->delete();
        $deletedCampaignVenueRecords = TagCampaignVenue::where('campaign_id',$campaign_id)->where('product_id',$product_id)->delete();
        $camp_id  = $request->campaign_id;
        $prod_id  = $request->product_id;
        
        foreach($request->city_id as $city_id){
            $city_id  = $city_id;
        
            $tag_campaign_venue = new TagCampaignVenue;
            $tag_campaign_venue->campaign_id = $camp_id;
            $tag_campaign_venue->product_id = $prod_id;
            $tag_campaign_venue->city_id = $city_id;
        
            $tag_campaign_venue->touch();
            if($tag_campaign_venue->id){

                foreach($request['venue_id'] as $i => $venue_id){
                     $venue = explode(',', $venue_id);
                      
                       if($venue[1] == $city_id){
                        
                        $tagCityVenue = new TaggedCityVenue;
                        
                        $tagCityVenue->tagged_campaign_id = $camp_id;
                        $tagCityVenue->tagged_product_id = $prod_id;
                        $tagCityVenue->tagged_venue_id = $venue[0];
                        $tagCityVenue->tagged_city_id = $venue[1];
                        
                        $tagCityVenue->save();
                     }
                }
            }
        }

        /*if($tag_campaign_venue->id){
            foreach($request->venue_id as $key => $venue_id){
                
                $tagged_city_id  = $request->input('city_id');
                
                $tagged_city_venue = new TaggedCityVenue;
                $tagged_city_venue->tagged_campaign_id = $camp_id;
                $tagged_city_venue->tagged_city_id = $tagged_city_id[0];
                $tagged_city_venue->tagged_venue_id = $venue_id;
                
                $tagged_city_venue->touch();
            }
        }*/

        return redirect('/admin/tag_campaign_venues')->withSuccess('You have successfully updated tagged Campaign with Venues!');
    }

    


    /*
    * Creating function to recieve Ajax request for product listing on behalf of campaign ID
    */
    public function get_productList(Request $request){
        
        $productExists = CampaignDetails::select('product_id','campaign_id')->where('campaign_id',$request->campaign_id)->get();
        
        return response()->json($productExists);
    }

    public function get_productName(Request $request){
        
        
        $campaign_detail = TagCampaignVenue::where('campaign_id',$request->campaign_id)->get();
        
        foreach($campaign_detail as $detail){
            $data[] = $detail->product_id;
            
        }
        if(!empty($data)){
            
            $product = Product::whereIn('id',explode(',',$request->id))->whereNotIn('id',$data)->get(['name','id']);
            //$campaign = Campaign::where('status','1')->whereNotIn('id',$data)->get();
            
        }else{
            
            $product = Product::whereIn('id',explode(',',$request->id))->get(['name','id']);
            //$campaign = Campaign::where('status','1')->get();
        }
        
        
        
        return response()->json($product);
    }

    
    /* 
        Below function is for to get all the venues tagged with requested city ID.
    */
    function get_venueList(request $request){

        // DB::enableQueryLog();
        $venues = Venue::whereIn('city_id',$request->id)->where('status','1')->get();
        //dd(DB::getQueryLog());
        return response()->json($venues);
    }
}
