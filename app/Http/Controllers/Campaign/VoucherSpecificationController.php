<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\VoucherType;
use App\VoucherSpecification;
use App\Exports\VoucherSpecificationExport;
use DB;
use Excel;

class VoucherSpecificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $voucher_specification = VoucherSpecification::get();

        return view('admin.voucherspecification.index')->with([
        'voucher_specification'  => $voucher_specification
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $voucher_type = VoucherType::get();
        return view('admin.voucherspecification.create',compact('voucher_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        
           if(null !== $request->get('voucher_type_id') && $request->get('voucher_type_id') == 1 || $request->get('voucher_type_id') == 3){

                $validate_Physical = $this->validate($request, [
                'voucher_type_id' =>'required|not_in:0',
                'size_name'       => 'required',
                'length'          => 'required',
                'breadth'         => 'required',
                'thickness'       => 'required',
            ]);
               
                VoucherSpecification::create($validate_Physical);
                
            }else if(null !== $request->get('voucher_type_id') && $request->get('voucher_type_id') == 4 ){

                $validate_Evoucher = $this->validate($request, [
                'voucher_type_id' =>'required|not_in:0|unique:tbl_voucher_specification',
                'evoucher_id'    => 'required|not_in:0',
                ]);

                 // DB::enableQueryLog();
                 $E_voucher = new VoucherSpecification;
                 $E_voucher->voucher_type_id = $request->get('voucher_type_id');
                 $E_voucher->evoucher_id = $request->get('evoucher_id');
                 $E_voucher->save();
                // dd(DB::getQueryLog());

            }else if(null !== $request->get('voucher_type_id') && $request->get('voucher_type_id') == 1 || $request->get('voucher_type_id') == 2 || $request->get('voucher_type_id') == 3){

                $validate_Physical_eVoucher = $this->validate($request, [
                'voucher_type_id' =>'required|not_in:0',
                'size_name'       => 'required',
                'length'          => 'required',
                'breadth'         => 'required',
                'thickness'       => 'required',
                'evoucher_id'    => 'required|not_in:0',
                
                ]);

                $Physical_eVoucher = new VoucherSpecification;
                $Physical_eVoucher->voucher_type_id = $request->get('voucher_type_id');
                $Physical_eVoucher->size_name = $request->get('size_name');
                $Physical_eVoucher->length = $request->get('length');
                $Physical_eVoucher->breadth = $request->get('breadth');
                $Physical_eVoucher->thickness = $request->get('thickness');
                $Physical_eVoucher->evoucher_id = $request->get('evoucher_id');
                $Physical_eVoucher->save();

            }else{

                $validatedData = $this->validate($request, [
                'voucher_type_id' =>'required|not_in:0|unique:tbl_voucher_specification',
                ]);
                VoucherSpecification::create($validatedData);
            }    


      return redirect('admin/voucherspecification')->withSuccess('You have successfully created a Voucher Specification!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $voucher_specification = VoucherSpecification::find($id);

        return view('admin.voucherspecification.show',compact('voucher_specification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $voucher_specification = VoucherSpecification::find($id);
        $voucher_type = VoucherType::get();
        return view('admin.voucherspecification.edit', compact('voucher_specification','voucher_type'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $deleteRecords = VoucherSpecification::where('id',$id)->delete();
        if($deleteRecords){
        if(null !== $request->get('voucher_type_id') && $request->get('voucher_type_id') == 1 || $request->get('voucher_type_id') == 3){

                $validate_Physical = $this->validate($request, [
                'voucher_type_id' =>'required|not_in:0',
                'size_name'       => 'required',
                'length'          => 'required',
                'breadth'         => 'required',
                'thickness'       => 'required',
            ]);
               
                VoucherSpecification::create($validate_Physical);
                
            }else if(null !== $request->get('voucher_type_id') && $request->get('voucher_type_id') == 4 ){

                $validate_Evoucher = $this->validate($request, [
                'voucher_type_id' =>'required|not_in:0',
                'evoucher_id'    => 'required|not_in:0',
                ]);

                 // DB::enableQueryLog();
                 $E_voucher = new VoucherSpecification;
                 $E_voucher->voucher_type_id = $request->get('voucher_type_id');
                 $E_voucher->evoucher_id = $request->get('evoucher_id');
                 $E_voucher->save();
                // dd(DB::getQueryLog());

            }else if(null !== $request->get('voucher_type_id') && $request->get('voucher_type_id') == 1 || $request->get('voucher_type_id') == 2 || $request->get('voucher_type_id') == 3){

                $validate_Physical_eVoucher = $this->validate($request, [
                'voucher_type_id' =>'required|not_in:0',
                'size_name'       => 'required',
                'length'          => 'required',
                'breadth'         => 'required',
                'thickness'       => 'required',
                'evoucher_id'    => 'required|not_in:0',
                
                ]);

                $Physical_eVoucher = new VoucherSpecification;
                $Physical_eVoucher->voucher_type_id = $request->get('voucher_type_id');
                $Physical_eVoucher->size_name = $request->get('size_name');
                $Physical_eVoucher->length = $request->get('length');
                $Physical_eVoucher->breadth = $request->get('breadth');
                $Physical_eVoucher->thickness = $request->get('thickness');
                $Physical_eVoucher->evoucher_id = $request->get('evoucher_id');
                $Physical_eVoucher->save();

            }else{

                $validatedData = $this->validate($request, [
                'voucher_type_id' =>'required|not_in:0|unique:tbl_voucher_specification',
                ]);
                VoucherSpecification::create($validatedData);
            } 
         }   


        return redirect('/admin/voucherspecification')->with('success', 'Voucher Specification updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function dashboard(Request $request)
    {
  
        return view('admin.voucherspecification.dashboard');
    }

    /*
    * This below function is to check for the values exists or not when voucher type onchange event get performed.
    */
    public function getVoucher_specification(request $request){
        // DB::enableQueryLog();
         $voucher_spec = VoucherSpecification::where('voucher_type_id',$request->id)->get()->first();
         // dd(DB::getQueryLog());
        return response()->json($voucher_spec);

    }
    public function voucherSpecificationDownload()
    {
        
        $voucher_data = VoucherSpecification::with('created_user','updated_user','voucher_type')->get()->toArray();
        
        $voucher_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($voucher_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Voucher Specification ID'           => $cat['id'],
                   'Voucher Specification size Name'         => $cat['size_name'],
                   'Voucher Specification Length'         => $cat['length'],
                   'Voucher Specification Breadth'         => $cat['breadth'],
                   'Voucher Specification Thickness'         => $cat['thickness'],
                   'Voucher Type Id'         => $cat['voucher_type']['id'],
                   'Voucher Type Name'         => $cat['voucher_type']['name'],
                   'Created By'            => isset($cat['created_user']['name']) ? $cat['created_user']['name'] : 'NA',
                   'Updated By'            => isset($cat['updated_user']['name']) ? $cat['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new VoucherSpecificationExport($data), 'VoucherSpecificationsheet.xlsx');
    }
}
