<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CampaignDetails;
use App\Campaign;
use App\PromoType;
use App\Product;
use App\MobileUsageRule;

class CampaignDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //campaign details listing page call
        $campaigns = Campaign::where('status','1')->get();
        
        foreach($campaigns as $detail){
            $data[] = $detail->id;
            
        }
        if(!empty($data)){
            
            $campaignDetails = CampaignDetails::whereIn('campaign_id',$data)->get();
        }else{
            $campaignDetails = CampaignDetails::get();
        }
       
        return view('admin.campaign_detail.index',compact('campaignDetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //to add campaign details
        
        $campaign_detail = CampaignDetails::get();;
        
        foreach($campaign_detail as $detail){
            $data[] = $detail->campaign_id;
            
        }
        if(!empty($data)){
            
            $campaigns = Campaign::where('status','1')->whereNotIn('id',$data)->get();
        }else{
            $campaigns = Campaign::where('status','1')->get();
        }
        
        $promo_type      = PromoType::where('status','1')->get();
        $products        = Product::where('status','1')->get();
        $mobile_no_usage = MobileUsageRule::where('status','1')->get();
        return view('admin.campaign_detail.create',compact('campaigns','promo_type','products','mobile_no_usage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //To check no of product is equal or less than reward value
        if(count($request->product_id) < $request->no_of_reward_redeem){
            
            return redirect()->back()->withErrors("You can't fill reward redeemable value more than the number of products")->withInput();

            
        }else{
            
            $campaign_detail = new CampaignDetails;
            $campaign_detail->product_id                    = implode(",", $request->product_id);
            $campaign_detail->promo_type_id                 = $request->promo_type_id;
            $campaign_detail->campaign_id                   = $request->campaign_id;
            $campaign_detail->no_of_reward_redeem           = $request->no_of_reward_redeem;
            $campaign_detail->mobile_no_usage_rules         = implode(",",$request->mobile_no_usage_rules);
            $campaign_detail->mobile_usage_value            = implode(",",$request->mobile_usage_value);
            $campaign_detail->save();
          
          return redirect('/admin/campaign_details')->withSuccess('You have successfully added campaign details!');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign_detail = CampaignDetails::find($id);
        
        return view('admin.campaign_detail.show',compact('campaign_detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //for update campaign details
        $campaigns       = Campaign::where('status','1')->get();
        $promo_type      = PromoType::where('status','1')->get();
        $products        = Product::where('status','1')->get();
        $mobile_no_usage = MobileUsageRule::where('status','1')->get();
        $camp_detail     = CampaignDetails::find($id);
        /*foreach($camp_detail as $detail){
            
            $products        = Product::where('status','1')->where('id',$product->id)->get();
        }
        */
        return view('admin.campaign_detail.edit',compact('camp_detail','campaigns','promo_type','products','mobile_no_usage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(count($request->product_id) < $request->no_of_reward_redeem){
            
            return redirect()->back()->withErrors("You can't fill reward redeemable value more than the number of products")->withInput();

            
        }else{

            $campaign_detail = CampaignDetails::find($id);
            $campaign_detail->product_id                    = implode(",", $request->product_id);
            $campaign_detail->promo_type_id                 = $request->promo_type_id;
            $campaign_detail->campaign_id                   = $request->campaign_id;
            $campaign_detail->no_of_reward_redeem   = $request->no_of_reward_redeem;
            $campaign_detail->mobile_no_usage_rules = implode(",",$request->mobile_no_usage_rules);
            $campaign_detail->mobile_usage_value    = implode(",",$request->mobile_usage_value);
            $campaign_detail->touch();
            return redirect('/admin/campaign_details')->withSuccess('Campaign Details Updated successfully!');
        }
          
    }

    

    public function get_ProductRuleList(Request $request){

        $productruleExists = ProductRule::where('product_id',$request->id)->where('status','1')->get();
        
           return response()->json($productruleExists);  
           
    }
}
