<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\CampaignType;
use Excel;

class CampaignTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $campaigns = CampaignType::get();

        return view('admin.campaigntype.index')->with([
        'campaigns'  => $campaigns
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.campaigntype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            
      ]);

      CampaignType::create($validatedData);

      return redirect('admin/campaigntype')->withSuccess('You have successfully created a Campaign Type!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaigntype = CampaignType::find($id);
        return view('admin.campaigntype.show',compact('campaigntype'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $campaigntype = CampaignType::find($id);
        return view('admin.campaigntype.edit', compact('campaigntype'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $campaigntype = CampaignType::find($id);
        $campaigntype->name =  $request->get('name');
        $campaigntype->save();        
        return redirect('/admin/campaigntype')->with('success', 'Campaign Type updated!');
    }

    
    

    
}
