<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DeliveryMechanicType;
use App\Exports\DeliveryMechanicTypeExport;
use Excel;

class DeliveryMechanicTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $deliveryMechanicType = DeliveryMechanicType::get();

        return view('admin.deliverymechanictype.index')->with([
        'deliveryMechanicType'  => $deliveryMechanicType
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.deliverymechanictype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            
      ]);

      DeliveryMechanicType::create($validatedData);

      return redirect('admin/deliverymechanictype')->withSuccess('You have successfully created a Delivery Mechanic Type !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $deliveryMechanicType = DeliveryMechanicType::find($id);
        return view('admin.deliverymechanictype.show',compact('deliveryMechanicType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deliveryMechanicType = DeliveryMechanicType::find($id);
        return view('admin.deliverymechanictype.edit', compact('deliveryMechanicType'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $deliveryMechanicType = DeliveryMechanicType::find($id);
        $deliveryMechanicType->name =  $request->get('name');
        $deliveryMechanicType->save();        
        return redirect('/admin/deliverymechanictype')->with('success', 'Delivery Mechanic Type updated!');
    }

    
    public function dashboard(Request $request)
    {
  
        return view('admin.deliverymechanictype.dashboard');
    }
    
    public function deliveryMechanicTypeDownload()
    {
        
        $deliveryMechanicType_data = DeliveryMechanicType::with('created_user','updated_user')->get()->toArray();
        
        $deliveryMechanicType_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($deliveryMechanicType_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Delivery Mechanic Type ID'           => $cat['id'],
                   'Delivery Mechanic Type Name'         => $cat['name'],
                   'Created By'            => isset($cat['created_user']['name']) ? $cat['created_user']['name'] : 'NA',
                   'Updated By'            => isset($cat['updated_user']['name']) ? $cat['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new DeliveryMechanicTypeExport($data), 'DeliveryMechanicTypesheet.xlsx');
    }

    
}
