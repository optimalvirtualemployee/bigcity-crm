<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exports\PromoTypeExport;
use App\PromoType;
use Excel;

class PromoTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $promos = PromoType::get();

        return view('admin.promotype.index')->with([
        'promos'  => $promos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promotype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            
      ]);

      PromoType::create($validatedData);

      return redirect('admin/promotype')->withSuccess('You have successfully created a Promo Type!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promotype = PromoType::find($id);
        return view('admin.promotype.show',compact('promotype'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotype = PromoType::find($id);
        return view('admin.promotype.edit', compact('promotype'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $promotype = PromoType::find($id);
        $promotype->name =  $request->get('name');
        $promotype->save();        
        return redirect('/admin/promotype')->with('success', 'Promo Type updated!');
    }

    
    public function dashboard(Request $request)
    {
  
        return view('admin.promotype.dashboard');
    }
    
    public function promoTypeDownload()
    {
        
        $promoType_data = PromoType::with('created_user','updated_user')->get()->toArray();
        
        $promoType_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($promoType_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Promo Type ID'           => $cat['id'],
                   'Promo Type Name'         => $cat['name'],
                   'Created By'            => isset($cat['created_user']['name']) ? $cat['created_user']['name'] : 'NA',
                   'Updated By'            => isset($cat['updated_user']['name']) ? $cat['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($cat['created_at'])).'&'.date("H:i", strtotime($cat['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($cat['updated_at'])).'&'.date("H:i", strtotime($cat['updated_at'])),
                   'Status'                => $cat['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new PromoTypeExport($data), 'PromoTypesheet.xlsx');
    }

    
}
