<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\MobileUsageRule;

class MobileUsageRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $mobile_usage_rules = MobileUsageRule::get();

        return view('admin.mobileusagerule.index')->with([
        'mobile_usage_rules'  => $mobile_usage_rules
        
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mobileusagerule.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required|min:3|max:255|string',
            
      ]);

      MobileUsageRule::create($validatedData);

      return redirect('admin/mobile-usage-rules')->withSuccess('You have successfully created a Mobile Usage Rule!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mobile_usage_rules = MobileUsageRule::find($id);
        return view('admin.mobileusagerule.show',compact('mobile_usage_rules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mobile_usage_rules = MobileUsageRule::find($id);
        return view('admin.mobileusagerule.edit', compact('mobile_usage_rules'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $mobile_usage_rules = MobileUsageRule::find($id);
        $mobile_usage_rules->name =  $request->get('name');
        $mobile_usage_rules->save();        
        return redirect('/admin/mobile-usage-rules')->with('success', 'Mobile Usage Rule updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mobile_usage_rules = MobileUsageRule::find($id);
        $mobile_usage_rules->delete();        
        return redirect('/admin/mobile-usage-rules')->with('success', 'Mobile Usage Rule deleted!');
    }

    
    
    
}
