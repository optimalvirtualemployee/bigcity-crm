<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Campaign;
use App\CampaignType;
use App\DeliveryMechanicType;
use App\VoucherType;
use App\VoucherSpecification;
use App\Company;
use App\User;
class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    //get camapaign list
    public function index()
    {
        $campaignlist = Campaign::get();
        return view('admin.campaign.index',compact('campaignlist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaign_type  = CampaignType::where('status','1')->get();
        $mechanic_type  = DeliveryMechanicType::where('status','1')->get();
        $voucher_type   = VoucherType::where('status','1')->get();
        $voucher_spec   = VoucherSpecification::where('status','1')->get();
        $companies      = Company::where('status','1')->get();
        $bd_person_name = User::select('users.*')->where('users.status','1')->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '7')->get();
        $cs_person_name = User::select('users.*')->where('users.status','1')->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '6')->get();
                        
        
        return view('admin.campaign.create',compact('campaign_type','mechanic_type','voucher_type','voucher_spec','companies','bd_person_name','cs_person_name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // dd($request->all());die();
        $validatedData = $this->validate($request, [
            'campaign_name'                  => 'required',
            'campaign_brand_id'              => 'required|not_in:0',
            'campaign_type_id'               => 'required|not_in:0',
            'campaign_estimate_no'           => 'required',
            'campaign_estimate_date'         => 'required',
            'campaign_bd_person_name_id'     => 'required|not_in:0',
            'campaign_cs_person_name_id'     => 'required|not_in:0',
            'campaign_gateway_details'                => 'required',
            'campaign_mechanic_type_id'      => 'required|not_in:0',
            // 'sms_keyword'                    => 'required|min:3|max:255|string',
            // 'sms_long_code'                  => 'required|min:3|max:255|string',
            // 'web_microsite_name'             => 'required|min:3|max:255|string',
            'campaign_voucher_type_id'       => 'required|not_in:0',
            //'campaign_voucher_spec_id'       => 'required|not_in:0',
            'is_printing_bigcity'            => 'required',
            // 'campaign_voucher_cost'          => 'required|min:3|max:255|string',
            // 'campaign_cost_to_client'        => 'required|min:3|max:255|string',
            // 'campaign_cost_to_bigcity'       => 'required|min:3|max:255|string',
            'campaign_upload_signed_estimate_po'      => 'file|mimes:jpeg,jpg,png,pdf,docx|max:2048',
            
      ]);

      $campaign = Campaign::create($validatedData);
      if($request->campaign_upload_signed_estimate_po){
          $fileName = time().'.'.$request->campaign_upload_signed_estimate_po->extension();
          $campaign->campaign_upload_signed_estimate_po = $fileName;
          $request->campaign_upload_signed_estimate_po->move(public_path('/assets/file/signed-estimate-po/'), $fileName);
      }
      
      
      $campaign->voucher_spec_type_id = $request->voucher_spec_type_id;
      $campaign->campaign_voucher_cost = $request->campaign_voucher_cost;
      $campaign->campaign_cost_to_client = $request->campaign_cost_to_client;
      $campaign->campaign_cost_to_bigcity = $request->campaign_cost_to_bigcity;
      $campaign->sms_keyword = $request->sms_keyword;
      $campaign->campaign_cost_to_bigcity = $request->campaign_cost_to_bigcity;
      $campaign->sms_long_code = $request->sms_long_code;
      $campaign->web_microsite_name = $request->web_microsite_name;
      
      
      $campaign->save();
      

      return redirect('admin/campaigns')->withSuccess('You have successfully created a Campaign!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $campaigns = Campaign::find($id);
        return view('admin.campaign.show',compact('campaigns'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $campaign = Campaign::find($id);
        
        $campaign_type  = CampaignType::where('status','1')->get();
        $mechanic_type  = DeliveryMechanicType::where('status','1')->get();
        $voucher_type   = VoucherType::where('status','1')->get();
        $voucher_spec   = VoucherSpecification::where('status','1')->get();
        $companies      = Company::where('status','1')->get();
        $bd_person_name = User::select('users.*')->where('users.status','1')->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '7')->get();
        $cs_person_name = User::select('users.*')->where('users.status','1')->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '6')->get();

        return view('admin.campaign.edit', compact('campaign','campaign_type','mechanic_type','voucher_type','voucher_spec','companies','bd_person_name','cs_person_name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'campaign_name'                  => 'required',
            'campaign_brand_id'              => 'required|not_in:0',
            'campaign_type_id'               => 'required|not_in:0',
            'campaign_estimate_no'           => 'required',
            'campaign_estimate_date'         => 'required',
            'campaign_bd_person_name_id'     => 'required|not_in:0',
            'campaign_cs_person_name_id'     => 'required|not_in:0',
            'gateway_details'                => 'required',
            'campaign_mechanic_type_id'      => 'required|not_in:0',
            // 'sms_keyword'                    => 'required|min:3|max:255|string',
            // 'sms_long_code'                  => 'required|min:3|max:255|string',
            // 'web_microsite_name'             => 'required|min:3|max:255|string',
            'campaign_voucher_type_id'       => 'required|not_in:0',
            //'campaign_voucher_spec_id'       => 'required|not_in:0',
            'is_printing_bigcity'            => 'required',
            // 'campaign_voucher_cost'          => 'required|min:3|max:255|string',
            // 'campaign_cost_to_client'        => 'required|min:3|max:255|string',
            // 'campaign_cost_to_bigcity'       => 'required|min:3|max:255|string',
             'campaign_upload_signed_estimate_po'      => 'file|mimes:jpeg,jpg,png,pdf,docx|max:2048',
            
        ]);



        $campaign = Campaign::find($id);
        $campaign->campaign_name              =  $request->get('campaign_name');
        $campaign->campaign_brand_id          =  $request->get('campaign_brand_id');
        $campaign->campaign_type_id           =  $request->get('campaign_type_id');
        $campaign->campaign_estimate_no       =  $request->get('campaign_estimate_no');
        $campaign->campaign_estimate_date     =  $request->get('campaign_estimate_date');
        $campaign->campaign_bd_person_name_id =  $request->get('campaign_bd_person_name_id');
        $campaign->campaign_cs_person_name_id =  $request->get('campaign_cs_person_name_id');
        $campaign->campaign_gateway_details   =  $request->get('gateway_details');
        $campaign->campaign_mechanic_type_id  =  $request->get('campaign_mechanic_type_id');
        $campaign->sms_keyword                =  $request->get('sms_keyword');
        $campaign->sms_long_code              =  $request->get('sms_long_code');
        $campaign->web_microsite_name         =  $request->get('web_microsite_name');
        $campaign->campaign_voucher_type_id   =  $request->get('campaign_voucher_type_id');
        $campaign->voucher_spec_type_id       =  $request->get('voucher_spec_type_id');
        $campaign->is_printing_bigcity        =  $request->get('is_printing_bigcity');
        $campaign->campaign_voucher_cost      =  $request->get('campaign_voucher_cost');
        $campaign->campaign_cost_to_client    =  $request->get('campaign_cost_to_client');
        $campaign->campaign_cost_to_bigcity   =  $request->get('campaign_cost_to_bigcity');
        // $campaign->campaign_upload_signed_estimate_po =  $request->get('campaign_upload_signed_estimate_po');
        if($request->campaign_upload_signed_estimate_po){
            
            $fileName = time().'.'.$request->campaign_upload_signed_estimate_po->extension();
            $request->campaign_upload_signed_estimate_po->move(public_path('/assets/file/signed-estimate-po/'), $fileName);
            $campaign->campaign_upload_signed_estimate_po = $fileName;
            $campaign->save();
        }
        
        $campaign->save();        
        return redirect('/admin/campaigns')->with('success', 'Campaign updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $campaign = Campaign::find($id);
        $campaign->delete();        
        return redirect('/admin/campaigns')->with('success', 'Campaign deleted!');
    }

    public function dashboard(Request $request)
    {
  
        return view('admin.campaign.dashboard');
    }
}
