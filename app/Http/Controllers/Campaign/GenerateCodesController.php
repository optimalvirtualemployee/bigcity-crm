<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GenerateCodes;
use App\Campaign;
use App\Product;
use App\VoucherCodes;
use App\Exports\AllVoucherCodeExport;
use Illuminate\Support\Str;
use DB;
use App\Jobs\VoucherCode;
use App\CronJob;
use App\CampaignDetails;
use Artisan;
use Excel;
class GenerateCodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        
        $generate_codes = GenerateCodes::get();

        return view('admin.generate_codes.index')->with([
        'generate_codes'  => $generate_codes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $campaigns = Campaign::where('status','1')->get();
        $products = Product::where('status','1')->get();
        return view('admin.generate_codes.create',compact('campaigns','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            
            'campaign_id'                           => 'required|not_in:0',
            /*'creation_type'                         => 'required|not_in:0',*/
            'product_id'                            => 'required|not_in:0',
            'length_of_code_required'               => 'required|not_in:0',
            'gc_codes_type'                         => 'required|not_in:0',
            'gc_voucher_code_batch_name'            => 'required|min:2|string',
            'gc_no_of_codes_required'               => 'required|min:2|numeric',
            'gc_last_date_to_register_date'         => 'required|date',
            'gc_offer_valid_till_date'              => 'required|date|after_or_equal:gc_last_date_to_register_date'
            ]);
        
        $generateCodeData = $request->all();
        
        VoucherCode::dispatch($generateCodeData);
        /*Artisan::call('queue:work');*/
        /*exec('nohup /usr/local/bin/php /home/m7l4siiy3hry/public_html/bgctycrm.optimaldevelopments.com/artisan queue:work > /dev/null &');*/
        exec('nohup /usr/local/bin/php artisan queue:work  &');
        return redirect('/admin/generate_codes')->withSuccess('Code Generated successfully!');
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        /*ini_set('memory_limit', '1024M');*/
        $generate_codes = GenerateCodes::find($id);
        
        $folder_name = public_path('/assets/file/voucher-code/');
        $csv = Reader::createFromPath($folder_name.'campaign_'.$generate_codes->gc_campaign_id.'_'.$generate_codes->created_at->format('d-i').'.csv', 'r');
        
        $csv->setHeaderOffset(0); //set the CSV header offset
        
        $page = $request->input('page') ? $request->input('page') : 1;

        $no_of_records_per_page = 10;
        //get 25 records starting from the 11th row
        $previous_page = $page - 1;
        $next_page = $page + 1;
        $adjacents = "2";
        $total_records = count($csv);
        $total_pages = ceil($total_records / $no_of_records_per_page);
        
        $statement = (new Statement())
                  ->offset(($page - 1) * $no_of_records_per_page)
                  ->limit($no_of_records_per_page);
          
        
        $headers = $csv->getHeader();
        $voucher_codes = $statement->process($csv);
        $fp = file($folder_name.'campaign_'.$generate_codes->gc_campaign_id.'_'.$generate_codes->created_at->format('d-i').'.csv', FILE_SKIP_EMPTY_LINES);


        $totalRows = count($fp) - 1;

        $totalPages = ceil( $totalRows / $no_of_records_per_page );
        $second_last = $totalPages - 1; // total pages minus 1
        
        return view('admin.generate_codes.voucher_code',compact('voucher_codes','totalPages',
            'page','previous_page','next_page','second_last','generate_codes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $generate_codes = GenerateCodes::find($id);
        $campaigns = Campaign::where('status','1')->get();
        $products = Product::where('status','1')->get();
        return view('admin.generate_codes.edit',compact('generate_codes','campaigns','products'));
    }
    public function allVoucherCode($id)
    {
       
        $generate_codes = GenerateCodes::find($id);
        
        $voucher_code = VoucherCodes::where('generate_code_id',$generate_codes->id)->get();
        
        return view('admin.generate_codes.voucher_code',compact('voucher_code','generate_codes'));
    }
    public function update(Request $request,$id)
    {
        $validatedData = $this->validate($request, [
            
            'whole_batch'                           => 'required|not_in:0',
            'gc_last_date_to_register_date'         => 'required|date',
            'gc_offer_valid_till_date'              => 'required|date|after_or_equal:gc_last_date_to_register_date'
            ]);
            
            if($request->whole_batch == 'NO'){
                $code = VoucherCodes::select('voucher_code')->where('voucher_code',$request->voucher_code)->first();
                //Validate code in voucher table
                if(!$code['voucher_code']){
                    
                    return redirect()->back()->withErrors('This code is not valid.Please enter valid code !!!')->withInput();
                }
                DB::table('tbl_voucher_codes')->where('voucher_code', $code->voucher_code)->update(['gc_last_date_to_register_date' => $request->gc_last_date_to_register_date,
                      'gc_offer_valid_till_date' => $request->gc_offer_valid_till_date]);
                      
            }else{
                
                $generate_code = GenerateCodes::find($id);
                $generate_code->gc_last_date_to_register_date = $request['gc_last_date_to_register_date'];
                $generate_code->gc_offer_valid_till_date = $request['gc_offer_valid_till_date'];
                $generate_code->whole_batch = $request['whole_batch'];
                $generate_code->touch();
                DB::table('tbl_voucher_codes')->where('generate_code_id', $generate_code->id)->update(['gc_last_date_to_register_date' => $generate_code->gc_last_date_to_register_date,
                'gc_offer_valid_till_date' => $generate_code->gc_offer_valid_till_date]);
            }
            
            
         
            return redirect('/admin/generate_codes')->withSuccess('Genearte code updated successfully!');
        
    
    }
    public function csvDownload($id)
    {
       
        $generate_codes = GenerateCodes::find($id);
        $folder_name = public_path('/assets/file/voucher-code/');
        $csv = Reader::createFromPath($folder_name.'campaign_'.$generate_codes->gc_campaign_id.'_'.$generate_codes->created_at->format('d-i').'.csv', 'r');
        $csv->output('campaign_'.$generate_codes->gc_campaign_id.'_'.$generate_codes->created_at->format('d-i').'.csv');
    }

    
    private function createCsv($modelCollection,$request,$created_at,$code_prefix){

        
        $folder_name = public_path('/assets/file/voucher-code/');
        $csv = Writer::createFromPath($folder_name.'campaign_'.$request['campaign_id'].'_'.$created_at.'.csv', 'w');
        
        // This creates header columns in the CSV file - probably not needed in some cases.
        $csv->insertOne(['SN','Campaign Id','Campaign Name','Product Id','Product Name','voucher Code']);
        
        if($csv->insertAll($modelCollection)){
            
            $voucher_code = new GenerateCodes;
        
            $voucher_code->voucher_code_file = 'campaign_'.$request['campaign_id'].'_'.$created_at.'.csv';
            $voucher_code->gc_campaign_id = $request['campaign_id'];
            $voucher_code->gc_codes_type = $request['gc_codes_type'];
            $voucher_code->gc_product_id = $request['product_id'];
            $voucher_code->length_of_code_required = $request['length_of_code_required'];
            $voucher_code->gc_voucher_code_batch_name = $request['gc_voucher_code_batch_name'];
            $voucher_code->gc_no_of_codes_required = $request['gc_no_of_codes_required'];
            $voucher_code->gc_last_date_to_register_date = $request['gc_last_date_to_register_date'];
            $voucher_code->gc_offer_valid_till_date = $request['gc_offer_valid_till_date'];
            $voucher_code->code_prefix = $code_prefix;
            
            
            $voucher_code->save();
            
        
        }
        
    }
    
    /*
    * Creating function to recieve Ajax request for product listing on behalf of campaign ID
    */
    public function get_genProductList(Request $request){
        
        $productExists = CampaignDetails::select('product_id','campaign_id')->where('campaign_id',$request->campaign_id)->get();
        
        return response()->json($productExists);
    }

    public function get_genProductName(Request $request){
         
        $product = Product::whereIn('id',explode(',',$request->id))->get(['name','id']);
        
        return response()->json($product);
    }
    
    public function generateCoupons($count, $length,$prefix)
    {
        $coupons = [];
        while(count($coupons) < $count) {
            do {
                $coupon = $prefix.substr(str_shuffle("0123456789"), 0, $length);
            } while (in_array($coupon, $coupons));
            $coupons[] = $coupon;
        }

        $existing = VoucherCodes::whereIn('voucher_code', $coupons)->count();
        if ($existing > 0)
            $coupons += $this->generateCoupons($existing, $length);

        return (count($coupons) == 1) ? $coupons[0] : $coupons;
    }
            
    public function voucherCodeDownload($id)
    {
        
        /*$category_data = VoucherCodes::paginate(100);
    
        $category_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($category_data as $index=>$cat)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Voucher Code ID'           => $cat['id'],
                   'Voucher Code'           => $cat['voucher_code'],
                   
                );
            }*/
        
        return Excel::download(new AllVoucherCodeExport($id), 'voucher_'.$id.'.xlsx');
    }


    
}
