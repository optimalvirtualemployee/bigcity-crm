<?php

namespace App\Http\Controllers\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\VoucherType;
use App\Exports\VoucherTypeExport;
use Excel;

class VoucherTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $vouchers = VoucherType::get();

        return view('admin.vouchertype.index')->with([
        'vouchers'  => $vouchers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vouchertype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required',
            
      ]);

      VoucherType::create($validatedData);

      return redirect('admin/vouchertype')->withSuccess('You have successfully created a Voucher Type!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vouchertype = VoucherType::find($id);
        return view('admin.vouchertype.show',compact('vouchertype'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vouchertype = VoucherType::find($id);
        return view('admin.vouchertype.edit', compact('vouchertype'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            
        ]);        
        $vouchertype = VoucherType::find($id);
        $vouchertype->name =  $request->get('name');
        $vouchertype->save();        
        return redirect('/admin/vouchertype')->with('success', 'Voucher Type updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function dashboard(Request $request)
    {
  
        return view('admin.vouchertype.dashboard');
    }
    
    public function voucherTypeDownload(Request $request)
    {
        
        
        $voucher_type_data = VoucherType::with('updated_user','created_user')->get()->toArray();
        
        $voucher_type_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($voucher_type_data as $index=>$type)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Voucher Type ID'           => $type['id'],
                   'Voucher Type Name'         => $type['name'],
                   
                   'Created By'            => isset($type['created_user']['name']) ? $type['created_user']['name'] : 'NA',
                   'Updated By'            => isset($type['updated_user']['name']) ? $type['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($type['created_at'])).'&'.date("H:i", strtotime($type['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($type['updated_at'])).'&'.date("H:i", strtotime($type['updated_at'])),
                   'Status'                => $type['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new VoucherTypeExport($data), 'AllVoucherType.xlsx');
    }

    
}
