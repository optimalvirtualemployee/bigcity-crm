<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\OperationInbound;

class OperationInboundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        
        $categories = OperationInbound::get();

        return view('admin.category.index')->with([
        'categories'  => $categories
        ]);
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.operation_inbound.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required|min:3|regex:/^[a-zA-Z\s]*$/|unique:tbl_categories',
            
      ]);

      Category::create($validatedData);

      return redirect('admin/categories')->withSuccess('User has been created successfully!');
    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function show($id)
    {
        $categories = Category::find($id);
        return view('admin.category.show',compact('categories'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        $categories = Category::find($id);
        return view('admin.category.edit', compact('categories'));       
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|regex:/^[a-zA-Z\s]*$/',
            
        ]);        
        $category = Category::find($id);
        $category->name =  $request->get('name');
        $category->save();        
        return redirect('/admin/categories')->with('success', 'Category updated!');
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();        
        return redirect('/admin/categories')->with('success', 'Category deleted!');
    }
    
    
}
