<?php
namespace App\Http\Controllers\QC;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\VenueProduct;
use App\VenueBillingContact;
use App\VenueBookingContact;
use App\User;
use App\Venue;
use App\VenueListType;
use App\VenueQC;
use DB;
use Session;
use Crypt;
use Form;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    //Get Admin Dashboard  Data

    public function dashboard(){
        
        
        $totalVenueQC = Venue::where('send_QC','YES')->get();
        $totalVenueRejected = VenueQC::where('status','Rejected')->get();
        $totalVenueConfirmed = VenueQC::where('status','Confirmed')->get();

        return view('QC.dashboard.dashboard',compact('totalVenueQC','totalVenueRejected','totalVenueConfirmed'));
    }

    //Get Branch Dashboard  Data

    public function getVenueList(){
        
        $venues = Venue::where('send_QC','YES')->get();
        
        return view('QC.venue.index',compact('venues'));

    }
    
    //Get Branch Dashboard  Data

    public function venueDetail($id)
    {
        $venues = Venue::find($id);
        $venue_product = VenueProduct::where('venue_id',$venues->id)->get();
        $venue_billing = VenueBillingContact::where('venue_id',$venues->id)->get();
        $venue_booking = VenueBookingContact::where('venue_id',$venues->id)->get();
        return view('QC.venue.show',compact('venues','venue_product','venue_billing','venue_booking'));
    }
    
    public function updateVenueDetail(Request $request, $id)
    {
        
        $request->validate([
            'comment'=>'required',
            'status'=> 'required|not_in:0',
            
        ]);        
        $venue_qc = new VenueQC;
        $venue_qc->venue_id =  $id;
        $venue_qc->comment  =  $request->get('comment');
        $venue_qc->status   =  $request->get('status');
        $venue_qc->save();        
        return redirect('/QC/venue-list')->with('success', 'Status updated!');
    }
    
    


     




}
