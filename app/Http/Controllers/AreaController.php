<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Imports\AreaImport;
use App\City;
use App\State;
use App\Area;
use App\AreaZone;
use App\Exports\AllAreaExport;
use Excel;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $areas = Area::get();

        return view('admin.area.index')->with([
        'areas'  => $areas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states= State::get();
        $area_zone= AreaZone::get();
        return view('admin.area.create',compact('states','area_zone'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'area_name'      => 'required',
            'zone_id'      => 'required|not_in:0',
            'pin_code'      => 'required',
            'city_id'      => 'required|not_in:0',
            'state_id'      => 'required|not_in:0',

            
      ]);

      Area::create($validatedData);

      return redirect('admin/areas')->withSuccess('You have successfully created a Area!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $areas = Area::find($id);
        return view('admin.area.show',compact('areas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $areas = Area::find($id);
        $states= State::get();
        $cities= City::get();
        $area_zone= AreaZone::get();
        return view('admin.area.edit', compact('areas','states','cities','area_zone'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'area_name'      => 'required',
            'zone_id'      => 'required',
            'pin_code'      => 'required',
            'city_id'      => 'required|not_in:0',
            'state_id'      => 'required|not_in:0',
            
        ]);        
        $area = Area::find($id);
        $area->area_name =  $request->get('area_name');
        $area->zone_id =  $request->get('zone_id');
        $area->pin_code =  $request->get('pin_code');
         $area->lat =  $request->get('lat');
          $area->lng =  $request->get('lng');
        $area->city_id =  $request->get('city_id');
        $area->state_id =  $request->get('state_id');

        $area->save();        
        return redirect('/admin/areas')->with('success', 'Area updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = Area::find($id);
        $area->delete();        
        return redirect('/admin/areas')->with('success', 'Area deleted!');
    }
    
    public function dashboard(Request $request)
    {
  
        return view('admin.area.dashboard');
    }
    
    public function getCityList(Request $request)
    {

        $cities= City::where("state_id",$request->state_id)
                       ->pluck("name","id");
        return response()->json($cities);
    }
    public function getAreaList(Request $request)
    {

        $areas = Area::where("city_id",$request->city_id)
                       ->pluck("area_name","id");
        return response()->json($areas);
    }
    

    //Upload mulitple area through excel file
    public function uploadArea(Request $request)
    {   
        $state_id = $request->state_id;
        $city_id = $request->city_id;
        try {
            Excel::import(new AreaImport($state_id,$city_id),$request->file);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            
            return back()->withError($e->getMessage())->withInput();
        }
        return redirect('/admin/areas')->withSuccess('You have successfully Uploaded Area!');
    }
    
    public function allAreaDownload(Request $request)
    {
        
        
        $area_data = Area::with('city','area_zone','state','updated_user','created_user')->get()->toArray();
        
        $area_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($area_data as $index=>$area)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Area ID'           => $area['id'],
                   'Area Name'         => $area['area_name'],
                   'Area Zone Id'         => $area['area_zone']['id'],
                   'Area Zone Name'         => $area['area_zone']['name'],
                   'state ID'           => $area['state']['id'],
                   'State Name'         => $area['state']['name'],
                   'City ID'           => $area['city']['id'],
                   'City Name'           => $area['city']['name'],
                   'Pin Code'           => $area['pin_code'],
                   'Latitide'           => $area['lat'],
                   'longitude'           => $area['lng'],
                   'Created By'            => isset($area['created_user']['name']) ? $area['created_user']['name'] : 'NA',
                   'Updated By'            => isset($area['updated_user']['name']) ? $area['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($area['created_at'])).'&'.date("H:i", strtotime($area['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($area['updated_at'])).'&'.date("H:i", strtotime($area['updated_at'])),
                   'Status'                => $area['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new AllAreaExport($data), 'Allarea.xlsx');
    }
    
}
