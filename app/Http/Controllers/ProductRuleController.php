<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductRule;
use App\Product;
use App\WeekList;
use Carbon\Carbon;

class ProductRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productrule = ProductRule::get();

        return view('admin.product_rule.index')->with([
        'productrule'  => $productrule
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $products = Product::where('status','1')->get();

        return view('admin.product_rule.create',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());die();
        $validator = $this->validate($request, [
            "product_id"           => 'required|not_in:0',
            
        ]);
        $prodRule = ProductRule::create($request->all());
        /*$prod_id    = $request->product_id;
        $week_list_type    = $request->week_list_id;
        $title  = $request->title;
        $from_time = $request->from_time;
        $to_time   = $request->to_time;*/
        
        /*foreach($week_list_type as $i => $rname){
          
          $prodRule = new ProductRule;
          
          $prodRule->product_id       = $prod_id;
          $prodRule->week_list_id   = $rname;
          $prodRule->title            = $title;
          $prodRule->from_time        = $from_time[$i];
          $prodRule->to_time          = $to_time[$i];
         
          $prodRule->save();
        }*/
      
      
      return redirect('/admin/productrules')->withSuccess('You have successfully created a Product Rule!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $productrule = ProductRule::find($id);
        /*$productrule = ProductRule::where('id',$id)->get()->toarray();*/
        $product = Product::where('id',$productrule->product_id)->get('name');
        return view('admin.product_rule.show',compact('productrule','product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productrule = ProductRule::find($id);
        /*$productrule = ProductRule::where('id',$id)->get()->toarray();*/
        
        $products = Product::get();
        
        return view('admin.product_rule.edit', compact('productrule','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
           // dd($request->all());die();

        $product_rule = ProductRule::find($id);
        $product_rule->title =  $request->get('title');
        $product_rule->mon_from_time = $request->mon_from_time;
        $product_rule->tue_from_time = $request->tue_from_time;
        $product_rule->wed_from_time = $request->wed_from_time;
        $product_rule->thus_from_time = $request->thus_from_time;
        $product_rule->fri_from_time = $request->fri_from_time;
        $product_rule->sat_from_time = $request->sat_from_time;
        $product_rule->sun_from_time = $request->sun_from_time;

        $product_rule->mon_to_time = $request->mon_to_time;
        $product_rule->tue_to_time = $request->tue_to_time;
        $product_rule->wed_to_time = $request->wed_to_time;
        $product_rule->thus_to_time = $request->thus_to_time;
        $product_rule->fri_to_time = $request->fri_to_time;
        $product_rule->sat_to_time = $request->sat_to_time;
        $product_rule->sun_to_time = $request->sun_to_time;
        $product_rule->touch();    
        return redirect('/admin/productrules')->with('success', 'Product Rule updated!');
        }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        ProductRule::where('product_id',$id)->delete();        
        return redirect('/admin/productrules')->with('success', 'Product Rule deleted!');
    }


    // public function changeProductRuleStatus(Request $request)
    // {
    //     $productrule = ProductRule::find($request->id);
    //     $productrule->status = $request->status;
    //     $productrule->save();
  
    //     return response()->json(['success'=>'Product Rule status change successfully.']);
    // }

    public function changingProduct(Request $request)
    {
        $productruleExists = ProductRule::where('product_id',$request->id)->get();
        if(count($productruleExists) > 0){
            return response()->json(['success'=>'Rule with this product is already exists, Go on update for any modifications']);  
        }else{
            return response()->json(['failure'=>'failed']);  
        }
        
    }
}
