<?php

namespace App\Http\Controllers\Pd;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Imports\VenueLocationImport;
use App\Category;
use App\Product;
use App\VenueProduct;
use App\VenueBillingContact;
use App\VenueBookingContact;
use App\User;
use App\Venue;
use App\VenueListType;
use App\VenueBlackOutDay;
use App\City;
use App\State;
use App\Area;
use Excel;
use App\VenueQC;

class QCRejectedVenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $venues = Venue::where('comment_status', 'Rejected')->get();

        return view('admin.rejected_venue.index')->with([
        'venues'  => $venues
        ]);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venues = Venue::find($id);
        $venue_product = VenueProduct::where('venue_id',$venues->id)->get();
        $venue_billing = VenueBillingContact::where('venue_id',$venues->id)->get();
        $venue_booking = VenueBookingContact::where('venue_id',$venues->id)->get();
        $venueList = VenueListType::where('venue_id',$venues->id)->get();
        $venue_black_out_day = VenueBlackOutDay::where('venue_id',$venues->id)->get();
        
        return view('admin.rejected_venue.show',compact('venues','venue_product','venue_billing','venue_booking','venueList','venue_black_out_day'));
    }

    /**
     * Show the form for editinvenue_qcg the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRejectedVenueDetail(Request $request, $id)
    {
        
        $request->validate([
            'comment'=>'required',
            
            
        ]);  
        $venue_check = Venue::where('id',$id)->first();
        
        if($venue_check){
            
            $venue_check->rejected_venue_comment  =  $request->get('comment');
            $venue_check->comment_status   =  'Request For Edit';
            $venue_check->save();
        }
                
        return redirect('/admin/qc-rejected-venue')->with('success', 'Comment updated!');
    }
    
}
