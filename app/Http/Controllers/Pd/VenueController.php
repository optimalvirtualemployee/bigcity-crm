<?php

namespace App\Http\Controllers\Pd;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Imports\VenueLocationImport;
use App\Category;
use App\Product;
use App\VenueProduct;
use App\VenueBillingContact;
use App\VenueBookingContact;
use App\User;
use App\Venue;
use App\VenueListType;
use App\VenueBlackOutDay;
use App\City;
use App\State;
use App\Area;
use Excel;
use App\VenueProductDayCost;
use DB;
use App\ListType;
use App\TargetAssign;

class VenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    function __construct()

    {

         /*$this->middleware('permission:venue-list|venue-create|venue-edit|venue-delete', ['only' => ['index','store']]);

         $this->middleware('permission:venue-create', ['only' => ['create','store']]);

         $this->middleware('permission:venue-edit', ['only' => ['edit','update']]);

         $this->middleware('permission:venue-delete', ['only' => ['destroy']]);*/

    }
    public function index()
    {
        abort_unless(\Gate::allows('venue-list'), 403);
        $venues = Venue::get();

        return view('admin.venue.index')->with([
        'venues'  => $venues
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('venue-create'), 403);
        $users = User::select('users.*')->where('users.status','1')->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '5')->get();

        
        $categories = Category::where('status','1')->get();
        $product = Product::where('status','1')->get();
        $cities = City::where('status','1')->get();
        $states = State::where('status','1')->get();
        $areas = Area::where('status','1')->get();
        $list_type = ListType::all()->pluck('name', 'id');
        
        return view('admin.venue.create',compact('list_type','users','categories','product','cities','states','areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if($request->venue_type== '2'){
          
           $venue = Venue::create($request->all());

           if($venue){

                //Product/experience tag 
                $description = $request->description;
        
                $mon_mrp = $request->mon_mrp;
                $tue_mrp = $request->tue_mrp;
                $wed_mrp = $request->wed_mrp;
                $thus_mrp = $request->thus_mrp;
                $fri_mrp = $request->fri_mrp;
                $sat_mrp = $request->sat_mrp;
                $sun_mrp = $request->sun_mrp;

                $mon_cost = $request->mon_cost;
                $tue_cost = $request->tue_cost;
                $wed_cost = $request->wed_cost;
                $thus_cost = $request->thus_cost;
                $fri_cost = $request->fri_cost;
                $sat_cost = $request->sat_cost;
                $sun_cost = $request->sun_cost;
                
        
                //Billing Contact
        
                $billing_email = $request->billing_email;
        
                $billing_phone = $request->billing_phone;
                $billing_designation = $request->billing_designation;
        
                //Booking Contact
                $booking_email = $request->booking_email;
                $booking_phone = $request->booking_phone;
                $booking_designation = $request->booking_designation;
        
                //save Product/experience tag multiple with venue id 
                foreach($request->product_id as $i => $value){
                    $venue_product = new VenueProduct;
                    $venue_product->venue_id = $venue->id;
                    $venue_product->product_id = $value;
                    $venue_product->description = $description[$i];
                    $venue_product->mon_mrp = $mon_mrp[$i];
                    $venue_product->tue_mrp = $tue_mrp[$i];
                    $venue_product->wed_mrp = $wed_mrp[$i];
                    $venue_product->thus_mrp = $thus_mrp[$i];
                    $venue_product->fri_mrp = $fri_mrp[$i];
                    $venue_product->sat_mrp = $sat_mrp[$i];
                    $venue_product->sun_mrp = $sun_mrp[$i];
                    $venue_product->mon_cost = $mon_cost[$i];
                    $venue_product->tue_cost = $tue_cost[$i];
                    $venue_product->wed_cost = $wed_cost[$i];
                    $venue_product->thus_cost = $thus_cost[$i];
                    $venue_product->fri_cost = $fri_cost[$i];
                    $venue_product->sat_cost = $sat_cost[$i];
                    $venue_product->sun_cost = $sun_cost[$i];
                    $venue_product->tue_cost = $tue_cost[$i];

                    $venueProduct = $venue_product->save();
                    
                }
        
                //save Billing Contact multiple with venue id 
                foreach($request->billing_name as $i => $value){
                    $venue_billing_contact = new VenueBillingContact;
                    $venue_billing_contact->venue_id = $venue->id;
                    $venue_billing_contact->name = $value;
                    $venue_billing_contact->email = $billing_email[$i];
                    $venue_billing_contact->phone = $billing_phone[$i];
                    $venue_billing_contact->designation = $billing_designation[$i];
                    $venue_billing_contact->save();
                }
                
                //save Billing Contact multiple with venue id 
                foreach($request->booking_name as $i => $value){
                    $venue_booking_contact = new VenueBookingContact;
                    $venue_booking_contact->venue_id = $venue->id;
                    $venue_booking_contact->name = $value;
                    $venue_booking_contact->email = $booking_email[$i];
                    $venue_booking_contact->phone = $booking_phone[$i];
                    $venue_booking_contact->designation = $booking_designation[$i];
                    $venue_booking_contact->save();
                }
                
                //save list  multiple with venue id 
                if(!empty($request->list_type)){
                    foreach($request->list_type as $i => $value){
                        $venue_list_type = new VenueListType;
                        $venue_list_type->venue_id = $venue->id;
                        $venue_list_type->list_type_id = $value;
                        
                        $venue_list_type->save();
                    }
                }
                //save Black Out Daya  multiple with venue id 
                
                //save Black Out Daya  multiple with venue id 
                if(!empty($request->black_out_dates)){
                    foreach($request->black_out_dates as $i => $value){
                        $venue_black_out_day = new VenueBlackOutDay;
                        $venue_black_out_day->venue_id = $venue->id;
                        $venue_black_out_day->date = $value;
                        
                        $venue_black_out_day->save();
                    }
                }
            }
            return redirect('/admin/venues')->withSuccess('You have successfully created a Venue!');
            
        }

ini_set('memory_limit', '-1');
        $errors = [];
        if(request()->file('venue_file')){
            
            
            $chain_name = $request->chain_name;
            $pd_lead_name = $request->pd_lead_id;
            $pd_lead_id = $request->pd_lead_id;
            $send_QC = $request->send_QC;
            $chain_name = $request->chain_name;
            
            $usersImport = new VenueLocationImport($pd_lead_id,$chain_name);
            $usersImport->import(request()->file('venue_file'));

            // I made function getErrors on UsersImport class:
            $usersValidationErrors = $usersImport->getErrors();
            if($usersValidationErrors){
                
                return redirect('admin/venues/create')->withErrors($usersValidationErrors)->withInput();
            }

            return redirect('/admin/venues')->withSuccess('You have successfully created a Venue!');
            

        } 

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $venues = Venue::find($id);
        $venue_product = VenueProduct::where('venue_id',$venues->id)->get();
        $venue_billing = VenueBillingContact::where('venue_id',$venues->id)->get();
        $venue_booking = VenueBookingContact::where('venue_id',$venues->id)->get();
        $venueList = VenueListType::where('venue_id',$venues->id)->get();
        $venue_black_out_day = VenueBlackOutDay::where('venue_id',$venues->id)->get();
        
        return view('admin.venue.show',compact('venues','venue_product','venue_billing','venue_booking','venueList','venue_black_out_day'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_unless(\Gate::allows('venue-edit'), 403);
        
        $venues = Venue::find($id);
        $billingContact = VenueBillingContact::where('venue_id',$id)->get()->toarray();
        $blackOutDates = VenueBlackOutDay::where('venue_id',$id)->get()->toarray();
        $bookingContact = VenueBookingContact::where('venue_id',$id)->get()->toarray();
        $categories = Category::where('status','1')->get();
        $list_type = ListType::all()->pluck('name', 'id');
        $venues->load('list_type');
        $products = Product::where('status','1')->where('category_id',$venues->category_id)->pluck('name', 'id');
        $cities = City::where('status','1')->get();
        $users = User::select('users.*')->where('users.status','1')->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '5')->get();
        $states = State::where('status','1')->get();
        $areas = Area::where('status','1')->get();
        return view('admin.venue.edit', compact('blackOutDates','venues','users','states','areas','products','categories','cities','billingContact','bookingContact','list_type'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $deleteVenueProductRecords = VenueProduct::where('venue_id',$id)->delete();
        $deleteVenueBillingRecords = VenueBillingContact::where('venue_id',$id)->delete(); 
        $deleteVenueBookingRecords = VenueBookingContact::where('venue_id',$id)->delete();
        $deleteVenueListRecords    = VenueListType::where('venue_id',$id)->delete();
        $deleteVenueBlackRecords    = VenueBlackOutDay::where('venue_id',$id)->delete();
     
        $venue = Venue::find($id);
        $venue->venue_type                 =  $request->get('venue_type');
        $venue->state_id                   =  $request->get('state_id');
        $venue->area_id                    =  $request->get('area_id');
        $venue->chain_name                 =  $request->get('chain_name');
        $venue->pd_lead_id                 =  $request->get('pd_lead_id');
        $venue->category_id                =  $request->get('category_id');
        $venue->city_id                    =  $request->get('city_id');
        $venue->venue_name                 =  $request->get('venue_name');
        $venue->address                    =  $request->get('address');
        $venue->latitude                   =  $request->get('latitude');
        $venue->longitude                  =  $request->get('longitude');
        $venue->rating                     =  $request->get('rating');
        
        $venue->billing_terms              =  $request->get('billing_terms');
        $venue->booking_alert_required     =  $request->get('booking_alert_required');
        $venue->bank_name                  =  $request->get('bank_name');
        $venue->account_number                    =  $request->get('account_number');
        $venue->ifsc_code                  =  $request->get('ifsc_code');
        $venue->name_on_account            =  $request->get('name_on_account');
        $venue->other_value                =  $request->get('other_value');
        $venue->comment_status             =  'Send For QC';
        $updatedVenue = $venue->touch();
        if($updatedVenue){

                //Product/experience tag 
                $description = $request->description;
               
                $mon_mrp  = $request->mon_mrp;
                $tue_mrp  = $request->tue_mrp;
                $wed_mrp  = $request->wed_mrp;
                $thus_mrp = $request->thus_mrp;
                $fri_mrp  = $request->fri_mrp;
                $sat_mrp  = $request->sat_mrp;
                $sun_mrp  = $request->sun_mrp;
 
                $mon_cost  = $request->mon_cost;
                $tue_cost  = $request->tue_cost;
                $wed_cost  = $request->wed_cost;
                $thus_cost = $request->thus_cost;
                $fri_cost  = $request->fri_cost;
                $sat_cost  = $request->sat_cost;
                $sun_cost  = $request->sun_cost;
                
                //Billing Contact
                $billing_email       = $request->billing_email;
                $billing_phone       = $request->billing_phone;
                $billing_designation = $request->billing_designation;
        
                //Booking Contact
                $booking_email       = $request->booking_email;
                $booking_phone       = $request->booking_phone;
                $booking_designation = $request->booking_designation;
        
                //save Product/experience tag multiple with venue id 
                foreach($request->product_id as $i => $value){
                    $venue_product              = new VenueProduct;
                    $venue_product->venue_id    = $venue->id;
                    $venue_product->product_id  = $value;
                    $venue_product->description = $description[$i];
                    $venue_product->mon_mrp     = $mon_mrp[$i];
                    $venue_product->tue_mrp     = $tue_mrp[$i];
                    $venue_product->wed_mrp     = $wed_mrp[$i];
                    $venue_product->thus_mrp    = $thus_mrp[$i];
                    $venue_product->fri_mrp     = $fri_mrp[$i];
                    $venue_product->sat_mrp     = $sat_mrp[$i];
                    $venue_product->sun_mrp     = $sun_mrp[$i];
                    $venue_product->mon_cost    = $mon_cost[$i];
                    $venue_product->tue_cost    = $tue_cost[$i];
                    $venue_product->wed_cost    = $wed_cost[$i];
                    $venue_product->thus_cost   = $thus_cost[$i];
                    $venue_product->fri_cost    = $fri_cost[$i];
                    $venue_product->sat_cost    = $sat_cost[$i];
                    $venue_product->sun_cost    = $sun_cost[$i];
                    $venue_product->tue_cost    = $tue_cost[$i];
                    $venueProduct               = $venue_product->save();
                    
                }
        
                //save Billing Contact multiple with venue id 
                foreach($request->billing_name as $i => $value){
                    $venue_billing_contact = new VenueBillingContact;
                    $venue_billing_contact->venue_id = $venue->id;
                    $venue_billing_contact->name = $value;
                    $venue_billing_contact->email = $billing_email[$i];
                    $venue_billing_contact->phone = $billing_phone[$i];
                    $venue_billing_contact->designation = $billing_designation[$i];
                    $venue_billing_contact->save();
                }
                
                //save Billing Contact multiple with venue id 
                foreach($request->booking_name as $i => $value){
                    $venue_booking_contact = new VenueBookingContact;
                    $venue_booking_contact->venue_id = $venue->id;
                    $venue_booking_contact->name = $value;
                    $venue_booking_contact->email = $booking_email[$i];
                    $venue_booking_contact->phone = $booking_phone[$i];
                    $venue_booking_contact->designation = $booking_designation[$i];
                    $venue_booking_contact->save();
                }
                
                //save list  multiple with venue id 
                if(!empty($request->list_type)){
                    foreach($request->list_type as $i => $value){
                        $venue_list_type = new VenueListType;
                        $venue_list_type->venue_id = $venue->id;
                        $venue_list_type->list_type_id = $value;
                        
                        $venue_list_type->save();
                    }
                }
                //save Black Out Daya  multiple with venue id 
                
                //save Black Out Daya  multiple with venue id 
                if(!empty($request->black_out_dates)){
                    foreach($request->black_out_dates as $i => $value){
                        $venue_black_out_day = new VenueBlackOutDay;
                        $venue_black_out_day->venue_id = $venue->id;
                        $venue_black_out_day->date = $value;
                        
                        $venue_black_out_day->save();
                    }
                }
            }
            
        return redirect('/admin/venues')->with('success', 'Venue updated!');
    }

    
    

    //venue sub dashboard
    public function dashboard(Request $request)
    {
        $totalVenue = Venue::get();
        $totalVenueQC = Venue::where('comment_status','Send For QC')->orWhereNull('comment_status')->get();
        $totalRejected = Venue::where('comment_status', 'Rejected')->get();
        $totalRequestEditVenue = Venue::Where('comment_status','Request For Edit')->get();
        $totalConfirmed = Venue::where('comment_status','Confirmed')->get();
        $totalTargetAssign = TargetAssign::get();

        return view('admin.venue.dashboard',compact('totalVenue','totalVenueQC','totalRejected','totalConfirmed','totalRequestEditVenue','totalTargetAssign'));
    }
    
    //get product list according to category id 
    public function getProductList(Request $request)
    {

        $products= Product::where("category_id",$request->category_id)->where("status",'1')->pluck("name","id");
        return response()->json($products);
    }
}
