<?php

namespace App\Http\Controllers\Pd;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Imports\VenueLocationImport;
use App\Category;
use App\Product;
use App\VenueProduct;
use App\VenueBillingContact;
use App\VenueBookingContact;
use App\User;
use App\Venue;
use App\VenueListType;
use App\VenueBlackOutDay;
use App\City;
use App\State;
use App\Area;
use Excel;
use App\VenueQC;

class VenueQcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $venues = Venue::where('comment_status','Send For QC')->orWhereNull('comment_status')->get();

        return view('admin.venue_qc.index')->with([
        'venues'  => $venues
        ]);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venues = Venue::find($id);
        $venue_product = VenueProduct::where('venue_id',$venues->id)->get();
        $venue_billing = VenueBillingContact::where('venue_id',$venues->id)->get();
        $venue_booking = VenueBookingContact::where('venue_id',$venues->id)->get();
        $venueList = VenueListType::where('venue_id',$venues->id)->get();
        $venue_black_out_day = VenueBlackOutDay::where('venue_id',$venues->id)->get();
        /*$venue_qc = VenueQC::where('venue_id',$venues->id)->first();*/
        return view('admin.venue_qc.show',compact('venues','venue_product','venue_billing','venue_booking','venueList','venue_black_out_day'));
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function updateVenueDetail(Request $request, $id)
    {
        
        $request->validate([
            
            'status'=> 'required|not_in:0',
            
        ]);  
        $venue_qc_check = Venue::where('id',$id)->first();
        
        if($venue_qc_check){
            
            $venue_qc_check->qc_venue_comment  =  $request->get('comment');
            $venue_qc_check->comment_status   =  $request->get('status');
            $venue_qc_check->save();
        }
                
        return redirect('/admin/venue-qc')->with('success', 'Status updated!');
    }

}
