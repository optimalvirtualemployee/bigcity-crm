<?php

namespace App\Http\Controllers\Pd;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\TargetAssign;
use App\Product;
use App\User;

class TargetAssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('target-assign-list'), 403);
        $target_assign = TargetAssign::get();

        return view('admin.target_assign.index')->with([
        'target_assign'  => $target_assign
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('target-assign-list'), 403);
        $products = Product::where('status','1')->get();
        $users = User::select('users.*')->where('users.status','1')->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->where('model_has_roles.role_id', '=', '5')->get();
        return view('admin.target_assign.create',compact('users','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            
            'product_id' => 'required|not_in:0',
            'pd_lead_id' => 'required|not_in:0',
            'comment' => 'required',
            'target_mrp' => 'required',
            'target_bcp_price' => 'required',
            'target_no' => 'required',
            'target_closure_date' => 'required',
            
      ]);

      TargetAssign::create($validatedData);

      return redirect('admin/target-assign')->withSuccess('Target  has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function show($id)
    {
        $categories = Category::find($id);
        return view('admin.category.show',compact('categories'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        $categories = Category::find($id);
        return view('admin.category.edit', compact('categories'));       
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|regex:/^[a-zA-Z\s]*$/',
            
        ]);        
        $category = Category::find($id);
        $category->name =  $request->get('name');
        $category->save();        
        return redirect('/admin/categories')->with('success', 'Category updated!');
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();        
        return redirect('/admin/categories')->with('success', 'Category deleted!');
    }*/

    //Export Excel

    
    
    
}
