<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Booking;
use App\BookingData;
use App\Campaign;
use App\User;
use App\BookingAssignAgent;
use App\Product;
use DB;
use Carbon\Carbon;

class BookingChampionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//-------------------------------------------------------------------------------------------------------//   
    public function index(Request $request)
    {
        $campaign  = Campaign::where('status','1')->get();
        $product   = Product::where('status','1')->get();
        
        $agentUser = User::select('users.*')
                           ->where('users.status','1')
                           ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                           ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                           ->where('model_type', '=', 'App\\User')
                           ->where('model_has_roles.role_id', '=', '8')
                           ->get();
       
        $today_total_booking = BookingData::whereDate('created_on', Carbon::today())
                                            ->whereHas('booking_assign_agent', 
                                                function($q){
                                                    $q->where('user_id', Auth::user()->id);
                                                })
                                            ->where('assign_to_agent', 'YES')
                                            ->count();
        
        $yesterday_total_booking = BookingData::whereDate('created_on', Carbon::yesterday())
                                                ->whereHas('booking_assign_agent', 
                                                    function($q){
                                                        $q->where('user_id', Auth::user()->id);
                                                    })
                                                ->where('assign_to_agent', 'YES')
                                                ->count();
        
        $week_total_booking = BookingData::where('created_on', '>', Carbon::now()->startOfWeek())
                                           ->where('created_on', '<', Carbon::now()->endOfWeek())
                                           ->whereHas('booking_assign_agent', 
                                                function($q){
                                                    $q->where('user_id', Auth::user()->id);
                                                })
                                           ->where('assign_to_agent', 'YES')
                                           ->count();
            
        $month_total_booking = BookingData::whereMonth('created_on', Carbon::now()->month())->count();

        return  view('admin.booking_champion.index',
                    compact(
                            'campaign',
                            'product',
                            'agentUser',
                            'today_total_booking',
                            'yesterday_total_booking',
                            'week_total_booking',
                            'month_total_booking'
                    )
                );
    }

//-------------------------------------------------------------------------------------------------------//

    function fetch_data(Request $request)
    {
        
        if($request->ajax()){
            
            if($request->from_date != '' && $request->to_date != ''){
                
                $data = BookingData::with('campaign','products')->where('assign_to_agent', 'YES')->whereBetween('from_date', array($request->from_date, $request->to_date))->get();
                
            }elseif($request->campaign_id != ''){
                
                $data = BookingData::with('campaign','products')->where('assign_to_agent', 'YES')->where('campaign_id', $request->campaign_id)->get();
                
            }elseif($request->product_id != ''){
                
                $data = BookingData::with('campaign','products')->get();
            }

            else{
                
              
                $data = BookingData::with('campaign','products','venues','cities')->whereHas('booking_assign_agent', function($q){
                        $q->where('user_id', Auth::user()->id);
                        })->where('assign_to_agent', 'YES')->get();
            }
            echo json_encode($data);
        }
    }

//-------------------------------------------------------------------------------------------------------//
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validatedData = $this->validate($request, [
            'assign_type_id'      => 'required|not_in:0',
            'user_id'             => 'required|not_in:0',
            
      ]);
      
      DB::table('tbl_booking')->whereIn('id', $request->booking_assign)->update(array('assign_to_agent' => 'YES'));
      foreach($request->booking_assign as $i=>$assign){
          $booking =new  BookingAssign;
          $booking->booking_id = $assign;
          $booking->assign_type_id = $request->assign_type_id;
          $booking->user_id  = $request->user_id;
          $booking->save();
      }
      

      return redirect('admin/booking-assign')->withSuccess('Booking assign to agent successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function show($id)
    {
        $categories = Category::find($id);
        return view('admin.category.show',compact('categories'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        $categories = Category::find($id);
        return view('admin.category.edit', compact('categories'));       
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|regex:/^[a-zA-Z\s]*$/',
            
        ]);        
        $category = Category::find($id);
        $category->name =  $request->get('name');
        $category->save();        
        return redirect('/admin/categories')->with('success', 'Category updated!');
    }*/

//-------------------------------------------------------------------------------------------------------//
    
    public function dashboard()
    {
        $totalConfirmedBooking = BookingAssignAgent::where('user_id',Auth::user()->id)
                                                     ->whereHas('booking', 
                                                        function($query){
                                                            $query->where('status', '1');
                                                        })
                                                     ->get();
                                
        $totalPendingBooking   = BookingAssignAgent::where('user_id',Auth::user()->id)
                                                     ->whereHas('booking', 
                                                        function($query){
                                                            $query->where('status', '2');
                                                        })
                                                     ->get();
        $totalBooking          = BookingAssignAgent::where('user_id',Auth::user()->id)->get();
         
        return view('admin.booking_champion.dashboard',compact('totalConfirmedBooking','totalPendingBooking','totalBooking'));
    }
//-------------------------------------------------------------------------------------------------------// 
    public function validateBooking(Request $request){
        $bookingID = $request->post('booking_id');
        $getData = BookingAssignAgent::select('booking_id')->where('booking_id',$bookingID)->get();

        echo json_encode($getData);
    }   

}
