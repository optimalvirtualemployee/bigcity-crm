<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\BookingData;
use App\Campaign;
use App\Product;
use DB;
use App\User;
use App\AssignType;
use App\BookingAssign;
use Carbon\Carbon;


class BookingAssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         
        $campaign  = Campaign::where('status','1')->get();
        $product   = Product::where('status','1')->get();
        $agentUser = User::select('users.*')
                                            ->where('users.status','1')
                                            ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                                            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                                            ->where('model_type', '=', 'App\\User')
                                            ->where('model_has_roles.role_id', '=', '8')
                                            ->get();
       
        $assignType              = AssignType::where('status','1')->get();
        $today_total_booking     = BookingData::whereDate('created_on', Carbon::today())->count();
        $yesterday_total_booking = BookingData::whereDate('created_on', Carbon::yesterday())->count();
        $week_total_booking      = BookingData::where('created_on', '>', Carbon::now()->startOfWeek())
                                                ->where('created_on', '<', Carbon::now()->endOfWeek())
                                                ->count();
        $month_total_booking     = BookingData::whereMonth('created_on', Carbon::now()->month())->count();
        return view('admin.booking_assign.index',compact('campaign','product','agentUser','assignType','today_total_booking','yesterday_total_booking','week_total_booking','month_total_booking'));
    }

    function fetch_data(Request $request)
    {
        
        if($request->ajax()){
            
            if($request->from_date != '' && $request->to_date != ''){
                
                $data = BookingData::with('campaign','products')->where('assign_to_agent', 'NO')->whereBetween('from_date', array($request->from_date, $request->to_date))->get();
                
            }elseif($request->campaign_id != ''){
                
                $data = BookingData::with('campaign','products')->where('assign_to_agent', 'NO')->where('campaign_id', $request->campaign_id)->get();
                
            }elseif($request->product_id != ''){
                
                $data = BookingData::with('campaign','products')->get();
            }

            else{
              
                $data = BookingData::with('campaign','products')->where('assign_to_agent', 'NO')->get();
            }
            echo json_encode($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        return view('admin.booking_assign.create');
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validatedData = $this->validate($request, [
            'assign_type_id'      => 'required|not_in:0',
            'user_id'             => 'required|not_in:0',
            
        ]);
      
        DB::table('tbl_booking_data')->whereIn('id', $request->booking_assign)->update(array('assign_to_agent' => 'YES'));

        if(count($request->user_id) == count($request->booking_assign)){
            foreach($request->booking_assign as $i=>$assign){

              $booking =new  BookingAssign;
              $booking->booking_id = $assign;
              $booking->assign_type_id = $request->assign_type_id;
              $booking->user_id  = $request->user_id[$i];
              $booking->save();
              $insertedId[] = $booking->id;   

            }
        }else if(count($request->user_id) > count($request->booking_assign)){
            $j = 0;
            foreach($request->booking_assign as $i=>$assign){
                if($j >= count($request->user_id)){
                    $j = 0;
                }
              $booking =new  BookingAssign;
              $booking->booking_id = $assign;
              $booking->assign_type_id = $request->assign_type_id;
              $booking->user_id  = (isset($request->user_id[$i]) ? $request->user_id[$i] : $request->user_id[$j]);
              $booking->save();
              $insertedId[] = $booking->id;   
              $j++;
            }
        }else if(count($request->user_id) < count($request->booking_assign)){
             $j = 0; 
            foreach($request->booking_assign as $i=>$assign){
                if($j >= count($request->user_id)){
                    $j = 0;
                }
                
              $booking =new  BookingAssign;
              $booking->booking_id = $assign;
              $booking->assign_type_id = $request->assign_type_id;
              $booking->user_id  = (isset($request->user_id[$i]) ? $request->user_id[$i] : $request->user_id[$j]);
              $booking->save();
              $insertedId[] = $booking->id;   
              $j++;
            }
            
        }else{
            foreach($request->booking_assign as $i=>$assign){
              
              $booking =new  BookingAssign;
              $booking->booking_id = $assign;
              $booking->assign_type_id = $request->assign_type_id;
              $booking->user_id  = $request->user_id[$i];
              $booking->save();
              $insertedId[] = $booking->id;   
            }
        }

        //------------------------------------------------------------------------------------------//
            $email_data = DB::table('tbl_booking_assign_agent as baa')
                            ->join('users as u', 'u.id', '=', 'baa.user_id')
                            ->select('baa.user_id','baa.booking_id','baa.created_at', 'u.email')
                            ->whereIn('baa.id',$insertedId)
                            ->get();
            foreach($email_data as $key => $value){
                $userId[] = $value->user_id;
                $bookingId[] = $value->booking_id;
                $getDate = explode(' ', $value->created_at);
                $createdAt[] = $getDate[0];
                $email[] = $value->email;
            }                            
            // echo "<pre>";print_r(count($bookingId));die();
        //------------------------------------------------------------------------------------------//
        
        return redirect('admin/booking-assign')->withSuccess('Booking assign to agent successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function show($id)
    {
        $categories = Category::find($id);
        return view('admin.category.show',compact('categories'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        $categories = Category::find($id);
        return view('admin.category.edit', compact('categories'));       
    }*/ 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|regex:/^[a-zA-Z\s]*$/',
            
        ]);        
        $category = Category::find($id);
        $category->name =  $request->get('name');
        $category->save();        
        return redirect('/admin/categories')->with('success', 'Category updated!');
    }*/

    
    public function dashboard()
    {
        $totalConfirmedBooking = BookingData::where('status','1')->get();
        $totalPendingBooking   = BookingData::where('status','2')->get();
        $totalBooking          = BookingData::get();
        $campaignWiseBooking   = BookingData::select('campaign_id', DB::raw('COUNT(campaign_id) AS campaign_count'))->with('campaign')
                                    ->groupBy('campaign_id')
                                    ->get();
        /*$category = Category::find($id);
        $category->delete();    */    
        return view('admin.booking_assign.dashboard',compact('totalConfirmedBooking','totalPendingBooking','totalBooking','campaignWiseBooking'));
    }

}
