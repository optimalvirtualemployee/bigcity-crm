@extends('common.default')
@section('title', 'Company List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 20px;">
       
          <div class="d-sm-flex align-items-center justify-content-between pb-5">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Communication Template</h1>
             <a href="{{url('admin/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back To Dashboard</a>
          </div>
          
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card shadow ">
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  <li style="margin-right: 20px;"><a href="{{url('admin/communication-template/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Communication Template
                     </button></a>
                  </li>
                 
                 
               </ul>
            </nav>
         </div>
      
         <div class="card-body" >
                @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                         <th>SN</th>
                        <th>Communication Template Id</th>
                        <th>Subject</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                        
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                     @forelse($comm_temp as $index=> $company)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$company->id}}</td>
                        <td>{{$company->subject}}</td>
                        
                        <td>{{isset($company->created_user['name']) ? $company->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($company->updated_user['name']) ? $company->updated_user['name'] : 'NA'}}</td>
                        <td>{{$company->created_at->format('d M Y')}}/{{$company->created_at->format('g:i A')}}</td>
                        <td>{{$company->updated_at->format('d M Y')}}/{{$company->updated_at->format('g:i A')}}</td>
                        <td>
                           <a href="{{ route('communication-template.show',$company->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('communication-template.edit',$company->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        </td>
                        <td>
                            
                              <input data-id="{{$company->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $company->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="CommunicationTemplate">  
                        </td>
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>

@stop

