@extends('common.default')
@section('title', 'Category Create')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
         <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">add Communication Template</h1>
             <a href="{{url('admin/communication-template')}}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card login-card" style="width: 100%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <form class="user" action="{{ route('communication-template.store') }}" method="POST">
                           @csrf

                           <div class="form-group">
                              <label for="name" class="mr-sm-2">Subject:</label>
                              <input type="text" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Subject Name" name="subject" value="{{ old('subject') }}">
                           </div>
                           <div class="form-group">
                              <label for="name" class="mr-sm-2">Template Content:</label>
                              <textarea class="ckeditor form-control" name="template-content"></textarea>
                           </div>
                           <div class="form-group">
                              <label for="name" class="mr-sm-2">SMS:</label>
                              <textarea  class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter SMS" name="sms" value="{{ old('sms') }}"></textarea>
                           </div>
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary mt-4">Submit</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<!-- End of Main Content -->
@stop