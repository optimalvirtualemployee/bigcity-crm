@extends('common.default')
@section('title', 'Show Category')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
         <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Category</h1>
             <a href="{{url('admin/categories')}}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
  
             <table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>ID</th>
                        <th>Name</th>
                        <th>Created By</th>
                        <th>Created Date/Time</th>
                        <th>Status</th>
                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    <tr>
                        <td>{{$categories['id']}} </td>
                        <td>{{$categories['name']}} </td>
                        <td>{{Auth::user()->name}}</td>
                        <td>{{$categories->created_at->format('d M Y')}}/{{$categories->created_at->format('g:i A')}}</td>
                        <td>{{$categories->status ? 'Active':'Inactive'}}</td>
                        
                   
                   </tr>
                  </tbody>
             </table>
    
   </div>
</div>
@stop