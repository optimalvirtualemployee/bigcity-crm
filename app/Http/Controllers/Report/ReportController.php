<?php

namespace App\Http\Controllers\Report;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\BookingData;
use App\Exports\AllAreaExport;
use Excel;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function totalBookingReport()
    {
        
        $BookingData = BookingData::get();

        return view('admin.report.total_booking_report')->with([
        'booking_data'  => $BookingData
        ]);
    }

    public function reBookingReport()
    {
        
        $BookingData = BookingData::get();

        return view('admin.report.re_booking_report')->with([
        'booking_data'  => $BookingData
        ]);
    }

    public function cancelBookingReport()
    {
        
        $BookingData = BookingData::where('status','3')->get();

        return view('admin.report.cancel_booking_report')->with([
        'booking_data'  => $BookingData
        ]);
    }

    public function confirmBookingReport()
    {
        
        $BookingData = BookingData::where('status','1')->get();

        return view('admin.report.confirm_booking_report')->with([
        'booking_data'  => $BookingData
        ]);
    }

    public function venueWiseBookingReport()
    {
        
        $BookingData = BookingData::get();

        return view('admin.report.venue_wise_booking_report')->with([
        'booking_data'  => $BookingData
        ]);
    }

    public function dashboard()
    {
        
      return view('admin.report.dashboard');
        
    }

    
    
    
    public function allAreaDownload(Request $request)
    {
        
        
        $area_data = Area::with('city','area_zone','state','updated_user','created_user')->get()->toArray();
        
        $area_array[] = array('SN','ID','Catgeory Name','Created By','Created Date & Time','Status');
             
            foreach($area_data as $index=>$area)
            {
                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'Area ID'           => $area['id'],
                   'Area Name'         => $area['area_name'],
                   'Area Zone Id'         => $area['area_zone']['id'],
                   'Area Zone Name'         => $area['area_zone']['name'],
                   'state ID'           => $area['state']['id'],
                   'State Name'         => $area['state']['name'],
                   'City ID'           => $area['city']['id'],
                   'City Name'           => $area['city']['name'],
                   'Pin Code'           => $area['pin_code'],
                   'Latitide'           => $area['lat'],
                   'longitude'           => $area['lng'],
                   'Created By'            => isset($area['created_user']['name']) ? $area['created_user']['name'] : 'NA',
                   'Updated By'            => isset($area['updated_user']['name']) ? $area['updated_user']['name'] : 'NA',
                   
                   'Created Date & Time'   => date("Y-m-d", strtotime($area['created_at'])).'&'.date("H:i", strtotime($area['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($area['updated_at'])).'&'.date("H:i", strtotime($area['updated_at'])),
                   'Status'                => $area['status'] == 1 ? 'Active':'Inactive',
                );
            }
        return Excel::download(new AllAreaExport($data), 'Allarea.xlsx');
    }
    
}
