<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VendorBranchMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->usertype == '2' && Auth::user()->role == '5')
        {
            return $next($request);
        }
        else
        {
            return redirect('/')->with('error', 'Wrong Login Details');
        }
    }
}
