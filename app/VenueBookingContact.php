<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueBookingContact extends Model
{
    //outlet table
    protected $table = 'tbl_venue_booking_contact';
    protected $fillable = [
        'venue_id','name','email','phone','designation'
    ];
    //Venue  function
    public function Venue()
    {
        return $this->belongsTo('App\Venue','venue_id');
    }

   
    

}
