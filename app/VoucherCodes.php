<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class VoucherCodes extends Model
{
    use UserStampsTrait;
    
    protected $table = 'tbl_voucher_codes';
    protected $fillable = ['generate_code_id','voucher_code','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
    
     public function genCode()
    {
        return $this->belongsTo('App\GenerateCodes','generate_code_id','id');
    }
}
