<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListType extends Model
{
    //outlet table
    protected $table = 'tbl_list_type';
    protected $fillable = [
        'id', 'name',
    ];
    
    
    

}
