<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingAssignAgent extends Model
{
    
    
    protected $table = 'tbl_booking_assign_agent';
    

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    public function booking(){
    	return $this->belongsTo('App\BookingData','id');
    }

    
}
