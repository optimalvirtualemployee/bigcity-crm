<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueArea extends Model
{
   //Campaign venue table
   protected $table = 'tbl_campaign_venues';
}
