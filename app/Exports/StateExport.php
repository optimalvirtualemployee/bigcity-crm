<?php

namespace App\Exports;

use App\State;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StateExport implements FromView
{
	

    use Exportable;
     private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('admin.category.xml', [
            'data' => $this->data
        ]);
    }
}
