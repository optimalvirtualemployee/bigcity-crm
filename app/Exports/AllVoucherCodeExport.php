<?php

namespace App\Exports;

use App\VoucherCodes;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AllVoucherCodeExport implements FromCollection,WithHeadings
{
	protected $id;

	function __construct($id) {

	        $this->id = $id;
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return VoucherCodes::select('voucher_code')->where('generate_code_id',$this->id)->get();
    }

    public function headings(): array
    {
        return [
       
            'Voucher Code',
            
         
        ];
    }
}
