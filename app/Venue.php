<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class Venue extends Model
{
    use UserStampsTrait;
    //outlet table
    protected $table = 'tbl_venues';
    protected $fillable = [
        'venue_name','pd_lead_id', 'category_id','chain_name', 'city_id','state_id','area_id','venue_type','address','latitude','longitude','rating','black_out_day','billing_terms','booking_alert_required'
        ,'send_QC','bank_name','account_number','ifsc_code','name_on_account','other_value','bcp_cost'
        
    ];
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
    //User  function
    public function user()
    {
        return $this->belongsTo('App\User','pd_lead_id');
    }

    //Category  function
    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }
    //city  function
    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }
    //city  function
    public function state()
    {
        return $this->belongsTo('App\State','state_id');
    }
    //city  function
    public function area()
    {
        return $this->belongsTo('App\Area','area_id');
    }
    //city  function
    public function venue_qc()
    {
        return $this->hasOne('App\VenueQC','venue_id');
    }
    public function list_type()
    {
        return $this->belongsToMany('App\ListType');
    }

    public function venue_product()
    {
        return $this->belongsToMany('App\Product')->withPivot('description','mon_mrp','tue_mrp','wed_mrp','thus_mrp','fri_mrp','sat_mrp','sun_mrp','mon_cost','tue_cost',
        'wed_cost','thus_cost','fri_cost','sat_cost','sun_cost');
    }
    

}
