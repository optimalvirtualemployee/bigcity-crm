<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueListType extends Model
{
    //outlet table
    protected $table = 'list_type_venue';
    protected $fillable = [
        'venue_id', 'name',
    ];
    
    
    

}
