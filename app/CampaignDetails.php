<?php

namespace App;
use App\UserStampsTrait;

use Illuminate\Database\Eloquent\Model;

class CampaignDetails extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_campaign_details';
    protected $fillable = ['campaign_id','promo_type_id','no_of_reward_redeem','product_id','mobile_no_usage_rules','mobile_usage_value'];

    public function campaign(){
        
    	return $this->belongsTo('App\Campaign','campaign_id','id');
    }

    public function promo_type(){
        
    	return $this->belongsTo('App\PromoType','promo_type_id','id');
    }

    public function products(){
        
    	return $this->belongsTo('App\Product','product_id','id');
    }

    public function mobile_no_usage(){
    	return $this->belongsTo('App\MobileUsageRule','mobile_no_usage_rules','id');
    }
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
    
}
