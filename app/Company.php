<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class Company extends Model
{
    use UserStampsTrait;
    
    protected $table = 'tbl_companies';
    protected $fillable = ['name','brand_name'];
    
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }

    
}
