<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class VoucherSpecification extends Model
{
    use UserStampsTrait;
    //outlet table
    protected $table = 'tbl_voucher_specification';
    protected $fillable = ['voucher_type_id','size_name','length','breadth','thickness'];


    //Voucher Type  function
    public function voucher_type()
    {
        return $this->belongsTo('App\VoucherType','voucher_type_id');
    }
    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
