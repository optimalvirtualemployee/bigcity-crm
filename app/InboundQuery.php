<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InboundQuery extends Model
{
    
    
    protected $table = 'tbl_inbound_query';
    

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    public function campaign(){
    	return $this->belongsTo('App\Campaign','campaign_id','id');
    }

    public function cust_query(){
        return $this->belongsTo('App\CustomerQuery','query_id','id');
    }

    public function booking(){
    	return $this->belongsTo('App\Booking','voucher_code','voucher_code');	
    }
    
}
