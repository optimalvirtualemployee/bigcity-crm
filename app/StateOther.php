<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;


class StateOther extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_state_other_lang';
    protected $fillable = ['name','other_lang_id','state_id'];

    
    
    
    //Primary  Language function
    public function state()
    {
    	return $this->belongsTo('App\State','state_id','id');
    }
    
    //Other Language  function
    public function other_lang()
    {
    	return $this->belongsTo('App\OtherLanguage','other_lang_id','id');
    }
    
}
