<?php

namespace App\Jobs;
use App\GenerateCodes;
use App\Campaign;
use App\Product;
use App\VoucherCodes;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use romanzipp\QueueMonitor\Traits\IsMonitored;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;

class VoucherCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, IsMonitored;

    protected $generateCodeData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
     /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 50000;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 20;
    public function __construct($generateCodeData)
    {

        
        $this->generateCodeData = $generateCodeData;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        ini_set("memory_limit", "-1");
        /*if($this->gc_codes_type = '2' && !$this->generateCodeData->letters->isEmpty()) {

            if(count($this->generateCodeData['letters']) + count($this->generateCodeData['numbers']) >= 25)   {
                
            $dynamicAlphanumeric = str_replace( ',', '', implode(',',$this->generateCodeData['letters'])).str_replace( ',', '', implode(',',$this->generateCodeData['numbers']));
            
            } else{
                
                return redirect()->back()->withErrors('The count of both numeric and alpha must be Choose 25 character')->withInput();
            }

        }    */ 
        
               
        $campaign = Campaign::where('id',$this->generateCodeData['campaign_id'])->first();
        $product = Product::where('id',$this->generateCodeData['product_id'])->first();
        $count = $this->generateCodeData['gc_no_of_codes_required'];
        $length = $this->generateCodeData['length_of_code_required'];  
        $counter = 1;
        $test_data = array();
                              
        $generate_code = GenerateCodes::select('code_prefix')->latest()->first();
            
        if($generate_code != ''){
            
            $prefix = ($generate_code->code_prefix) + 1;
            
        }else{
            
            $prefix = 101;
            
        }
        $generate_codes = GenerateCodes::create($this->generateCodeData);
            $generate_codes->gc_campaign_id = $this->generateCodeData['campaign_id'];
            $generate_codes->gc_product_id = $this->generateCodeData['product_id'];
            
            $generate_codes->code_prefix = $prefix;
            $generateSavedData = $generate_codes->save();
            //save voucher in DB
            $test_data = $this->generateCoupons($count,$length,$prefix,$this->generateCodeData['gc_codes_type']);
           
        foreach (array_chunk($test_data, 100) as $responseChunk)
        {
            
            $insertableArray = [];
            foreach($responseChunk as $i=>$value) {
                $insertableArray[] = [
                    'generate_code_id' => $generate_codes->id,
                    'voucher_code' => $value,
                    'gc_last_date_to_register_date' => $generate_codes->gc_last_date_to_register_date,
                    'gc_offer_valid_till_date' => $generate_codes->gc_offer_valid_till_date,
                ];
            }
            
            DB::table('tbl_voucher_codes')->insert($insertableArray);
        }
        
           
            /*$generate_codes = GenerateCodes::where('id',$generateSavedData->id)->delete();*/
            
                 
            
    }
        
    public function generateCoupons($count, $length,$prefix,$gc_codes_type)
    {
        ini_set("memory_limit", "-1");
        $coupons = [];
        while(count($coupons) < $count) {
            do {
                if($gc_codes_type == '1'){

                    $coupon = $prefix.substr(str_shuffle("0123456789"), 0, $length);

                }else{

                    $dynamicAlphanumeric = str_replace( ',', '', implode(',',$this->generateCodeData['letters'])).str_replace( ',', '', implode(',',$this->generateCodeData['numbers']));

                    $coupon = substr(str_shuffle($dynamicAlphanumeric), 0, $length); 
                }
            } while (in_array($coupon, $coupons));
            $coupons[] = $coupon;
        }

        $existing = VoucherCodes::whereIn('voucher_code', $coupons)->count();
        if ($existing > 0)
            $coupons += $this->generateCoupons($existing, $length);

        return (count($coupons) == 1) ? $coupons[0] : $coupons;
    }
    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return Carbon::now()->addMinutes(15);
    }
}
