<!DOCTYPE html>
<html>
  <head>
   @include('common.header')
  </head>
  
  <body id="page-top">
    <!-- Side Navbar -->
     @include('common.sidebar')
    
    <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
        <div id="loading">
     		  <img id="loading-image" src="https://2.bp.blogspot.com/-R4i03mAdarY/WMksQxeJPeI/AAAAAAAAA90/NqrPy8TfTIQNqVI89vJ_uCce45WvklHhwCLcB/s1600/infinity.gif" alt="Loading..." />
    	  </div>
    <!-- End of Content Wrapper -->
    
      @yield('content') 
    @include('common.footer') 
  
  </body>
</html>   