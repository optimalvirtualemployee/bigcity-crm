

<!-- Footer -->
    
   <!--
    <footer class="sticky-footer">
       <div class="container my-auto">
          <div class="copyright text-center my-auto">
             <span>Copyright &copy; BigCity CRM 2020</span>
          </div>
       </div>
    </footer>
   -->
    

    </div>
  </div>
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
  </a>
     
<!-- End of Footer -->
<!-- Bootstrap JS-->
<!-- Bootstrap core JavaScript-->

<script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendor/jquery/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>-->
<script type="text/javascript">
    

    var form = $("#productrulefrm");

        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            
        });
          var form = $("#assignBookingForm");
    
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            
        });
        var form = $("#campaignDetail_frm");
    
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            
        });
        
        var form = $("#contact");
        
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            
        });



        form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true,
        /*saveState: true,*/

        onStepChanging: function (event, currentIndex, newIndex)
        {
            /*if(currentIndex == 2){
                if($('#mrp_of_product').val() >= $('#cost_of_product').val()){
                    form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
                }else{
                    return alert('MRP of cost should be less than or equal to cost of product');
                }
            }*/
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
            
        },
        onFinishing: function (event, currentIndex)
        {
            
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
              
            /*alert('are you sure you want to save venue.!!');*/
                $('#contact').submit();
        }

    });
</script>
<script type="text/javascript">
      $(function () {
      // Apply select2
      var bcp_cost = $( ".bcp_cost option:selected" ).val();
      
      $('.select2').select2()
  
      // Just a small helper
      var _h = {
        capitalize: function (str) {
          return str.charAt(0).toUpperCase() + str.slice(1)
        },
        render: function (id, name) {
          var html = ''
          
          $.each([1], function (k, v) {
        html += '<div class="col-md-12 col-lg-6"><label for="name">' + _h.capitalize(name) +'*</label> <textarea name="description[]" placeholder="' + _h.capitalize(name) + '&nbsp;' + 'Description' + '" class="form-control  block required">{{ old('description') }}</textarea></div><div class="col-md-12 col-lg-6"><label for="name">' + _h.capitalize(name) +'*</label><div class="form-group row mx-auto"><div class="col-md-12 col-lg-4"><input type="checkbox" id="weekday-mon" name="day_name[]" value="Monday"/><label for="weekday-mon">Monday</label></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" id="mrp_of_product" placeholder="MRP Of Product" name="mon_mrp[]" value="{{ old('mrp_of_product') }}"></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" id="cost_of_product" placeholder="Cost Of Product" name="mon_cost[]" value="{{ old('cost_of_product') }}"></div></div><div class="form-group row mx-auto"><div class="col-md-12 col-lg-4"> <input type="checkbox" name="day_name[]" id="weekday-tue" value="Tuesday" /><label for="weekday-tue">Tuesday</label></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="MRP Of Product" name="tue_mrp[]" value="{{ old('mrp_of_product') }}"></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control required number" placeholder="Cost Of Product" name="tue_cost[]" value="{{ old('cost_of_product') }}"></div></div><div class="form-group row mx-auto"><div class="col-md-12 col-lg-4"> <input type="checkbox" name="day_name[]" id="weekday-wed" value="Wednesday" /><label for="weekday-wed">Wednesday</label></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="MRP Of Product" name="wed_mrp[]" value="{{ old('mrp_of_product') }}"></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="Cost Of Product" name="wed_cost[]" value="{{ old('cost_of_product') }}"></div></div><div class="form-group row mx-auto"><div class="col-md-12 col-lg-4"> <input type="checkbox" id="weekday-thu" name="day_name[]" value="Thursday" /><label for="weekday-thu">Thursday</label></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="MRP Of Product" name="thus_mrp[]" value="{{ old('mrp_of_product') }}"></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="Cost Of Product" name="thus_cost[]" value="{{ old('cost_of_product') }}"></div></div><div class="form-group row mx-auto"><div class="col-md-12 col-lg-4"> <input type="checkbox" name="day_name[]" id="weekday-fri" value="Friday"/><label for="weekday-fri">Friday</label></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="MRP Of Product" name="fri_mrp[]" value="{{ old('mrp_of_product') }}"></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="Cost Of Product" name="fri_cost[]" value="{{ old('cost_of_product') }}"></div></div><div class="form-group row mx-auto"><div class="col-md-12 col-lg-4 weekday"> <input type="checkbox" name="day_name[]" id="weekday-sat" value="Saturday" /><label for="weekday-sat">Saturday</label></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="MRP Of Product" name="sat_mrp[]" value="{{ old('mrp_of_product') }}"></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="Cost Of Product" name="sat_cost[]" value="{{ old('cost_of_product') }}"></div></div><div class="form-group row mx-auto"><div class="col-md-12 col-lg-4 weekday"> <input type="checkbox" name="day_name[]" id="weekday-sun" value="Sunday" /><label for="weekday-sun">Sunday</label></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="MRP Of Product" name="sun_mrp[]" value="{{ old('mrp_of_product') }}"></div><div class="col-md-12 col-lg-4"><input type="text" class="form-control number" placeholder="Cost Of Product" name="sun_cost[]" value="{{ old('cost_of_product') }}"></div></div></div>'
    })
      
      

      
      return html
    }
  }
  
  // Now listed to change event of select2
  $(document).on('change', '.product', function () {
      
    $('.parent_clone').html('') // truncate any appended nodes
    
    $(this).find(':selected').each(function (k, v) {
      $('.parent_clone').append(_h.render($(this).val(), $(this).text()))
    })
  })
})
</script>
<!-- Core plugin JavaScript-->
<script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<!-- Custom scripts for all pages-->
<script src="{{url('assets/js/sb-admin-2.js')}}"></script>
<!-- Page level plugins -->
<!--<script src="{{url('assets/vendor/chart.js/Chart.min.js')}}"></script>-->
<!-- Page level custom scripts -->
<!--<script src="{{url('assets/js/demo/chart-area-demo.js')}}"></script>
<script src="{{url('assets/js/demo/chart-pie-demo.js')}}"></script>
<script src="{{url('assets/js/demo/chart-bar-demo.js')}}"></script>-->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script src="{{url('assets/js/select2.min.js')}}"></script>
<script src="{{url('assets/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.js')}}"></script>

<script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.js')}}"></script>

<link rel="stylesheet" href="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}">
<!-- Page level custom scripts -->
<script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
<script src="{{url('assets/js/custom.js')}}"></script>
<script src="{{url('assets/js/multiselect.js')}}"></script>

<!-- Product Rule JS  -->
<script src="{{url('assets/js/jquery.datetimepicker.js')}}"></script>
<!-- Product Rule JS  -->


<script>

    $( '#content .navbar-nav a' ).on( 'click', function () {
       $( '#content .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
       $( this ).parent( 'li' ).addClass( 'active' );
    });
   
   //Start js for mulitple text box open
   $(document).ready(function() {  
        $('.ckeditor').ckeditor();
        var max_fields_limit      = 10; //set limit for maximum input fields
        var x = 1; //initialize counter for text box
        
        $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
            e.preventDefault();
            if(x < max_fields_limit){ //check conditions
                x++; //counter increment
                
                $('.input_fields_container').append('<div class="col-md-12 col-lg-3"><textarea name="description[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Product Description">{{ old('description') }}</textarea></div>'); //add input field
    
                $('.input_fields_container').append('<div class="col-md-12 col-lg-3"><input type="number" name="mrp_of_product[]" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter MRP of Product" value="{{ old('mrp_of_product') }}" min="1"></div>'); //add input field
                $('.input_fields_container').append('<div class="col-md-12 col-lg-3"><input type="number" name="cost_of_product[]" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Cost of Product" value="{{ old('cost_of_product') }}" min="1"></div>'); //add input field
                /*$('.input_fields_container').append('<div class="col-md-12 col-lg-3"><a href="#" class="remove_field" style="margin-left:10px;">Remove</a></div>');*/ //add input field
            }
        });
        $('.add_more_billing_button').click(function(e){ //click event on add more fields button having class add_more_button
            e.preventDefault();
            if(x < max_fields_limit){ //check conditions
                x++; //counter increment
                
                $('.billing_fields_container').append('<div class="col-md-12 col-lg-3"><input type="text" name="billing_name[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Name" value="{{ old('billing_name') }}"></div>'); //add input field
    
                $('.billing_fields_container').append('<div class="col-md-12 col-lg-3"><input type="email" name="billing_email[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email" value="{{ old('billing_email') }}"></div>'); //add input field
                $('.billing_fields_container').append('<div class="col-md-12 col-lg-3"><input type="tel" name="billing_phone[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter phone" value="{{ old('billing_phone') }}"></div>'); //add input field
                $('.billing_fields_container').append('<div class="col-md-12 col-lg-3"><input type="text" name="billing_designation[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Designation" value="{{ old('billing_designation') }}"></div>'); //add input field
                /*$('.input_fields_container').append('<div class="col-md-12 col-lg-3"><a href="#" class="remove_field" style="margin-left:10px;">Remove</a></div>');*/ //add input field
            }
        });  
        $('.add_more_booking_button').click(function(e){ //click event on add more fields button having class add_more_button
            e.preventDefault();
            if(x < max_fields_limit){ //check conditions
                x++; //counter increment
                
                $('.booking_fields_container').append('<div class="col-md-12 col-lg-3"><input type="text" name="booking_name[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Name" value="{{ old('booking_name') }}"></div>'); //add input field
    
                $('.booking_fields_container').append('<div class="col-md-12 col-lg-3"><input type="email" name="booking_email[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email" value="{{ old('booking_email') }}"></div>'); //add input field
                $('.booking_fields_container').append('<div class="col-md-12 col-lg-3"><input type="tel" name="booking_phone[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter phone" value="{{ old('booking_phone') }}"></div>'); //add input field
                $('.booking_fields_container').append('<div class="col-md-12 col-lg-3"><input type="text" name="booking_designation[]" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Designation" value="{{ old('booking_designation') }}"></div>'); //add input field
                /*$('.input_fields_container').append('<div class="col-md-12 col-lg-3"><a href="#" class="remove_field" style="margin-left:10px;">Remove</a></div>');*/ //add input field
            }
        });  
        $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    
    
    });
    //End js for mulitple text box open
    
    //Start js for state append city
    $('.state').on('change',function(){
        
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                
                type:"GET",
              
                url:"{{url('admin/get-city-list')}}?state_id="+stateID,
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res){
        
                    $(".city").empty();
                    $(".city").append('<option value="0">Select City</option>')
                    $.each(res,function(key,value){
                        $(".city").append('<option value="'+key+'">'+value+'</option>');
                    });
             
                    }else{
                     $(".city").empty();
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".city").empty();
        }
      
    });
    $('.city').on('change',function(){
        
       
        var cityId =  $(this).val();
        if(cityId){
            $.ajax({
                type:"GET",
             
                url:"{{url('admin/get-area-list')}}?city_id="+cityId,
                beforeSend: function(){
                    // Show image container
                    $("#loading").css('display','block');
                   },
                success:function(res){               
                    if(res){
        
                    $(".area").empty();
                    $(".area").append('<option value="0">Select Area</option>')
                    $.each(res,function(key,value){
                        $(".area").append('<option value="'+key+'">'+value+'</option>');
                    });
             
                    }else{
                     $(".area").empty();
                    }
                },
                complete:function(data){
                // Hide image container
                $("#loading").css('display','none');
               }
            });
            
        }else{
            
          $(".area").empty();
        }
      
    });
    //End js for state append city
    
    //Start js for category append product
    $('.category').on('change',function(){
      
      $('.product').data('placeholder','Select Product');
      $('.product').select2();

      var category_id = $(this).val();
      if(category_id){
        $.ajax({
           type:"GET",
           
           url:"{{url('admin/get-product-list')}}?category_id="+category_id,
           beforeSend: function(){
                    // Show image container
                    $("#loading").show();
                   },
           success:function(res){               
            if(res){

                $(".product").empty();
                
                $.each(res,function(key,value){
                  $(".product").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $(".product").empty();
               $(".product").hide();
            }
           },
           complete: function(){
                    $('#loading').hide();

                }
        });
      }else{
        $(".product").empty();
        $(".product").hide();
      }
      
    });
    //End js for category append product
    
    //Start js for category status change
    $('.categoryStatus').change(function() {
        var status = $(this).prop('checked') == true ? '1' : '0'; 
        var id = $(this).data('id'); 
        var model_name = $('#mdoelName').val(); 
        //alert(model_name);
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{url('/admin/changeStatusCategory')}}",
            beforeSend: function(){
                    // Show image container
                    $("#loading").show();
                   },
            
            data: {'status': status, 'id': id,'model_name': model_name},
            success: function(data){
              console.log(data.success)
            },
            complete: function(){
                    $('#loading').hide();

                }
        });
    });
    //End js for category status change
    
    //Start js for communication_template_type status change
    $('.communication_template_type').change(function() {
        var status = $(this).prop('checked') == true ? '1' : '0'; 
        var id = $(this).data('id'); 
        var model_name = $('#mdoelName').val(); 
        //alert(model_name);
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{url('/admin/changeStatusCommunicationTemplateType')}}",
            beforeSend: function(){
                    // Show image container
                    $("#loading").show();
                   },
            
            data: {'status': status, 'id': id,'model_name': model_name},
            success: function(data){
              console.log(data.success)
            },
            complete: function(){
                    $('#loading').hide();

                }
        });
    });
    //End js for communication_template_type status change

    
    $('.campaign').on('change',function(){
      
      $('.product').data('placeholder','Select Product');
      $('.product').select2();

      var campaign_id = $(this).val();
      if(campaign_id){
        $.ajax({
           type:"GET",
           
           url:"{{url('admin/get_productList')}}?campaign_id="+campaign_id,
           beforeSend: function(){
                    // Show image container
                    $("#loading").show();
                   },
           success:function(res){               
            if(res){

                $(".product").empty();
                
                $.each(res,function(key,value){
                  $(".product").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $(".product").empty();
               $(".product").hide();
            }
           },
           complete: function(){
                    $('#loading').hide();

                }
        });
      }else{
        $(".product").empty();
        $(".product").hide();
      }
      
    });
      $(document).ready(function() {

      window.history.pushState(null, "", window.location.href);        

      window.onpopstate = function() {

          window.history.pushState(null, "", window.location.href);

      };

  });
    

    
</script>



