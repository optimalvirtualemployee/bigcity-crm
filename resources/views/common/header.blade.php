<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>@yield('title')</title>
<!-- Custom fonts for this template-->

<!-- Product Rule Css -->
<link href="{{url('assets/css/datetimepicker.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('assets/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<!-- Product Rule Css -->


<link href="{{url('assets/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<!-- Custom styles -->
<link href="{{url('assets/css/style.css')}}" rel="stylesheet">
<style>
#loading {
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  position: fixed;
  display: none;
  opacity: 0.7;
  background-color: #fff;
  z-index: 99;
  text-align: center;
}

#loading-image {
  
position: absolute;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
transform: -webkit-translate(-50%, -50%);
transform: -moz-translate(-50%, -50%);
transform: -ms-translate(-50%, -50%);

z-index: 9999;
 
} 
#dvPreview
{
   
    width: 200px;
    display:none;
    margin:0px auto;
  
}
div#dvPreview img {
   
    width: 100%;
    height: auto;
    max-width: 100%;
    display: block;
 

}

.icon i{
font-size: 50px;
color: #5a5c69;
}
.icon i:hover{
font-size: 50px;
color: #d33f3e;
 
transition: .5s; 

}

header h6 i {
float: left;
font-size: 24px;
margin: 0;
margin-top: 0px;
margin-right: 0px;
margin-right: 10px;
margin-top: 5px;
}
header h6 span {
overflow: hidden;
display: block;
float: left;
font-size: 12px;
line-height: 18px;    
}
.bd-placeholder-img {
background: linear-gradient(to right, #5d2d88, #ea4c11);
}
#default2.fixed-top {
position:fixed;
top:0; left:0;
width:100%;
background-color: #222;
}
#logo-small img{
max-height:40px;
}
#logo-small
{
display:none;
}
img.logo {
max-width:100%;
}
.navbar-light .navbar-nav .nav-link {
color: rgba(0, 0, 0, 0.5);
color: #f8f9fc;
font-weight: 600;
padding: 10px;
}
.nav-link {
display: block;
 
 

font-weight: 400;
 
padding: 10px ;
}
.nav-link:hover {
background-color: #FFF;
color: #224abe;
 

}
.card-body {
flex: 1 1 auto;
padding: 1rem !important;
}
.shadow
{

  box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12) !important;
}
.card:hover
{
  box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18), 0 4px 15px 0 rgba(0,0,0,0.15) !important;
}
#wrapper #content-wrapper {
    background-color: #FFF;
    width: 100%;
    overflow-x: hidden;
}
.bg-light{
    /* background-color: #f8f9fc !important; */
    background-color: rgb(16, 49, 62) !important;
    color: #FFF;
}
.navbar {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-between;
    padding:0px !important;
}
.navbar-nav .nav-item:not(:last-child) {
   border-right: 1px solid silver;
   
}

 






 

#content .navbar-nav li.active > a {
    background-color: #d33f3e;
    color: #fff;
  transition: background-color .2s, color .2s;
}
.navbar-brand {
    display: inline-block;
     padding-top:0px !important;
     padding-bottom: 0px !important;
    /* padding-bottom: 0.3125rem; */
    margin-right: 1rem;
    font-size: 1.25rem;
    line-height: inherit;
    white-space: nowrap;
}


#pointer {
      width: 100px;
      height: 45px;
      position: relative;
      background: #d33f3e;
      margin-left: -25px;
    margin-right: 10px;
    }
    #pointer:after {
      content: "";
      position: absolute;
      left: 0;
      bottom: 0;
      width: 0;
      height: 0;
     
      border-top: 10px solid transparent;
      border-bottom: 10px solid transparent;
    }
    #pointer:before {
      content: "";
      position: absolute;
      right: -20px;
      bottom: 0;
      width: 0;
      height: 0;
      border-left: 20px solid #d33f3e;
      border-top: 25px solid transparent;
      border-bottom: 20px solid transparent;
    }

</style>
