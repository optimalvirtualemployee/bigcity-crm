 
<header class="bg-light">
   <div class="container-fluid">
   <div class="row" style="padding: 0px 20px;">
      <div class="col-sm-12 col-lg-12 col-md-12 m-auto  d-lg-block" style="text-align: center;">
         <img class="logo" src="{{url('/assets/img/Bigcity_logo.png')}}" alt="Bigcity Logo">
      </div>
     <div class="col-lg-12 col-sm-12 col-md-12  d-none d-sm-block">
         <nav class="navbar navbar-expand-md" style="justify-content: center;margin-top: -30px;">
            <ul class="navbar-nav pb-3">
               <li class="nav-item pl-3" style="border-right: 0px solid silver;">
                  <h6><i class="fa fa-map-marker" style="color: #cbdf1c;font-size: 10px"></i> <span>Noida, Uttar Pradesh, India</span></h6>
               </li>
               <li class="nav-item pl-3" style="border-right: 0px solid silver;">
                  <h6><i class="fa fa-phone"  style="color: #cbdf1c;font-size: 10px;"></i><span><span class="ringo-phone">+38 (999) 999 9999</span> <!-- <br>Uttar Pradesh, India --></span></h6>
               </li>
               <li class="nav-item pl-3" style="border-right: 0px solid silver;">
                <h6><i class="fas fa-envelope-open-text"  style="color: #cbdf1c;font-size: 10px;"></i> <span>support@bbigcitycity.com</span></h6>
               </li>
               <li class="nav-item pl-3">
                  <h6><i class="fas fa-hands-helping"  style="color: #cbdf1c;font-size: 10px;"></i> <span>Help</span></h6>
               </li>
            </ul>
         </nav>
      
      </div>
   </div>
</div>

</header>
<div id="wrapper">
<!-- Sidebar -->
<!-- End of Sidebar -->
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Topbar -->
      <nav class="navbar navbar-expand-xl navbar-light     shadow-sm" style="background-color: #106db2;">
         <div class="container-fluid">
            <a class="navbar-brand dept_head" style="color: #FFF;position: relative;">
               <div id=pointer>
                  <span style="margin-left: 20px;position: relative;top:3px;font-size: 25px;"> <b>CRM</b></span>
               </div>
            </a>
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
            </button>
            <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">
            <span class="navbar-toggler-icon"></span>
            </button>
            @php 
            $qc_user =  App\User::select('users.*')->where('users.status','1')->where('users.id',Auth::user()->id)->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->whereIn('model_has_roles.role_id', ['4','5'])->first()
            @endphp
            
            <div id="navbarContent" class="collapse navbar-collapse">
               @if($_SERVER['REQUEST_URI'] != '/admin/dashboard' && !$qc_user)
               <ul class="navbar-nav mr-auto">
                  <li class="nav-item "><a href="{{url('admin/dashboard')}}" class="nav-link ">Home</a></li>
                  <!-- Level one dropdown -->
                  <!--<li class="nav-item"><a href="#" class="nav-link">About</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Services</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Contact</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Lead</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Ticket</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Custom</a></li>-->
                  <!--<li class="nav-item dropdown">
                     <a id="dropdownMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Dropdown</a>
                     <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow">
                        <li><a href="#" class="dropdown-item">Some action </a></li>
                        <li><a href="#" class="dropdown-item">Some other action</a></li>
                        <li class="dropdown-divider"></li>-->
                        <!-- Level two dropdown-->
                        <!-- End Level two -->
                     <!--</ul>
                  </li>-->
                  <!-- End Level one -->
               </ul>
               @endif
               <ul class="nav navbar-nav ml-auto">
                   
                  @if($_SERVER['REQUEST_URI'] != '/admin/dashboard' && !$qc_user)
                 
                  <li class="nav-item"><a href="{{url('admin/dashboard')}}" class="nav-link"> &nbsp;Dashboard</a></li>
                  
                  @endif
                  <li class="nav-item dropdown arrow">
                     <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <i class="fas fa-user-tie"></i> &nbsp;Welcome <span class="">{{Auth::user()->name}}</span>
                     </a>
                     <!-- Dropdown - User Information -->
                     <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <!--<a class="dropdown-item" href="#">
                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                        Profile
                        </a>
                        <a class="dropdown-item" href="#">
                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                        Settings
                        </a>
                        <a class="dropdown-item" href="#">
                        <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                        Activity Log
                        </a>-->
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                           </form>
                           <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                           Logout
                        </a>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </nav>

      
      <!-- Page Wrapper -->
      <div id="wrapper">
          
          @if(request()->segment(2) != 'dashboard' && request()->segment(3) != 'dashboard')
         <!-- Sidebar -->
         <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar"  
            style="background-color: #106db2!important;margin-top: 15px;">
             
            <!-- Sidebar - Brand -->
            <li class="nav-item {{ (request()->is('admin/dashboard')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/dashboard')}}">
               <i class="fas fa-fw fa-tachometer-alt"></i>
               <span>Dashboard</span></a>
               
               
            </li>
            @if(request()->segment(2) == 'categories')
            <hr class="sidebar-divider my-0">
            
            <li class="nav-item {{ (request()->is('admin/categories')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/categories')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Category Listing </span></a>
               
               
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/categories/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/categories/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Category</span></a>
            </li>
            @endif
            
            @if(request()->segment(2) == 'products')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/products')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/products')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Product Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/products/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/products/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Product</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'productrules')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/productrules')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/productrules')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Product Rule Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/productrules/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/productrules/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Product Rule</span></a>
            </li>
            @endif
            
            @if(request()->segment(2) == 'users')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/users')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/users')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>User Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/users/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/users/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add User</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'roles')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/roles')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/roles')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Role Permission Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/roles/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/roles/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Role Permission</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'states')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/states')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/states')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>State Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/states/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/states/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add State</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'cities')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/cities')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/cities')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>City Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/cities/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/cities/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add City</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'vouchertype')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/vouchertype')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/vouchertype')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Voucher Type Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/vouchertype/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/vouchertype/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Voucher Type</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'deliverymechanictype')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/deliverymechanictype')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/deliverymechanictype')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Delivery Mechanic Type Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/deliverymechanictype/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/deliverymechanictype/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Delivery Mechanic Type</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'promotype')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/promotype')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/promotype')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Promo Type Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/promotype/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/promotype/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Promo Type</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'companies')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/companies')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/companies')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Company Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/companies/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/companies/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Company</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'campaigntype')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/campaigntype')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/campaigntype')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Campaign Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/campaigntype/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/campaigntype/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Campaign</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'customerquery')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/customerquery')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/customerquery')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Customer Query Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/customerquery/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/customerquery/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Customer Query</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'venue-qc')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/venue-qc')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/venue-qc')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Venue QC Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            
            @endif
            @if(request()->segment(2) == 'target-assign')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/target-assign')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/target-assign')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Target Assign Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/target-assign/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/target-assigncreate')}}">
               <i class="fas fa-plus"></i>
               <span>Add Target Assign</span></a>
            </li>
            
            @endif
            @if(request()->segment(2) == 'venues')
            
            <hr class="sidebar-divider my-0">
            
            <li class="nav-item  {{ (request()->is('admin/venues')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/venues')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Venue Listing </span></a>
            </li>
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/venues/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/venues/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Venue</span></a>
            </li>
            
           
           
           <!--
            <hr class="sidebar-divider my-0">
            <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="false" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Utilities</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" style="">
            <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="utilities-color.html">Colors</a>
            <a class="collapse-item" href="utilities-border.html">Borders</a>
            <a class="collapse-item" href="utilities-animation.html">Animations</a>
            <a class="collapse-item" href="utilities-other.html">Other</a>
            </div>
            </div>
            </li>
          -->
            
            
            
            @endif
            @if(request()->segment(2) == 'voucherspecification')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/voucherspecification')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/voucherspecification')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Voucher Specification Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/voucherspecification/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/voucherspecification/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Voucher Specification</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'primary_language')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/primary_language')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/primary_language')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Primary Language List</span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/primary_language/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/primary_language/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Primary Language</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'otherlanguage')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/otherlanguage')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/otherlanguage')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Other Language </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/otherlanguage/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/otherlanguage/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Other Language</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'areas')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/areas')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/areas')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Area Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/areas/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/areas/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Area</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'campaigns')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/campaigns')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/campaigns')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Campaigns Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/campaigns/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/campaigns/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Campaigns</span></a>
            </li>
            @endif
            @if(request()->segment(2) == 'campaign_details')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/campaign_details')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/campaign_details')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Campaign Detail Listing</span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/campaign_details/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/campaign_details/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Campaign Details</span></a>
            </li>
            @endif 
            @if(request()->segment(2) == 'tag_campaign_venues')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/tag_campaign_venues')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/tag_campaign_venues')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Campaign Venue Tag List</span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/tag_campaign_venues/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/tag_campaign_venues/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Campaign Venue Tag List</span></a>
            </li>
            @endif 
            @if(request()->segment(2) == 'generate_codes')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/generate_codes')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/generate_codes')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Generate Code Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/generate_codes/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/generate_codes/create')}}">
               <i class="fas fa-plus"></i>
               <span>Generate Code</span></a>
            </li>
            @endif   
            
            @if(request()->segment(2) == 'mobile-usage-rules')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/mobile-usage-rules')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/mobile-usage-rules')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Mobile No. Usage Rules Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/mobile-usage-rules/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/mobile-usage-rules/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Mobile No. Usage Rules</span></a>
            </li>
            @endif  
            @if(request()->segment(2) == 'winnercampaign')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/winnercampaign')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/winnercampaign')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Winner Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/winnercampaign/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/winnercampaign/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Winner</span></a>
            </li>
            @endif 
            @if(request()->segment(2) == 'set-gift-attribute')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/set-gift-attribute')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/set-gift-attribute')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Gift Attribute Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/set-gift-attribute/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/set-gift-attribute/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Gift Attribute</span></a>
            </li>
            @endif 
            @if(request()->segment(2) == 'holiday-attribute')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/holiday-attribute')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/holiday-attribute')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Holiday Attribute Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/holiday-attribute/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/holiday-attribute/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Holiday Attribute</span></a>
            </li>
            @endif 
            @if(request()->segment(2) == 'setgiftallocation')
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item  {{ (request()->is('admin/setgiftallocation')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/setgiftallocation')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Set Gift Allocation Listing </span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/setgiftallocation/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/setgiftallocation/create')}}">
               <i class="fas fa-plus"></i>
               <span>Set Gift Allocation</span></a>
            </li>
            @endif
            
            @if(request()->segment(2) == 'communication-template')
            <hr class="sidebar-divider my-0">
            
            <li class="nav-item {{ (request()->is('admin/communication-template')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/communication-template')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Communication Template Listing </span></a>
               
               
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/communication-template/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/communication-template/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Communication Type</span></a>
            </li>
            @endif

            @if(request()->segment(2) == 'communication_template_type')
            <hr class="sidebar-divider my-0">
            
            <li class="nav-item {{ (request()->is('admin/communication_template_type')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/communication_template_type')}}">
               <i class="fas fa-fw fa-table"></i>
               <span>Communication Template Type Listing </span></a>
               
               
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (request()->is('admin/communication_template_type/create')) ? 'active' : '' }}">
               <a class="nav-link" href="{{url('/admin/communication_template_type/create')}}">
               <i class="fas fa-plus"></i>
               <span>Add Communication Template Type</span></a>
            </li>
            @endif
            
             <hr class="sidebar-divider d-none d-md-block">
            <li class="nav-item">
               <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                           </form>
               <i class="fas fa-sign-out-alt"></i>
               <span>Logout </span></a>
               
               
            </li>
            <!-- <li class="nav-item">
               <a class="nav-link" href="import-lead.html">
               <i class="fas fa-print"></i>
               <span>Import Lead</span></a>
               </li>
               <hr class="sidebar-divider my-0">
               <li class="nav-item ">
               <a class="nav-link" href="print-lead.html">
               <i class="fas fa-fw fa-table"></i>
               <span>Print Lead</span></a>
               </li>
               <hr class="sidebar-divider my-0">
               <li class="nav-item">
               <a class="nav-link" href="export-lead.html">
               <i class="fas fa-file-export"></i>
               <span>Export Lead</span></a>
               </li>
                -->
            <!-- Divider -->
           
            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
               <button class=" border-0" id="sidebarToggle"></button>
            </div>
         </ul>
         <!-- End of Sidebar -->
         @endif

      
      
