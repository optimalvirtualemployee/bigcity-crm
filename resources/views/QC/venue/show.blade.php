@extends('common.default')
@section('title', 'Show Venue')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
     <div class="card shadow ">
      <div class="card-header">
      Venue
      
      </div>
      <div class="card-body">
        <strong>Name:</strong>
        {{ $venues->venue_name }}
      </div>
      <div class="card-body">
        <strong>Venue Type:</strong>
        {{ ($venues->venue_type == '1' )? 'Chain' : 'Standalone'}}
      </div>
      <div class="card-body">

           <strong>Product Category:</strong>
    
           {{ $venues->category->name }}

       </div>
       <div class="card-body">

           <strong>PD Lead Person Name:</strong>
    
           {{ $venues->user['name'] }}

       </div>
       <div class="card-body">

           <strong>Address:</strong>
    
           {{ $venues->address }}

       </div>
       <div class="card-body">

           <strong>Product Tag:</strong>
    
<table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >Product Tag Name</th>
                        <th>Description</th>
                        <th>MRP of Prodcut</th>
                        <th>Cost of Product</th>

                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    @foreach($venue_product as $venue_pro)
                    <tr>
                        <td>{{$venue_pro->product['name']}} </td>
                        <td>{{$venue_pro->description}}</td>
                        <td>{{$venue_pro->mrp_of_product}}</td>
                        <td>{{$venue_pro->cost_of_product}}</td>
                   
                   </tr>
                   @endforeach
                  </tbody>
             </table>

       </div>
       <div class="card-body">

           <strong>Billing Contact :</strong>
    
<table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Designation</th>

                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    @foreach($venue_billing as $venue_bill)
                    <tr>
                        <td>{{$venue_bill->name}}</td>
                        <td>{{$venue_bill->email}}</td>
                        <td>{{$venue_bill->phone}}</td>
                        <td>{{$venue_bill->designation}}</td>
                   
                   </tr>
                   @endforeach
                  </tbody>
             </table>

       </div>
       <div class="card-body">

           <strong>Booking Contact :</strong>
    
<table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Designation</th>

                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    @foreach($venue_booking as $venue_bok)
                    <tr>
                        <td>{{$venue_bok->name}}</td>
                        <td>{{$venue_bok->email}}</td>
                        <td>{{$venue_bok->phone}}</td>
                        <td>{{$venue_bok->designation}}</td>
                   
                   </tr>
                   @endforeach
                  </tbody>
             </table>

       </div>
       <form method="post" action="{{ url('QC/venue-detail', $venues->id) }}">
                           @method('PATCH') 
                           @csrf
                        
                           
                           <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                              <textarea class="form-control" placeholder="Comment" name="comment" ></textarea>
                              </div>
                              <div class="col-md-12 col-lg-4">
                              <select class="inputbox form-control category" name="status">
                                      <option value="0">Select Status</option>
                                      
                                      <option value="Confirmed" {{old('status') == 'Confirmed' ? 'Selected' : ''}}>Confirmed</option>
                                      <option value="Rejected" {{old('status') == 'Rejected' ? 'Selected' : ''}}>Rejected</option>
                                      
                                      
                                   </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary">Update</button>
                           </a>
                           </div>
                           </div>
                           
                        </form>
       
   </div>
   
       
      </div>
</div>
@stop


