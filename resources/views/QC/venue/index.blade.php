@extends('common.default')
@section('title', 'Venue List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 20px;">
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card shadow ">
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  
               </ul>
            </nav>
         </div>
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>Venue Type</th>
                        <th>PD Name</th>
                        <th>Product Catgeory Name</th>
                        <th>Name</th>
                        <!--<th>Created By</th>-->
                        <th>Created Date/Time</th>
                        
                        <th>Status</th>
                        <th>Action</th>
                        
                        <!--<th>Status</th>-->
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                     @forelse($venues as $index=>$venue)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{($venue->venue_type == '1')? 'Chain' : 'Standalone'}}</td>

                        <td>{{$venue->user['name']}}</td>
                        <td>{{$venue->category['name']}}</td>
                        <td>{{$venue->venue_name}}</td>
                        @if(Auth::user()->role == '1')
                        <td>{{Auth::user()->name}}</td>
                        @endif
                        <td>{{$venue->created_at->format('d M Y')}}/{{$venue->created_at->format('g:i A')}}</td>
                        <td>{{$venue->venue_qc['status']}}</td>
                        
                        
                        
                        
                        
                        <td>
                           <a href="{{ url('QC/venue-detail',$venue->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <!--<a href="{{ route('venues.edit',$venue->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            <form action="{{ route('venues.destroy', $venue->id) }}" method="POST" id="delete-form-{{ $venue->id }}" style="display: none;">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <input type="hidden" value="{{ $errors->id }}" name="id">
                           </form>-->
                           <!--<a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                            event.preventDefault(); document.getElementById('delete-form-{{ $venue->id }}').submit();">
                            <span style="color: red;padding:5px;"  ><i class="fas fa-trash fa-primary fa-md"></i></span></a>-->
                           <!--<span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                           <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span> -->
                        <!--</td>
                        -->
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
