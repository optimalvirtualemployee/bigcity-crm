@extends('common.default')
@section('content')
<!-- For demo purpose -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-5 mt-5">
                <div class="row">
                    <!--<div class="col-6 col-sm-6 col-md-3">
                        <a href="{{url('QC/venue-list')}}">
                        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                            <div class="mainflip">
                               <div class="frontside">
                                  <div class="card  shadow pb-0">
                                     <div class="card-body text-center">
                                        
                                           <p class="icon" > <i class="fas fa-object-group"></i></p>
                                        
                                        <h4 class="h6 mb-0 text-uppercase font-weight-bold text-gray-800">Venue</h4>
                                     </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                        </a>
                    </div>-->
                </div>
                <div class="row mb-5">
                   
                   
                  
                  
                  <div class="col-xl-3 col-sm-6 py-2">
                    <div class="card text-white bg-danger h-100">
                        <div class="card-body bg-danger">
                            <div class="rotate">
                                <i class="fa fa-list fa-2x"></i>
                            </div>
                            <h6 class="text-uppercase">Total Confirmed Venue</h6>
                            <h1 class="display-4">{{$totalVenueRejected->count()}}</h1>
                              <a href="{{url('QC/venue-list')}}" class="btn btn-primary btn-sm" style="background-color: transparent;
    border: 1px solid #FFF;">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-xl-3 col-sm-6 py-2">
                    <div class="card text-white bg-info h-100">
                        <div class="card-body bg-success">
                            <div class="rotate">
                                <i class="fa fa-list fa-2x"></i>
                            </div>
                            <h6 class="text-uppercase">Total Rejected Venue</h6>
                            <h1 class="display-4">{{$totalVenueConfirmed->count()}}</h1>
                               <a href="{{url('QC/venue-list')}}" class="btn btn-primary btn-sm" style="background-color: transparent;
    border: 1px solid #FFF;">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-xl-3 col-sm-6 py-2">
                    <div class="card text-white bg-primary h-100">
                        <div class="card-body">
                            <div class="rotate">
                                <i class="fa fa-list fa-2x"></i>
                            </div>
                            <h6 class="text-uppercase">Total Venue QC</h6>
                            <h1 class="display-4">{{$totalVenueQC->count()}}</h1>
                            <a href="{{url('QC/venue-list')}}" class="btn btn-primary btn-sm" style="background-color: transparent;
    border: 1px solid #FFF;">Read More</a>
                        </div>
                    </div>
                  </div>
                  
               </div>
            </div>
        </div>
    </div>
                
@stop