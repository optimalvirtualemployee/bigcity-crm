@extends('common.default')
@section('title', 'Edit Company')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
       
         <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Company</h1>
             <a href="{{url('admin/companies')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back </a>
          </div>
          
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center pt-5">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('companies.update', $company->id) }}">
                           @method('PATCH') 
                           @csrf
                        
                           
                           <div class="form-group">
                               <label for="Enter Company Name" class="mr-sm-2"> Company Name</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Company Name" name="name" value="{{$company->name}}">
                           </div>
                           
                           <div class="form-group">
                               <label for="Enter Brand Name" class="mr-sm-2"> Brand Name</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Brand Name" name="brand_name" value="{{$company->brand_name}}">
                           </div>
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop