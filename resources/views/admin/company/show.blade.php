@extends('common.default')
@section('title', 'Show Company')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
       
          <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Company Detail</h1>
             <a href="{{url('admin/companies')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back </a>
          </div>
          
    
      
        <div class="form-group row">
      <div class="card-body col-md-12 col-lg-4">

           <strong>Company ID:</strong>
    
           {{$company->id}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>Company Name:</strong>
    
           {{ $company->name }}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>Company Brand Name:</strong>
    
           {{ $company->brand_name }}

       </div>
       
       
       
       
       <div class="card-body col-md-12 col-lg-4">

           <strong>Created By:</strong>
    
           {{isset($company->created_user['name']) ? $company->created_user['name'] : 'NA'}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>Updated By:</strong>
    
           {{isset($company->updated_user['name']) ? $company->updated_user['name'] : 'NA'}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>Created Date/Time:</strong>
    
           {{$company->created_at->format('d M Y')}}/{{$company->created_at->format('g:i A')}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>Updated Date/Time:</strong>
    
           {{$company->updated_at->format('d M Y')}}/{{$company->updated_at->format('g:i A')}}

       </div>
       
       <div class="card-body col-md-12 col-lg-4">

           <strong>Status:</strong>
    
           {{ ($company->status == '1') ? 'Active': 'In-Active' }}

       </div>
       
       
       

       </div>
    
   </div>
</div>
@stop