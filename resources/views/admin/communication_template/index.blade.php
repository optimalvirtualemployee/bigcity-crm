@extends('common.default')
@section('title', 'Company List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
       
           
          
              <div class="sub-headeing">
 
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Communication Template</h3>
                    
             
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                        <a href="{{url('admin/communication-template/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Communication Template
                     </button></a>
                
                        
                        <a  href="{{url('admin/communication_template_type/dashboard')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
  
</div>
          
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card shadow ">
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  <li style="margin-right: 20px;">
                   
                  </li>
                 
                 
               </ul>
            </nav>
         </div>
      
         <div class="card-body" >
                @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                         <th>SN</th>
                         <th>Communication Template Id</th>
                        <th>Campaign ID</th>
                        <th>Campaign Name</th>
                        <th>Product Id</th>
                        <th>Product Name</th>
                        
                        <th>Subject</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                        
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($comm_temp as $index=> $company)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$company->id}}</td>
                        <td>{{$company->campaign->id}}</td>
                        <td>{{$company->campaign->campaign_name}}</td>
                        <td>{{$company->products->id}}</td>
                        <td>{{$company->products->name}}</td>
                        <td>{{$company->subject}}</td>
                        <td>{{isset($company->created_user['name']) ? $company->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($company->updated_user['name']) ? $company->updated_user['name'] : 'NA'}}</td>
                        <td>{{$company->created_at->format('d M Y')}}/{{$company->created_at->format('g:i A')}}</td>
                        <td>{{$company->updated_at->format('d M Y')}}/{{$company->updated_at->format('g:i A')}}</td>
                        <td>
                           <a href="{{ route('communication-template.show',$company->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           <a href="{{ route('communication-template.edit',$company->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        </td>
                        <td>
                            
                              <input data-id="{{$company->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $company->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="CommunicationTemplate">  
                        </td>
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>

@stop

