@extends('common.default')
@section('title', 'Edit Category')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
         
        
            <div class="sub-headeing">
 
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Communication Template </h3>
                    
             
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                      
                
                        
                        <a  href="{{url('admin/communication-template')}}" class="btn btn-primary btn-sm   js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
  
</div>

   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card login-card" style="width: 70%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('communication-template.update', $com_temp->id) }}">
                           @method('PATCH') 
                           @csrf
                        
                           
                           <div class="row">
                                <div class="col-md-6">
                                    <label>Select Campaign</label>
                                    <select class="form-control select2" name="campaign_id" onchange="get_genproductList_fn(this.value);">
                                      <option value="0">Select Campaign</option>
                                      @forelse($campaign as $camp)
                                      <option value="{{$camp->id}}" {{$com_temp->campaign_id == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                                      @empty
                                        <p>No Product List</p>
                                      @endforelse
                                    </select>
                                </div>

                                <div id="campaignMapped_productList" class="col-md-6" style="margin-top:27px">
                                    <select class="form-control form-control-user select_box" name="product_id" onchange="get_productList_fn(this.value);">
                                      <option value="0">Select Campaign</option>
                                      @forelse($product as $camp)
                                      <option value="{{$camp->id}}" {{$com_temp->product_id == $camp->id ? 'Selected' : ''}}>{{$camp->name}}</option>
                                      @empty
                                        <p>No Product List</p>
                                      @endforelse
                                    </select>
                                </div><br>
                            </div>
                            
                            <br>
                             <div class="row">
                                <div class="col-md-6">
                                    <label>Select Communication Template Type</label>
                                    <select class="form-control select2" name="communication_template_type_id">
                                      <option value="0">Select Communication Template Type</option>
                                      @forelse($communication_template_type as $ctt)
                                      <option value="{{$ctt->id}}" {{ $com_temp->communication_template_type_id == $ctt->id ? 'Selected' : ''}}>{{$ctt->name}}</option>
                                      @empty
                                        <p>No Communication Template Type List</p>
                                      @endforelse
                                    </select>
                                </div>
                            </div>
                            <br>
                            
                            <div class="form-group">
                              <label for="name" class="mr-sm-2">Subject:</label>
                              <input type="text" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Subject Name" name="subject" value="{{ $com_temp->subject}}">
                           </div>
                           <div class="form-group">
                              <label for="name" class="mr-sm-2">Template Content:</label>
                              <textarea class="ckeditor form-control" name="template_content" value="{{$com_temp->template_content}}">{{$com_temp->template_content}}</textarea>
                           </div>
                           <div class="form-group">
                              <label for="name" class="mr-sm-2">SMS:</label>
                              <textarea  class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter SMS" name="sms" value="{{ $com_temp->sms }}">{{ $com_temp->sms }}</textarea>
                           </div>
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop
 