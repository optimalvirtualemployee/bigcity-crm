@extends('common.default')
@section('title', 'Show Company')
@section('content')
<style>
strong {
    display: inline-block;
    width: 60%;

}
strong::after { 
  
    content: ' \25B6';
    color: #106db2;
    position: absolute;
    right: 0px;
    width: 45%;
    margin-top: 0px;
}
</style>
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
       
          
          
              <div class="sub-headeing">
 
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                
              <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Communication Details </h3>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a  href="{{url('admin/communication-template')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
            </div>
      
      
      
      
      
      
      
      
       <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card login-card" style="width: 70%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
      
      
      
        <div class="form-group">
      <div class="card-body col-md-12 col-lg-12">

           <strong>Communication Template ID </strong>
    
           {{$com_temp->id}}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Campaign Id </strong>
    
           {{ $com_temp->campaign->id }}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Campaign Name </strong>
    
           {{ $com_temp->campaign->campaign_name }}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Product Id </strong>
    
           {{ $com_temp->products->id }}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Product Name </strong>
    
           {{ $com_temp->products->name }}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Subject </strong>
    
           {{ $com_temp->subject }}

       </div>
       @if($com_temp->sms)
       <div class="card-body col-md-12 col-lg-12">

           <strong>SMS:</strong>
    
           {{ $com_temp->sms }}

       </div>
       @endif
       @if($com_temp->template_content)
       <div class="card-body col-md-12 col-lg-12">

           <strong>Template Content </strong>
    
           {{ $com_temp->template_content }}

       </div>
       @endif
       
       
       
       
       <div class="card-body col-md-12 col-lg-12">

           <strong>Created By </strong>
    
           {{isset($com_temp->created_user['name']) ? $com_temp->created_user['name'] : 'NA'}}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Updated By </strong>
    
           {{isset($com_temp->updated_user['name']) ? $com_temp->updated_user['name'] : 'NA'}}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Created Date/Time </strong>
    
           {{$com_temp->created_at->format('d M Y')}}/{{$com_temp->created_at->format('g:i A')}}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Updated Date/Time </strong>
    
           {{$com_temp->updated_at->format('d M Y')}}/{{$com_temp->updated_at->format('g:i A')}}

       </div>
       
       <div class="card-body col-md-12 col-lg-12">

           <strong>Status </strong>
    
           {{ ($com_temp->status == '1') ? 'Active': 'In-Active' }}

       </div>
       
       
       

       </div>
     </div>
      </div>
       </div>
        </div>
         </div>
          </div>
   </div>
</div>
@stop