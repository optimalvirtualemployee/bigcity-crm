@extends('common.default')
@section('title', 'Campaign Detail')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
 
      
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">
                        Campaign Detail List</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                       <a href="{{url('admin/campaign_details/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Campaign Detail
                     </button></a>
                        
                        <a  href="{{url('admin/campaign/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
     
     <br>
        
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card">
     

        <div class="card-body" >
                    @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
        @endif
          <div class="table-responsive">
            <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
              <thead>
                <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                  <th >SN</th>
                  <th>Campaign Id</th>
                  <th>Campaign Name</th>
                  <th>Promo Type</th>
                  <th>Created By</th>
                    <th>Updated By</th>
                    <th>Created Date/Time</th>
                    <th>Updated Date/Time</th>
                  <!--<th>No of Products<br>(For Multi Voucher)</th>
                  <th>Product Name</th>
                  <th>Product Rule Name <br>(if Defined)</th>-->
                  <!--<th>Mob No Usage Rule</th>-->
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1 ; ?>
                @forelse($campaignDetails as $cd)
                  <tr>
                    <td>{{$i}}</td>
                    <td>{{$cd->campaign['id']}}</td>
                    <td>{{$cd->campaign['campaign_name']}}</td>
                    <td>{{$cd->promo_type['name']}}</td>
                    <td>{{isset($cd->created_user['name']) ? $cd->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($cd->updated_user['name']) ? $cd->updated_user['name'] : 'NA'}}</td>
                        <td>{{$cd->created_at->format('d M Y')}}/{{$cd->created_at->format('g:i A')}}</td>
                        <td>{{$cd->updated_at->format('d M Y')}}/{{$cd->updated_at->format('g:i A')}}</td>
                    <!--<td>{{isset($cd->no_of_products) ? $cd->no_of_products : "None" }}</td>
                    <td>{{$cd->products['name']}}</td>
                    <td>{{isset($cd->product_rule_id) ? $cd->product_rules['product_rule_name'] : "None" }}</td>
                    <td>{{$cd->mobile_no_usage['name']}}</td>-->
                    <td style="width: 200px;">
                      <a href="{{ route('campaign_details.show',$cd->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                      <a href="{{ route('campaign_details.edit',$cd->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                      
                    </td>
                    
                  </tr>
                  <?php $i++; ?>
                  @empty
                    <p>No Product List</p>
                  @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- End of Main Content -->
@stop
