@extends('common.default')
@section('title', 'Campaign Detail')
@section('content')
<?php
$selected = explode(",", $camp_detail->product_id);
$selectedMobile = explode(",", $camp_detail->mobile_no_usage_rules);
$selectedMobileValue = explode(",", $camp_detail->mobile_usage_value);
?>
<style>
span.btn.btn-primary.btn-sm.select-all {
    margin-top: 10px;
    margin-bottom: 10px;
}
</style>
  <!-- Main Content -->
  <div id="content">
    <div class="container-fluid" style="margin-top:15px;">
      
      
        <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Campaign Detail</h3>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/campaign_details')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        </div>
        </div>
            
      <div class="container">
      <!-- Outer Row -->
        <div class="row justify-content-center mt-3">
          <div class="card" style="width: 100%;">
            <div class="card-body p-0">
              <!-- Nested Row within Card Body -->
              <div class="row">
                <div class="col-lg-12">
                  <div class="p-5">
                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div> 
                    @endif
                    <div id="error_msg"></div>
                    <form id="campaignDetail_frm" action="{{ route('campaign_details.update',$camp_detail->id) }}" method="POST">
                      @method('PATCH')
                      @csrf
                      <div class="col-md-12 row">
                            <div class="col-md-6">
                           
                                <label for="" class="mr-sm-2">Select Campaign</label>
                                <select   name="campaign_id" class="inputbox form-control required">
                                  <option value="">Select Campaign</option>
                                  @forelse($campaigns as $camp)
                                    <option value="{{$camp->id}}" {{ $camp_detail->campaign_id == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                                  @empty
                                    <p>No Campaign List</p>
                                  @endforelse
                                </select>
                            </div>

                            <div class="col-md-6">
                        
                                <label for="" class="mr-sm-2">Select Promo Type</label>
                                <select   name="promo_type_id" class="inputbox form-control required">
                                  <option value="">Select Promo Type</option>
                                  @forelse($promo_type as $pt)
                                    <option value="{{$pt->id}}" {{ $camp_detail->promo_type_id == $pt->id ? 'Selected' : ''}}>{{$pt->name}}</option>
                                  @empty
                                    <p>No Promo Type List</p>
                                  @endforelse
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-12 row">
                            <div class="col-md-6">
                                
                                <label for="" class="mr-sm-2">Select Products
                                <span class="btn btn-primary btn-sm select-all">Select all</span>
                                    <span class="btn btn-primary btn-sm deselect-all">Deselect all</span></label> 
                                <select   name="product_id[]" class="inputbox form-control select2 required" multiple>
                                  <option value="">Select Products</option>
                                  
                                  @forelse($products as $pro)
                                    <option value="{{$pro->id}}" {{ (in_array($pro->id, $selected)) ? 'selected' : '' }}>{{$pro->name}}</option>
                                  @empty
                                    <p>No Product List</p>
                                  @endforelse
                                </select>
                            </div>
                            
                            <div class="col-md-6 mt-4">
                                <label for="" class="mr-sm-2 ">No of Reward Redeemable</label>
                                <input type="text"  name="no_of_reward_redeem" class="form-control required" value="{{$camp_detail->no_of_reward_redeem}}" placeholder="No. of Reward Redeemable">
                            </div>
                        </div>
                     
                         
                        <div class="col-md-12 mt-5" id="dayschecked">                    
                            
                          
                            <label>Set Mobile Number & Email ID Usage Rule</label>
                            <div class="col-md-6 mt-2 ml-2">
                            @foreach($mobile_no_usage as $i=>$mnu)       
                            <input type="checkbox" id="voucherperday_{{$mnu->id}}" name="mobile_no_usage_rules[]" class="form-check-input required" value="{{$mnu->id}}" {{(in_array($mnu->id, $selectedMobile)) ? 'checked' : '' }}>
                            <label for="voucherperday" style="font-weight: normal;"><input type="text" class="voucherperday_{{$mnu->id}} latFeild" id="voucherperday_{{$mnu->id}}" name="mobile_usage_value[]" style="width: 50px;" value="{{trim($selectedMobileValue[$i])}}"
                            > {{$mnu->name}}</label> 
                            @endforeach
                          
                            </div>
                            
                        </div>
                      
                        <a href="#" class="ml-3">
                            <button type="submit"  class="btn btn-primary mt-4">Update</button>
                        </a>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Main Content -->
  @stop