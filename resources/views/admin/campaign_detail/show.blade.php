@extends('common.default')
@section('title', 'Campaign Detail')
@section('content')
<?php

$selectedMobileValue = explode(",", $campaign_detail->mobile_usage_value);
?>
<div id="content">
   <div class="container-fluid" style="margin-top: 15px;">
     
      
        <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Campaign Detail</h3>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/campaign_details')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        </div>
        </div>
        
        
        
        
           <div class="container" style="margin-top:50px;">
            <!-- Outer Row -->
            <div class="row justify-content-center">
            <div class="card login-card" style="width: 70%;">
            <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
            <div class="col-lg-12">
            
            
            
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Campaign ID </p>
            
            {{ $campaign_detail->campaign_id }}
            
            </div>
            
            
             <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Campaign Name</p>
            
             {{ $campaign_detail->campaign->campaign_name }}
            
            </div>
            
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Promo Type Id  </p>
            
             {{ $campaign_detail->promo_type_id }}
            
            </div>
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Promo Type Name </p>
            
           {{ $campaign_detail->promo_type->name}}
            
            </div>
            
               <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Number of Reward Redeemable </p>
            
            {{ $campaign_detail->no_of_reward_redeem }}
            
            </div>
            
            
            
            
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
            
            
  
      
      
      <div class="form-group row">
        <div class="card-body col-md-12 col-lg-6">

            <table class="table table-bordered"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                <thead>
                    <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>Mobile Number Usage Rule Id</th>
                        
                        <th>Mobile Number Usage Rule Value</th>
                    

                     
                    </tr>
                </thead>
                <tbody style="text-align: center">
                @foreach(explode(",", $campaign_detail->mobile_no_usage_rules) as $i=>$list)
                    <tr>
                        <td>{{$list}} </td>
                        
                        <td>{{$selectedMobileValue[$i]}} </td>
                        
                   
                   </tr>
               @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-body col-md-12 col-lg-6">
            <table class="table  table-bordered"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                <thead>
                    <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>Product Id</th>
                       
                        
                    </tr>
                </thead>
                <tbody style="text-align: center">
                @foreach(explode(",", $campaign_detail->product_id) as $pro)
                    <tr>
                        <td>{{$pro}} </td>
                        
                        
                    </tr>
               @endforeach
               </tbody>
            </table>

        </div>
       </div>
    </div>
  </div>
@stop


