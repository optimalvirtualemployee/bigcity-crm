@extends('common.default')
@section('title', 'Campaign List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
       
      
        <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Campaign</h3>
            
        </div>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                
              <a href="{{url('admin/campaigns/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Campaign
                     </button></a>
                
                <a  href="{{url('admin/campaign/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                    <i class="fas fa-chevron-left mr-1"></i> Back
                </a>
                 
            </span>
        </div>
        </div>
        </div>
        </div>
        
        
    <br>
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card">
        
     
         <div class="card-body" >
                 @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>Campaign ID</th>
                        <th>Campaign Name</th>
                        <th>Brand Name</th>
                        <th>Campaign Type</th>
                        <th>BD Person Name</th>
                        <th>CS Person Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                        
                        
                        
                        <!-- <th>Status</th> -->
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($campaignlist as $index=>$cl)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        
                        <td>{{$cl->id}}</td>
                        <td>{{$cl->campaign_name}}</td>
                        <td>{{$cl->company['brand_name']}}</td>
                        <td>{{$cl->campaign_type->name}}</td>
                        <td>{{$cl->bd_user['name']}}</td>
                        <td>{{$cl->cs_user['name']}}</td>
                        <td>{{isset($cl->created_user['name']) ? $cl->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($cl->updated_user['name']) ? $cl->updated_user['name'] : 'NA'}}</td>
                        <td>{{$cl->created_at->format('d M Y')}}/{{$cl->created_at->format('g:i A')}}</td>
                        <td>{{$cl->updated_at->format('d M Y')}}/{{$cl->updated_at->format('g:i A')}}</td>
                        
                        
                        
                        <td>
                           <a href="{{ route('campaigns.show',$cl->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('campaigns.edit',$cl->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                           <!--<span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                           <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span> -->
                        </td>
                        <!-- <td>
                          <input data-id="{{$cl->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $cl->status ==1 ? 'checked' : '' }}>
                        </td>  -->
                        <td>
                            
                              <input data-id="{{$cl->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $cl->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="Campaign">  
                        </td>
                    </tr>
                    @empty
                    <p>No Campaign List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
