@extends('common.default')
@section('title', 'Create Campaign')
@section('content')
 
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
             
        
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Campaign</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/campaigns')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
        
        
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center mt-5">
         <div class="card login-card" style="width:100%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                      
                      
          @if($errors->any())
            @foreach($errors->all() as $error)
              <div class="alert alert-danger">
                {{$error}}
              </div>
            @endforeach
          @endif
                  
          
           
           
              
              <div class="Campaign-button">
              <br>
              <form id="campaignfrm" class="form-horizontal campaign-form" action="{{ route('campaigns.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                
                
                <div class="form-group row mx-auto">
               
                    <div class="col-md-12 col-lg-6">
                       <label for="name" class="mr-sm-2">Campaign Name</label>
                      <input type="text" name="campaign_name" placeholder="Enter Campaign Name" class="form-control " value="{{ old('campaign_name') }}">
                    </div>
                    
                    
                    <div class="col-md-12 col-lg-6">
                     <label for="name" class="mr-sm-2">Select Campaign Brand</label>
                      @csrf
                    <select class="form-control" name="campaign_brand_id">
                      <option value="0">Select Brand</option>
                      @forelse($companies ?? '' as $comp)

                        <option value="{{$comp->id}}" {{old('campaign_brand_id') == $comp->id ? 'Selected' : ''}}>{{$comp->brand_name}}</option>
                        @empty
                          <p>No Brand List</p>
                      @endforelse
                    </select>
                    </div>
                    
                </div>
                
                
                <div class="form-group row mx-auto">
                 
                  <div class="col-md-12 col-lg-6">
                      <label for="name" class="mr-sm-2">Select Campaign Type</label>
                    @csrf
                    <select class="form-control" name="campaign_type_id">
                      <option value="0">Select Campaign Type</option>
                      @forelse($campaign_type ?? '' as $ct)
                        <option value="{{$ct->id}}" {{old('campaign_type_id') == $ct->id ? 'Selected' : ''}}>{{$ct->name}}</option>
                        @empty
                          <p>No Campaign Type List</p>
                      @endforelse
                    </select>
                  </div>
                
                  <div class="col-md-12 col-lg-3">
                      <label for="name" class="mr-sm-2">Estimate No</label>
                    <input type="text" name="campaign_estimate_no" placeholder="Enter Estimate No" class="form-control" value="{{ old('campaign_estimate_no') }}">
                  </div>
                 
                  <div class="col-md-12 col-lg-3">
                      <label for="name" class="mr-sm-2">Estimate date</label>
                    <input type="date" name="campaign_estimate_date" placeholder="Enter Estimate date" class="form-control" value="{{ old('campaign_estimate_date') }}">
                  </div>
                </div>
                
                
                <div class="form-group row mx-auto">
            
                  <div class="col-md-12 col-lg-6">
                     <label for="name" class="mr-sm-2">Select BD Person Name </label>
                    @csrf
                    <select class="form-control" name="campaign_bd_person_name_id">
                      <option value="0">Select BD Person Type</option>
                      @forelse($bd_person_name ?? '' as $bpn)
                        <option value="{{$bpn->id}}" {{old('campaign_bd_person_name_id') == $bpn->id ? 'Selected' : ''}}>{{$bpn->name}}</option>
                        @empty
                          <p>No BD Person Name List</p>
                      @endforelse
                    </select>
                  </div>
                
                  <div class="col-md-12 col-lg-6">
                    <label for="name" class="mr-sm-2">Select CS Person Name </label>
                    @csrf
                    <select class="form-control" name="campaign_cs_person_name_id">
                      <option value="0">Select CS Person Type</option>
                      @forelse($cs_person_name ?? '' as $csn)
                        <option value="{{$csn->id}}" {{old('campaign_cs_person_name_id') == $csn->id ? 'Selected' : ''}}>{{$csn->name}}</option>
                        @empty
                          <p>No CS Person Name List</p>
                      @endforelse
                    </select>
                  </div>
                </div>
                
                
                <div class="form-group row mx-auto">
                  
                  <div class="col-md-12 col-lg-6">
                     <label for="name" class="mr-sm-2">Gateway Details </label>
                    <textarea class="form-control" name="campaign_gateway_details" rows="2" placeholder="Details">{{ old('campaign_gateway_details') }}</textarea>
                  </div>
               
                  <div class="col-md-12 col-lg-6">
                    <label for="name" class="mr-sm-2">Select Mechanic Type</label>
                    @csrf
                    <select class="form-control" id="mt" name="campaign_mechanic_type_id" onchange="mt_function(this.value);">
                      <option value="0">Select Mechanic Type</option>
                      @forelse($mechanic_type ?? '' as $mt)
                        <option value="{{$mt->id}}" {{old('campaign_mechanic_type_id') == $mt->id ? 'Selected' : ''}}>{{$mt->name}}</option>
                        @empty
                        <p>No Mechanic Type List</p>
                      @endforelse
                    </select>
                  </div>
                </div>
                
                
                <div class="form-group row mx-auto" id="sms" style="display: none;">
             
                <div class="col-md-12 col-lg-6">
                 <label for="name" class="mr-sm-2">SMS Keyword </label>
                  <input type="text" class="form-control" name="sms_keyword" placeholder="Enter Keyword" value="{{ old('sms_keyword') }}">
                </div>
               
                  <div class="col-md-12 col-lg-6">
                    <label for="name" class="mr-sm-2">long code</label>
                    <input type="text" class="form-control" name="sms_long_code" placeholder="Enter long code" value="{{ old('sms_long_code') }}">
                  </div>
                </div>
                
                

                <div class="form-group row mx-auto" id="web" style="display: none;">
                
                  <div class="col-md-12 col-lg-6">
                      <label class="">Microsite Name</label>
                      
                    <input type="text" class="form-control" name="web_microsite_name" placeholder="Enter Microsite Name" value="{{ old('web_microsite_name') }}">
                  </div>
                </div> 



                <div class="form-group row mx-auto">
                 
                  <div class="col-md-12 col-lg-6">
                   <label class=""> Select Voucher Type</label>
                  @csrf
                    <select class="form-control" id="vt" name="campaign_voucher_type_id" onchange="vt_function(this.value);">
                      <option value="0">Select voucher type</option>
                        @forelse($voucher_type as $vt)
                          <option value="{{$vt->id}}" {{old('campaign_voucher_type_id') == $vt->id ? 'Selected' : ''}}>{{$vt->name}}</option>
                          @empty
                          <p>No Voucher Type List</p>
                        @endforelse
                    </select>
                  </div>

                  <div class="col-md-12 col-lg-6" style="display: none;" id="voucher_spec">
                
                    <div class="col-md-12 col-lg-12">
                    <label class="">Select Voucher Type Specification</label>
                      @csrf
                      <select class="form-control v_spec" name="voucher_spec_type_id">
                        <option value="0">Select Voucher Type Specification</option>
                        @forelse($voucher_spec as $vs)
                          <option value="{{$vs->id}}" {{old('voucher_spec_type_id') == $vs->id ? 'Selected' : ''}}>{{$vs->size_name}}</option>
                          @empty
                          <p>No Voucher Specification</p>
                        @endforelse
                      </select>
                    </div>
                  </div>
                </div>



                <div class="form-group row mx-auto">
              
                  <div class="col-md-12 col-lg-6">
                    <label class="">Printing Bigcity</label>
                    <select name="is_printing_bigcity" id="is_printing_bigcity" class="form-control" onchange="printing(this.value);">
                      <option value="">Select Option</option>
                      <option value="1" {{old('is_printing_bigcity') == '1' ? 'Selected' : ''}}>Yes</option>
                      <option value="2" {{old('is_printing_bigcity') == '2' ? 'Selected' : ''}}>No</option>
                    </select>
                  </div> 
                </div>
                
                <div class="form-group row mx-auto" style="display: none;" id="is_printing_done_by_bigcity_yes_show">
                  
                  <div class="col-md-12 col-lg-4">
                      <label class="">Voucher Cost</label>
                    <input type="text" class="form-control" name="campaign_voucher_cost" placeholder="Enter Voucher Cost" value="{{ old('campaign_voucher_cost') }}">
                  </div>
                  
                  <div class="col-md-12 col-lg-4">
                    <label class="">Cost To Client</label>
                    <input type="text" class="form-control" name="campaign_cost_to_client" placeholder="Cost to Client" value="{{ old('campaign_cost_to_client') }}">
                  </div>
                  
                  <div class="col-md-12 col-lg-4">
                      <label class="">Cost To BigCity</label>
                    <input type="text" class="form-control" name="campaign_cost_to_bigcity" placeholder="Cost to BigCity" value="{{ old('campaign_cost_to_bigcity') }}">
                  </div>
                </div>
                <!--
                <div class="col-md-12 col-lg-12">
                 <label class=""></label>
                  <div class="form-group row mx-auto">
                 
                  <input type="file" class="form-control-file col-md-12 col-lg-3"  >
                </div>
                </div>
                -->
                
                 <div class="form-group row mx-auto">
                      <div class="col-md-12 col-lg-6">
                     <label for="exampleFormControlFile1">Upload Signed Estimate PO</label>
                     <input type="file" class="form-control-file col-md-12 col-lg-6" name="campaign_upload_signed_estimate_po" style="margin-left: -10px;">
                     </div>
                  </div>
                
                  <div class="col-md-12 text-right">
                    <input type="button" onclick="campaignbtn();" class="btn btn-primary bigcity-button " value="Submit">
                  </div>
                
                
              
            </div>
          
            
         
       
                      
                      
                      
                      
                      
                      
                      
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>





  
  <script type="text/javascript">
    
</script>
@stop




