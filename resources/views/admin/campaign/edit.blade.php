@extends('common.default')
@section('title', 'Create Campaign')
@section('content')
<style>
label {
    margin-top: 20px;
}
</style>

<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
          
        
         <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Campaign</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/campaigns')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center mt-5">
         <div class="card login-card">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                            
          @if($errors->any())
            @foreach($errors->all() as $error)
              <div class="alert alert-danger">
                {{$error}}
              </div>
            @endforeach
          @endif
                  
          
         
            <div class="Campaign-button">
              
              <form id="campaignfrm" class="form-horizontal campaign-form" action="{{ route('campaigns.update', $campaign->id) }}" method="post" enctype="multipart/form-data">
               @method('PATCH') 
                @csrf
                <div class="form-group row mx-auto">
                 
                    <div class="col-md-12 col-lg-6">
                       <label class="">Campaign Name</label>
                      <input type="text" name="campaign_name" placeholder="Enter Campaign Name" class="form-control " value="{{$campaign->campaign_name}}">
                    </div>
                   
                    <div class="col-md-12 col-lg-6">
                     <label class="">Brand</label>
                      @csrf
                    <select class="form-control" name="campaign_brand_id">
                      <option value="0">Select Type</option>
                      @forelse($companies ?? '' as $comp)
                        <option value="{{$comp->id}}" {{$comp->id == $campaign->campaign_brand_id  ? 'selected' : ''}}>{{$comp->brand_name}}</option>
                        @empty
                          <p>No Brand List</p>
                      @endforelse
                    </select>
                    </div>
                </div>
                
                
                
                <div class="form-group row mx-auto">
                 
                  <div class="col-md-12 col-lg-6">
                       <label class="">Campaign Type</label>
                   
                    <select class="form-control" name="campaign_type_id">
                      <option value="0">Select Campaign Type</option>
                      @forelse($campaign_type ?? '' as $ct)
                        <option value="{{$ct->id}}" {{$ct->id == $campaign->campaign_type_id  ? 'selected' : ''}}>{{$ct->name}}</option>
                        @empty
                          <p>No Campaign Type List</p>
                      @endforelse
                    </select>
                  </div>
                  
                 
                  <div class="col-md-12 col-lg-6">
                     <label class="">Estimate No.</label>
                    <input type="text" name="campaign_estimate_no" placeholder="Enter Est. No." class="form-control" value="{{$campaign->campaign_estimate_no}}">
                  </div>
                  
                 
                  <div class="col-md-12 col-lg-6">
                      <br>
                      <label class="">Estimated Date</label>
                    <input type="date" name="campaign_estimate_date" placeholder="Enter Estimate date" class="form-control" value="{{$campaign->campaign_estimate_date}}">
                  </div>
                  
                </div>
                
                
                
                <div class="form-group row mx-auto">
               
                  <div class="col-md-12 col-lg-6">
                    <label class="">BD Person Name</label>
                    
                    <select class="form-control" name="campaign_bd_person_name_id">
                      <option value="0">Select BD Person Type</option>
                      @forelse($bd_person_name ?? '' as $bpn)
                        <option value="{{$bpn->id}}" {{$bpn->id == $campaign->campaign_bd_person_name_id  ? 'selected' : ''}}>{{$bpn->name}}</option>
                        @empty
                          <p>No BD Person Name List</p>
                      @endforelse
                    </select>
                  </div>
                  
                  <div class="col-md-12 col-lg-6">
                    <label class="">CS Person Name</label>
                    
                    <select class="form-control" name="campaign_cs_person_name_id">
                      <option value="0">Select CS Person Type</option>
                      @forelse($cs_person_name ?? '' as $csn)
                        <option value="{{$csn->id}}" {{$csn->id == $campaign->campaign_cs_person_name_id  ? 'selected' : ''}}>{{$csn->name}}</option>
                        @empty
                          <p>No CS Person Name List</p>
                      @endforelse
                    </select>
                  </div>
                </div>
                <div class="form-group row mx-auto">
               
                  <div class="col-md-12 col-lg-6">
                     <label class="">Gateway Details</label>
                    <textarea class="form-control" name="gateway_details" rows="2" placeholder="Details">{{$campaign->campaign_gateway_details}}</textarea>
                  </div>
                 
                  <div class="col-md-12 col-lg-6">
                     <label class="">Mechanic Type</label>
                    
                    <select class="form-control" id="mt" name="campaign_mechanic_type_id" onchange="mt_function(this.value);">
                      <option value="0">Select Type</option>
                      @forelse($mechanic_type ?? '' as $mt)
                        <option value="{{$mt->id}}" {{$mt->id == $campaign->campaign_mechanic_type_id  ? 'selected' : ''}}>{{$mt->name}}</option>
                        @empty
                        <p>No Mechanic Type List</p>
                      @endforelse
                    </select>
                  </div>
                </div>
                <div class="form-group row mx-auto" id="sms" style="display: none;">
               
                <div class="col-md-12 col-lg-6">
                   <label class="">Keyword</label>
                  <input type="text" class="form-control" name="sms_keyword" placeholder="Enter Keyword" value="{{ $campaign->sms_keyword }}">
                </div>
               
               
                  <div class="col-md-12 col-lg-6">
                      <label class="">Long Code</label>
                    <input type="text" class="form-control" name="sms_long_code" placeholder="Enter long code" value="{{ $campaign->sms_long_code }}">
                  </div>
                </div>

                <div class="form-group row mx-auto" id="web" style="display: none;">
              
                  <div class="col-md-12 col-lg-6">
                    <label class="">Microsite Name</label>
                    <input type="text" class="form-control" name="web_microsite_name" placeholder="Enter Microsite Name" value="{{ $campaign->web_microsite_name }}">
                  </div>
                </div> 

                <div class="form-group row mx-auto">
                  
                  <div class="col-md-12 col-lg-6">
                  <label class="">Voucher Type</label>
                  
                    <select class="form-control" id="vt" name="campaign_voucher_type_id" onchange="vt_function(this.value);">
                      <option value="0">Select Type</option>
                        @forelse($voucher_type  as $vt)
                          <option value="{{$vt->id}}" {{$vt->id == $campaign->campaign_voucher_type_id  ? 'selected' : ''}}>{{$vt->name}}</option>
                          @empty
                          <p>No Voucher Type List</p>
                        @endforelse
                    </select>
                  </div>

                  <div class="col-md-12 col-lg-6" style="display: none;" id="voucher_spec">
                   
                    <div class="col-md-12 col-lg-12">
                     <label class="">Voucher Specification</label>
                      
                      <select class="form-control v_spec" name="voucher_spec_type_id">
                        <option value="0">Select Type</option>
                        @forelse($voucher_spec as $vs)
                          <option value="{{$vs->id}}" {{$vs->id == $campaign->voucher_spec_type_id  ? 'selected' : ''}}>{{$vs->size_name}}</option>
                          @empty
                          <p>No Voucher Specification</p>
                        @endforelse
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group row mx-auto">
                  
                  <div class="col-md-12 col-lg-6">
                    <label class="">Is Printing done by Bigcity ?</label>
                    <select name="is_printing_bigcity" id="is_printing_bigcity" class="form-control" onchange="printing(this.value);">
                      <option value="">Select Option</option>
                      <option value="1" {{$campaign->is_printing_bigcity == 1  ? 'selected' : ''}}>Yes</option>
                      <option value="2" {{$campaign->is_printing_bigcity != 1  ? 'selected' : ''}}>No</option>
                    </select>
                  </div> 
                </div>
                
                <div class="form-group row mx-auto" style="display: none;" id="is_printing_done_by_bigcity_yes_show">
                  <label class="col-md-12 col-lg-2">Voucher Cost</label>
                  <div class="col-md-12 col-lg-2">
                    <input type="text" class="form-control" name="campaign_voucher_cost" placeholder="Enter Voucher Cost" value="{{ $campaign->campaign_voucher_cost }}">
                  </div>
                  <label class="col-md-12 col-lg-2">Cost To Client</label>
                  <div class="col-md-12 col-lg-2">
                    <input type="text" class="form-control" name="campaign_cost_to_client" placeholder="Cost to Client" value="{{ $campaign->campaign_cost_to_client }}">
                  </div>
                  <label class="col-md-12 col-lg-2">Cost To BigCity</label>
                  <div class="col-md-12 col-lg-2">
                    <input type="text" class="form-control" name="campaign_cost_to_bigcity" placeholder="Cost to BigCity" value="{{ $campaign->campaign_cost_to_bigcity }}">
                  </div>
                </div>
               
                @if($campaign->campaign_upload_signed_estimate_po)
                  <div class="form-group row mx-auto">
                     <img class="col-md-12 col-lg-4" src="{{url('/assets/file/signed-estimate-po/',$campaign->campaign_upload_signed_estimate_po)}}" height="60px" width="60px">
                  </div> 
               @endif
               
                 <div class="form-group row mx-auto">
                      <div class="col-md-12 col-lg-6">
                     <label for="exampleFormControlFile1">Upload File</label>
                     <input type="file" class="form-control-file col-md-12 col-lg-6" id="exampleFormControlFile1" name="campaign_upload_signed_estimate_po">
                     </div>
                  </div>
                <div class="form-group row">
                  <div class="col-md-12 col-lg-12">
                    <input type="button" onclick="campaignbtn();" class="btn btn-primary bigcity-button float-right" value="Update">
                  </div>
                </div>
              </form>
            </div>
         
        </div>
                      
                      
                      
                      
                      
                      
                      
                      
                      
                 
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
 
  
   
@stop




