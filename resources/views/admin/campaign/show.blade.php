@extends('common.default')
@section('title', 'Show Product')
@section('content')
<div id="content">
<div class="container-fluid" style="margin-top:15px;">
         
        
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Campaign</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/campaigns')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
        
        
             <table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e;font-size:12px"  >
                         <th>ID</th>
                        <th >Campaign Name</th>
                        <th>Brand Name</th>
                        <th>Campaign Type</th>
                        <th>BD Person Name</th>
                        <th >CS Person Name</th>
                        <th>Estimate No</th>
                        <th>Estimate Date:</th>
                        <th>Mechanic Type</th>
                        <th>Gateway Details</th>
                        <th>Keyword</th>
                        <th>Long Code</th>
                        <th>Microsite Name</th>
                        <th>Voucher Type</th>
                        <th>Voucher Specification</th>
                        <th>Voucher Cost</th>
                        <th>Cotst To Client</th>
                        <th>Cost To BigCity</th>
                        <th>Signed estimate PO</th>
                     </tr>
                  </thead>
                  <tbody style="text-align: center;text-transform:capitalize;">
                     <tr>
                         <td>{{ $campaigns->id }} </td>
                        <td>{{ $campaigns->campaign_name }} </td>
                        <td>{{ $campaigns->company['brand_name'] }} </td>
                        <td>{{ $campaigns->campaign_type->name}} </td>
                        <td>{{ $campaigns->bd_user['name'] }} </td>
                        <td>{{ $campaigns->cs_user['name'] }} </td>
                        <td>{{ $campaigns->campaign_estimate_no }} </td>
                        <td>{{ $campaigns->campaign_estimate_date }}  </td>
                        <td>{{ $campaigns->delivery_mechanic_type->name }} </td>
                        <td>{{ $campaigns->campaign_gateway_details }} </td>
                        <td>{{ $campaigns->sms_keyword}}  </td>
                        <td>{{ $campaigns->sms_long_code }} </td>
                        <td>{{ $campaigns->web_microsite_name }}</td>
                        <td>{{ $campaigns->voucher_type->name }}</td>
                        <td>{{ $campaigns->voucher_specification['size_ame'] }}</td>
                        <td>{{ $campaigns->campaign_voucher_cost }}</td>
                        <td>{{ $campaigns->campaign_cost_to_client }}</td>
                        <td> {{ $campaigns->campaign_cost_to_bigcity }}</td>
                        <td> <img src="url('/assets/file/signed-estimate-po/',$cl->campaign_upload_signed_estimate_po)}}" height="60px" width="60px"></td>
                         </tr>
                  </tbody>
             </table>
   </div>
</div>
@stop


