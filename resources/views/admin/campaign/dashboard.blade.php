@extends('common.default')
@section('content')
<style>
   
 .icon i{
      font-size: 50px;
      color: #FFF;
      }
      .icon i:hover{
      font-size: 50px;
      color: #FFF;
       
      transition: .5s; 
      
      }
</style>
<!-- For demo purpose -->


<div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="dahboard-title">Campaign</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


    <div class="container">
 
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/campaigns')}}">
                                             <p class="icon" > <i class="fas fa-handshake"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0   text-white">
                                          Campaign</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/campaigntype')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0  text-white">
                                          Campaign Type</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/companies')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary pb-0">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-cogs"></i></p>
                                    
                                    <h4 class="h6 mb-0   text-white">Company</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                     </div>
                     <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/deliverymechanictype')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary pb-0">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" ><i class="fas fa-lock"></i></p>
                                    
                                    <h4 class="h6 mb-0  text-white">Delivery Machine Type</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/promotype')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-credit-card"></i></p>
                                    
                                    <h4 class="h6 mb-0  text-white">Promo Type</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/campaign_details')}}">
                                             <p class="icon" > <i class="fas fa-info-circle"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0   text-white">
                                          Campaign Details</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/mobile-usage-rules')}}">
                                             <p class="icon" > <i class="fas fa-handshake"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0    text-white">
                                          Mobile No & Usage Rules </h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card bg-gradient-x-primary  pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/tag_campaign_venues')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0   text-white">
                                          Add Campaign Venue Tag</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/generate_codes')}}">
                                             <p class="icon" > <i class="fas fa-code"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0  text-white">
                                          Generate Code</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                  </div>
               </div>
               
               <!--<div class="card shadow mb-4">
                  <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                     <h6 class="m-0 font-weight-bold text-uppercase">Bar Chart</h6>
                  </div>
                  <div class="card-body">
                     <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                     </div>
                  </div>
               </div>-->
            </div>
@stop