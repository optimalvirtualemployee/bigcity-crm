@extends('common.default')
@section('title', 'Product Rule List')
@section('content')
<!-- Main Content -->
<div id="content">
  <div class="container-fluid" style="margin-top:15px;">
        
        <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Add Products Rule</h3>
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a  href="{{url('admin/productrules')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
   <!-- <div class="container-fluid" style="margin-top: 50px;"> -->
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card" style="width: 70%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <div id="error_msg"></div>
                        <div class="d-flex flex-wrap align-items-center justify-content-between ">
                           <div class="redemption d-flex">
                              <!--<h4><strong>Create Product Rule</strong></h4>-->
                           </div>
                        </div>     
                        <form id="productrulefrm" class="user" action="{{ route('productrules.store') }}" method="POST" >
                           @csrf
                                <div class="form-group row mx-auto">
                                    <div class="col-md-12 col-lg-6">
                                        <label for="weekday-mon">Select Product Type</label>
                                    <select class="form-control required select2" name="product_id"  id="product_id">
                                      <option value="">Select Product</option>
                                      @forelse($products as $pro)
                                        <option value="{{$pro->id}}" {{old('product_id') == $pro->id ? 'Selected' : ''}}>{{$pro->name}}</option>
                                      @empty
                                        <p>No Product List</p>
                                      @endforelse
                                    </select>
                                  
                                    </div>
                                    <div class="col-md-12 col-lg-6 weekday"> 
                                      <label for="weekday-mon">Product Rule Title</label>
                                      <input type="text"  class="form-control required" name="title" value="{{ old('title') }}"/>
                                      
                                    </div>
                                </div>
                                  <div class="form-group" id="dayschecked">  
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;">            
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input type="checkbox" class="form-check-input" id="timeslot_1" name="daysavailibility_monday" value="monday" <?php if(isset($timesslotdata) && strlen($timesslotdata->monday)>4 ){ echo 'checked="checked"'; }?>>Monday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_1">
                                        <div class="col-md-6"> 
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="mon_from_time" id="mon_from_time" value="{{old('mon_from_time')}}">
                                        </div>
                                        <div class="col-md-6"> 
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="mon_to_time" id="mon_to_time" value="{{old('mon_to_time')}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                          <input id="timeslot_2" type="checkbox" class="form-check-input" name="daysavailibility_tuesday" value="tuesday" <?php if(isset($timesslotdata) && strlen($timesslotdata->tuesday)>4){ echo 'checked="checked"'; }?>>Tuesday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_2">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="tue_from_time" value="{{old('tue_from_time')}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="tue_to_time" value="{{old('tue_to_time')}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_3" type="checkbox" class="form-check-input" name="daysavailibility_wednesday" value="wednesday" <?php if(isset($timesslotdata) && strlen($timesslotdata->wednesday)>4){ echo 'checked="checked"'; }?>>Wednesday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row"  id="timeslottxt_3">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="wed_from_time" value="{{old('wed_from_time')}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="wed_to_time" value="{{old('wed_to_time')}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_4" type="checkbox" class="form-check-input" name="daysavailibility_thrusday" value="thrusday" <?php if(isset($timesslotdata) && strlen($timesslotdata->thursday)>4){ echo 'checked="checked"'; }?>>Thrusday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_4">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="thus_from_time" value="{{old('thus_from_time')}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="thus_to_time" value="{{old('thus_to_time')}}">
                                        </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_5" type="checkbox" class="form-check-input" name="daysavailibility_friday" value="friday">Friday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row"  id="timeslottxt_5">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="fri_from_time" value="{{old('fri_from_time')}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="fri_to_time" value="{{old('fri_to_time')}}">
                                        </div>
                                        
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_6" type="checkbox" class="form-check-input" name="daysavailibility_saturday" value="saturday" >Saturday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_6">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="sat_from_time" value="{{old('sat_from_time')}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="sat_to_time" value="{{old('sat_to_time')}}">
                                        </div>
                                        
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_7" type="checkbox" class="form-check-input" name="daysavailibility_sunday" value="sunday" >Sunday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_7">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="sun_from_time" value="{{old('sun_from_time')}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="sun_to_time" value="{{old('sun_to_time')}}">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <div class="col-md-12 col-lg-12">
                              </div>
                              <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-12 text-center">
                                   <button type="submit" class="btn btn-primary submit">Submit</button>
                                </div>
                              </div>
                            </table>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
<script type="text/javascript">
 
  $(document).ready(function(){
    $("#timeslottxt_1").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_2").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_3").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_4").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_5").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_6").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_7").find('input').attr('readonly', 'readonly');
    var val = [];
    $('#dayschecked input[type=checkbox]:checked').each(function(i){
      val[i] = $(this).val();
      
      //alert(val[i])
      if(val[i] == 'monday'){
        $('#timeslottxt_1').find('input').removeAttr('readonly');
        /*alert($('#mon_from_time').val());
        if($('#mon_from_time').val() < $('#mon_to_time').val()){
            alert('From date should be less than To date');
        }*/
        
        
      } else if (val[i] == 'tuesday') {
        $('#timeslottxt_2').find('input').removeAttr('readonly');
        
      } else if (val[i] == 'wednesday') {
        $('#timeslottxt_3').find('input').removeAttr('readonly');
      } else if (val[i] == 'thrusday') {
        $('#timeslottxt_4').find('input').removeAttr('readonly');
      } else if (val[i] == 'friday') {
        $('#timeslottxt_5').find('input').removeAttr('readonly');
      } else if (val[i] == 'saturday') {
        $('#timeslottxt_6').find('input').removeAttr('readonly');
      } else if (val[i] == 'sunday') {
        $('#timeslottxt_7').find('input').removeAttr('readonly');
      }
    });

    $(".form-check-input").click(function() {
      var idVal = $(this).attr("id");
      
      var res = idVal.split("_");
      
      if($(this).prop("checked") == true){
          
        
        $('#timeslottxt_'+res[1]).find('input').removeAttr('readonly');
        
        
      } else if($(this).prop("checked") == false){
         
        $('#timeslottxt_'+res[1]).find('input').attr('readonly','readonly');
        $('#timeslottxt_'+res[1]).find('input').val(' ');
        
      }      
    });



    $("input[name='setruleformobandemail']").click(function(){
      var idVal = $(this).attr("id");
      $('input[name="voucherperday"]').attr('readonly','readonly');
      $('input[name="voucherperweek"]').attr('readonly','readonly');
      $('input[name="voucherpermonth"]').attr('readonly','readonly');
      $('input[name="voucherduringpromotion"]').attr('readonly','readonly');
      $('input[name="voucherperday"]').val('');
      $('input[name="voucherperweek"]').val('');
      $('input[name="voucherpermonth"]').val('');
      $('input[name="voucherduringpromotion"]').val('');      
      var inputname = $('input[name="'+idVal+'"]').attr('class');
      if(inputname==idVal) {
        $('input[name="'+idVal+'"]').removeAttr('readonly');
      }
    });

    $("input[name='setruleformobandemail']").click(function(){
      var idVal = $(this).attr("id");
      $('input[name="voucherperday"]').attr('readonly','readonly');
      $('input[name="voucherperweek"]').attr('readonly','readonly');
      $('input[name="voucherpermonth"]').attr('readonly','readonly');
      $('input[name="voucherduringpromotion"]').attr('readonly','readonly');
      $('input[name="voucherperday"]').val('');
      $('input[name="voucherperweek"]').val('');
      $('input[name="voucherpermonth"]').val('');
      $('input[name="voucherduringpromotion"]').val('');      
      var inputname = $('input[name="'+idVal+'"]').attr('class');
      if(inputname==idVal) {
        $('input[name="'+idVal+'"]').removeAttr('readonly');
      }
    });

    /*$('#generalsettingForm').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var themename     = $("#themename").val();
      var subdomain     = $("#subdomain").val();

      if(themename == "") {
        $('#themename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#themename').css('border','1px solid #eee');
        noError = true;
      }
      if(subdomain == "") {
        $('#subdomain').css('border','1px solid red');
        noError = false;               
      } else {
        $('#subdomain').css('border','1px solid #eee');
        noError = true;
      }
      if(noError == true) {
        $('#generalsettingForm')[0].submit();   
      }
    });*/

  });
</script>
<!-- End of Main Content -->
@stop