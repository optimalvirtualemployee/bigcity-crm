@extends('common.default')
@section('title', 'Product Rule List')
@section('content')
<!-- Main Content -->
<div id="content">
  <div class="container-fluid" style="margin-top:15px;">
         
        
          <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Update Products Rule</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/productrules')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            
   <!-- <div class="container-fluid" style="margin-top: 100px;"> -->
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card login-card" style="width: 100%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <div id="error_msg"></div>
                        <div class="d-flex flex-wrap align-items-center justify-content-between ">
                           <div class="redemption d-flex">
                              <!--<h4><strong>Update Product Rule</strong></h4>-->
                           </div>
                        </div>     
                        <form id="productrulefrm" class="user" action="{{ route('productrules.update',isset($productrule->id)) }}" method="post" >

                          @method('PATCH')
                          @csrf
                            <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-6">
                                    <label for="weekday-mon">Select Product Type</label>
                                    <select class="inputbox form-control" name="product_id" onchange="changingProductforEdit(this.value);">
                                      <option value="0">Select Product</option>
                                      @forelse($products as $pro)
                                      <option value="{{$pro->id}}" {{isset($productrule->product_id) && $pro->id == $productrule->product_id  ? 'selected' : ''}}>{{$pro->name}}</option>
                                      @empty
                                        <p>No Product List</p>
                                      @endforelse
                                    </select>
                                </div>
                                <div class="col-md-12 col-lg-6 weekday"> 
                                  <label for="weekday-mon">Product Rule Title</label>
                                  <input type="text"  class="form-control required" name="title" value="{{ $productrule->title }}"/>
                                  
                                </div>
                            </div>
                            
                                  <div class="form-group" id="dayschecked">  
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;">            
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input type="checkbox" class="form-check-input" id="timeslot_1" name="daysavailibility_monday" value="monday" <?php if(isset($productrule->mon_from_time)) { echo 'checked="checked"'; }?>>Monday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_1">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="mon_from_time" value="{{$productrule->mon_from_time}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="mon_to_time" value="{{$productrule->mon_to_time}}">
                                        </div>
                                        
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                          <input id="timeslot_2" type="checkbox" class="form-check-input" name="daysavailibility_tuesday" value="tuesday" <?php if(isset($productrule->tue_from_time)) { echo 'checked="checked"'; }?>>Tuesday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_2">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="tue_from_time" value="{{$productrule->tue_from_time}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="tue_to_time" value="{{$productrule->tue_to_time}}">
                                        </div>  
                                        
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_3" type="checkbox" class="form-check-input" name="daysavailibility_wednesday" value="wednesday" <?php if(isset($productrule->wed_from_time)) { echo 'checked="checked"'; }?>>Wednesday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_3">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="wed_from_time" value="{{$productrule->wed_from_time}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="wed_to_time" value="{{$productrule->wed_to_time}}">
                                        </div>
                                       
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_4" type="checkbox" class="form-check-input" name="daysavailibility_thrusday" value="thrusday" <?php if(isset($productrule->thus_from_time)) { echo 'checked="checked"'; }?>>Thrusday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_4">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="thus_from_time" value="{{$productrule->thus_from_time}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="thus_to_time" value="{{$productrule->thus_to_time}}">
                                        </div>
                                        
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_5" type="checkbox" class="form-check-input" name="daysavailibility_friday" value="friday" <?php if(isset($productrule->fri_from_time)) { echo 'checked="checked"'; }?>>Friday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_5">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="fri_from_time" value="{{$productrule->fri_from_time}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="fri_to_time" value="{{$productrule->fri_to_time}}">
                                        </div>
                                       
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_6" type="checkbox" class="form-check-input" name="daysavailibility_saturday" value="saturday" <?php if(isset($productrule->sat_from_time)) { echo 'checked="checked"'; }?>>Saturday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_6">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="sat_from_time" value="{{$productrule->sat_from_time}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="sat_to_time" value="{{$productrule->sat_to_time}}">
                                        </div>
                                        
                                      </div>
                                    </div>
                                    <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label" style="font-weight: normal;">
                                            <input id="timeslot_7" type="checkbox" class="form-check-input" name="daysavailibility_sunday" value="sunday" <?php if(isset($productrule->sun_from_time)) { echo 'checked="checked"'; }?>>Sunday
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-12 row" id="timeslottxt_7">
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">From Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="sun_from_time" value="{{$productrule->sun_from_time}}">
                                        </div>
                                        <div class="col-md-6">
                                          <label class="form-check-label" style="font-weight: normal;">To Date</label>
                                          <input type="time"  id="" class="form-control txtslot"  name="sun_to_time" value="{{$productrule->sun_to_time}}">
                                        </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                           
                             
                                            <div class="form-group row mx-auto">
                                            <div class="col-md-12 col-lg-12 text-center">
                                            <button type="submit" class="btn btn-primary btn-block mt-4">Submit</button>
                                            </div>
                                            </div
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<!-- End of Main Content -->
<script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
<script type="text/javascript">
 

  $(document).ready(function(){
    $("#timeslottxt_1").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_2").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_3").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_4").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_5").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_6").find('input').attr('readonly', 'readonly');
    $("#timeslottxt_7").find('input').attr('readonly', 'readonly');
    var val = [];
    $('#dayschecked input[type=checkbox]:checked').each(function(i){
      val[i] = $(this).val();
      
      //alert(val[i])
      if(val[i] == 'monday'){
        $('#timeslottxt_1').find('input').removeAttr('readonly');
        
        
      } else if (val[i] == 'tuesday') {
        $('#timeslottxt_2').find('input').removeAttr('readonly');
        
      } else if (val[i] == 'wednesday') {
        $('#timeslottxt_3').find('input').removeAttr('readonly');
      } else if (val[i] == 'thrusday') {
        $('#timeslottxt_4').find('input').removeAttr('readonly');
      } else if (val[i] == 'friday') {
        $('#timeslottxt_5').find('input').removeAttr('readonly');
      } else if (val[i] == 'saturday') {
        $('#timeslottxt_6').find('input').removeAttr('readonly');
      } else if (val[i] == 'sunday') {
        $('#timeslottxt_7').find('input').removeAttr('readonly');
      }
    });

    $(".form-check-input").click(function() {
      var idVal = $(this).attr("id");
      
      var res = idVal.split("_");
      
      if($(this).prop("checked") == true){
          
        
        $('#timeslottxt_'+res[1]).find('input').removeAttr('readonly');
        
        
      } else if($(this).prop("checked") == false){
         
        $('#timeslottxt_'+res[1]).find('input').attr('readonly','readonly');
        $('#timeslottxt_'+res[1]).find('input').val(' ');
        
      }      
    });

  });
</script>
@stop