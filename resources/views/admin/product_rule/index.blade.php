@extends('common.default')
@section('title', 'Product Rule List')
@section('content')
<!-- Main Content -->
<div id="content">
<!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 20px;">
      
      
    <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Product Rule</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                       
                     
                         <a href="{{url('admin/productrules/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Product Rule
                     </button></a>
                        
                        <a  href="{{url('admin/product/dashboard')}}" class="btn btn-primary btn-sm   js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
      
      
      
      
      
      
      
      
      <!-- Page Heading -->
      <div class="card">
         
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                         <th>SN</th>
                        
                        <th>Product Id</th>
                        <th>Product Name</th>
                        <th>Product Rule Title</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                        
                        
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i = 1; 
                     // echo "<pre>";print_r($productrule);die();
                     ?> 
                     @forelse($productrule as $index=>$pro)
                    
                    <tr>
                        <td>{{++$index}}</td>
                        
                        <td>{{$pro->products['id']}}</td>
                        <td>{{$pro->products['name']}}</td>
                        <td>{{$pro->title}}</td>
                        <td>{{isset($pro->created_user['name']) ? $pro->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($pro->updated_user['name']) ? $pro->updated_user['name'] : 'NA'}}</td>
                        <td>{{$pro->created_at->format('d M Y')}}/{{$pro->created_at->format('g:i A')}}</td>
                        <td>{{$pro->updated_at->format('d M Y')}}/{{$pro->updated_at->format('g:i A')}}</td>
                        <td>
                           <a href="{{ route('productrules.show',$pro->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('productrules.edit',$pro->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                           <!--<span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                           <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span> -->
                        </td>
                        <td>
                            
                              <input data-id="{{$pro->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $pro->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="ProductRule">  
                        </td> 
                        
                    </tr><?php $i++ ?>
                    @empty
                    <p>No Product Rule List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop
