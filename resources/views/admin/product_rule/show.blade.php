@extends('common.default')
@section('title', 'Show Product Rule')
@section('content')
<div id="content">

   <div class="container-fluid">
     
     

        <div class="sub-headeing" style="margin-top:15px">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Product Rule Detail</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/productrules')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            
     <div class="card">
      <div class="card-body card-header">
        <strong>Product Tagged:</strong>
        {{ $product[0]->name }}
      </div>
      <div class="card-body">
      <table class="table  table-bordered table-striped"   width="100%" cellspacing="0" >
        <thead>
          <tr>
            <th>Product Rule Name:</th>
            <th>Starting Date:</th>
            <th>Ending Date:</th>
          </tr>
        </thead>
        <tbody>
      @foreach($productrule as $key => $value)
          <tr>
            <td>{{ $value['product_rule_name'] }}</td>
            <td>{{ date('d/m/yy h:i A', strtotime($value['start_date'])) }}</td>
            <td>{{ date('d/m/yy h:i A', strtotime($value['end_date'])) }}</td>
          </tr>
      @endforeach
      </tbody>
    </table>
    </div>
  </div>
</div>
</div>
@stop



