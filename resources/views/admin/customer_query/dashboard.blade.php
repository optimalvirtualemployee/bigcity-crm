@extends('common.default')
@section('title', 'Operation Inbound Dashboard')
@section('content')
<!-- For demo purpose -->
<style>
 
 .icon i{
      font-size: 50px;
      color: #FFF;
      }
      .icon i:hover{
      font-size: 50px;
      color: #FFF;
       
      transition: .5s; 
      
      }
</style>
    <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="dahboard-title">Operation Dashboard</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="container">
               
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/customerquery')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary  pb-0">
                                       <div class="card-body text-center">
                                          
                                             <p class="icon" > <i class="fa fa-question-circle"></i></p>
                                          
                                          <h3 class="h6 mb-0 text-white">
                                          Customer Query</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/assign-type')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary  pb-0">
                                       <div class="card-body text-center">
                                          
                                             <p class="icon" > <i class="fas fa-language"></i></p>
                                          
                                          <h3 class="h6 mb-0 text-white">
                                         Assign Type</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        
                        <div class="col-6 col-sm-6 col-md-3">
                           <a href="{{url('admin/inbound_module')}}">
                              <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                 <div class="mainflip">
                                    <div class="frontside">
                                       <div class="card bg-gradient-x-primary pb-0">
                                          <div class="card-body text-center">
                                             <p class="icon" > <i class="fa fa-phone" aria-hidden="true"></i></p>
                                             <h3 class="h6 mb-0 text-white">
                                             Inbound Module</h3>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </a>
                        </div>

                        <div class="col-6 col-sm-6 col-md-3">
                        <a href="{{url('admin/booking')}}">
                          <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                            <div class="mainflip">
                              <div class="frontside">
                                <div class="card shadow pb-0">
                                  <div class="card-body text-center">
                                    <p class="icon" ><i class="fas fa-ticket-alt"></i></p>
                                    <h3 class="h6 mb-0 text-uppercase font-weight-bold text-gray-800">
                                    Booking Assign</h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                     </div>
                  </div>
               </div>
            

            </div>
@stop