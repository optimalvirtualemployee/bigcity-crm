@extends('common.default')
@section('title', 'Show Primary Language')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
    
        
        <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="module-title">Customer Query</h3>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/customerquery')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        </div>
        </div>
        
        
  
           <div class="container" style="margin-top:50px;">
            <!-- Outer Row -->
            <div class="row justify-content-center">
            <div class="card" style="width: 70%;">
            <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
            <div class="col-lg-12">
            
            
            
            <div class="form-group">
                
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">ID </p>
            {{ $customer_query->id }} 
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Customer Query</p>
           {{$customer_query->name}}
            </div>
            
            
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Created By</p>
            {{Auth::user()->name}}
            </div>
            
            
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Created Date & Time</p>
                {{$customer_query->created_at->format('d M Y')}}/{{$customer_query->created_at->format('g:i A')}}
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Status</p>
                 {{$customer_query->status == 1 ? 'Active':'Inactive'}}
            </div>
            
            
            
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
  
 
    
   </div>
</div>
@stop