@extends('common.default')
@section('title', 'Primary Language')
@section('content')

<meta name="csrf-token" content="{!! csrf_token() !!}">
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">

    <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="module-title">Customer  Query</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                        <a href="{{route('customerquery.create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Customer  Query
                     </button></a>
                     
                     
                        
                        <a href="{{url('admin/operation-management/dashboard')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple">
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>



      <div class="card">
         
        
         <div class="card-body" >
                
               @if(session()->get('success'))
                <div class="alert alert-success" id="customerStatusMsg">
                  {{ session()->get('success') }}  
                </div>
               @endif
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >S.No</th>
                        <th >ID</th>
                        <th>Customer Query</th>
                        <th >Created By</th>
                        <th>Created Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i = 1;?>
                     @forelse($customer_query as $pl)
                      <tr>
                          <td>{{$i}}</td>
                          <td>{{$pl->id}}</td>
                          <td>{{$pl->name}}</td>
                          <td>{{Auth::user()->name}}</td>
                          <td>{{$pl->created_at->format('d M Y')}}/{{$pl->created_at->format('g:i A')}}</td>
                          <td>
                             <a href="{{ route('customerquery.show',$pl->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                             
                             <a href="{{ route('customerquery.edit',$pl->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                             <!--  <form action="{{ route('primary_language.destroy', $pl->id) }}" method="POST" id="delete-form-{{ $pl->id }}" style="display: none;">
                                  {{csrf_field()}}
                                  {{ method_field('DELETE') }}
                                  <input type="hidden" value="{{ $pl->id }}" name="id">
                             </form>
                             
                             <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                              event.preventDefault(); document.getElementById('delete-form-{{ $pl->id }}').submit();">
                              <span style="color: red;padding:5px;"  ><i class="fas fa-trash fa-primary fa-md"></i></span></a>
                             <span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                             <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span> -->
                             <td>
                                <input data-id="{{$pl->id}}" class="toggle-class customerQueryStatus" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $pl->status ==1 ? 'checked' : '' }}>
                             </td> 
                          </td>
                      </tr>
                      <?php $i++; ?>
                    @empty
                      <p>No Customer Query</p>
                    @endforelse
                   </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

