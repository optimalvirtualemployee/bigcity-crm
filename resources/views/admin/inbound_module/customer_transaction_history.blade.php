@extends('common.default')
@section('title', 'Company List')
@section('content')
<!-- Main Content -->
<div id="content">
  <!-- Begin Page Content -->
  <div class="container-fluid" style="margin-top:15px;">
    <div class="bg-primary-dark-op">
      <div class="content content-narrow content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
          <div class="flex-sm-fill">
          <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Customer Transaction History</h3>
        </div>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
          <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a href="{{url('admin/inbound_module')}}">
              <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                <i class="fas fa-plus"></i> &nbsp; Inbound Module
              </button>
            </a>
            <a  href="{{url('admin/operation-management/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
              <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
          </span>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="card">
    <div class="card-body" >
      @if(session()->get('success'))
        <div class="alert alert-success">
          {{ session()->get('success') }}  
        </div>
      @endif
        <div class="table-responsive">
          <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                <th>SN</th>
                <th>Id</th>
                <th>Customer Query</th>
                <th>Voucher Code</th>
                <th>Customer Phone No</th>
                <th>Booking Status</th>
                <th>Remark</th>
                <th>Assign to Agent</th>
              </tr>
            </thead>
            <tbody>
              @forelse($booking_data as $index=> $data)
                <tr>
                  <td>{{++$index}}</td>
                  <td>{{$data->id}}</td>
                  <td>{{$data->cust_query['name']}}</td>
                  <td>{{$data->voucher_code}}</td>
                  <td>{{$data->customer_mob_no}}</td>
                  <td>{{($data->status == 0 ? 'Not Booked' : 'Booked')}}</td>
                  <td>{{$data->remarks}}</td>
                  <td>{{$data->assign_to_agent}}</td>
                </tr>
              @empty
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop
