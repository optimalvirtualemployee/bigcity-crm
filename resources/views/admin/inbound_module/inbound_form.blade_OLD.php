@extends('common.default')
@section('title', 'Operation Inbound Form')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
tbody {
    font-size: 15px;
    text-align: left;
}
</style>
<!-- Main Content -->
<div id="content">
   	<div class="container-fluid" style="margin-top:15px;">
       
        
               
        <div class="sub-headeing">
        
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            
         <h3 class="module-title">Inbound Module</h3>
        
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/operation-inbound/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        
        </div>
        </div>
        
        
     		<div class="container">
        		<div class="row justify-content-center" style="margin-top:70px;">
           		<div class="card" style="width: 500px;">
              		<div class="card-body p-0">
                 			<div class="row">
                    			<div class="col-lg-12">
                       			<div class="p-5">
                          			<div class="error_msg"></div>
                          			<form class="user">
                             				@csrf
                             				<div class="form-group">
                             				    <label for="name" class="mr-sm-2">Enter Voucher Code </label>
                                				<input type="number" class="form-control" id="voucher_code" name="voucher_code" placeholder="Enter Voucher Code" value="{{ old('voucher_code') }}" autocomplete="off">
                             				</div>
                             				<div class="form-group">
                             				    <label for="name" class="mr-sm-2">Enter Mobile No </label>
                                				<input type="number" min="10" maxlength="10" id="mobile_no" class="form-control" name="mobile_no" placeholder="Enter Mobile No" value="{{ old('mobile_no') }}" onkeypress="if(this.value.length==10) return false;" autocomplete="off">
                             				</div>
                             				<a href="#" class="">
                             					<button type="button" onclick="popup();" class="btn btn-primary   btn-block mt-4 sub_btn2">Submit</button>
                             				</a>
                          			</form>
                         			</div>
                    			</div>
                 			</div>
              		</div>
           		</div>
        		</div>
     		</div>

    		<!-- Modal -->
    		<div id="myModal" class="modal fade" aria-hidden="true">
    			<div class="modal-dialog modal-lg">
    				<div class="modal-content">
    					<div class="modal-header bg-primary" style="background-color:#106db2!important">
    						<h4 class="modal-title text-white text-center">Campaign and Voucher Details</h4>
    						<button type="button" class="close" data-dismiss="modal">&times;</button>
    					</div>
    					<div class="modal-body"></div>
    					<div class="modal-footer">
    						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    					</div>
    				</div>
    			</div>
    		</div>
    	  <!-- Modal -->
	    </div>
    </div>   
  </div>
</div>
<!-- End of Main Content -->
@stop