@extends('common.default')
@section('title', 'Operation Inbound Form')
@section('content')
<style type="text/css">.error{ color: #78261f!important; }</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
  /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* Firefox */
  input[type=number] {
    -moz-appearance: textfield;
  }
</style>

<!-- Main Content -->
<div id="content">
 	<div class="container-fluid" style="margin-top:15px;">
    <div class="sub-headeing">
      <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Inbound Module</h3>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
          <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/operation-management/dashboard')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
              <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
          </span>
        </div>
      </div>
    </div>
    <div class="container">
    	<div class="row justify-content-center" style="margin-top: 70px;">
     		<div class="card login-card" style="width: 500px;">
       		<div class="card-body p-0">
       			<div class="row">
         			<div class="col-lg-12">
           			<div class="p-5">
             			<div class="error_msg"></div>
             			<form class="user">
             				@csrf
               				<div class="form-group">
                 				<input type="text" class="form-control" id="voucher_code" name="voucher_code" placeholder="Enter Voucher Code" value="{{ old('voucher_code') }}" autocomplete="off">
               				</div>
               				<div class="form-group">
                 				<input type="number" min="10" maxlength="10" id="mobile_no" class="form-control" name="mobile_no" placeholder="Enter Mobile No" value="{{ old('mobile_no') }}" onkeypress="if(this.value.length==10) return false;" autocomplete="off">
               				</div>
               				<a href="#" class="">
               					<button type="button" onclick="popup();" class="btn btn-primary  btn-block mt-4 sub_btn2">Submit</button>
               				</a>
               				<meta name="csrf-token" content="{{ csrf_token() }}">
             			</form>
           			</div>
         			</div>
       			</div>
      		</div>
     		</div>
    	</div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" aria-hidden="true">
    	<div class="modal-dialog modal-lg">
    		<div class="modal-content">
    			<div class="modal-header bg-gradient-light">
    				<h2 class="modal-title text-gray-800 text-center">Campaign and Voucher Details</h2>
    				<button type="button" class="close" data-dismiss="modal">&times;</button>
    			</div>
    			<div class="modal-body"></div>
    			<div class="modal-footer">
            <button type="button" class="btn bg-gradient-success text-white" id="view_btn" onclick="viewQueryDetails(this);">View</button>
            <button type="button" class="btn bg-gradient-danger text-white" data-dismiss="modal">Close</button>
    			</div>
    		</div>
    	</div>
    </div>
    <!-- Modal -->

    <!-- Modal -->
      <div id="confirm" class="modal fade mt-5" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-gradient-warning text-dark pl-5">
              <h2 class="modal-title text-white pl-5 ml-5">Warning <i class="fas fa-exclamation-triangle"></i></h2>
              <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-bod p-5" style="color: darkred;font-family: sans-serif;">
              <strong>ARE YOU SURE WANT TO CANCEL THIS BOOKING ?</strong>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn bg-gradient-warning text-white btn-sm" data-dismiss="modal" id="cancel_yes">Yes</button>
              <button type="button" class="btn bg-gradient-danger text-white btn-sm" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->


       <!-- Modal -->
    <div id="view_queryDetails" class="modal fade" aria-hidden="true" style="width: 50%;float: right;margin-top: 10%;margin-left: 50%;">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header bg-gradient-primary">
            <h2 class="modal-title text-white text-center">Customer Query Details</h2>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn bg-gradient-danger text-white" id="view_queryDetails_btn" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->

	  </div>
  </div>   
</div>
</div>
<!-- End of Main Content -->
@stop