@extends('common.default')
@section('title', 'Add City')
@section('content')
<!-- Main Content -->
<div id="content">
    
    
   <div class="container-fluid" style="margin-top: 15px;">
         
           <!-- <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Add City</h1>
             <a href="{{url('admin/cities')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
           </div>
           -->
        
        
          
            <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Add City</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/cities')}}" class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fa fa-arrow-left mr-1"></i> Back
            </a>
            
            </span>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
   <div class="container">
      <!-- Outer Row -->
      
      <div class="row justify-content-center pt-5">
         <div class="card login-card" style="width: 80%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <div class="form-check" style="padding-bottom: 10px;">
                            <input class="form-check-input addCity" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                              Add single City
                            </label>

                        </div>
                        <div class="form-check" style="padding-bottom: 10px;">
                            <input class="form-check-input uploadCity" type="radio" name="exampleRadios" id="exampleRadios1" value="option2">
                            <label class="form-check-label" for="exampleRadios1">
                              Upload multiple city
                            </label>
                            
                        </div>
                        <div class="form-check" style="padding-bottom: 10px;">
                            <input class="form-check-input downloadCity" type="radio" name="exampleRadios" id="exampleRadios1" value="option2">
                            <label class="form-check-label" for="exampleRadios1">
                              Download city
                            </label>
                            
                        </div>
                        <form class="user" id="addCityForm" action="{{ route('cities.store') }}" method="POST">
                           @csrf
                           
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Select State</label>
                           <select   name="state_id" class="form-control js-example-basic-single state" style="height: 45px;">
                              <option value="0">Select State</option>
                              @forelse($states as $state)
                              <option value="{{$state->id}}" {{old('state_id') == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                              @empty
                                <p>No State List</p>
                              @endforelse
                           </select>
                           
                            </div>
                           </br>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">City Name</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter City Name" name="name" value="{{ old('name') }}">
                           </div>
                           
                          
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary  mt-4">Submit</button>
                           </a>
                        </form>
                        <form class="user" id="addUploadForm" action="{{ route('city.upload-city-excel') }}" method="POST" style="display: none;" enctype="multipart/form-data">
                           @csrf
                           
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Select State</label>
                           <select   name="state_id" class="form-control js-example-basic-single" style="height: 45px;">
                              <option value="0">Select State</option>
                              @forelse($states as $state)
                              <option value="{{$state->id}}" {{old('state_id') == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                              @empty
                                <p>No State List</p>
                              @endforelse
                           </select>
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Download Sample File</label>
                           <a href="{{url('assets/file/city.xlsx')}}" download><button class="btn btn-primary btn-sm"    type="button" id="dropdownMenuButton">Download Sample File</button></a>
                            </div>
                           </br>
                           
                           <div class="form-group">
                              <label for="exampleFormControlFile1">City Upload</label>
                              <input type="file" class="form-control"  name="file" accept=".xlsx, .xls, .csv">
                              <span id="imageErrorMsg" style="color:red;"></span>
                            </div>
                             <!--<div id="dvPreview">
                            </div>-->
                          
                           <a href="#" class="">
                           <button type="button" class="btn btn-primary btn-user btn-block mt-4" onclick="uploadCityBtn();">Submit</button>
                           </a>
                        </form>
                        <form id="downloadCityForm" style="display:none;" method="GET" action="{{ route('city.download-city-excel') }}">
                            
                        <div class="form-group" >
                            <div class="form-group">
                                <label for="name" class="mr-sm-2">Select State</label>
                           <select   name="state_id" class="form-control js-example-basic-single" style="height: 45px;">
                              <option value="0">Select State</option>
                              @forelse($states as $state)
                              <option value="{{$state->id}}" {{old('state_id') == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                              @empty
                                <p>No State List</p>
                              @endforelse
                           </select>
                           
                            </div>
                            <label for="name" class="mr-sm-2">Download City</label>
                            <a><button class="btn btn-primary btn-sm"  onclick="downloadCityBtn();"   type="button" id="dropdownMenuButton">
                             <i class="fas fa-file-import"></i> &nbsp; Download City
                            </button></a>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop