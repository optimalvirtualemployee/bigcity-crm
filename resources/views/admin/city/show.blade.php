@extends('common.default')
@section('title', 'State List')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
       
       <!--
              <div class="d-sm-flex align-items-center justify-content-between pt-5">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">City</h1>
             <a href="{{url('admin/cities')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back </a>
        </div>
        -->
        
             <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Show City</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/cities')}}" class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fa fa-arrow-left mr-1"></i> Back
            </a>
            
            </span>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
            
            
             <div class="row justify-content-center"style="margin-top:50px;">
            <div class="card login-card" style="width: 90%;">
            <div class="card-body p-0">
   
    <div class="form-group row">
      <div class="card-body col-md-12 col-lg-4">

           <strong>City ID:</strong>
    
           {{$cities->id}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>City Name:</strong>
    
           {{ $cities->name }}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>State Id:</strong>
    
           {{$cities->state['id']}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>State Name:</strong>
    
           {{$cities->state['name']}}

       </div>
       
       <div class="card-body col-md-12 col-lg-4">

           <strong>Created By:</strong>
    
           {{isset($cities->created_user['name']) ? $cities->created_user['name'] : 'NA'}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>Updated By:</strong>
    
           {{isset($cities->updated_user['name']) ? $cities->updated_user['name'] : 'NA'}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>Created Date/Time:</strong>
    
           {{$cities->created_at->format('d M Y')}}/{{$cities->created_at->format('g:i A')}}

       </div>
       <div class="card-body col-md-12 col-lg-4">

           <strong>Updated Date/Time:</strong>
    
           {{$cities->updated_at->format('d M Y')}}/{{$cities->updated_at->format('g:i A')}}

       </div>
       
       <div class="card-body col-md-12 col-lg-4">

           <strong>Status:</strong>
    
           {{ ($cities->status == '1') ? 'Active': 'In-Active' }}

       </div>
       
       
       

       </div>
      
      </div>
      </div>
      </div>
      
           
      
  </div>
</div>
@stop