@extends('common.default')
@section('title', 'Product List')
@section('content')
<!-- Main Content -->
<div id="content">
<div class="container-fluid" style="margin-top:15px;">
<div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
        
    <div class="bg-primary-dark-op mb-3">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Product</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                         <a href="{{url('admin/products/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Product
                     </button></a>
                           <a href="{{url('admin/product/download-product-excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Product
                     </button></a>
                        
                        <a  href="{{url('admin/product/dashboard')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
        
 
      <div class="card">
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th >Product ID</th>
                        <th>Product Name</th>
                        <th>Category Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($products as $index=>$pro)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$pro->id}}</td>
                        <td>{{$pro->name}}</td>
                        <td>{{$pro->category['name']}}</td>
                        <td>{{isset($pro->created_user['name']) ? $pro->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($pro->updated_user['name']) ? $pro->updated_user['name'] : 'NA'}}</td>
                        <td>{{$pro->created_at->format('d M Y')}}/{{$pro->created_at->format('g:i A')}}</td>
                        <td>{{$pro->updated_at->format('d M Y')}}/{{$pro->updated_at->format('g:i A')}}</td>    

                        
                        <td style="width: 200px;" class="text-center">
                           <a href="{{ route('products.show',$pro->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('products.edit',$pro->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        </td>
                        <td>
                            
                          <input data-id="{{$pro->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $pro->status ==1 ? 'checked' : '' }}>  
                              
                              <input type="hidden" name="mdoelName" id="mdoelName" value="Product">
                              </div>
                    </tr>
                    @empty
                    <p>No Product List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      
   </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
