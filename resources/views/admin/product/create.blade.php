@extends('common.default')
@section('title', 'Product Create')
@section('content')
<!-- Main Content -->
<style>
input#exampleFormControlFile1 {
    border: 0px !important;
    padding: 1px;
}
</style>
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
    <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
      <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
          <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <div class="flex-sm-fill">
          <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Add product</h3>
        </div>
    
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
          <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/products')}}" class="btn btn-primary btn-sm   js-click-ripple-enabled" data-toggle="click-ripple" >
              <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
<!-- Outer Row -->
  <div class="row justify-content-center mt-5">
    <div class="card login-card small_forms">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div><br />
              @endif
              <form class="user" id="productForm" action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="form-group">
                    <label for="name" class="mr-sm-2">Select Category</label>
                    <select   name="category_id" class="inputbox form-control select2 category_id" style="height: 45px;">
                      <option value="0">Select Category</option>
                        @forelse($categories as $cat)
                          <option value="{{$cat->id}}" {{old('category_id') == $cat->id ? 'Selected' : ''}}>{{$cat->name}}</option>
                        @empty
                          <p>No Category List</p>
                        @endforelse
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="name" class="mr-sm-2">Product Name</label>
                    <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Product Name" name="name" value="{{ old('name') }}">
                  </div>
                  <div class="form-group">
                    <label for="name" class="mr-sm-2">Product Description</label>
                    <textarea class="form-control" rows="5" placeholder="Enter Product Description" id="comment" name="description">{{ old('description') }}</textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlFile1">Upload Image</label>
                    <input type="file" class="form-control" id="exampleFormControlFile1" name="image">
                  </div>
                  <div id="dvPreview"></div>
                    <a href="#" class="">
                      <button type="submit" class="btn btn-primary  btn-block mt-4">Submit</button>
                    </a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- End of Main Content -->
@stop
<script type="text/javascript">
  $('.category_id').select2();
$.validator.setDefaults({ignore: []});   
     
     
$("#productForm").validate({
  ignore: [],       
  rules: {
    category_id: 'required',
  },
  messages: {
    category_id:'this field is required',
  }

   submitHandler: function(form) {
    form.submit();
   }
}); 
</script>