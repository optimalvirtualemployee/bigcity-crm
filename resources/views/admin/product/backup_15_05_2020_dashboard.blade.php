@extends('common.default')
@section('content')
<!-- For demo purpose -->
    <div class="container">
        
        <div class="d-sm-flex align-items-center justify-content-between pt-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Products</h1>
             <a href="{{url('admin/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back </a>
        </div>
      
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        
                        <div class="col-6 col-sm-6 col-md-2">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/products')}}">
                                             <p class="icon" > <i class="fas fa-handshake"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Product</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-2">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/products')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Add Product</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--<div class="col-6 col-sm-6 col-md-2">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/productrules')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Product Rule</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-2">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/productrules')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Add Product Rule</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>-->
                        
                        
                        
                     </div>
                  </div>
               </div>
               <div class="row" pb-4>
                  <!-- Area Chart -->
                  <div class="col-xl-8 col-lg-7">
                     <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" style="background-color: #d33f3e;color: #FFF;">
                           <h6 class="m-0 font-weight-bold   text-uppercase">Earnings Overview</h6>
                           <div class="dropdown no-arrow">
                              <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                 <div class="dropdown-header">Dropdown Header:</div>
                                 <a class="dropdown-item" href="#">Action</a>
                                 <a class="dropdown-item" href="#">Another action</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="#">Something else here</a>
                              </div>
                           </div>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                           <div class="chart-area">
                              <canvas id="myAreaChart"></canvas>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Pie Chart -->
                  <div class="col-xl-4 col-lg-5">
                     <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" style="background-color: #d33f3e;color: #FFF;">
                           <h6 class="m-0 font-weight-bold text-uppercase">Revenue Sources</h6>
                           <div class="dropdown no-arrow">
                              <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                 <div class="dropdown-header">Dropdown Header:</div>
                                 <a class="dropdown-item" href="#">Action</a>
                                 <a class="dropdown-item" href="#">Another action</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="#">Something else here</a>
                              </div>
                           </div>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                           <div class="chart-pie pt-4 pb-2">
                              <canvas id="myPieChart"></canvas>
                           </div>
                           <div class="mt-4 text-center small">
                              <span class="mr-2">
                              <i class="fas fa-circle text-primary"></i> Direct
                              </span>
                              <span class="mr-2">
                              <i class="fas fa-circle text-success"></i> Social
                              </span>
                              <span class="mr-2">
                              <i class="fas fa-circle text-info"></i> Referral
                              </span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row" pb-4>
                  <!-- Content Column -->
                  <div class="col-lg-12 mb-4">
                     <!-- Project Card Example -->
                     <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                           <h6 class="m-0 font-weight-bold  text-uppercase" style="background-color: d33f3e;color: #FFF;">Projects</h6>
                        </div>
                        <div class="card-body">
                           <h4 class="small font-weight-bold">Server Migration <span class="float-right">20%</span></h4>
                           <div class="progress mb-4">
                              <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                           <h4 class="small font-weight-bold">Sales Tracking <span class="float-right">40%</span></h4>
                           <div class="progress mb-4">
                              <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                           <h4 class="small font-weight-bold">Customer Database <span class="float-right">60%</span></h4>
                           <div class="progress mb-4">
                              <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                           <h4 class="small font-weight-bold">Payout Details <span class="float-right">80%</span></h4>
                           <div class="progress mb-4">
                              <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                           <h4 class="small font-weight-bold">Account Setup <span class="float-right">Complete!</span></h4>
                           <div class="progress">
                              <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 mb-4">
                     <!-- Illustrations -->
                     <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                           <h6 class="m-0 font-weight-bold text-uppercase" style="background-color: d33f3e;color: #FFF;">Illustrations</h6>
                        </div>
                        <div class="card-body">
                           <div class="text-center">
                              <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="img/undraw_posting_photo.svg" alt="">
                           </div>
                           <p>Add some quality, svg illustrations to your project courtesy of <a target="_blank" rel="nofollow" href="https://undraw.co/">unDraw</a>, a constantly updated collection of beautiful svg images that you can use completely free and without attribution!</p>
                        </div>
                     </div>
                     <!-- Approach -->
                  </div>
               </div>
               <div class="card shadow mb-4">
                  <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                     <h6 class="m-0 font-weight-bold text-uppercase">Bar Chart</h6>
                  </div>
                  <div class="card-body">
                     <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                     </div>
                  </div>
               </div>
            </div>
@stop