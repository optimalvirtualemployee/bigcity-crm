@extends('common.default')
@section('title', 'Show Product')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
    
        
    <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
    <div class="bg-primary-dark-op">
    <div class="content content-narrow content-full">
    <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
    <div class="flex-sm-fill">
    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Product Detail</h3>
    </div>
    
    <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
    <a  href="{{url('admin/products')}}" class="btn btn-primary btn-sm   js-click-ripple-enabled" data-toggle="click-ripple" >
    <i class="fas fa-chevron-left mr-1"></i> Back
    </a>
    </span>
    </div>
    
    
    </div>
    </div>
    </div>
    </div>
    
    
    
    <div class="container" style="margin-top:50px;">
    <!-- Outer Row -->
    <div class="row justify-content-center">
    <div class="card login-card  medium_forms">
    <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
    <div class="row">
    <div class="col-lg-12">
    
    
    
    <div class="form-group">
        
    <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Product ID </p>
     {{$products->id}}
    </div>
    
    <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Product Name </p>
    {{ $products->name }}
    </div>
    
    
    <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Product Description </p>
     {{ $products->description }}
    </div>
    
    
    <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Category Name </p>
    {{$products->category['name']}}
    </div>
    
    
    <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Created By </p>
    {{isset($products->created_user['name']) ? $products->created_user['name'] : 'NA'}}
    </div>
    
    
    <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Updated By </p>
   {{isset($products->updated_user['name']) ? $products->updated_user['name'] : 'NA'}}
    </div>
    
    
    
    <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Created Date/Time </p>
    {{$products->created_at->format('d M Y')}}/{{$products->created_at->format('g:i A')}}
    
    </div>
    
    
    <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Updated Date/Time </p>
    {{$products->updated_at->format('d M Y')}}/{{$products->updated_at->format('g:i A')}}
    </div>
    
    
    
        <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Status </p>
    {{ ($products->status == '1') ? 'Active': 'In-Active' }}
    </div>
    
    
        <div class="card-body col-md-12 col-lg-12">
    <p class="show-details">Product Image </p>
     <img src="{{url('/assets/img/product-images/',$products->image)}}" height="100px" width="100px">
    </div>
    
    
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
            
            
            
            
            
      
       
      
      
      
           
               
               
               
   </div>
</div>
@stop


