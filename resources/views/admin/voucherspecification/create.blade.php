@extends('common.default')
@section('title', 'Voucher Specification Create')

@section('content')
<meta name="csrf-token" content="{!! csrf_token() !!}">
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
     
      
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Voucher Specification</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/voucherspecification')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
        
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center mt-5">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <div id="error_msg"></div>
                        <form id="frmvoucher_spec" class="user" action="{{ route('voucherspecification.store') }}" method="POST">
                           @csrf
                           <div class="form-group">
                                <label for="name" class="mr-sm-2">Voucher Type</label>
                              <select class="form-control" name="voucher_type_id" style="height: 45px;" onchange="voucher_type(this.value)">
                                 <option value="0">Select Voucher Type</option>
                                 @forelse($voucher_type as $voucher)
                                  <option value="{{$voucher->id}}" {{old('voucher_type_id') == $voucher->id ? 'Selected' : ''}}>{{$voucher->name}}</option>
                                 @empty
                                  <p>No Voucher Type List</p>
                                 @endforelse
                              </select>
                           </div>
                           <div id="physical"></div>
                           <div id="E_voucher"></div>      
                           <a href="#" class="">
                           <button type="button" class="btn btn-primary btn-user btn-block mt-4" onclick="submit_voucher()">Submit</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- End of Main Content -->
@stop