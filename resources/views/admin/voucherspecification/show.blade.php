@extends('common.default')
@section('title', 'Show Voucher Specification')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
       
    
      
        <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px"> 
        Voucher type Detail</h3>
        
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/vouchertype')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        </div>
        </div>
    
    
    
           <div class="container" style="margin-top:50px;">
            <!-- Outer Row -->
            <div class="row justify-content-center">
            <div class="card" style="width: 70%;">
            <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
            <div class="col-lg-12">
            
            
            
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Voucher Specification ID </p>
            {{$voucher_specification->id}}
            </div>
            </div>
            
              
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Voucher Specification Name </p>
             {{ $voucher_specification->name }}
            </div>
            </div>
            
        
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Voucher Specification ID </p>
             {{$voucher_specification->voucher_type->id}}
            </div>
            </div>
            
            
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Voucher Type Name </p>
             {{ $voucher_specification->voucher_type->name }}
            </div>
            </div>
            
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Created By</p>
              {{isset($voucher_specification->created_user['name']) ? $voucher_specification->created_user['name'] : 'NA'}}
            </div>
            </div>
            
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Updated By</p>
               {{isset($voucher_specification->updated_user['name']) ? $voucher_specification->updated_user['name'] : 'NA'}}
            </div>
            </div>
            
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Updated Date/Time:</p>
             {{$voucher_specification->updated_at->format('d M Y')}}/{{$voucher_specification->updated_at->format('g:i A')}}
            </div>
            </div>
            
            
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Status</p>
             {{ ($voucher_specification->status == '1') ? 'Active': 'In-Active' }}
            </div>
            </div>
            
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
 
       
       
       
   </div>
</div>
@stop