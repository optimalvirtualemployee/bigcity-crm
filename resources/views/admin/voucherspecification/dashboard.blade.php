@extends('common.default')
@section('content')
<!-- For demo purpose -->
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-between pt-4">
                  <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Voucher Specification</h1>
                  <a href="{{url('admin/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back </a>
               </div>
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                      
                     <div class="row">
                        
                        
                        
                        <div class="col-6 col-sm-6 col-md-2">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/voucherspecification')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Add Voucher Specification</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        
                     </div>
                  </div>
               </div>
               
               
               <div class="card shadow mb-4">
                  <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                     <h6 class="m-0 font-weight-bold text-uppercase">Bar Chart</h6>
                  </div>
                  <div class="card-body">
                     <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                     </div>
                  </div>
               </div>
            </div>
@stop