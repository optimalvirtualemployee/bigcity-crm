@extends('common.default')
@section('title', 'Edit Voucher Specification')

@section('content')
<meta name="csrf-token" content="{!! csrf_token() !!}">
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
     
      
             <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Voucher Specification</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/voucherspecification')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
      
      <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center mt-5">
         <div class="card" style="width: 500px;">
            <div class="card-body p-0">
              <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <div id="error_msg"></div>
                        <form id="frmvoucher_spec" method="post" action="{{ route('voucherspecification.update', $voucher_specification->id) }}">
                           @method('PATCH') 
                           @csrf
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Select Voucher Type</label>
                              <select class="form-control" id="sel1" name="voucher_type_id" style="height: 45px;" onchange="voucher_type(this.value)">
                                 <option value="0">Select Voucher Type</option>
                                 @forelse($voucher_type as $voucher)
                                    <option value="{{$voucher->id}}" {{$voucher_specification->voucher_type_id == $voucher->id ? 'Selected' : ''}}>{{$voucher->name}}</option>
                                 @empty
                                    <p>No Voucher Type List</p>
                                 @endforelse
                              </select>
                           </div>
                           
                           <div id="physical">
                              @if($voucher_specification && $voucher_specification->voucher_type_id == 1 || $voucher_specification->voucher_type_id == 2 || $voucher_specification->voucher_type_id == 3)
                              <div class="physical_elements">
                                 <div class="form-group">
                                     <label for="name" class="mr-sm-2">Size Name</label>
                                    <input type="text" class="form-control" id="size_name" placeholder="Enter Size Name" name="size_name" value="{{$voucher_specification->size_name}}">
                                 </div>
                                 <div class="form-group">
                                     <label for="name" class="mr-sm-2">Length</label>
                                    <input type="text" class="form-control" id="length" placeholder="Enter Length" name="length" value="{{$voucher_specification->length}}">
                                 </div>
                                 <div class="form-group">
                                     <label for="name" class="mr-sm-2">Breadth</label>
                                    <input type="text" class="form-control" id="breadth" placeholder="Enter Breadth" name="breadth" value="{{$voucher_specification->breadth}}">
                                 </div>
                                 <div class="form-group">
                                     <label for="name" class="mr-sm-2">Thickness</label>
                                    <input type="text" class="form-control" id="thickness" placeholder="Enter Thickness" name="thickness" value="{{$voucher_specification->thickness}}">
                                 </div>
                              </div>
                              @endif
                           </div>
                           
                           <div id="E_voucher">
                              @if($voucher_specification->evoucher_id)
                                 <div class="form-group e_voucher_elements">
                                     <label for="name" class="mr-sm-2">Select E-Voucher Type</label>
                                    <select class="form-control" name="evoucher_id" style="height: 45px;">
                                       <option value="0">Select E-Voucher Type</option>
                                       <option value="1" {{ $voucher_specification->evoucher_id == 1 ? 'selected' : '' }}>SMS</option>
                                       <option value="2" {{ $voucher_specification->evoucher_id == 2 ? 'selected' : '' }}>E-mail</option>
                                    </select>
                                 </div>
                               @endif   
                           </div>
                           
                           <a href="#" class="">
                           <button type="button" class="btn btn-primary btn-user btn-block mt-4" onclick="submit_voucher()">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop