@extends('common.default')
@section('title', 'CRM-Dashboard')
@section('content')
<!-- For demo purpose -->
<style>

 .icon i{
      font-size: 50px;
      color: #FFF;
      }
      .icon i:hover{
      font-size: 50px;
      color: #FFF;
       
      transition: .5s; 
      
      }
</style>
    <div style="background-color: #f5f5f5;">
    <div class="container">
    <div class="text-center main-dashboard">
    <h3>Dashboard</h3>
    </div>
    </div>
    </div>


      <div class="container">
         <div class="row">
            <div class="col-lg-12  mt-5">
               <div class="row mb-5">
                  <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/category/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card bg-gradient-x-primary  pb-0">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-object-group"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Category</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/product/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-dumpster"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Product</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/user/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon"> <i class="fas fa-users"></i> </p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">User Management</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/city/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-users"></i> </p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">City</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3">
                       <a href="{{url('admin/state/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                   
                                       <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">State</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/area/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-balance-scale-left"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Area</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/voucher-type/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-balance-scale-left"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Voucher Type</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  
                  
                  
                     
                     <div class="col-6 col-sm-6 col-md-3">
                         <a href="{{url('admin/venue/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-cogs"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Pd Lead</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                     </div>
                     
                     <div class="col-6 col-sm-6 col-md-3">
                         <a href="{{url('admin/campaign/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-cogs"></i></p>
                                   
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Campaign</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     
                  </div>
                   </a>
                  </div>
                  
                     <div class="col-6 col-sm-6 col-md-3">
                       <a href="{{url('admin/operation-management/dashboard')}}">
                        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-cogs"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Operation Management</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                     </div>
                     <div class="col-6 col-sm-6 col-md-3">
                       <a href="{{url('admin/winner-campaign/dashboard')}}">
                        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="fas fa-cogs"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Winner Campaign </h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                     </div>
                     
                     
                     
                     <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/communication_template_type/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="far fa-comments"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Communication Tempalte</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3">
                      <a href="{{url('admin/report/dashboard')}}">
                     <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                           <div class="frontside">
                              <div class="card  bg-gradient-x-primary">
                                 <div class="card-body text-center">
                                    
                                       <p class="icon" > <i class="far fa-comments"></i></p>
                                    
                                    <h4 class="h6 mb-0 text-uppercase font-weight-bold text-white">Report</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </a>
                  </div>
                </div>
                
                
                
                <div class="row mb-5">
                   
                   
                  <div class="col-xl-3 col-sm-4 py-2">
                    <div class="card  text-white">
                        <div class="card-body  card-totalvenue11">
                            
                            <p class="text-uppercase text-center">Total Venue</p>
                            <h1 class="display-4 totalvenue">{{$totalVenue->count()}}</h1>
                              <a href="{{url('admin/venues')}}" class="btn btn-primary btn-sm">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  
                  
                  
                  <div class="col-xl-3 col-sm-4 py-2">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue22">
                             
                            <p class="text-uppercase text-center">Total Pending Venue</p>
                            <h1 class="display-4 totalvenue">{{$totalVenuePending->count()}}</h1>
                               <a href="{{url('admin/venues')}}" class="btn btn-primary btn-sm">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  <div class="col-xl-3 col-sm-4 py-2">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue33">
                            
                            <p class="text-uppercase text-center">Total Category</p>
                            <h1 class="display-4 totalvenue">{{$totalCategory->count()}}</h1>
                               <a href="{{url('admin/categories')}}" class="btn btn-primary btn-sm">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-xl-3 col-sm-4 py-2">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue44">
                             
                            <p class="text-uppercase text-center">Total Venue QC</p>
                            <h1 class="display-4 totalvenue">{{$totalVenueQC->count()}}</h1>
                            <a href="{{url('admin/venues')}}" class="btn btn-primary btn-sm">Read More</a>
                        </div>
                    </div>
                  </div>
                  <div class="col-xl-3 col-sm-4 py-2">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue33">
                            
                            <p class="text-uppercase text-center">Total Target Assign</p>
                            <h1 class="display-4 totalvenue">{{$totalTargetAssign->count()}}</h1>
                               <a href="{{url('admin/target-assign')}}" class="btn btn-primary btn-sm">Read More</a>
                        </div>
                    </div>
                  </div>
                  </div>
                  
                  <div class="row mb-5">
                    @foreach ($number_blocks as $block)
                    <div class="col-md-4 ">
                      
                        
                <div class="card-content card-totalvenue text-white media-body">
                <div class="media align-items-stretch">
                    <div class="p-2  ml-3 text-center">
                        <i class="fas fa-user  font-large-2 text-white"></i>
                    </div>
                    <div class="p-2 ml-3" style="width:100%">
                        <p class="text-bold-400 text-white text-uppercase" style="font-size: 14px !important;text-align: center;margin-top: 6px;">{{ $block['title'] }}</p>
                        <p class="text-bold-400 users_today font-weight-bold">{{ $block['number'] }}</p>
                    </div>
                </div>
            </div>
                    </div>
                    @endforeach
                </div>

                <div class="row">
                @foreach ($list_blocks as $block)
                
                
                
             
                
                
                    <div class="col-md-12">
                           <div class="card mb-5">
            <div class="card-header border-0-bottom">
                <h4 class="card-title">{{ $block['title'] }}</h4>
                        <div class="table-responsive">
                        <table class="table table-bordered" cellspacing="0" cellpadding="0">
                            <thead style="text-align: left;">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Last login at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($block['entries'] as $entry)
                                <tr>
                                    <td>{{ $entry->name }}</td>
                                    <td>{{ $entry->email }}</td>
                                    <td>{{ $entry->last_login_at }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">{{ __('No entries found') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    
                    </div>
                      </div>
                        </div>
                    
                    
                    
                @endforeach
                </div>
                  
               </div>
           
         </div>
      </div>
                
@stop