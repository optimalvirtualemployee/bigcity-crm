@extends('common.default')
@section('title', 'Assign Type List')
@section('content')
<!-- Main Content -->
<style>
.ques {
color: darkslateblue;
}
.switch {
  position: relative;
  display: inline-block;
  width: 260px;
  height: 100px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  overflow: hidden;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #f2f2f2;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  z-index: 2;
  content: "";
  height: 96px;
  width: 96px;
  left: 2px;
  bottom: 2px;
  background-color: darkslategrey;
      -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.22);
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.22);
  -webkit-transition: .4s;
  transition: all 0.4s ease-in-out;
}
.slider:after {
  position: absolute;
  left: 0;
  z-index: 1;
  content: "YES";
    font-size: 45px;
    text-align: left !important;
    line-height: 95px;
  padding-left: 0;
    width: 260px;
    color: #fff;
    height: 100px;
    border-radius: 100px;
    background-color: #ff6418;
    -webkit-transform: translateX(-160px);
    -ms-transform: translateX(-160px);
    transform: translateX(-160px);
    transition: all 0.4s ease-in-out;
}

input:checked + .slider:after {
  -webkit-transform: translateX(0px);
  -ms-transform: translateX(0px);
  transform: translateX(0px);
  /*width: 235px;*/
  padding-left: 25px;
}

input:checked + .slider:before {
  background-color: #fff;
}

input:checked + .slider:before {
  -webkit-transform: translateX(160px);
  -ms-transform: translateX(160px);
  transform: translateX(160px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 100px;
}

.slider.round:before {
  border-radius: 50%;
}
.absolute-no {
	position: absolute;
	left: 0;
	color: darkslategrey;
	text-align: right !important;
    font-size: 45px;
    width: calc(100% - 25px);
    height: 100px;
    line-height: 95px;
    cursor: pointer;
}
  
 

 
</style>

<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      <!-- Page Heading -->
      <!-- DataTales Example -->
      
       <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="module-title">Assign Type Listing</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                          <a href="{{url('admin/assign-type/create')}}">  <button class="btn btn-danger btn-sm  js-click-ripple-enabled"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Assign Type
                     </button></a>
                     
                         <!--<a href="{{url('admin/category/download-category-excel')}}"><button class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Category
                     </button></a>-->
                        
                        <a  href="{{url('admin/operation-management/dashboard')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
      
      <!--
       <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Category</h1>
             <a href="{{url('admin/category/dashboard')}}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
        -->
      
     
      <div class="card ">
          <!--
         <div class="card-header d-none d-sm-block mt-4">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                   
                  <li style="margin-right: 20px;">
                      <a href="{{url('admin/categories/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Category
                     </button></a>
                  </li>
                  <li style="margin-right: 20px;">
                      
                  <a href="{{url('admin/category/download-category-excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Category
                     </button></a>
                     </li>
                     
                  <li style="margin-right: 20px;">  <button class="btn btn-primary btn-sm"     type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Import Category
                     </button>
                  </li>
                  <li style="margin-right: 20px;">  <button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-print"></i> &nbsp; Print Category
                     </button>
                  </li>
                  <li class="nav-item dropdown" ml-2>
                     <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-file-export"></i>&nbsp; Export Category
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton ">
                           <a class="dropdown-item font-weight-bold" href="{{route('export_category.excel')}}"><i class="fas fa-file-excel"></i> &nbsp; Export to Excel</a>
                           <a class="dropdown-item font-weight-bold" href="abc.docx"><i class="fas fa-file-word"></i> &nbsp; Export to Word</a>
                           <a class="dropdown-item font-weight-bold" href="abc.pdf"><i class="fas fa-file-pdf"></i> &nbsp;Export to PDF</a>
                        </div>
                     </div>
                  </li> 
               </ul>
            </nav>
         </div>
         -->
         
       
         <div class="card-body" >
               @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
       
            <div class="table-cont">
            <div class="table-responsive">
               <table class="table   table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>Assign Type ID</th>
                        <th>Assign Type Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($assign_type as $index=>$cat)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$cat->id}}</td>
                        <td>{{$cat->name}}</td>
                        <td>{{isset($cat->created_user['name']) ? $cat->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($cat->updated_user['name']) ? $cat->updated_user['name'] : 'NA'}}</td>
                        <td>{{$cat->created_at->format('d M Y')}}/{{$cat->created_at->format('g:i A')}}</td>
                        <td>{{$cat->updated_at->format('d M Y')}}/{{$cat->updated_at->format('g:i A')}}</td>
                        
                        <td>
                           <a href="{{ route('assign-type.show',$cat->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('assign-type.edit',$cat->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                           <!--<span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                           <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span>-->
                          
                        </td>
                        <td>
                            
                              <input data-id="{{$cat->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $cat->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="AssignType">  
                        </td> 
                        </tr>
                    @empty
                    <p>No Category List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
            </div>
         </div>
      </div>
      
 
        
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

