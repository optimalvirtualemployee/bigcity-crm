@extends('common.default')
@section('title', 'Show Assign Type')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
      
        
        <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
         <h3 class="module-title">Assign Type Detail</h3>
        
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/assign-type')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        
        </div>
        </div>
        
        
        
        
        
        <div class="container" style="margin-top:50px;">
        <!-- Outer Row -->
        <div class="row justify-content-center">
        <div class="card" style="width: 70%;">
        <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        
        <div class="row">
        <div class="col-lg-12">
        <div class="form-group">
            
        <div class="card-body col-md-12 col-lg-12">
        <p class="show-details"> Assign Type ID </p>
        {{$assign_type->id}}
        </div>
        
        <div class="card-body col-md-12 col-lg-12">
        <p class="show-details">Assign Type Name</p>
        {{$assign_type->name}}
        </div>
        
        
        <div class="card-body col-md-12 col-lg-12">
        <p class="show-details">Created By</p>
       {{isset($assign_type->created_user['name']) ? $assign_type->created_user['name'] : 'NA'}}
        </div>
        
        
        <div class="card-body col-md-12 col-lg-12">
        <p class="show-details">Updated By</p>
        {{isset($assign_type->updated_user['name']) ? $assign_type->updated_user['name'] : 'NA'}}
        </div>
        
        
        <div class="card-body col-md-12 col-lg-12">
        <p class="show-details">Created Date/Time</p>
        {{$assign_type->created_at->format('d M Y')}}/{{$assign_type->created_at->format('g:i A')}}
        </div>
        
        
        <div class="card-body col-md-12 col-lg-12">
        <p class="show-details">Updated Date/Time</p>
        {{$assign_type->updated_at->format('d M Y')}}/{{$assign_type->updated_at->format('g:i A')}}
        </div>
        
        
        <div class="card-body col-md-12 col-lg-12">
        <p class="show-details">Status</p>
        {{($assign_type->status == '1') ? 'Active' : 'In-Active' }}
        </div>
        
        
        
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
    
   </div>
</div>
@stop