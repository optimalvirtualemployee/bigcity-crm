<style>
    .card:hover {
     box-shadow: 0px 0px 0px #aaaaaa !important;
}
</style>
@extends('common.default')
@section('title', 'Show Category')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
     
        
           <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Category Detail</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/categories')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
        
  
  
            <div class="container" style="margin-top:50px;">
            <!-- Outer Row -->
            <div class="row justify-content-center">
            <div class="card login-card small_forms">
            <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                
                
            <div class="col-lg-12">
            <div class="card">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Category ID </p>
            {{$categories->id}}
            </div>
            </div>
            
            
             <div class="card">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Category Name </p>
              {{$categories->name}}
            </div>
            </div>
            
            
             <div class="card">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Created By</p>
            {{isset($categories->created_user['name']) ? $categories->created_user['name'] : 'NA'}}
            </div>
            </div>
            
            
             <div class="card">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Updated By</p>
            {{isset($categories->created_user['name']) ? $categories->created_user['name'] : 'NA'}}
            </div>
            </div>
            
             <div class="card">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Created Date/Time</p>
            {{$categories->created_at->format('d M Y')}}/{{$categories->created_at->format('g:i A')}}
            </div>
            </div>
            
            <div class="card">
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Status</p>
            {{($categories->status == '1') ? 'Active' : 'In-Active' }}
            </div>
            </div>
            
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
            
            
            
            
        <!--    
             <table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                       
                        <th>Category ID</th>
                        <th>Category Name</th>
                        <th></th>
                        <th>Updated By</th>
                        <th></th>
                        <th>Updated Date/Time</th>
                        
                        <th></th>
                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    <tr>
                        <td>{{$categories->id}}</td>
                        <td></td>
                        <td></td>
                        <td>{{isset($categories->updated_user['name']) ? $categories->updated_user['name'] : 'NA'}}</td>
                        <td></td>
                        <td>{{$categories->updated_at->format('d M Y')}}/{{$categories->updated_at->format('g:i A')}}</td>
                        <td></td>
                   </tr>
                  </tbody>
             </table>
             -->
    
   </div>
</div>
@stop