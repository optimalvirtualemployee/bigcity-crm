@extends('common.default')
@section('title', 'Booking List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
        
          
          
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <!-- <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Company</h3> -->
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                
              <!-- <a href="{{url('admin/companies/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
              <i class="fas fa-plus"></i> &nbsp; Add Company
                     </button></a> -->
              <!-- <a href="{{route('company.download-company-excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
              <i class="fas fa-file-import"></i> &nbsp; Download Company
                     </button></a> -->
              <a  href="{{url('admin/report/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
              <i class="fas fa-chevron-left mr-1"></i> Back</a>
                 
            </span>
            </div>
            </div>
            </div>
            </div>
            
          
     <br>
     
      <div class="card">
      
      
         <div class="card-body" >
                @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                         <th>SN</th>
                        <th>Campaign Id</th>
                        <th>Campaign Name</th>
                        <th>Product Id</th>
                        <th>Product Name</th>
                        <th>Voucher Code</th>
                        <th>Customer Name</th>
                        <th>Customer Email</th>
                        <th>Customer Mobile</th>
                        <!-- <th>Action</th> -->
                        <th>Status</th>
                        
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($booking_data as $index=> $data)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$data->campaign_id}}</td>
                        <td>{{$data->campaign->campaign_name}}</td>
                        <td>{{$data->product_id}}</td>
                        <td>{{$data->products->name}}</td>
                        <td>{{$data->voucher_code}}</td>
                        <td>{{$data->cname}}</td>
                        <td>{{$data->customer_email_id}}</td>
                        <td>{{$data->customer_mob_no}}</td>
                        <td>{{$data->status}}</td>
                        
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

