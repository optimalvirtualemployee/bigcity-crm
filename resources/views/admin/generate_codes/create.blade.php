@extends('common.default')
@section('title', 'Generate Codes')
@section('content')
<style type="text/css">.err{ border: 1px solid red; }
.form-check-label {
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 5px;
    color: #000;
    width: 20px;
    height: 15px;
    font-size: 12px;
    font-weight: 500;
    text-align: center;
    line-height: 15px;
    font-weight: 600;
    
}
.form-check {
position: relative;
    display: inline-block;
    padding-left: 1.25rem;
    background-color: #d8d8d8;
    /* margin: 0px !important; */
    padding: auto 5px !important;
    /* padding: 0px 0px 0px 0px; */
    /* margin: auto .5px; */
    border-right: .5px solid #fdfdfd;
    line-height: 5px;
    /* padding: 1px 10px; */
    /* border-radius: 5px; */
    /* margin: 0px 10px;
}
</style>
<div id="content">
  <div class="container-fluid" style="margin-top:15px;">
      
      
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Generate Codes</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/generate_codes')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
  
    
    
    
    <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center mt-5">
        <div class="card login-card">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <div class="Campaign-button">
                  <br>
                  <form id="generatecodefrm" class="form-horizontal" action="{{ route('generate_codes.store') }}" method="POST">
                    @csrf
                    
                    
                    <div class="form-group row mx-auto">
                      
                        <div class="col-md-12 col-lg-3">
                            <label>Select Campaign</label>
                            <select class="form-control select2" name="campaign_id" onchange="get_genproductList_fn(this.value);">
                              <option value="0">Select Campaign</option>
                              @forelse($campaigns as $camp)
                              <option value="{{$camp->id}}" {{old('campaign_id') == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                              @empty
                                <p>No Product List</p>
                              @endforelse
                            </select>
                        </div>

                        <div id="campaignMapped_productList" class="col-md-12 col-lg-3"></div><br>
                        <div class="col-md-12 col-lg-3">
                            <label>Select Creation Type</label>
                            <select class="form-control" name="creation_type">
                              <option value="0">Select Creation Type</option>
                              
                              <option value="1" {{old('creation_type') == '1' ? 'Selected' : ''}}>Instant</option>
                              <option value="2" {{old('creation_type') == '2' ? 'Selected' : ''}}>Long Process</option>
                              
                            </select>
                        </div>
                        <div class="col-md-12 col-lg-3">
                           <label for="" class="mr-sm-2">No of Codes Required</label>
                          <input type="text" name="gc_no_of_codes_required" placeholder="No of Codes Required" class="form-control" value="{{ old('gc_no_of_codes_required') }}">
                        </div>
                        
                    <!--<div class="col-md-12 col-lg-4">
                      
                      <label for="name" class="mr-sm-2">Order Type</label>
                      <select class="form-control" name="gc_types_of_order_id">
                        <option value="0">Select Order Type</option>
                        <option value="1" {{old('gc_types_of_order_id') == 1 ? 'Selected' : ''}}>New Order</option>
                        <option value="2" {{old('gc_types_of_order_id') == 2 ? 'Selected' : ''}}>Re-Order</option>
                    </select>
                    </div>-->
                  </div>
                
                  <!--<div class="form-group row mx-auto">
                    <div class="col-md-12 col-lg-4">
                      <label for="name" class="mr-sm-2">Estimate No</label>
                      <input type="text" name="gc_estimate_no" placeholder="Enter Estimate No" class="form-control {{($errors->first('gc_estimate_no') ? 'err'  : '')}}"  value="{{ old('gc_estimate_no') }}" autocomplete="off">
                    </div>
                    <div class="col-md-12 col-lg-4">
                     <label for="name" class="mr-sm-2">Estimate date</label>
                      <input type="text" name="gc_estimate_date" placeholder="Enter Estimate date" class="form-control datetimepicker2 {{($errors->first('gc_estimate_date') ? 'err'  : '')}}" value="{{ old('gc_estimate_date') }}" autocomplete="off">
                    </div>
                    <div class="col-md-12 col-lg-4">
                     <label for="name" class="mr-sm-2">PO No</label>
                      <input type="text" name="gc_po_no" placeholder="Enter PO No" class="form-control {{($errors->first('gc_estimate_date') ? 'err'  : '')}}" value="{{ old('gc_po_no') }}" autocomplete="off">
                    </div>
                  </div>-->

                 <!-- <div class="form-group row mx-auto">
                    <div class="col-md-12 col-lg-4">
                       <label for="" class="mr-sm-2"> No of codes</label>
                      <input type="text" name="gc_no_of_codes_as_per_estimate_po" title="Enter No of required codes as per Estimate / PO" placeholder="No of required codes as per Estimate / PO" class="form-control {{($errors->first('gc_no_of_codes_as_per_estimate_po') ? 'err'  : '')}}" value="{{ old('gc_no_of_codes_as_per_estimate_po') }}" autocomplete="off">
                    </div>
                    <div class="col-md-12 col-lg-4">
                      <label for="" class="mr-sm-2">Campaign Value</label>
                      <input type="text" name="gc_campaign_value" placeholder="Enter Campaign Value" class="form-control {{($errors->first('gc_campaign_value') ? 'err'  : '')}}" value="{{ old('gc_campaign_value') }}" autocomplete="off">
                    </div>
                    <div class="col-md-12 col-lg-4">
                      <label for="" class="mr-sm-2">Campaign Cost</label>
                      <input type="text" name="gc_campaign_cost" placeholder="Enter Campaign Cost" class="form-control {{($errors->first('gc_campaign_cost') ? 'err'  : '')}}" value="{{ old('gc_campaign_cost') }}" autocomplete="off">
                    </div>
                  </div>-->
 
                  <div class="form-group row mx-auto">
                    <div class="col-md-12 col-lg-4">
                      <label for="" class="mr-sm-2">Voucher Code Batch Name</label>
                      <input type="text" name="gc_voucher_code_batch_name" placeholder="Voucher Code Batch Name" class="form-control" value="{{ old('gc_voucher_code_batch_name') }}">
                    </div>
                    <div class="col-md-12 col-lg-4">
                      @csrf
                      <label for="" class="mr-sm-2">Code Types</label>
                      <select class="form-control" name="gc_codes_type" id="gc_codes_type">
                        <option value="0">Select Code Types</option>
                        <option value="1" {{old('gc_codes_type') == '1' ? 'Selected' : ''}}>Numeric</option>
                        <option value="2" {{old('gc_codes_type') == '2' ? 'Selected' : ''}}>Alphanumeric</option>
                    </select>
                    </div>
                    <div class="col-md-12 col-lg-4 length_of_code_required">
                      
                      <label for="" class="mr-sm-2">Select Length of Codes Required</label>
                      <select class="form-control" name="length_of_code_required">
                        <option value="0">Select Length of Codes Required</option>
                        <option value="4" {{old('length_of_code_required') == 4 ? 'Selected' : ''}}>4</option>
                        <option value="5" {{old('length_of_code_required') == 5 ? 'Selected' : ''}}>5</option>
                    
                        <option value="6" {{old('length_of_code_required') == 6 ? 'Selected' : ''}}>6</option>
                        <option value="7" {{old('length_of_code_required') == 7 ? 'Selected' : ''}}>7</option>
                        <option value="8" {{old('length_of_code_required') == 8 ? 'Selected' : ''}}>8</option>
                        <option value="9" {{old('length_of_code_required') == 9 ? 'Selected' : ''}}>9</option>
                        <option value="10" {{old('length_of_code_required') == 10 ? 'Selected' : ''}}>10</option>
                        <option value="11" {{old('length_of_code_required') == 11 ? 'Selected' : ''}}>11</option>
                        <option value="12" {{old('length_of_code_required') == 12 ? 'Selected' : ''}}>12</option>
                        
                    </select>
                    </div>
                    
                    
                    <!--<div class="col-md-12 col-lg-4">
                      
                      <label for="" class="mr-sm-2">OTP Required</label>
                      <select class="form-control" name="gc_otp_required">
                        <option value="0">Select OTP Required</option>
                        <option value="1" {{old('gc_otp_required') == 1 ? 'Selected' : ''}}>Yes</option>
                        <option value="2" {{old('gc_otp_required') == 2 ? 'Selected' : ''}}>No</option>
                    </select>
                    </div>-->
                    
                    </div>

                   <!--<div class="form-group row mx-auto">
                    <div class="col-md-12 col-lg-4">
                       <label for="" class="mr-sm-2">No of Codes Required</label>
                      <input type="text" name="gc_no_of_codes_required" placeholder="No of Codes Required" class="form-control {{($errors->first('gc_no_of_codes_required') ? 'err'  : '')}}" value="{{ old('gc_no_of_codes_required') }}" autocomplete="off">
                    </div>
                   <div class="col-md-12 col-lg-4">
                       <label for="" class="mr-sm-2">No of Test Codes Required</label>
                      <input type="text" name="gc_no_of_test_codes_required" placeholder="No of Test Codes Required" class="form-control {{($errors->first('gc_no_of_test_codes_required') ? 'err'  : '')}}" value="{{ old('gc_no_of_test_codes_required') }}" autocomplete="off">
                    </div>
                    <div class="col-md-12 col-lg-4">
                      <label for="" class="mr-sm-2">No of Client Test Codes Required</label>
                      <input type="text" name="gc_no_of_client_test_codes_required" placeholder="No of Client Test Codes Required" class="form-control {{($errors->first('gc_no_of_client_test_codes_required') ? 'err'  : '')}}" value="{{ old('gc_no_of_client_test_codes_required') }}" autocomplete="off">
                    </div>-->
                  </div>
                  <div class="col-md-12 col-lg-12 num_letter" style="padding-bottom:15px;">
                    <label for="" class="mr-sm-2">Select the Nos</label>
                    <div style="display:flex;">
                        @foreach (range(0,9) as $letter) 
    
                        <div class="form-check">
                          <input class="form-check-input required" type="checkbox" name="numbers[]" id="exampleRadios1" value="{{$letter}}" id="acceptTerms">
                          <label class="form-check-label" for="exampleRadios1">
                           {{$letter}}
                          </label>
                          &nbsp;
                        </div>
                        @endforeach
                    </div>
                        
                    </div>
                  <div class="col-md-12 col-lg-12 num_letter" style="padding-bottom:15px;">
                        <label for="" class="mr-sm-2">Select the Letters</label>
                        <div style="display:flex;">
                        @foreach (range('A','Z') as $letter) 
    
                        <div class="form-check">
                          <input class="form-check-input required" type="checkbox" name="letters[]" id="exampleRadios1" value="{{$letter}}" id="acceptTerms">
                          <label class="form-check-label" for="exampleRadios1">
                           {{$letter}}
                          </label>
                          &nbsp;
                        </div>
                        @endforeach 
                        </div>
                    </div>
                  <div class="form-group row mx-auto">
                    <div class="col-md-12 col-lg-4">
                      <label for="" class="mr-sm-2">Code Start Date</label>
                      <input type="text" name="gc_last_date_to_register_date" placeholder="Enter Last Date to Register date" class="form-control datetimepicker2" value="{{ old('gc_last_date_to_register_date') }}" >
                    </div>
                    
                    <div class="col-md-12 col-lg-4 ">
                       <label for="" class="mr-sm-2">Code End Date</label>
                      <input type="text" name="gc_offer_valid_till_date" placeholder="Enter Offer Valid till date" class="form-control datetimepicker2" value="{{ old('gc_offer_valid_till_date') }}">
                    </div>
                  </div>
                
                  <div class="form-group row">
                    <div class="col-md-12 text-right">
                        
                      <button type="button" class="btn btn-primary bigcity-button" onClick="$('#loading').show();
                    $('#generatecodefrm').submit();"> Generate Code</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@stop




