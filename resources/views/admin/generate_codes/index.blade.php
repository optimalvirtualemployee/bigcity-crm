@extends('common.default')
@section('title', 'Generate Code List')
@section('content')
<!-- Main Content -->
<div id="content">
<!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      
      
    
      
        <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Generate Code</h3>
            
        </div>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                
               <a href="{{url('admin/generate_codes/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Generate Code
                     </button></a>
               <a href="{{url('admin/background-jobs')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Moniter Background Process
                     </button></a>
                
                <a  href="{{url('admin/campaign/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                    <i class="fas fa-chevron-left mr-1"></i> Back
                </a>
                 
            </span>
        </div>
        </div>
        </div>
        </div>
      
      <br>
      <!-- Page Heading -->
      <div class="card">
     
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table table-striped table-bordered " id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>Campaign</th>
                        <th>Product</th>
                        <th>Order Type</th>
                        <th>Total Voucher Code</th>
                        <th>Length of code</th>
                        <th>Batch Name</th>
                        <th>Code Type</th>
                        <th>Last Date to register</th>
                        <th>Offer valid till date</th>      
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                       
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i = 1; ?> 
                     @forelse($generate_codes as $code)
                    
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$code->campaign['campaign_name']}}</td>
                        <td>{{$code->products['name']}}</td>
                        <td>{{$code->gc_types_of_order_id == 1 ? 'New Order' : 'Re-Order'}}</td>
                        <td>{{$code->gc_no_of_codes_required}}</td>
                        <td>{{$code->length_of_code_required}}</td>
                        <td>{{$code->gc_voucher_code_batch_name}}</td>
                        <td>{{($code->gc_codes_type == '1') ? 'Numeric' :'Alphanumeric'}}</td>
                        <td>{{$code->gc_last_date_to_register_date}}</td>
                        <td>{{$code->gc_offer_valid_till_date}}</td>
                        <td>{{isset($code->created_user['name']) ? $code->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($code->updated_user['name']) ? $code->updated_user['name'] : 'NA'}}</td>
                        <td>{{$code->created_at->format('d M Y')}}/{{$code->created_at->format('g:i A')}}</td>
                        <td>{{$code->updated_at->format('d M Y')}}/{{$code->updated_at->format('g:i A')}}</td>
                        <td>
                           
                           @if($code->voucher_code_file)
                           <a href="{{ route('generate_codes.show',$code->id) }}" class="btn btn-primary btn-sm pull-left">Voucher Code</a></p></a>
                           @endif
                           <a href="{{ route('generate_codes.edit',$code->id) }}" class="btn btn-primary btn-sm pull-left">Edit Voucher</a></p></a>
                           <a href="{{ route('generate_code.voucher_code',$code->id) }}" class="btn btn-primary btn-sm pull-left">Voucher Code</a></p></a>
                           <!-- <span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                           <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span> --> 
                        </td>
                        
                    </tr><?php $i++ ?>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop
