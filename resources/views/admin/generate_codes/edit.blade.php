@extends('common.default')
@section('title', 'Generate Codes')
@section('content')
<style type="text/css">.err{ border: 1px solid red; }</style>
<div id="content">
  <div class="container-fluid" style="margin-top:15px;">
   
    
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Edit Voucher Code</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/generate_codes')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
    <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center mt-5">
        <div class="card login-card">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <div class="Campaign-button">
                  <br>
                  <form id="generatecodefrm" class="form-horizontal" action="{{ route('generate_codes.update',$generate_codes->id) }}" method="POST">
                    @method('PATCH')
                    @csrf
                    <div class="form-group row mx-auto">
                      <div class="col-md-12 col-lg-3">
                            <label>Select Campaign</label>
                            <select class="form-control select2" name="campaign_id" onchange="get_genproductList_fn(this.value);" id="camp_readonly">
                              <option value="0">Select Campaign</option>
                              @forelse($campaigns as $camp)
                              <option value="{{$camp->id}}" {{$generate_codes->gc_campaign_id == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                              @empty
                                <p>No Product List</p>
                              @endforelse
                            </select>
                        </div>

                        <div id="" class="col-md-12 col-lg-3">
                            <label>Select Product</label>
                            <select class="form-control select2" name="product_id" id="product_readonly">
                              <option value="0">Select Product</option>
                              @forelse($products as $pro)
                              <option value="{{$pro->id}}" {{$generate_codes->gc_product_id == $pro->id ? 'Selected' : ''}}>{{$pro->name}}</option>
                              @empty
                                <p>No Product List</p>
                              @endforelse
                            </select>
                        </div><br>
                        
                    
                  </div>
                  <div class="form-group row mx-auto">
                    <div class="col-md-12 col-lg-4 batch_name">
                      <label for="" class="mr-sm-2">Voucher Code Batch Name</label>
                      <input type="text" name="gc_voucher_code_batch_name" placeholder="Voucher Code Batch Name" class="form-control" value="{{ $generate_codes->gc_voucher_code_batch_name }}">
                    </div>
                    <div class="col-md-12 col-lg-4">
                        
                        <label for="name" class="mr-sm-2">Whole Batch</label>
                        <select class="form-control" name="whole_batch" id="whole_batch">
                          <option value="0">Select Whole Batch</option>
                          
                          <option value="YES" {{old('whole_batch') ==  'YES' ? 'Selected' : ''}}>Yes</option>
                          <option value="NO"  {{old('whole_batch') == 'NO' ? 'Selected' : ''}}>No</option>
                            
                        </select>
                      </div>
                    <div class="col-md-12 col-lg-4 voucher_code" style="display:none;">
                      <label for="" class="mr-sm-2">Voucher code</label>
                      <input type="text" name="voucher_code" placeholder="Enter Voucher Code" class="form-control" value="{{ old('voucher_code') }}">
                    </div>
                    
                    
                    
                  </div>
                  
                  <div class="form-group row mx-auto">
                    <div class="col-md-12 col-lg-4">
                      <label for="" class="mr-sm-2">Last Date to Register date</label>
                      <input type="text" name="gc_last_date_to_register_date" placeholder="Enter Last Date to Register date" class="form-control datetimepicker2" value="{{ $generate_codes->gc_last_date_to_register_date }}" >
                    </div>
                    
                    <div class="col-md-12 col-lg-4 ">
                       <label for="" class="mr-sm-2">Offer Valid till date</label>
                      <input type="text" name="gc_offer_valid_till_date" placeholder="Enter Offer Valid till date" class="form-control datetimepicker2" value="{{ $generate_codes->gc_offer_valid_till_date }}">
                    </div>
                  </div>
                
                  <div class="form-group row">
                    <div class="col-md-12 text-right">
                      <button type="button" class="btn btn-primary bigcity-button" onClick="$('#loading').show();
                    $('#generatecodefrm').submit();"> Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop