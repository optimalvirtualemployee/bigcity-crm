@extends('common.default')
@section('title', 'Product Rule List')
@section('content')

<!-- Main Content -->
<div id="content">
<!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
       
       
   
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Voucher Codes</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/generate_codes')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
      
      <!-- Page Heading -->
      <div class="card shadow">
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  <!--<li style="margin-right: 20px;"><a href="{{url('admin/generate_codes/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Generate Code
                     </button></a>
                  </li>-->
                 <li style="margin-right: 20px;">
                      
                  <a href="{{ url('admin/generate_code/csv-download',$generate_codes->id)}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download All Voucher Code
                     </button></a>
                     </li>
                  
               </ul>
            </nav>
         </div>
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table table-striped " id="" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        
                        <th>SN</th>
                        <th>Campaign Id</th>
                        <th>Campaign Name</th>
                        <th>Product Id</th>
                        <th>Product Name</th>
                        
                        <th>Voucher Code</th>
                        
                        
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i = 1; ?> 

                     @forelse($voucher_codes as $index=>$code)
                 
                    <tr>
                        <td>{{$code['SN']}}</td>
                        <td>{{$code['Campaign Id']}}</td>
                        <td>{{$code['Campaign Name']}}</td>
                        <td>{{$code['Product Id']}}</td>
                        <td>{{$code['Product Name']}}</td>
                        <td>{{$code['voucher Code']}}</td>
                        
                        
                        
                        
                        
                    </tr><?php $i++ ?>
                    @empty
                    
                    @endforelse
                    
                    
                  </tbody>
               </table>
               

              <div style='padding: 10px 20px 0px; border-top: dotted 1px #CCC;'>
                <strong>Page <?php echo $page." of ".$totalPages; ?></strong>
              </div>
              
              <nav aria-label="Page navigation example">
              <ul class="pagination">
              @if($page > 1)
              <li class="page-item"><a class="page-link" href='?page=1'>First Page</a></li>
              @endif
                  
              <li class="page-item" <?php if($page <= 1){ echo "class='disabled'"; } ?>>
              <a class="page-link" <?php if($page > 1){
              echo "href='?page=$previous_page'";
              } ?>>Previous</a>
              </li>
                  
              
                
                @for ($counter = 1; $counter < 8; $counter++)
                @if ($counter == $page)
                    <li class='page-item active'><a class="page-link">{{$counter}}</a></li>
                @else
                  <li class="page-item"><a class="page-link" <?php echo "href='?page=$counter'";?>>{{$counter}}</a></li>
                @endif
                @endfor         
                
                <li  class="page-item"><a class="page-link" >...</a></li>
                <li class="page-item"><a class="page-link" <?php echo "href='?page=$second_last'";?>>{{$second_last}}</a></li>
                <li class="page-item"><a class="page-link" <?php echo "href='?page=$totalPages'";?>>{{$totalPages}}</a></li>
                
                <li class="page-item" <?php if($page >= $totalPages){
              echo "class='disabled'";
              } ?>>
              <a class="page-link" <?php if($page < $totalPages) {
              echo "href='?page=$next_page'";
              } ?>>Next</a>
              </li>

              @if($page < $totalPages)

              <li class="page-item"><a class="page-link"  <?php echo "href='?page=$totalPages'";?>>Last &rsaquo;&rsaquo;</a></li>
              @endif

              </ul>
              
              </nav>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
