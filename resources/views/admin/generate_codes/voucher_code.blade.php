@extends('common.default')
@section('title', 'Generate Code List')
@section('content')

<!-- Main Content -->
<div id="content">
<!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      
  
      
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Voucher Codes</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                
                       <a href="{{ url('admin/generate_code/download-voucher-excel',$generate_codes->id)}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download All Voucher Code
                     </button></a>  
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/generate_codes')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
      
      <!-- Page Heading -->
      <div class="card">
 
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        
                        <th>SN</th>
                
                        <th>Voucher Code</th>
                        <th>Last Date to Register</th>
                        <th>Offer Valid Date</th>
                        
                        
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i = 1; ?> 

                     @forelse($voucher_code as $index=>$code)
                 
                    <tr>
                        <td>{{++$index}}</td>
                        
                        <td>{{$code->voucher_code}}</td>
                        <td>{{$code->gc_last_date_to_register_date}}</td>
                        <td>{{$code->gc_offer_valid_till_date}}</td>
                        
                        
                        
                        
                        
                    </tr><?php $i++ ?>
                    @empty
                    
                    @endforelse
                    
                    
                  </tbody>
               </table>
               
               

                          </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
