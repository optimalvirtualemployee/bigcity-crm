@extends('common.default')
@section('title', 'Edit User')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
       
            <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Users</h1>
             <a href="{{url('admin/users')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center pt-4">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('users.update', $users->id) }}">
                           @method('PATCH') 
                           @csrf
                          
                           <div class="form-group">
                              <label>Name <span class="required-field">*</span></label>
                              <input type="text" class="form-control" placeholder="Name" value="{{$users->name}}" name="name" id="name">
                           <!--@if($errors->has('name'))
                           <div class="error">{{ $errors->first('name') }}</div>
                           @endif-->
                           </div>
                          
                           <div class="form-group">
                              <label>Phone <span class="required-field">*</span></label>
                              <input type="text" class="form-control" placeholder="Phone" value="{{$users->phone}}" name="phone" id="phone">
                              <!--@if($errors->has('phone'))
                              <div class="error">{{ $errors->first('phone') }}</div>
                              @endif-->
                           </div>
                           <!--<div class="form-group">
                              <label>Email <span class="required-field">*</span></label>
                              <input type="email" class="form-control" placeholder="Email" value="{{$users->email}}" name="email" id="txtEmail" onkeyup="ValidateEmail();">
                               <span id="lblError" style="color: red"></span>-->
                           <!--@if($errors->has('email'))
                           <div class="error">{{ $errors->first('email') }}</div>
                           @endif-->
                           <!--</div>-->
                           <div class="form-group">
                              <label>Role<span class="required-field">*</span></label>
                              <select name="role" class="form-control">

                              <option value="0">Select Role</option>
                              @forelse($roles as $role)
                              <option value="{{$role->id}}" {{$role->id == $users->role  ? 'selected' : ''}}>{{$role->name}}</option>
                              @empty
                             <p>No Role List</p>
                             @endforelse
                              </select>
                              
                              <!--@if($errors->has('role'))
                              <div class="error">{{ $errors->first('role') }}</div>
                              @endif-->
                           </div>
                           <div class="form-group">
                              <label>Password <span class="required-field">*</span></label>
                              <input type="password" class="form-control" placeholder="Password" value="" name="password">
                              <!--@if($errors->has('password'))
                              <div class="error">{{ $errors->first('password') }}</div>
                              @endif-->
                           </div>
                           
                           <div class="form-group">
                              <label>Confirm Password <span class="required-field">*</span></label>
                              <input type="password" class="form-control" placeholder="Confirm Password" value="" name="confirm_pass">
                              <!--@if($errors->has('password'))
                              <div class="error">{{ $errors->first('password') }}</div>
                              @endif-->
                           </div>
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop