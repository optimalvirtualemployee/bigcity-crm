@extends('common.default')
@section('content')
<style>
 
  .icon i{
      font-size: 50px;
      color: #FFF;
      }
      .icon i:hover{
      font-size: 50px;
      color: #FFF;
       
      transition: .5s; 
      
      }
      
</style>
<!-- For demo purpose -->
    <div class="container">
        
           <div class="d-sm-flex align-items-center justify-content-between pt-4">
            <h1 class="h3 mb-0 text-gray-800    h-100 px-1 text-capitalize">Users</h1>
             <a href="{{url('admin/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary  pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/users')}}">
                                             <p class="icon" > <i class="fas fa-handshake"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          User</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary  pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/users')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Add User</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        
                     </div>
                  </div>
               </div>
               
            </div>
@stop