@extends('common.default')
@section('title', 'User Create')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
          <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize"> Add Users</h1>
             <a href="{{url('admin/users')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card login-card small_forms">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div>
                        @endif
                        <form class="user" action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
                           @csrf
                           <div class="form-group">
                              <label>Name <span class="required-field">*</span></label>
                              <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name') }}">
                           <!--@if($errors->has('name'))
                           <div class="error">{{ $errors->first('name') }}</div>
                           @endif-->
                           </div>
                           
                       
                           <div class="form-group">
                              <label>Phone <span class="required-field">*</span></label>
                              <input type="text" class="form-control" placeholder="Phone"  name="phone" id="phone" value="{{ old('phone') }}">
                              <!--@if($errors->has('phone'))
                              <div class="error">{{ $errors->first('phone') }}</div>
                              @endif-->
                           </div>
                           <div class="form-group">
                              <label>Email <span class="required-field">*</span></label>
                              <input type="email" class="form-control" placeholder="Email"  name="email" id="txtEmail" onkeyup="ValidateEmail();" value="{{ old('email') }}">
                               <span id="lblError" style="color: red"></span>
                           <!--@if($errors->has('email'))
                           <div class="error">{{ $errors->first('email') }}</div>
                           @endif-->
                           </div>

                           <div class="form-group">
                              <label>Role<span class="required-field">*</span></label>
                              <select name="role" class="form-control">

                              <option value="0">Select Role</option>
                              @forelse($roles as $role)
                              <option value="{{$role->id}}" {{ (old('role') ==  $role->id)? 'Selected' : ''}}>{{$role->name}}</option>
                              @empty
                             <p>No Role List</p>
                             @endforelse
                              </select>
                              
                              <!--@if($errors->has('role'))
                              <div class="error">{{ $errors->first('role') }}</div>
                              @endif-->
                           </div>
                            <div class="form-group">
                              <label>Password <span class="required-field">*</span></label>
                              <input type="password" class="form-control" placeholder="Password"  name="password" id="txtpassword">
                              <!--@if($errors->has('password'))
                              <div class="error">{{ $errors->first('password') }}</div>
                              @endif-->
                           </div>
                           <div class="form-group">
                              <label>Confirm Password <span class="required-field">*</span></label>
                              <input type="password" class="form-control" placeholder="Confirm Password"  name="confirm_pass" id="txtconfirmpass" onChange="checkPasswordMatch();">
                              <!--@if($errors->has('password'))
                              <div class="error">{{ $errors->first('password') }}</div>
                              @endif-->
                           </div> 
                           <div class="form-group">
                              <label>Status<span class="required-field">*</span></label>
                              <select name="status" class="form-control">

                              
                              
                              <option value="0" {{ (old('status') ==  '0')? 'Selected' : ''}}>Inactive</option>
                              <option value="1" {{ (old('status') ==  '1')? 'Selected' : ''}}>Active</option>
                              
                              </select>
                              
                              <!--@if($errors->has('role'))
                              <div class="error">{{ $errors->first('role') }}</div>
                              @endif-->
                           </div>
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary  btn-block mt-4">Submit</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop