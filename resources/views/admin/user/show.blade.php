@extends('common.default')
@section('title', 'Show User')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
       
       
             
       <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Users</h1>
             <a href="{{url('admin/users')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>

        <table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Role</th>
                        <th>Status</th>
                        
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                     
                      
                    <tr>
                        <td>  	{{ $users->id }} </td>
                        <td>  	{{ $users->name }} </td>
                        
                        <td>   	   {{ $users->email }} </td>
                        
                         <td>  {{ $users->phone }}  </td>
                        <td>   {{ $users->user_role['name'] }} </td>
                        <td>       {{ ($users->status == '1')? 'Active':'Inactive' }}</td>
                 
                    
                  </tbody>
               </table>
              
   </div>
</div>
@stop




