@extends('common.default')
@section('title', 'User List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 20px;">
      <!-- Page Heading -->
      <!-- DataTales Example -->
          <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Users</h1>
             <a href="{{url('admin/user/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
        
      <div class="card shadow ">
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  <li style="margin-right: 20px;"><a href="{{url('admin/users/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add User
                     </button></a>
                  </li>
                  <!--<li style="margin-right: 20px;">  <button class="btn btn-primary btn-sm"     type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Import User
                     </button>
                  </li>
                  <li style="margin-right: 20px;">  <button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-print"></i> &nbsp; Print User
                     </button>
                  </li>
                  <li class="nav-item dropdown" ml-2>
                     <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-file-export"></i>&nbsp; Export User
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton ">
                           <a class="dropdown-item font-weight-bold" href="abc.xlsx"><i class="fas fa-file-excel"></i> &nbsp; Export to Excel</a>
                           <a class="dropdown-item font-weight-bold" href="abc.docx"><i class="fas fa-file-word"></i> &nbsp; Export to Word</a>
                           <a class="dropdown-item font-weight-bold" href="abc.pdf"><i class="fas fa-file-pdf"></i> &nbsp;Export to PDF</a>
                        </div>
                     </div>
                  </li>-->
               </ul>
            </nav>
         </div>
    
         <div class="card-body" >
                  @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
       
       
       
            <div class="table-responsive">
               <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important; width: 100%;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>SN</th>
                        <th>User Id</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                     @forelse($list as $index=>$data)
                      
                    <tr>
                        <th scope="row">{{++$index}}</th>
                        <td>{{$data->id}}</td>
                        <td>{{$data->name}}</td>
                        <td>{{$data->user_role['name']}}</td>
                        <td>{{$data->phone}}</td>
                        <td>{{$data->email}}</td>
                       
                        
                        
                        
                        <td>
                           <a href="{{ route('users.show',$data->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('users.edit',$data->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        </td>
                        <td>
                            
                              <input data-id="{{$data->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $data->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="User">  
                        </td> 
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop
