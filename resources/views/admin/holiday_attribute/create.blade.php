@extends('common.default')
@section('title', 'Holiday Attribute Create')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
        
        
        <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Add Holiday Attribute</h3>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/holiday-attribute')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>  
        </span>
        </div>
        </div>
        </div>
        
        
        
        
        
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center" style="margin-top:50px;">
         <div class="card login-card">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <form class="user" action="{{ route('holiday-attribute.store') }}" method="POST">
                           @csrf
                          <div class="form-group row mx-auto">
                          
                            <div class="col-md-12 col-lg-3">
                             <label> City</label>
                              
                              <select class="inputbox form-control js-example-basic-single" name="winner_city_id">
                                <option value="0">Select Winner City</option>
                                @forelse($cities as $city)
                                <option value="{{$city->id}}" {{old('winner_city_id') == $city->id ? 'Selected' : ''}}>{{$city->name}}</option>
                                @empty
                                  <p>No State List</p>
                                @endforelse
                                
                                
                              </select>
                            </div>
                            <div class="col-md-12 col-lg-3">
                                <label>Destination City</label>
                                
                              <select class="inputbox form-control js-example-basic-single" name="destination_city_id">
                                <option value="0">Select Destination City</option>
                                @forelse($cities as $city)
                                <option value="{{$city->id}}" {{old('destination_city_id') == $city->id ? 'Selected' : ''}}>{{$city->name}}</option>
                                @empty
                                  <p>No State List</p>
                                @endforelse
                                
                                
                              </select>
                            </div>
                            <div class="col-md-12 col-lg-3">
                              <label for="">Traveler Date</label>
                              <input type="date" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" name="travel_date" value="{{ old('travel_date') }}">
                            </div>
                            <div class="col-md-12 col-lg-3">
                              <label for="">Co Traveler Name</label>
                              <input type="number" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter No of Co-Traveler Name " maxlength="20" name="no_of_cotraveler" value="{{ old('no_of_cotraveler') }}">
                            </div>
                          </div>
                          <div class="form-group row mx-auto">
                          
                            <div class="col-md-12 col-lg-3">
                             <label for="">Winner Name</label>
                              <input type="text" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Winner Name" name="win_name" value="{{ old('win_name') }}">
                            </div>
                            
                            <div class="col-md-12 col-lg-3">
                              <label for="">Winner DOB</label>
                              <input type="date" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Winner DOB" name="win_dob" value="{{ old('win_dob') }}">
                            </div>
                            <div class="col-md-12 col-lg-3">
                                 <label for=""> Winner Passport No</label>
                              <input type="text" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Winner Passport No" name="win_passport_no" value="{{ old('win_passport_no') }}">
                            </div>
                            <div class="col-md-12 col-lg-3">
                              <label for="">Winner Passport Expiry Date</label>
                              <input type="date" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Winner Passport Expiry Date" name="win_passport_expiry_date" value="{{ old('win_passport_expiry_date') }}">
                            </div>
                            
                          </div>
                          <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-3">
                                 <label for="">Winner Meal Preference</label>
                              <input type="text" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Winner Meal Preference" name="win_meal_preference" value="{{ old('win_meal_preference') }}">
                            </div>
                            <div class="col-md-12 col-lg-3">
                                 <label for="">Co Traveler Name</label>
                              <input type="text" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Co-Traveler Name" name="co_name" value="{{ old('co_name') }}">
                            </div>
                            
                            <div class="col-md-12 col-lg-3">
                              <label for="">Co-Traveler DOB</label>
                              <input type="date" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Co-Traveler DOB" name="co_dob" value="{{ old('co_dob') }}">
                            </div>
                            <div class="col-md-12 col-lg-3">
                                 <label for="">Co-Traveler Passport No</label>
                              <input type="text" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Co-Traveler Passport No." name="co_passport_no" value="{{ old('co_passport_no') }}">
                            </div>
                          </div>
                          <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-3">
                              <label for="">Passport Expiry Date</label>
                              <input type="date" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" name="co_passport_expiry_date" value="{{ old('co_passport_expiry_date') }}">
                            </div>
                            <div class="col-md-12 col-lg-3">
                               <label for="">Winner Meal Preference</label>
                              <input type="text" class="form-control form-control-user"
                              id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Winner Meal Preference" name="co_meal_preference" value="{{ old('co_meal_preference') }}">
                            </div>
                            <div class="col-md-12 col-lg-3">
                              <label for="">Winner Upload Passport</label>
                              

                              <input type="file" class="form-control" id="exampleFormControlFile1" name="win_upload_pass" value="{{ old('win_upload_pass') }}">
                            
                              <div id="dvPreview">
                              </div>
                            </div>
                            
                            <div class="col-md-12 col-lg-3">
                              <label for="">Co Upload Pass</label>
                              <input type="file" class="form-control" id="exampleFormControlFile1" name="co_upload_pass" value="{{ old('co_upload_pass') }}">
                            
                              <div id="dvPreview">
                              </div>
                            </div>
                            
                            
                          </div>
                          
                          
                          <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-3">
                                 <label for="">Gift Tax Required</label>
                              <select class="inputbox form-control js-example-basic-single" id="gift_tax_required" name="gift_tax_required">
                                <option value="0">Select Gift Tax Required</option>
                                
                                <option value="YES" {{old('gift_tax_required') == 'YES' ? 'Selected' : ''}}>Yes</option>
                                <option value="NO" {{old('gift_tax_required') == 'NO' ? 'Selected' : ''}}>No</option>
                                
                                
                                
                              </select>
                            </div>
                            <div class="col-md-12 col-lg-3 gift_tax_amount">
                                 <label for="">Gift Tax Amount</label>
                                <input type="text" class="form-control form-control-user"
                                id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Gift Tax Amount" name="gift_tax_amount" value="{{ old('gift_tax_amount') }}">
                            </div>
                          </div>
                            
                             <div class="col-md-12 col-lg-12">                         
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary">Submit</button>
                           </a>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop