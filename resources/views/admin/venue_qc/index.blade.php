@extends('common.default')
@section('title', 'Venue List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 20px;">
       
       
       
        
         <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
                <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Venue QC List</h3>
                
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                    
                     <!--<a href="{{url('admin/target-assign/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Target Assign
                     </button></a>-->
                    
                    <a  href="{{url('admin/venue/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                        <i class="fas fa-chevron-left mr-1"></i> Back
                    </a>
                     
                </span>
            </div>
        </div>
        </div>
        </div>
       <br>
        
        
        
        
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card shadow" style="margin-top:0px;">
         
         
         <div class="card-body" >
             @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th >Venue Id</th>
                        <th>Venue Name</th>
                        <th>Venue Type</th>
                        <th>PD Name</th>
                        <th>Product Catgeory Name</th>
                        <!--<th>QC Comment</th>-->
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        
                        <th>Action</th>
                        
                        
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($venues as $index=>$venue)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$venue->id}}</td>
                        <td>{{$venue->venue_name}}</td>
                        <td>{{($venue->venue_type == '1')? 'Chain' : 'Standalone'}}</td>
                        <td>{{$venue->user['name']}}</td>
                        <td>{{$venue->category['name']}}</td>
                        <!--<td>{{isset($venue->qc_venue_comment) ? $venue->qc_venue_comment : 'NA' }}</td>-->
                        <td>{{isset($venue->created_user['name']) ? $venue->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($venue->updated_user['name']) ? $venue->updated_user['name'] : 'NA'}}</td>
                        <td>{{$venue->created_at->format('d M Y')}}/{{$venue->created_at->format('g:i A')}}</td>
                        <td>{{$venue->updated_at->format('d M Y')}}/{{$venue->updated_at->format('g:i A')}}</td>
                        
                        <td>
                           <a href="{{ route('venue-qc.show',$venue->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <!--<a href="{{ route('venues.edit',$venue->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>-->
                            
                           
                        </td>
                        
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
