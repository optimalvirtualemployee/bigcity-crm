@extends('common.default')
@section('content')
<!-- For demo purpose -->
    <div class="container">
        
           <div class="d-sm-flex align-items-center justify-content-between  pt-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Venues</h1>
             <a href="{{url('admin/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        
                        <div class="col-6 col-sm-6 col-md-2">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/venues')}}">
                                             <p class="icon" > <i class="fas fa-handshake"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Venues</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-2">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/venues')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Add Venues</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        
                     </div>
                  </div>
               </div>
               <div class="d-sm-flex align-items-center justify-content-between  pt-4">
                   <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Venue QC</h1>
             
               </div>
               <div class="row mb-5">
                   
                   
                  <div class="col-xl-3 col-sm-6 py-2">
                    <div class="card bg-success text-white">
                        <div class="card-body bg-danger">
                            <div class="rotate">
                                <i class="fa fa-user fa-2x"></i>
                            </div>
                            <h6 class="text-uppercase">Total Venue</h6>
                            <h1 class="display-4">{{$totalVenue->count()}}</h1>
                              <a href="#" class="btn btn-primary btn-sm" style="background-color: transparent;
    border: 1px solid #FFF;">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  <div class="col-xl-3 col-sm-6 py-2">
                    <div class="card text-white bg-danger h-100">
                        <div class="card-body bg-danger">
                            <div class="rotate">
                                <i class="fa fa-list fa-2x"></i>
                            </div>
                            <h6 class="text-uppercase">Total Confirmed Venue</h6>
                            <h1 class="display-4">0</h1>
                              <a href="#" class="btn btn-primary btn-sm" style="background-color: transparent;
    border: 1px solid #FFF;">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-xl-3 col-sm-6 py-2">
                    <div class="card text-white bg-info h-100">
                        <div class="card-body bg-danger">
                            <div class="rotate">
                                <i class="fa fa-list fa-2x"></i>
                            </div>
                            <h6 class="text-uppercase">Total Rejected Venue</h6>
                            <h1 class="display-4">0</h1>
                               <a href="#" class="btn btn-primary btn-sm" style="background-color: transparent;
    border: 1px solid #FFF;">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-xl-3 col-sm-6 py-2">
                    <div class="card text-white bg-danger h-100">
                        <div class="card-body">
                            <div class="rotate">
                                <i class="fa fa-list fa-2x"></i>
                            </div>
                            <h6 class="text-uppercase">Total Venue QC</h6>
                            <h1 class="display-4">{{$totalVenueQC->count()}}</h1>
                            <a href="#" class="btn btn-primary btn-sm" style="background-color: transparent;
    border: 1px solid #FFF;">Read More</a>
                        </div>
                    </div>
                  </div>
                  
               </div>
               
            </div>
@stop