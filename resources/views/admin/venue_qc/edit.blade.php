@extends('common.default')
@section('title', 'Venue Edit')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
       
          <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Venues</h1>
             <a href="{{url('admin/venue-qc')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
        
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center mt-5">
         <div class="card login-card">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('venues.update', $venues->id) }}" id="venuefrm">
                           @method('PATCH') 
                           @csrf
                          
                          <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                    <select class="inputbox form-control" name="venue_type" id="venue_type">
                                      
                                      <option value="2" {{$venues->venue_type == '2' ? 'Selected' : ''}}>Stand Alone</option>
                                      <option value="1" {{$venues->venue_type == '1' ? 'Selected' : ''}}>Chain</option> 
                                    </select>
                                </div>
                                
                                <div class="col-md-12 col-lg-4">
                                    <select class="form-control" name="pd_lead_id">
                                      <option value="0">Select PD Lead</option>
                                      @forelse($users as $user)
                                      <option value="{{$user->id}}" {{($user->id == $venues->pd_lead_id) ? 'selected' : ''}}>{{$user->name}}</option>
                                      @empty
                                        <p>No User List</p>
                                      @endforelse
                                      </div>
                                      
                                    </select>
                                </div>
                                <div class="col-md-12 col-lg-4 venueUpload">
                                  
                                  <label for="">Venue Upload</label>
                                  <input type="file" class="form-control latFeild" id="" name="venue_file" accept=".xlsx, .xls, .csv">
                                  <span id="venueFileErrorMsg" style="color:red;"></span>
                                    
                                </div>
                                <div class="col-md-12 col-lg-4 venueUpload">
                                  
                                  <label for="">Chain Name</label>
                                  <input type="text" class="form-control latFeild" id="" name="chain_name" placeholder="Enter Chain Name" value="{{$venues->chain_name}}">
                                  <span id="venueFileErrorMsg" style="color:red;"></span>
                                    
                                </div>
                                <div class="col-md-12 col-lg-4 for-standalone">
                                  <input type="text" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Venue Name" name="venue_name" value="{{ $venues->venue_name }}">
                                </div>
                                <!--<div class="col-md-12 col-lg-4">
                                    <select class="inputbox form-control category" name="category_id">
                                      <option value="0">Select Product Category</option>
                                      @forelse($categories as $cat)
                                      <option value="{{$cat->id}}" {{($cat->id == $venues->category_id) ? 'selected' : ''}}>{{$cat->name}}</option>
                                      @empty
                                        <p>No Category List</p>
                                      @endforelse
                                      </div>
                                      
                                   </select>
                                  </div>-->
                                </div>
                                <div class="for-standalone">
                                <div class="form-group row mx-auto">
                                    
                                    
                                    
                                    <div class="col-md-12 col-lg-4">
                                      <select class="inputbox form-control js-example-basic-single state" name="state_id">
                                        <option value="0">Select State</option>
                                        @forelse($states as $state)
                                        <option value="{{$state->id}}" {{$state->id == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                                        @empty
                                          <p>No State List</p>
                                        @endforelse
                                        
                                        
                                      </select>
                                    </div>
                                    
      
                                    <div class="col-md-12 col-lg-4" >
                                       <select class="form-control js-example-basic-single city" name="city_id" data-placeholder="First Select State" style="height: 45px;">
                                           @forelse($cities as $city)
                                          <option value="{{$city->id}}" {{($venues->city_id) == $city->id ? 'Selected' : ''}}>{{$city->name}}</option>
                                          @empty
                                          <p>No city List</p>
                                          @endforelse
                                       </select>
                                    </div>
                                    <div class="col-md-12 col-lg-4" >
                                       <select class="form-control js-example-basic-single area" name="area_id" data-placeholder="First Select City" style="height: 45px;">
                                           @forelse($areas as $area)
                                          <option value="{{$area->id}}" {{($venues->area_id) == $area->id ? 'Selected' : ''}}>{{$area->area_name}}</option>
                                          @empty
                                          <p>No city List</p>
                                          @endforelse
                                       </select>
                                    </div>
                                  
                                </div>
                                <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4 address">
                                  <input type="text" name="latitude" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Latitude" name="latitude" id="latitude" value="{{ $venues->latitude}}">
                                  <span id="venueLatErrorMsg" style="color:red;"></span>
                                </div>
                                <div class="col-md-12 col-lg-4 address">
                                  <input type="text" name="longitude" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Longitude" name="longitude" value="{{ $venues->longitude }}">
                                  <span id="venueLangErrorMsg" style="color:red;"></span>
                                </div>
                                <div class="col-md-12 col-lg-4 address">
                                    <select class="inputbox form-control" name="rating">
                                      <option value="0">Select Rating</option>
                                      
                                      <option value="1" {{$venues->rating == '1' ? 'Selected' : ''}}>1</option>
                                      <option value="2" {{$venues->rating == '2' ? 'Selected' : ''}}>2</option>
                                      <option value="3" {{$venues->rating == '3' ? 'Selected' : ''}}>3</option>
                                      <option value="4" {{$venues->rating == '4' ? 'Selected' : ''}}>4</option>
                                      <option value="5" {{$venues->rating== '5' ? 'Selected' : ''}}>5</option>
                                      
                                      
                                   </select>
                                  </div>
                                
                              </div>
                              <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-6 other_value">
                                    <input type="text" class="form-control latFeild" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="This is to capture information like Paytm number etc" name="other_value" value="{{ $venues->other_value }}">
                                </div>
                                <div class="col-md-12 col-lg-6 address">
                                  <textarea class="form-control  addressFeild" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Address" name="address">{{ $venues->address }}</textarea>
                                  

                                </div>
                              </div>
                              <div class="form-group row mx-auto"> 
                                  <div class="col-md-12 col-lg-3">
                                      <select name="black_out_day" id="black_out_day" class="inputbox form-control">
                                        <option value="0">Select Black Out Day</option>
                                        
                                        <option value="1" {{$venues->black_out_day == '1' ? 'Selected' : ''}}>No black out days </option>
                                        <option value="2" {{$venues->black_out_day == '2' ? 'Selected' : ''}}>Multiple black out days</option>
                                        
                                      </select>
                                  </div>
                                  <div class="col-md-12 col-lg-9 day_name" style="display:flex;">
                                    <label style="font-weight:700;"> Miltiple Black Out Days : </label>&nbsp;
                                    <div class="form-check list">
                                      <input class="form-check-input" type="checkbox" name="day_name[]" id="exampleRadios1" value="Monday">
                                      <label class="form-check-label" for="exampleRadios1">Monday</label>&nbsp;
                                    </div>
                                    
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="day_name[]" id="exampleRadios1" value="Tuesday">
                                      <label class="form-check-label" for="exampleRadios1">
                                        Tuesday
                                      </label>
                                      &nbsp;
                                    </div>
                                    
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="day_name[]" id="exampleRadios1" value="Wednesday"> 
                                      <label class="form-check-label" for="exampleRadios1">
                                        Wednesday
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="day_name[]" id="exampleRadios1" value="Thursday"> 
                                      <label class="form-check-label" for="exampleRadios1">
                                        Thursday
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="day_name[]" id="exampleRadios1" value="Friday"> 
                                      <label class="form-check-label" for="exampleRadios1">
                                        Friday
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="day_name[]" id="exampleRadios1" value="Saturday"> 
                                      <label class="form-check-label" for="exampleRadios1">
                                        Saturday
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="day_name[]" id="exampleRadios1" value="Sunday"> 
                                      <label class="form-check-label" for="exampleRadios1">
                                        Sunday
                                      </label>
                                    </div>
                                    
                                    <span id="venueListTypeErrorMsg" style="color:red;"></span>
                                  </div>
                                  
                              </div>
                                <div class="form-group row mx-auto"> 

                                <div class="col-md-12 col-lg-4 address" style="display:flex;">
                                  <label style="font-weight:700;"> Venue List : </label>&nbsp;
                                  <div class="form-check list">
                                    <input class="form-check-input" type="checkbox" name="list_type[]" id="exampleRadios1" value="A List">
                                    <label class="form-check-label" for="exampleRadios1">A List</label>&nbsp;
                                  </div>
                                    
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="list_type[]" id="exampleRadios1" value="B List">
                                    <label class="form-check-label" for="exampleRadios1">
                                      B List
                                    </label>
                                    &nbsp;
                                  </div>
                                    
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="list_type[]" id="exampleRadios1" value="C List"> 
                                    <label class="form-check-label" for="exampleRadios1">
                                      C List
                                    </label>
                                  </div>
                                    
                                  <span id="venueListTypeErrorMsg" style="color:red;"></span>
                                </div>
                                <div class="col-md-12 col-lg-4">
                              
                                  <select class="inputbox form-control" name="billing_terms" id="billing_terms">
                                    <option value="0">Select Billing Term</option>
                                    <option value="1" {{$venues->billing_terms == '1' ? 'Selected' : ''}}>Pre Paid</option>
                                    <option value="2" {{$venues->billing_terms == '2' ? 'Selected' : ''}}>Post Paid</option>
                                    <option value="3" {{$venues->billing_terms == '2' ? 'Selected' : ''}}>CC Form</option>
                                    <option value="4" {{$venues->billing_terms == '2' ? 'Selected' : ''}}>Any Other</option>
                                  </select>
                                </div> 
                                <div class="col-md-12 col-lg-4">
                                  <select class="inputbox form-control booking_alert_required" name="booking_alert_required">
                                    <option value="0">Select Booking Alert Required</option>
                                    <option value="4" {{$venues->booking_alert_required == '4' ? 'Selected' : ''}}>None</option>
                                    <option value="1" {{$venues->booking_alert_required == '1' ? 'Selected' : ''}}>SMS</option>
                                    <option value="2" {{$venues->booking_alert_required == '2' ? 'Selected' : ''}}>Both</option>
                                    <option value="3" {{$venues->booking_alert_required == '3' ? 'Selected' : ''}}>Email</option>
                                  </select>
                                </div>
                                  
                              </div>
                              <div class="bank_detail">
                                <label style="margin-left:15px;font-weight:700;">Bank Details:</label>
                              <div class="form-group row mx-auto">
                                        
                                <div class="col-md-12 col-lg-3">
                                    <input type="text" class="form-control latFeild" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Bank Name" name="bank_name" value="{{ $venues->bank_name }}">
                                </div>
                                <div class="col-md-12 col-lg-3">
                                    <input type="text" class="form-control latFeild" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Account Number" name="account_number" value="{{ $venues->account_number }}">
                                </div>
                                <div class="col-md-12 col-lg-3">
                                    <input type="text" class="form-control latFeild" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter IFSC Code" name="ifsc_code" value="{{ $venues->ifsc_code }}">
                                </div>
                                <div class="col-md-12 col-lg-3">
                                    <input type="text" class="form-control latFeild" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Name on Account" name="name_on_account" value="{{ $venues->name_on_account }}">
                                </div>
                                        
                              </div>
                              </div>
                                
                                <div class="form-group row mx-auto"> 
                                  <div class="col-md-12 col-lg-4">
                                  <select class="inputbox form-control category js-example-basic-single"  name="category_id">
                                    <option value="0">Select Category</option>
                                    @forelse($categories as $cat)
                                    <option value="{{$cat->id}}" {{$venues->category_id == $cat->id ? 'Selected' : ''}}>{{$cat->name}}</option>
                                    @empty
                                      <p>No Category List</p>
                                    @endforelse
                                    
                                    
                                  </select>
                                </div>
                                  <div class="col-md-12 col-lg-4">
                                    
                                    <select class="inputbox form-control product select2" multiple="multiple" name="product_id[]" data-placeholder="Select a product" id="sel1">
  
                                    
                                        <?php $counter = count($venueProduct); ?>  
            
                                        @foreach($venueProduct as $key => $value)
                                        
                                        <option value="{{$value['product_id']}}" selected>{{$value['product']['name']}}</option>
                                        @endforeach
                                    </select>


                                    
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <select class="inputbox form-control bcp_cost" name="bcp_cost">
                                    <option value="0">Select BCP Cost</option>
                                    <option value="1" {{old('bcp_cost') == '1' ? 'Selected' : ''}}>Same cost All days</option>
                                    <option value="2" {{old('bcp_cost') == '2' ? 'Selected' : ''}}>Variable cost day wise</option>
                                    <option value="3" {{old('bcp_cost') == '3' ? 'Selected' : ''}}>Different cost for different services</option>
                                    
                                  </select>
                                </div>
                                </div>
                                <div class="row mx-auto parent_clone"> 
                                <?php $counter = count($venueProduct); ?>  
            
                                    @foreach($venueProduct as $key => $value)
                                    
                                        
                                            <div class="col-md-12 col-lg-3">
                                                <textarea name="description[]" class="form-control form-control-user"  placeholder="Enter Product Description">
                                                    {{ $value['description'] }}
                                                </textarea>
                                            </div>
                                            <div class="col-md-12 col-lg-3">
                                                <input type="number" name="mrp_of_product[]" class="form-control mrp_of_product" placeholder="Enter MRP of Product" value="{{ $value['mrp_of_product'] }}" min="1">
                                                
                                            </div>
                                            <div class="col-md-12 col-lg-3">
                                                <input type="number" name="cost_of_product[]" class="form-control cost_of_product" placeholder="Enter Cost of Product" value="{{ $value['cost_of_product'] }}" min="1">
                                            </div>
                                            
                                        
                                    
                                    @endforeach
                                </div>
                                <br/>
                                <div class="parent_clone_billing"> 
                                <?php $counter = count($billingContact); ?>  
            
                                    @foreach($billingContact as $key => $value)
                                    <div class="child_clone_billing"> 
                                    <label style="margin-left:15px;font-weight:700;">Partner Billing :</label>
                                        <div class="row mx-auto">
                                            <div class="col-md-12 col-lg-2">
                                              <input type="text" name="billing_name[]" class="form-control form-control-user billing_name" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Name" value="{{ $value['name']}}">
                                            </div>
                                              
                                            <div class="col-md-12 col-lg-2">
                                              <input type="email" name="billing_email[]" class="form-control form-control-user billing_email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email" value="{{ $value['email']}}">
                                            </div>
                                            <div class="col-md-12 col-lg-2">
                                              <input type="tel" name="billing_phone[]" class="form-control form-control-user billing_phone" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter phone" value="{{ $value['phone']}}">
                                            </div>
                                            <div class="col-md-12 col-lg-2">
                                              <input type="text" name="billing_designation[]" class="form-control form-control-user billing_designation" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Designation" value="{{ $value['designation'] }}">
                                            </div>
                                            @if($key == 0)
                                            <div class="col-md-12 col-lg-3">
                                              <button type="button" name="add" id="addMoreBillingButton" class="btn btn-success col-md-1 col-lg-1 col-lg-1 offset-md-1 offset-lg-1">+
                                              </button>
                                            
                                            </div>
                                            @else
                                              <div class="col-md-12 col-lg-3"><button type="button" name="remove" class="btn btn-danger col-md-1 col-lg-1 col-lg-1 offset-md-1 offset-lg-1 removeBilling">x</button >
                                              </div>
                                            @endif
                                        </div>
                                    
                                    </div>
                                    @endforeach
                                   
                                </div>
                                <br/>
                                <div class="parent_clone_booking"> 
                                <?php $counter = count($bookingContact); ?>  
            
                                    @foreach($bookingContact as $key => $value)
                                    <div class="child_clone_bookingg"> 
                                    <label style="margin-left:15px;font-weight:700;">Partner Booking :</label>
                                        <div class="row mx-auto">
                                            <div class="col-md-12 col-lg-2">
                                              <input type="text" name="booking_name[]" class="form-control form-control-user booking_name" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Name" value="{{ $value['name'] }}">
                                            </div>
                                              
                                            <div class="col-md-12 col-lg-2">
                                              <input type="email" name="booking_email[]" class="form-control form-control-user booking_email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email" value="{{ $value['email'] }}">
                                            </div>
                                            <div class="col-md-12 col-lg-2">
                                              <input type="tel" name="booking_phone[]" class="form-control form-control-user booking_phone" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter phone" value="{{ $value['phone'] }}">
                                            </div>
                                            <div class="col-md-12 col-lg-2">
                                              <input type="text" name="booking_designation[]" class="form-control form-control-user booking_designation" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Designation" value="{{ $value['designation'] }}">
                                            </div>
                                            @if($key == 0)
                                            <div class="col-md-12 col-lg-3">
                                              <button type="button" name="add" id="addMoreBookingButton" class="btn btn-success col-md-1 col-lg-1 col-lg-1 offset-md-1 offset-lg-1">+
                                              </button>
                                            
                                            </div>
                                            @else
                                              <div class="col-md-12 col-lg-3"><button type="button" name="remove" class="btn btn-danger col-md-1 col-lg-1 col-lg-1 offset-md-1 offset-lg-1 removeBooking">x</button >
                                              </div>
                                            @endif
                                        </div>
                                    
                                    </div>
                                    @endforeach
                                    
                                </div>
                                </div>
                                <br/>
                                <div class="col-md-12 col-lg-4">
                                    <select class="form-control" name="send_QC">
                                      <option value="0">Select Send For QC</option>
                                     
                                      <option value="YES" {{old('send_QC') == 'YES' ? 'Selected' : ''}}>Yes</option>
                                      <option value="NO" {{old('send_QC') == 'NO' ? 'Selected' : ''}}>No</option>
                                      
                                      </div>
                                      
                                    </select>
                                </div>
                                
                           
                           <a href="#" class="">
                           <button type="button" class="btn btn-primary btn-user btn-block mt-4"onclick="venuebtn();" >Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<style>
.block {
  display: block;
  margin: .5rem 0;
  width: 100%;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- End of Main Content -->
<script type="text/javascript">
  $(function () {
  // Apply select2
  $('.select2').select2()
  
  // Just a small helper
  var _h = {
    capitalize: function (str) {
      return str.charAt(0).toUpperCase() + str.slice(1)
    },
    render: function (id, name) {
      var html = ''
      
      $.each([1], function (k, v) {
        html += '<div class="col-md-12 col-lg-4"><textarea name="description[]" placeholder="' + _h.capitalize(name) + '&nbsp;' + 'Description' + '" class="form-control form-control-user block"></textarea></div>'
    })
      $.each([1], function (k, v) {
        html += '<div class="col-md-12 col-lg-4"><input type="number" name="mrp_of_product[]" placeholder="' + _h.capitalize(name) + '&nbsp;' + 'MRP' + '" class="form-control form-control-user block"></div>'
    })
      $.each([1], function (k, v) {
        html += '<div class="col-md-12 col-lg-4"><input type="number" name="cost_of_product[]" placeholder="' + _h.capitalize(name) + '&nbsp;' + 'Cost' + '" class="form-control form-control-user block"></div>'
    })

      
      return html
    }
  }
  
  // Now listed to change event of select2
  $(document).on('change', '.product', function () {
    $('.parent_clone').html('') // truncate any appended nodes
    
    $(this).find(':selected').each(function (k, v) {
      $('.parent_clone').append(_h.render($(this).val(), $(this).text()))
    })
  })
})
</script>
@stop




