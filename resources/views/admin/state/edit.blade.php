@extends('common.default')
@section('title', 'Product List')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
      
        
            <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">States</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/states')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            
            </span>
            </div>
            </div>
            </div>
            </div>
            </div>
            
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center" style="margin-top:50px;">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('states.update', $states->id) }}">
                           @method('PATCH') 
                           @csrf
                          
                           
                           
                           <div class="form-group">
                                <label for="name" class="mr-sm-2">Enter State Name</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter State Name" name="name" value="{{$states->name}}">
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Select Zone</label>
                               <select   name="zone_id" class="inputbox form-control" style="height: 45px;">
                                  <option value="0">Select Zone</option>
                                  
                                 @forelse($state_zone as $zone)
                              <option value="{{$zone->id}}" {{$states->zone_id == $zone->id ? 'Selected' : ''}}>{{$zone->name}}</option>
                              @empty
                                <p>No Primary Language List</p>
                              @endforelse
                                  
                               </select>
                           
                            </div>
                           <div class="form-group">
                              <label for="name" class="mr-sm-2">Select Primary Lanuage</label>
                              <select   name="primary_lang_id" class="inputbox form-control" style="height: 45px;">
                              <option value="0">Select Primary Lanuage</option>
                              @forelse($primary_language as $primary)
                              <option value="{{$primary->id}}" {{$states->primary_lang_id == $primary->id ? 'Selected' : ''}}>{{$primary->name}}</option>
                              @empty
                                <p>No Primary Language List</p>
                              @endforelse
                           </select>
                           </div>
                           <div class="form-group">
                              <label for="name" class="mr-sm-2">Select Other Lanuage</label>
                              <select   name="other_lang_id" class="inputbox form-control" style="height: 45px;">
                              <option value="0">Select Other Lanuage</option>
                              @forelse($other_language as $other)
                              <option value="{{$other->id}}" {{$states->other_lang_id == $other->id ? 'Selected' : ''}}>{{$other->name}}</option>
                              @empty
                                <p>No Other Langauge List</p>
                              @endforelse
                           </select>
                           </div>
                           
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop