@extends('common.default')
@section('title', 'State List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 20px;">
       
       <!--
          <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">State</h1>
             <a href="{{url('admin/state/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back to Dashboard</a>
          </div>
         -->
        
        <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">State</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            
             <a href="{{url('admin/states/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add State
                     </button></a>
            
        <a href="{{ route('state.download-state-excel') }}"><button class="btn btn-primary btn-sm"     type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download State
                     </button></a>
            
            <a  href="{{url('admin/state/dashboard')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            
            </span>
            </div>
            </div>
            </div>
            </div>
            </div>
            
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card">
         <!--
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  <li style="margin-right: 20px;">
                      
                      <a href="{{url('admin/states/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add State
                     </button></a>
                  </li>
                  <li style="margin-right: 20px;"> 
                  <a href="{{ route('state.download-state-excel') }}"><button class="btn btn-primary btn-sm"     type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download State
                     </button></a>
                  </li>
                   <li style="margin-right: 20px;">  <button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-print"></i> &nbsp; Print State
                     </button>
                  </li> 
                  <li class="nav-item dropdown" ml-2>
                     <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-file-export"></i>&nbsp; Export State
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton ">
                           <a class="dropdown-item font-weight-bold" href="abc.xlsx"><i class="fas fa-file-excel"></i> &nbsp; Export to Excel</a>
                           <a class="dropdown-item font-weight-bold" href="abc.docx"><i class="fas fa-file-word"></i> &nbsp; Export to Word</a>
                           <a class="dropdown-item font-weight-bold" href="abc.pdf"><i class="fas fa-file-pdf"></i> &nbsp;Export to PDF</a>
                        </div>
                     </div>
                  </li> 
               </ul>
            </nav>
         </div>
         -->
      
         <div class="card-body" >
                @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>ID</th>
                        <th>State Name</th>
                        <th>Zone</th>
                        
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($states as $index=>$state)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$state->id}}</td>
                        <td>{{$state->name}}</td>
                        <td>{{$state->state_zone->name}}</td>
                        
                        <td>{{isset($state->created_user['name']) ? $state->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($state->updated_user['name']) ? $state->updated_user['name'] : 'NA'}}</td>
                        <td>{{$state->created_at->format('d M Y')}}/{{$state->created_at->format('g:i A')}}</td>
                        <td>{{$state->updated_at->format('d M Y')}}/{{$state->updated_at->format('g:i A')}}</td>    
                        
                        
                        <td>
                           <a href="{{ route('states.show',$state->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('states.edit',$state->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        </td>
                        <td>
                            
                              <input data-id="{{$state->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $state->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="State">  
                        </td>
                    </tr>
                    @empty
                    <p>No States List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop
