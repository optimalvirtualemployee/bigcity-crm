@extends('common.default')
@section('title', 'Show Communication Template Type')
@section('content')

<style>
strong {
    display: inline-block;
    width: 60%;
}
strong::after { 
    content: ' \25B6';
    color: #106db2;
    position: absolute;
    right: 0px;
    width: 45%;
    margin-top: 0px;
}
</style>

<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
     
           <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
              <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Communication Template Type Detail</h3>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a  href="{{url('admin/communication_template_type')}}" class="btn btn-primary btn-sm   js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                    </span>
                </div>
            </div>
            </div>
            
  
  
  
    <div class="container" style="margin-top:50px;">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card login-card" style="width: 70%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
      
      
      
        <div class="form-group">
      <div class="card-body col-md-12 col-lg-12">

           <strong>Communication Template Type ID </strong>
    
          {{$communication_template_type->id}}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Communication Template Type Name</strong>
    
          {{$communication_template_type->name}}

       </div>
       <div class="card-body col-md-12 col-lg-12">

           <strong>Created By </strong>
    
          {{isset($communication_template_type->created_user['name']) ? $communication_template_type->created_user['name'] : 'NA'}}

       </div>
       
        <div class="card-body col-md-12 col-lg-12">

           <strong>Updated By </strong>
    
           {{isset($communication_template_type->updated_user['name']) ? $communication_template_type->updated_user['name'] : 'NA'}}

       </div>
       
        <div class="card-body col-md-12 col-lg-12">

           <strong>Created Date/Time </strong>
    
         {{$communication_template_type->created_at->format('d M Y')}}/{{$communication_template_type->created_at->format('g:i A')}}

       </div>
       
        <div class="card-body col-md-12 col-lg-12">

           <strong>Updated Date/Time </strong>
    
         {{$communication_template_type->updated_at->format('d M Y')}}/{{$communication_template_type->updated_at->format('g:i A')}}

       </div>
       
       <div class="card-body col-md-12 col-lg-12">

           <strong> Status </strong>
    
           {{($communication_template_type->status == '1') ? 'Active' : 'In-Active' }}

       </div>
       
       
       </div>
       </div>
       </div>
       </div>
       </div>
       </div>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <!--
             <table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                       
                        <th>Communication Template Type ID</th>
                        <th>Communication Template Type Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    <tr>
                        <td>{{$communication_template_type->id}}</td>
                        <td>{{$communication_template_type->name}}</td>
                        <td>{{isset($communication_template_type->created_user['name']) ? $communication_template_type->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($communication_template_type->updated_user['name']) ? $communication_template_type->updated_user['name'] : 'NA'}}</td>
                        <td>{{$communication_template_type->created_at->format('d M Y')}}/{{$communication_template_type->created_at->format('g:i A')}}</td>
                        <td>{{$communication_template_type->updated_at->format('d M Y')}}/{{$communication_template_type->updated_at->format('g:i A')}}</td>
                        <td>{{($communication_template_type->status == '1') ? 'Active' : 'In-Active' }}</td>
                   </tr>
                  </tbody>
             </table>
             -->
    
   </div>
</div>
@stop