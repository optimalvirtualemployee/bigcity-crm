@extends('common.default')
@section('title', 'Communication Template Type List')
@section('content')
<!-- Main Content -->
<style>
.ques {
color: darkslateblue;
}
.switch {
  position: relative;
  display: inline-block;
  width: 260px;
  height: 100px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  overflow: hidden;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #f2f2f2;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  z-index: 2;
  content: "";
  height: 96px;
  width: 96px;
  left: 2px;
  bottom: 2px;
  background-color: darkslategrey;
      -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.22);
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.22);
  -webkit-transition: .4s;
  transition: all 0.4s ease-in-out;
}
.slider:after {
  position: absolute;
  left: 0;
  z-index: 1;
  content: "YES";
    font-size: 45px;
    text-align: left !important;
    line-height: 95px;
  padding-left: 0;
    width: 260px;
    color: #fff;
    height: 100px;
    border-radius: 100px;
    background-color: #ff6418;
    -webkit-transform: translateX(-160px);
    -ms-transform: translateX(-160px);
    transform: translateX(-160px);
    transition: all 0.4s ease-in-out;
}

input:checked + .slider:after {
  -webkit-transform: translateX(0px);
  -ms-transform: translateX(0px);
  transform: translateX(0px);
  /*width: 235px;*/
  padding-left: 25px;
}

input:checked + .slider:before {
  background-color: #fff;
}

input:checked + .slider:before {
  -webkit-transform: translateX(160px);
  -ms-transform: translateX(160px);
  transform: translateX(160px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 100px;
}

.slider.round:before {
  border-radius: 50%;
}
.absolute-no {
	position: absolute;
	left: 0;
	color: darkslategrey;
	text-align: right !important;
    font-size: 45px;
    width: calc(100% - 25px);
    height: 100px;
    line-height: 95px;
    cursor: pointer;
}
 .table-cont{
/**make table can scroll**/
max-height: 600px;
overflow: auto;

}
 

.table thead  {
  /* Important */
 
  position: sticky;
  z-index: 100;
  top: 0;
 
</style>
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 20px;">
    <div class="sub-headeing">
 
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Communication Template Type</h3>
                    
             
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                          <a href="{{url('admin/communication_template_type/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Communication Template Type
                     </button></a> 
                
                        
                        <a  href="{{url('admin/communication_template_type/dashboard')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
  
</div>
      
     
      <div class="card ">
         
       
         <div class="card-body" >
               @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
       
            <div class="table-cont">
            <div class="table-responsive">
               <table class="table   table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>Communication Template Type ID</th>
                        <th>Communication Template Type Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody style="">
                     @forelse($communication_template_type as $index=>$val)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$val->id}}</td>
                        <td>{{$val->name}}</td>
                        <td>{{isset($val->created_user['name']) ? $val->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($val->updated_user['name']) ? $val->updated_user['name'] : 'NA'}}</td>
                        <td>{{$val->created_at->format('d M Y')}}/{{$val->created_at->format('g:i A')}}</td>
                        <td>{{$val->updated_at->format('d M Y')}}/{{$val->updated_at->format('g:i A')}}</td>
                        
                        <td>
                           <a href="{{ route('communication_template_type.show',$val->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('communication_template_type.edit',$val->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                           <!--<span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                           <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span>-->
                          
                        </td>
                        <td>
                            
                              <input data-id="{{$val->id}}" class="toggle-class communication_template_type" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $val->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="CommunicationTemplateTypeModel">  
                        </td> 
                        </tr>
                    @empty
                    <p>No Communication Template Type List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
            </div>
         </div>
      </div>
      
 
        
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

