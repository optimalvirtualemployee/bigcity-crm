@extends('common.default')
@section('title', 'Communication Template Type')
@section('content')
<!-- For demo purpose -->
 <style>


 </style>
   

        
   <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="dahboard-title">Communication Template Type </h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
  
 
    <div class="container">
         

          
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card bg-gradient-x-primary">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/communication_template_type')}}">
                                             <p class="icon" ><i class="far fa-comments" style="color:#FFF!important;"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-white">
                                          Add Communication Template Type</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card bg-gradient-x-primary">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/communication-template')}}">
                                             <p class="icon" ><i class="far fa-comments" style="color:#FFF!important;"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-white">
                                          Add Communication Template</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                  </div>
               </div>
              <br><br>
            </div>
@stop