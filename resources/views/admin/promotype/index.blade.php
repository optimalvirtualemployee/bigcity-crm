@extends('common.default')
@section('title', 'Promo Type List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
           
         
          
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Promo Type</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                <a href="{{url('admin/promotype/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Promo Type
                     </button></a>
                   
                      <a href="{{route('promo-type.download-promo-type-excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Promo Type
                     </button></a>
                     
                <a  href="{{url('admin/campaign/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                    <i class="fas fa-chevron-left mr-1"></i> Back
                </a>
                 
            </span>
            </div>
            </div>
            </div>
            </div>
            
            <br>
          
          
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card">
         
         <div class="card-body" >
                 @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                         <th >SN</th>
                        <th >Promo Type Id</th>
                        <th>Promo Type Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                        
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($promos as $index=>$promo)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$promo->id}}</td>
                        <td>{{$promo->name}}</td>
                        <td>{{isset($promo->created_user['name']) ? $promo->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($promo->updated_user['name']) ? $promo->updated_user['name'] : 'NA'}}</td>
                        <td>{{$promo->created_at->format('d M Y')}}/{{$promo->created_at->format('g:i A')}}</td>
                        <td>{{$promo->updated_at->format('d M Y')}}/{{$promo->updated_at->format('g:i A')}}</td>
                        
                        <td>
                           <a href="{{ route('promotype.show',$promo->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('promotype.edit',$promo->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        </td>
                        <td>
                            
                              <input data-id="{{$promo->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $promo->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="PromoType">  
                        </td>
                        
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

