@extends('common.default')
@section('title', 'Tag Campaign Venue List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
       
        
        
        <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
                <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Tagged Campaign Venue List</h3>
                
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                    
                     <a href="{{url('admin/tag_campaign_venues/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Tag Campaign Venue
                     </button></a>
                    
                    <a  href="{{url('admin/campaign/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                        <i class="fas fa-chevron-left mr-1"></i> Back
                    </a>
                     
                </span>
            </div>
        </div>
        </div>
        </div>
    
    <br>
    
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card shadow ">
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  <li style="margin-right: 20px;">
                      
                     
                  </li>
                  
               </ul>
            </nav>
         </div>
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e">
                        <th >SN</th>
                        <th>Campaign Id</th>
                        <th>Campaign Name</th>
                        <th>Product Name</th>
                        
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <!--<th>Venue</th>-->
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?> 
                     @forelse($tag_campaign_venue as $venue)
                     
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$venue->campaign['id']}}</td>
                        <td>{{$venue->campaign['campaign_name']}}</td>
                        <td>{{$venue->products['name']}}</td>
                       
                        <td>{{isset($venue->created_user['name']) ? $venue->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($venue->updated_user['name']) ? $venue->updated_user['name'] : 'NA'}}</td>
                        <td>{{$venue->created_at->format('d M Y')}}/{{$venue->created_at->format('g:i A')}}</td>
                        <td>{{$venue->updated_at->format('d M Y')}}/{{$venue->updated_at->format('g:i A')}}</td>
                        <!--<td>{{$venue->venues['venue_name']}}</td>-->
                        
                        
                        <td>
                           <a href="{{ route('tag_campaign_venues.show',[$venue->campaign_id,$venue->product_id]) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('tag_campaign_venues.edit',[$venue->campaign_id,$venue->product_id])}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        <!--</td>-->
                        
                    </tr><?php $i++; ?> 
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
