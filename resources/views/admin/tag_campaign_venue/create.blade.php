@extends('common.default')
@section('title', 'Tag Campaign Venue Create')
@section('content')
<style>
.btn-primary {
    color: #fff;
    
    border-color: #36b9cc;
    /* padding-top: 16px; */
    margin-top: 10px;
    margin-bottom: 10px;
}
</style>
<!-- Main Content -->
<div id="content">
  <div class="container-fluid" style="margin-top:15px;">
    
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Tag Campaign Venue</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/tag_campaign_venues')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            <br>
   <!-- <div class="container-fluid" style="margin-top: 50px;"> -->
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card" style="width: 70%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <form action="{{ route('tag_campaign_venues.store') }}" method="POST" >
                           @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Select Campaign</label>
                                        <select class="form-control select2" name="campaign_id" onchange="get_productList_fn(this.value);">
                                          <option value="0">Select Campaign</option>
                                          @forelse($campaign as $camp)
                                          <option value="{{$camp->id}}" {{old('campaign_id') == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                                          @empty
                                            <p>No Product List</p>
                                          @endforelse
                                        </select>
                                    </div>

                                    <div id="campaignMapped_productList" class="col-md-6"></div> 
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                    <label>City List
                                     
                                    <!--<span class="btn btn-primary btn-sm select-all">Select all</span>
                                    <span class="btn btn-primary btn-sm deselect-all">Deselect all</span>-->
                                    <span class="btn btn-primary btn-sm select-all">Select all</span>
                                    <span class="btn btn-primary btn-sm deselect-all">Deselect all</span></label>
                                    
                                    <select  id="lstview" class="form-control select2" size="10" multiple="multiple">
                                      @forelse($cities as $cty)
                                      <option value="{{$cty->id}}" {{old('city_id') == $cty->id ? 'Selected' : ''}}>{{$cty->name}}</option>
                                      @empty
                                        <p>No City List</p>
                                      @endforelse
                                    </select>
                                  </div>
        
                                    <div class="col-lg-2 mt-5">
                                      <button type="button" id="lstview_rightAll" class="btn btn-primary btn-block border"><i class="fa fa-angle-double-right"></i></button>
                                      <button type="button" id="lstview_rightSelected" class="btn btn-primary btn-block border"><i class="fa fa-angle-right"></i></button>
                                      <button type="button" id="lstview_leftSelected" class="btn btn-primary btn-block border"><i class="fa fa-angle-left"></i></button>
                                      <button type="button" id="lstview_leftAll" class="btn btn-primary btn-block border"><i class="fa fa-angle-double-left"></i></button>
                                    </div>
        
                                    <div class="col-lg-5 mt-4">
                                      <label>Selected Cities</label>
                                      <select name="city_id[]" id="lstview_to" class="form-control" multiple="multiple">
                                      </select>
                                    </div>
                                  </div>
                                 
                                  <div class="col-lg-12" id="checkbox_list_wrap" >
                                    <label>Venue List
                                    <span class="btn btn-primary btn-sm select-all">Select all</span>
                                    <span class="btn btn-primary btn-sm deselect-all">Deselect all</span></label>
                                    
                                    <select  id="checkbox_list" class="form-control select2" multiple="multiple"   name="venue_id[]">
                                      
                                    </select>
                                  </div>
                                  <!--<div id="venue_list">
                                    <table id="checkbox_list" class="table table-striped table-bordered" style="display: none;">
                                      <thead>
                                        <tr>
                                          <th>City Wise Venue List</th>
                                        </tr>
                                        <tr>
                                          <th style="text-align:left!important;"><input type="checkbox" class="checkbox" id="ckbCheckAll">Select All</th>
                                        </tr>
                                      </thead>
                                      
                                      <tbody></tbody>
                                    </table>
                                  </div>-->
                                 <div class="col-lg-12 mt-3">
                                  <a href="#">
                                    <button type="submit" class="btn btn-primary btn-md submit">Tag City</button>
                                 </a>
                                 </div>
                              </form>
                      </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
</div>
</div>
<!-- End of Main Content -->

@stop

