@extends('common.default')
@section('title', 'Tag Campaign Venue List')
@section('content')

<meta name="csrf-token" content="{!! csrf_token() !!}">
<style type="text/css">
  .hide{ display: none; }
  .show{ display: block;}
</style>
<!-- Main Content -->
<div id="content">
  <div class="container-fluid" style="margin-top:15px;">
      
        
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Tag Campaign Venue</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/tag_campaign_venues')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple">
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            
   <!-- <div class="container-fluid" style="margin-top: 50px;"> -->
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center" >
         <div class="card" style="width:70%;margin-top:20px">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        @if(isset($tagCampaignVenue))
                        <form action="{{ route('tag_campaign_venues.update',[$tagCampaignVenue[0]['campaign_id'],$tagCampaignVenue[0]['product_id']]) }}" method="POST" >
                           @method('PATCH')
                           @csrf
                                <div class="row">
                                <div class="col-md-6">

                                <select class=" form-control" name="campaign_id" onchange="get_productList_fn(this.value);">
                                  <option value="0">Select Campaign</option>
                                  @forelse($campaign  as $camp)
                                  <option value="{{$camp->id}}" {{ $tagCampaignVenue[0]['campaign_id'] == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                                  @empty
                                    <p>No Product List</p>
                                  @endforelse
                                </select>
                                </div> 
                                <div class="col-md-6">
                                    <select id="select_box" class="form-control select_class" name="product_id">
                                    <option value="0">Select Product</option>
                                    @forelse($product as $pro)
                                    
                                    <option value="{{$pro->id}}" {{ $tagCampaignVenue[0]['product_id'] == $pro->id ? 'Selected' : ''}}>{{$pro->name}}</option>
                                    @empty
                                    <p>No Product List</p>
                                    @endforelse
                                    </select>
                                </div>
                                </div>
                                
                                
                                <div class="row" style="margin-top:10px;">
                                  <div class="col-lg-5  mt-4">
                                    <label>City List <span class="btn btn-primary btn-sm select-all mr-2">Select all</span>
                                    <span class="btn btn-primary btn-sm deselect-all">Deselect all</span></label><br>
                                    <select  id="lstview" class="form-control  select2" multiple="multiple">
                                      @foreach($tagCampaignVenue as $v)
                                      @forelse($cities as $cty)
                                      
                                      <option value="{{$cty->id}}" {{$cty->id == $v->city_id ? 'Selected' : ''}}>{{$cty->name}}</option>
                                      @empty
                                        <p>No City List</p>
                                      @endforelse
                                      
                                      @endforeach
                                    </select>
                                  </div>
        
                                    <div class="col-lg-2 mt-5">
                                      <button type="button" id="lstview_rightAll" class="btn btn-primary btn-block border"><i class="fa fa-angle-double-right"></i></button>
                                      <button type="button" id="lstview_rightSelected" class="btn btn-primary btn-block border"><i class="fa fa-angle-right"></i></button>
                                      <button type="button" id="lstview_leftSelected" class="btn btn-primary btn-block border"><i class="fa fa-angle-left"></i></button>
                                      <button type="button" id="lstview_leftAll" class="btn btn-primary btn-block border"><i class="fa fa-angle-double-left"></i></button>
                                    </div>
        
                                    <div class="col-lg-5 mt-4">
                                      <label>Selected Cities</label>
                                      <select name="city_id[]" id="lstview_to" class="form-control" size="10" multiple="multiple">
                                        
                                       @if(isset($tagCampaignVenue))
                                       
                                       <?php $i = 1;?>
                                        @foreach($tagCampaignVenue as $v)
                                        
                                          @foreach($cities as $cty)
                                            <option value="{{ $cty->id }}" {{ ( $cty->id == $v->city_id ) ? 'selected' : 'class=hide'}} >{{ $cty->name }}</option>
                                          @endforeach
                                          <?php $i++; ?>
                                        @endforeach
                                       @endif
                                      </select>
                                    </div>
                                  </div>
                                  <br>
                                  <!-- <div id="venue_list">
                                    <table id="checkbox_list" class="table table-striped table-bordered" style='display: none;'>
                                      <thead>
                                        <tr>
                                          <th>City Wise Venue List</th>
                                        </tr>
                                        <tr>
                                          <th style="text-align:left!important;"><input type="checkbox" class="checkbox" id="ckbCheckAll">Select All</th>
                                        </tr>
                                      </thead>
                                      <div id="venue_loading" style="display: none;text-align: center;"><b>Loading ! Please wait...</b></div>
                                      <tbody>

                                        @if(isset($tagCityVenue))
                                          @foreach($tagCityVenue as $id => $ven)
                                         
                                            <input type="hidden" class="venue_checklist" value="{{ $ven->tagged_city_id }}">
                                          @endforeach
                                        @endif
                                      </tbody>
                                    </table>
                                  </div> -->
                                  <div class="col-lg-12" id="checkbox_list_wrap" >
                                    <label>Venue List
                                    <span class="btn btn-primary btn-sm select-all">Select all</span>
                                    <span class="btn btn-primary btn-sm deselect-all">Deselect all</span></label><br>
                                    
                                    <select  id="checkbox_list" class="form-control select2" multiple="multiple"   name="venue_id[]">
                                      @if(isset($tagCampaignVenue))
                                          @foreach($tagCampaignVenue as $ven)
                                            
                                            <input type="hidden" class="venue_checklist" value="{{ $ven['city_id'] }}">
                                          @endforeach
                                        @endif
                                    </select>
                                  </div>
                                  @if(isset($CityVenue))
                                    
                                    @foreach($CityVenue as $vl)
                                    
                                    
                                        <input type="hidden" class="venue_checked" value="{{ $vl->tagged_venue_id }}">
                                          
                                    @endforeach
                                  @endif
                                 
                                 <div class="col-lg-12 mt-3">
                                  <a href="#" class="">
                                    <button type="submit" class="btn btn-primary btn-md submit">Click For Tag</button>
                                 </a>
                                 </div>
                              </form>
                              @endif
                      </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
</div>
</div>
<!-- End of Main Content -->

@stop

