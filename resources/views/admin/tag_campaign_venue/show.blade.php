@extends('common.default')
@section('title', 'Tag Campaign Venue Show')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
       
        
          
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Tag Campaign Venue</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/tag_campaign_venues')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            <br>
          
            <div class="container" style="margin-top:50px;">
            <!-- Outer Row -->
            <div class="row justify-content-center">
            <div class="card" style="width: 70%;">
            <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
            
            
                
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Campaign ID </p>
            
            {{$tagCampaignVenue->campaign_id}}
            
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Campaign Name </p>
            
             {{ $tagCampaignVenue->campaign->campaign_name }}
            
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Product Id</p>
              {{ $tagCampaignVenue->product_id }}
            </div>
            
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Product Name</p>
            
             {{ $tagCampaignVenue->products->name }}
            
            </div>
            
                
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Created By</p>
             {{isset($tagCampaignVenue->created_user['name']) ? $tagCampaignVenue->created_user['name'] : 'NA'}}
            </div>
            
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Updated By</p>
            
             {{isset($tagCampaignVenue->updated_user['name']) ? $tagCampaignVenue->updated_user['name'] : 'NA'}}
            
            </div>
            
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Created Date/Time </p>
            
              {{$tagCampaignVenue->created_at->format('d M Y')}}/{{$tagCampaignVenue->created_at->format('g:i A')}}
            
            </div>
            
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Updated Date/Time </p>
            
               {{$tagCampaignVenue->updated_at->format('d M Y')}}/{{$tagCampaignVenue->updated_at->format('g:i A')}}
            
            </div>
            
          
          
          
          
          
          
          
          
          
               <div class="card-body col-md-12">

           <h5>Tagged Venue List:</h5>
    
<table class="table  table-bordered"   width="100%" cellspacing="0" style="margin-top:5px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>City ID</th>
                        <th>City Name</th>
                        <th>Venue ID</th>
                        <th>Venue Name</th>
                       

                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    @foreach($tagCityVenue as $venue_bok)
                    <tr>
                        <td>{{$venue_bok->tagged_city_id}}</td>
                        <td>{{$venue_bok->city->name}}</td>
                        <td>{{$venue_bok->tagged_venue_id}}</td>
                        <td>{{$venue_bok->venue->venue_name}}</td>
                        
                   
                   </tr>
                   @endforeach
                  </tbody>
             </table>

       </div>
          
          
          
          
          
          
          
          
            
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
            

      
  
       
       
       
 
    
   </div>
</div>
@stop