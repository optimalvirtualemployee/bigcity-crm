@extends('common.default')
@section('content')
<!-- For demo purpose -->
<style>
 
  .icon i{
      font-size: 50px;
      color: #FFF;
      }
      .icon i:hover{
      font-size: 50px;
      color: #FFF;
       
      transition: .5s; 
      
      }
      
</style>


<div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="dahboard-title">Area</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


    <div class="container">
              
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/areas')}}">
                                             <p class="icon" > <i class="fas fa-handshake"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0 text-white">
                                          Area</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/areas')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0   text-white">
                                          Add Area</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        
                     </div>
                  </div>
               </div>
               <div class="card shadow mb-4">
                  <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                     <h6 class="m-0 font-weight-bold text-uppercase">Bar Chart</h6>
                  </div>
                  <div class="card-body">
                     <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                     </div>
                  </div>
               </div>
                  
               </div>
               <!--<div class="row" pb-4>-->
                  <!-- Content Column -->
                  <!--<div class="col-lg-12 mb-4">-->
                     <!-- Project Card Example -->
                     <!--<div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                           <h6 class="m-0 font-weight-bold  text-uppercase" style="background-color: d33f3e;color: #FFF;">Projects</h6>
                        </div>
                        <div class="card-body">
                           <h4 class="small font-weight-bold">Server Migration <span class="float-right">20%</span></h4>
                           <div class="progress mb-4">
                              <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                           <h4 class="small font-weight-bold">Sales Tracking <span class="float-right">40%</span></h4>
                           <div class="progress mb-4">
                              <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                           <h4 class="small font-weight-bold">Customer Database <span class="float-right">60%</span></h4>
                           <div class="progress mb-4">
                              <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                           <h4 class="small font-weight-bold">Payout Details <span class="float-right">80%</span></h4>
                           <div class="progress mb-4">
                              <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                           <h4 class="small font-weight-bold">Account Setup <span class="float-right">Complete!</span></h4>
                           <div class="progress">
                              <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                           </div>
                        </div>
                     </div>
                  </div>-->
                  <!--<div class="col-lg-12 mb-4">-->
                     <!-- Illustrations -->
                     <!--<div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                           <h6 class="m-0 font-weight-bold text-uppercase" style="background-color: d33f3e;color: #FFF;">Illustrations</h6>
                        </div>
                        <div class="card-body">
                           <div class="text-center">
                              <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="img/undraw_posting_photo.svg" alt="">
                           </div>
                           <p>Add some quality, svg illustrations to your project courtesy of <a target="_blank" rel="nofollow" href="https://undraw.co/">unDraw</a>, a constantly updated collection of beautiful svg images that you can use completely free and without attribution!</p>
                        </div>
                     </div>-->
                     <!-- Approach -->
                  <!--</div>
               </div>-->
               <!--<div class="card shadow mb-4">
                  <div class="card-header py-3" style="background-color: #d33f3e;color: #FFF;">
                     <h6 class="m-0 font-weight-bold text-uppercase">Bar Chart</h6>
                  </div>
                  <div class="card-body">
                     <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                     </div>
                  </div>
               </div>
            </div>-->
@stop