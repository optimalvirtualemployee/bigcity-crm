@extends('common.default')
@section('title', 'Area List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      <!-- Page Heading -->
      
      
      <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Area</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                     <a href="{{url('admin/areas/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Area
                     </button></a>
                     
                     
                     <a href="{{url('admin/area/download-area-excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download All Area
                     </button></a>
                        
                        <a  href="{{url('admin/area/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
      
      <br>
      
      
      <div class="card">
          
       
         <div class="card-body" >
               @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th >Area ID</th>
                        <th>State Name</th>
                        <th>City Name</th>
                        <th>Area Name</th>
                        
                        
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($areas as $index=>$area)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$area->id}}</td>
                        <td>{{$area->state['name']}}</td>
                        
                        <td>{{$area->city['name']}}</td>
                        <td>{{substr($area->area_name,0,20)}}</td>
                        
                        <td>{{isset($area->created_user['name']) ? $area->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($area->updated_user['name']) ? $area->updated_user['name'] : 'NA'}}</td>
                        <td>{{$area->created_at->format('d M Y')}}/{{$area->created_at->format('g:i A')}}</td>
                        <td>{{$area->updated_at->format('d M Y')}}/{{$area->updated_at->format('g:i A')}}</td>
                       
                        
                        <td>
                           <a href="{{ route('areas.show',$area->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('areas.edit',$area->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        </td>
                        <td>
                            
                              <input data-id="{{$area->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $area->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="Area">  
                        </td>
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>

@stop

