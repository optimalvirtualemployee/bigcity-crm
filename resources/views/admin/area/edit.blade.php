@extends('common.default')
@section('title', 'Edit Area')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
         
      
        
           <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Areas</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/areas')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
        
        
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card" style="width:70%;margin-top:30px">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('areas.update', $areas->id) }}">
                           @method('PATCH') 
                           @csrf
                        
                           
                           <div class="form-group">
                              <label for="name" class="mr-sm-2">Select State</label>
                              <select class="form-control js-example-basic-single state" id="sel1" name="state_id" style="height: 45px;">
                                 <option value="0">Select State</option>
                                 @forelse($states as $state)
                                 <option value="{{$state->id}}" {{$areas->state_id == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                                 @empty
                                 <p>No State List</p>
                                 @endforelse

                                 
                              </select>
                           </div>
                           <div class="form-group" >
                                 <label for="name" class="mr-sm-2">Select City</label>
                           <select class="form-control js-example-basic-single city" name="city_id" id="sel1" style="height: 45px;">
                               @forelse($cities as $city)
                              <option value="{{$city->id}}" {{($areas->city_id) == $city->id ? 'Selected' : ''}}>{{$city->name}}</option>
                              @empty
                              <p>No city List</p>
                              @endforelse
                           </select>
                           </div>
                           <div class="form-group">
                                <label for="name" class="mr-sm-2">Area Name</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Area Name" name="area_name" value={{ $areas->area_name }}>
                           </div>
                           
                           <div class="form-group">
                                <label for="name" class="mr-sm-2">Zone</label>
                               <select   name="zone" class="inputbox form-control" style="height: 45px;">
                                  <option value="0">Select Zone</option>
                                  
                                  @forelse($area_zone as $zone)
                                 <option value="{{$zone->id}}" {{$areas->zone_id == $zone->id ? 'Selected' : ''}}>{{$zone->name}}</option>
                                 @empty
                                 <p>No State List</p>
                                 @endforelse
                                  
                               </select>
                           
                            </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Pin Code</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Pin Code" name="pin_code" value={{$areas->pin_code }}>
                           </div>
                           
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Latitude</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Latitude Code" name="lat" value={{$areas->lat }}>
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Longitude</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Longitude" name="lng" value={{$areas->lng }}>
                           </div>
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop