@extends('common.default')
@section('title', 'Area Create')
@section('content')
<!-- Main Content -->
<style>
.select2-container {
    box-sizing: border-box;
    display: block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    /* width: 100%; */
}
</style>
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
       
           <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Add Area</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/areas')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
       
       
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
         <div class="card" style="width: 70%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <div class="form-check">
                            <input class="form-check-input addArea" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                              Add single Area
                            </label>

                        </div>
                        <div class="form-check">
                            <input class="form-check-input uploadArea" type="radio" name="exampleRadios" id="exampleRadios1" value="option2">
                            <label class="form-check-label" for="exampleRadios1">
                              Upload multiple Area
                            </label>
                            
                        </div>
                        <form class="user" action="{{ route('areas.store') }}" id="addAreaForm" method="POST">
                           @csrf
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Select State</label>
                              <select class="form-control js-example-basic-single state" id="sel1" name="state_id" style="height: 45px;">
                                 <option value="0">Select State</option>
                                 @forelse($states as $state)
                                 <option value="{{$state->id}}" {{old('state_id') == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                                 @empty
                                 <p>No State List</p>
                                 @endforelse

                                 
                              </select>
                           </div>
                           <div class="form-group" >
                            <label for="name" class="mr-sm-2">Single City</label>
                           <select class="form-control js-example-basic-single city" name="city_id" id="sel1" style="height: 45px;">
                           </select>
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Area Name</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Area Name" name="area_name" value={{ old('area_name') }}>
                           </div>
                           
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Select Zone</label>
                               <select   name="zone_id" class="inputbox form-control" style="height: 45px;">
                                  <option value="0">Select Zone</option>
                                  
                                  
                                  @forelse($area_zone as $zone)
                                 <option value="{{$zone->id}}" {{old('zone_id') == $zone->id ? 'Selected' : ''}}>{{$zone->name}}</option>
                                 @empty
                                 <p>No State List</p>
                                 @endforelse
                                  
                               </select>
                           
                            </div>
                           <div class="form-group">
                                <label for="name" class="mr-sm-2">Enter Pin Code</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Pin Code" name="pin_code" value={{ old('pin_code') }}>
                           </div>
                           <div class="form-group">
                            <label for="name" class="mr-sm-2">Enter Latitude</label>
                           <input type="text" class="form-control form-control-user" id="exampleInputEmail" placeholder="Enter Latitude" aria-describedby="emailHelp" name="lat">
                           </div>
                           <div class="form-group">
                            <label for="name" class="mr-sm-2">Enter Longitude</label>
                           <input type="text" class="form-control form-control-user" id="exampleInputEmail" placeholder="Enter Longitude" aria-describedby="emailHelp" name="lng">
                          </div>
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Submit</button>
                           </a>
                        </form>
                        <form class="user" action="{{ route('area.upload-area-excel') }}" id="uploadAreaForm" method="POST" style="display: none;" enctype="multipart/form-data">
                           @csrf
                           <div class="form-group">
                                <label for="name" class="mr-sm-2">Select State</label>
                              <select class="form-control state js-example-basic-single" id="sel1" name="state_id" style="height: 45px;">
                                 <option value="0">Select State</option>
                                 @forelse($states as $state)
                                 <option value="{{$state->id}}" {{old('state_id') == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                                 @empty
                                 <p>No State List</p>
                                 @endforelse

                                 
                              </select>
                           </div>
                           
                           <div class="form-group" >
                                <label for="name" class="mr-sm-2">Select City</label>
                           <select class="form-control city js-example-basic-single" name="city_id" id="sel1" style="height: 45px;">
                           </select>
                           </div>
                           <div class="form-group">
                                <label for="name" class="mr-sm-2">Download Sample File</label>
                           <a href="{{url('assets/file/area.xlsx')}}" download><button class="btn btn-primary btn-sm"    type="button" id="dropdownMenuButton">Download Sample File</button></a>
                            </div>
                           <div class="form-group">
                              <label for="exampleFormControlFile1">Area Upload</label>
                              <input type="file" class="form-control" id="" name="file" accept=".xlsx, .xls, .csv">
                              <spna id="imageErrorMsg" style="color:red;"></spna>
                            </div>
                            <!-- <div id="dvPreview">
                            </div>-->
                           <a href="#" class="">
                           <button type="button" class="btn btn-primary btn-user  mt-4" onclick="uploadAreaBtn();">Submit</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->

@stop