@extends('common.default')
@section('title', 'Show Gift Type')
@section('content')

<style>
p.show-details{
    display: inline-block;
    width: 60%;
    font-weight:700;
}
p.show-details::after { 
    content: ' \25B6';
    color: #106db2;
    position: absolute;
    right: 0px;
    width: 45%;
    margin-top: 0px;
}
.card-body.col-md-12.col-lg-12 {
    padding: 0px 1rem !important;
}
</style>

<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
          
        
         <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Category</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/categories')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
        </div>
        
  
  
  
            <div class="container" style="margin-top:50px;">
      <div class="row justify-content-center">
         <div class="card login-card" style="width: 70%;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
      
      
      
        <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">ID </p>
            
            {{$gift_type['id']}}
            
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Name </p>
            
            {{$gift_type['id']}}
            
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Created By </p>
            
          {{$gift_type['name']}}
            
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Created Date/Time </p>
            
            {{Auth::user()->name}}
            
            </div>
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Status </p>
            
            {{$gift_type->created_at->format('d M Y')}}/{{$gift_type->created_at->format('g:i A')}}
            
            </div>
       </div>
       
       </div>
       </div>
       </div>
      </div>
      </div>
      </div>
       
    
   </div>
</div>
@stop