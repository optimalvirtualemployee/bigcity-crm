@extends('common.default')
@section('content')
  <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
      <div class="content content-narrow content-full">
        <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
          <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;">Booking Champion Module</h3>
          </div>
          <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
              <!-- <a  href="{{url('admin/dashboard')}}" class="btn btn-primary px-4 py-2 js-click-ripple-enabled" data-toggle="click-ripple" > -->
                <!--<i class="fa fa-arrow-left mr-1"></i> Back
              </a>-->
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- For demo purpose -->
  <div class="container">
    <div class="row" pb-4>
      <div class="col-lg-12 mb-5 mt-5">
        <div class="row">
          <div class="col-6 col-sm-6 col-md-3">
            <a href="{{url('booking-champion/bookings')}}">
              <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                <div class="mainflip">
                  <div class="frontside">
                    <div class="card shadow pb-0">
                      <div class="card-body text-center">
                        <p class="icon" ><i class="fas fa-ticket-alt"></i></p>
                        <h3 class="h6 mb-0 text-uppercase font-weight-bold text-gray-800">
                        Booking Champion</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="d-sm-flex align-items-center justify-content-between  pt-4 pb-5">
      <h1 class="h3 mb-0 text-gray-800   h-100 px-1 text-capitalize">Booking Champion Dashboard</h1>
    </div>
    <div class="row mb-5">
      <div class="col-xl-3 col-sm-6 py-2">
        <div class="card shadow  h-100">
          <div class="card-body card-totalvenue44">
            <h6 class="text-uppercase text-center">Total booking</h6>
            <h1 class="display-4 totalvenue">{{$totalBooking->count()}}</h1>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 py-2">
        <div class="card shadow h-100">
          <div class="card-body card-totalvenue11">
            <h6 class="text-uppercase text-center">Total Pending Booking</h6>
            <h1 class="display-4 totalvenue">{{$totalPendingBooking->count()}}</h1>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 py-2">
        <div class="card shadow  h-100">
          <div class="card-body card-totalvenue11">
            <h6 class="text-uppercase text-center">Total Confirmed Booking</h6>
            <h1 class="display-4 totalvenue">{{$totalConfirmedBooking->count()}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop