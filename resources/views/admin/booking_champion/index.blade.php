@extends('common.default')
@section('title', 'Booking Champion List')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style type="text/css">
  .bttn{margin-top: 32px;height: 38px;font-weight: 800;}
  .row.border.p-3 {
    background-color: #f3f3f3;
    border-radius: 0px;
    padding: 2rem 20px !important;  
  }

  .assign {
    color: #5d5858;
    font-weight: 600;
    letter-spacing: 0px;
    margin: 0px 0px !important;
    display: inline-block;
    letter-spacing: 0px;
  }

  .border-boking {
    border: 1px solid #ebebeb;
    margin-top: 13.2px;
    margin-bottom: 15px;
  }

  .border-boking::before {
    content: "";
    / border: 2px solid red; /
    top: 36px;
    left: 0;
    width: 88px;
    position: absolute;
    height: 6px;
    background-image: linear-gradient(to right,#FF425C 0,#FFA8B4 100%); background-color: #106db2;
    z-index: 999;
    border-radius: 10px;
  }

  .assign-book {
    position: relative;
    margin-top: 30px;
    margin-bottom: 30px;
  }

  h1.display-4.totalvenue {
    margin: 0px auto !important;
  }

</style>

<div id="content">
  <div class="container-fluid" style="margin-top: 20px;">
    <!-- Booking Assign and Back Button header starts here -->
      <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-center text-sm-left" style="padding: 0px 20px;">
          <h3 class="font-w600 js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Booking Champion</h3>
          <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
              <a href="{{url('booking-champion/dashboard')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
                <i class="fas fa-chevron-left mr-1"></i> Back
              </a>
            </span>
          </div>
        </div>
      </div>
      <!-- Booking Assign and Back Button header ends here -->
      <div class="card">
        <div class="card-body">
          <div id="message"></div>
          <!-- Table Listing -->
          <div class="card-body" >
            <div class="table-responsive">
              <div class="row mb-5">
                <div class="col-xl-3 col-sm-6 py-2">
                  <div class="card text-white h-100">
                    <div class="card-body card-totalvenue44">
                      <h6 class="text-uppercase text-center">Total Totday Booking</h6>
                      <h1 class="display-4 totalvenue">{{$today_total_booking}}</h1>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-6 py-2">
                  <div class="card text-white h-100">
                    <div class="card-body card-totalvenue11">
                      <h6 class="text-uppercase text-center">Total Yesterday Booking</h6>
                      <h1 class="display-4 totalvenue">{{$yesterday_total_booking}}</h1>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-6 py-2">
                  <div class="card text-white h-100">
                    <div class="card-body card-totalvenue11">
                      <h6 class="text-uppercase text-center">Total Week Booking</h6>
                      <h1 class="display-4 totalvenue">{{$week_total_booking}}</h1>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Booking Filters Starts Here -->  
              <div class="assign-book">
                <h5 class="assign">Booking Filter</h5>
                <div class="border-boking"></div>
              </div>

              <div class="row border p-3">
                <div class="col-md-3">
                  <label>From Date</label>
                  <input type="date" name="from_date" id="from_date" class="form-control" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}">
                </div>
                <div class="col-md-3">
                  <label>To Date</label>
                  <input type="date" name="to_date" id="to_date" class="form-control" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}">
                </div>
                <div class="col-md-3">
                  <label>Select Campaign</label>
                  <select class="form-control" name="campaign_id" id="campaign_id">
                    <option value="0">Select Campaign</option>
                    @forelse($campaign as $camp)
                      <option value="{{$camp->id}}" {{(isset($_GET['campaign_id']) && $_GET['campaign_id'] == $camp->id) ? 'Selected' : ''}}>
                        {{$camp->campaign_name}}
                      </option>
                    @empty
                      <p>No Product List</p>
                    @endforelse
                  </select>
                </div>
                
                <div class="col-md-3">
                  <label>Select Product</label>
                  <select class="form-control" name="product_id" id="product_id">
                    <option value="0">Select Product</option>
                    @forelse($product as $pro)
                      <option value="{{$pro->id}}" {{(isset($_GET['product_id']) && $_GET['product_id'] == $pro->id) ? 'Selected' : ''}}>
                        {{$pro->name}}
                      </option>
                    @empty
                      <p>No Product List</p>
                    @endforelse
                  </select>
                </div>
                
                <div class="col-md-1">
                  <input type="button" class="btn btn-info btn-sm btn-block bttn" id="refresh" value="Refresh" >
                </div>
                
                <div class="col-md-1">
                  <input type="button" class="btn btn-primary btn-sm btn-block bttn" id="filter" value="Filter" >
                </div>
              </div>
              <!-- <div class="col-md-1"></div> -->
              <!-- Assign booking to Agent ends Here ---->
              <!-- Booking Filters Ends Here ---->

              <table class="table table-striped " id="table" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                <thead>
                  <tr style="text-transform: capitalize;color: #FFF;background-color:#d33f3e;">
                    <!--<th><input type="checkbox" id="select_all">&nbsp;Select All</th>-->
                    <th>SN</th>
                    <th>Booking ID</th>
                    <th>Campaign ID</th>
                    <th>Campaign Name</th>
                    <th>Product ID</th>
                    <th>Product Name</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="championDashboard">
                </tbody>
              </table>
              {{ csrf_field() }}
            </form>
            <!-- Table Listing -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal form to edit a form -->
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header bg-gradient-light">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" id="bookingData_form">
          <table class="table table-striped bg-gradient-light">
            <tr><th>Booking ID</th><td id="id_edit"></td></tr>
            <tr><th>Campaign</th><td id="title_edit"></td></tr>
            <tr><th>Product</th><td id="product_edit"></td></tr>
            <tr><th>City</th><td id="city_edit"></td></tr>
            <tr><th>Venue</th><td id="venue_edit"></td></tr>
          </table>
          <div class="productType bg-gradient-light p-2"></div>
          <div class="form-group bg-gradient-light p-2">
            <label for="name" class="mr-sm-2">Select Booking Status</label>
            <select name="booking_status" class="inputbox form-control" id="booking_status" style="height: 45px;">
              <option value="">Select Booking Status</option>
              <option value="1">Confirm</option>
              <option value="2">Pending</option>
            </select>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary bg-gradient-success" id="submitBooking" onclick="bookingValidation();">
              <span class='glyphicon glyphicon-check'></span> Submit booking
            </button>
          <button type="button" class="btn btn-danger bg-gradient-danger" data-dismiss="modal">
            <span class='glyphicon glyphicon-remove'></span> Close
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
<script>
  $(document).ready(function(){
    var _token = $('input[name="_token"]').val();
    fetch_data();
    
    function fetch_data(from_date = '', to_date = '', campaign_id = '', product_id = ''){
      $.ajax({
        url:"{{ route('bookings.fetch_data') }}",
        beforeSend: function(){
          // Show image container
          $("#loading").show();
        },
        method:"POST",
        data:{from_date:from_date, to_date:to_date, _token:_token, campaign_id:campaign_id, product_id:product_id},
        dataType:"json",
        success:function(data)
        {
          // console.log(data);
          var output = '';
          var counter = 1;
          $('#total_records').text(data.length);
          for(var count = 0; count < data.length; count++)
          {
            output += '<tr>';
            /*output += '<td class="chkbx"><input type="checkbox" name="booking_assign[' + count + ']" value=' + counter + '></td>';*/
            output += '<td>' + counter++ + '</td>';
            output += '<td>' + data[count].id + '</td>';
            output += '<td>' + data[count].campaign_id + '</td>';
            output += '<td>' + data[count].campaign.campaign_name + '</td>';
            output += '<td>' + data[count].product_id + '</td>';
            output += '<td>' + data[count].products.name + '</td>';
            output += '<td>' + data[count].created_on + '</td>';
            output += '<td>';
            output +=   '<button class="edit-modal btn btn-info" data-id="' + data[count].id + '" data-campaign="' + data[count].campaign.campaign_name + '" data-product="' + data[count].products.name + '" data-city="' + data[count].cities.name + '" data-venue="' + data[count].venues.venue_name + '" data-product_id="' + data[count].products.id + '">View Detail</button></td></tr>';
          }
          $('tbody#championDashboard').html(output);
        },
        complete: function(){
          $('#loading').hide();
        }
      })
    }

    $('#filter').click(function(){
      var from_date = $('#from_date').val();
      var to_date = $('#to_date').val();
      var campaign_id = $( "#campaign_id option:selected" ).val();
      var product_id = $( "#product_id option:selected" ).val();
      if((from_date != '' && to_date != '') || campaign_id != '' || product_id != ''){
        fetch_data(from_date, to_date, campaign_id, product_id);
      }/*else{
        alert('Both Date is required');
      }*/
    });

    $('#refresh').click(function(){
      $('#from_date').val('');
      $('#to_date').val('');
      $( "#campaign_id option:selected" ).val('');
      $( "#product_id option:selected" ).val('');
      fetch_data();
    });

    // Select all check - uncheck event trigger starts here
    $('#select_all').click(function() {
      var checked = this.checked;
      $('input[type="checkbox"]').each(function() {
        this.checked = checked;
      });
    });
  });

</script>

  <!-- AJAX CRUD operations -->
  <script type="text/javascript">
    // add a new post
    $(document).on('click', '.add-modal', function() {
      $('.modal-title').text('Add');
      $('#addModal').modal('show');
    });

    // Show a post
    // $(document).on('click', '.show-modal', function() {
    //   $('.modal-title').text('Show');
    //   $('#id_show').val($(this).data('id'));
    //   $('#title_show').val($(this).data('campaign'));
    //   $('#product_show').val($(this).data('product'));
    //   $('#city_show').val($(this).data('city'));
    //   $('#venue_show').val($(this).data('venue'));
    //   $('#showModal').modal('show');
    // });

    // Edit a post
    $(document).on('click', '.edit-modal', function() {
      $('.modal-title').html('Booking Detail').css({'font-weight':'600'});
      $('#id_edit').html($(this).data('id'));
      $('#title_edit').html($(this).data('campaign'));
      $('#product_edit').html($(this).data('product'));
      $('#city_edit').html($(this).data('city'));
      $('#venue_edit').html($(this).data('venue'));
      $('#submitBooking').val($(this).data('id'));
      // alert($(this).data('product_id'));
      $('#appendDiv').remove();
      if($(this).data('product_id') == '36'){
        var appendDiv  = '<div id="appendDiv">';
            appendDiv += '<div class="row">';
            
            appendDiv += '<div class="col-md-6">';
            appendDiv +=   '<label>Select Portal</label>';
            appendDiv +=   '<select class="form-control" name="portals" id="portals">';
            appendDiv +=     '<option value="">Select Portal</option>';
            appendDiv +=     '<option value="portal_1">Portal-1</option>';
            appendDiv +=     '<option value="portal_2">Portal-2</option>';
            appendDiv +=   '</select>';
            appendDiv += '</div>';
            
            appendDiv += '<div class="col-md-6">';
            appendDiv +=   '<label>Enter Booking ID</label>';
            appendDiv +=   '<input type="text" class="form-control" placeholder="enter booking id here..." name="bookingId" id="bookingId_01">'
            appendDiv += '</div>';

            appendDiv += '</div>';
            appendDiv += '</div>';
        $('.productType').append(appendDiv);
      }else{
        var appendDiv  = '<div id="appendDiv">';
            appendDiv += '<label>Enter Booking Confirmation ID</label>';
            appendDiv += '<input type="text" class="form-control" placeholder="enter booking confirmation id here..." name="bookingId" id="bookingId_02">'
            appendDiv += '</div>';
        $('.productType').append(appendDiv);
      }

      id = $('#id_edit').val();
      
      $('#editModal').modal('show').css({'width':'100%'});
    });

    //------------------------------------------------------//
    function bookingValidation(){
      var isError        = true;
      var portals        = $('#portals').val();
      var bookingId_01   = $('#bookingId_01').val();
      var bookingId_02   = $('#bookingId_02').val();
      var booking_status = $('#booking_status').val();
      var bookingID      = $('#submitBooking').val();
      
      $('#portalsID').remove();
      $('#bookingId_01ID').remove();
      $('#bookingId_02ID').remove();
      $('#booking_statusID').remove();

      if(portals == ""){
        $('#portals').css({'border':'1px solid red'});
        $('#portals').after('<div id="portalsID">This fields is required</div>');
        $('#portalsID').css({'color':'red'});
        isError = false;
      }else{
        $('#portals').css({'border':'1px solid #eee'});
        $('#portalsID').remove();
      }

      if(bookingId_01 == ""){
        $('#bookingId_01').css({'border':'1px solid red'});
        $('#bookingId_01').after('<div id="bookingId_01ID">This fields is required</div>');
        $('#bookingId_01ID').css({'color':'red'});
        isError = false;
      }else if(bookingId_01 != "" && bookingId_01 !=  bookingID){
        $('#bookingId_01').css({'border':'1px solid red'});
        $('#bookingId_01').after('<div id="bookingId_01ID">Enter a valid booking ID</div>');
        $('#bookingId_01ID').css({'color':'red'});
        isError = false;
      }else{
        $('#bookingId_01').css({'border':'1px solid #eee'});
        $('#bookingId_01ID').remove();
      }

      if(bookingId_02 == ""){
        $('#bookingId_02').css({'border':'1px solid red'});
        $('#bookingId_02').after('<div id="bookingId_02ID">This fields is required</div>');
        $('#bookingId_02ID').css({'color':'red'});
        isError = false;
      }else if(bookingId_02 != "" && bookingId_02 !=  bookingID){
        $('#bookingId_02').css({'border':'1px solid red'});
        $('#bookingId_02').after('<div id="bookingId_02ID">This fields is required</div>');
        $('#bookingId_02ID').css({'color':'red'});
        isError = false;
      }else{
        $('#bookingId_02').css({'border':'1px solid #eee'});
        $('#bookingId_02ID').remove();
      }

      if(booking_status == ""){
        $('#booking_status').css({'border':'1px solid red'});
        $('#booking_status').after('<div id="booking_statusID">This fields is required</div>');
        $('#booking_statusID').css({'color':'red'});
        isError = false;
      }else{
        $('#booking_status').css({'border':'1px solid #eee'});
        $('#booking_statusID').remove();
      }

      if(isError == true){
        $('#bookingData_form')[0].submit();
      }
    }
    //------------------------------------------------------//
</script>

@stop
