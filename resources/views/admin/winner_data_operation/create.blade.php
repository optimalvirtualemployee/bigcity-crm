@extends('common.default')
@section('title', 'Winner Data Operation')
@section('content')
<!-- Main Content -->
<style>
input#exampleFormControlFile1 {
    border: 0px !important;
    padding: 1px;
}
</style>
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
        
        
         <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Winner Data Operation</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/winner-campaign/dashboard')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>  
            </span>
            </div>
            </div>
            </div>
        
        
       
       
   <div class="container">
      <!-- Outer Row -->
       
      <div class="row justify-content-center mt-3" style="">
         <div class="card login-card" style="width: 500px;margin-top:50px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <form class="user" action="{{ route('winner-data-operation.store') }}" method="POST" enctype="multipart/form-data">
                           @csrf
                           
                           <div class="form-group">
                           <label>Select Campaign </label>
                           <select   name="campaign_id" class="inputbox form-control" style="height: 45px;">
                              <option value="0">Select Campaign</option>
                              @forelse($winner_campaign as $campaign)
                              <option value="{{$campaign->id}}" {{old('campaign_id') == $campaign->id ? 'Selected' : ''}}>{{$campaign->campaign_name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                           </select>
                           
                            </div>
                            
                           <div class="form-group">
                              <label>Release Callouts</label>
                              <input type="button" class="btn btn-primary  btn-block mt-4" name="release_callouts" value="Release Main Winners for Callouts">
                           </div>
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary  btn-block mt-4">Submit</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop









