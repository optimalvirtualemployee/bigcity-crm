<style>
.icon i {
    font-size: 50px;
    color: #ffffff !important;
}
</style>
@extends('common.default')
@section('content')
 
  <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="dahboard-title">Booking Module</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
  <!-- For demo purpose -->
  <div class="container">
    <div class="row" pb-4>
      <div class="col-lg-12 mb-5 mt-5">
        <div class="row">
          <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/customerquery')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary  pb-0">
                                       <div class="card-body text-center">
                                          
                                             <p class="icon" > <i class="fa fa-question-circle"></i></p>
                                          
                                          <h3 class="h6 mb-0 text-white">
                                          Customer Query</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/assign-type')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary  pb-0">
                                       <div class="card-body text-center">
                                          
                                             <p class="icon" > <i class="fas fa-language"></i></p>
                                          
                                          <h3 class="h6 mb-0 text-white">
                                         Assign Type</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        
                        <div class="col-6 col-sm-6 col-md-3">
                           <a href="{{url('admin/inbound_module')}}">
                              <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                 <div class="mainflip">
                                    <div class="frontside">
                                       <div class="card bg-gradient-x-primary pb-0">
                                          <div class="card-body text-center">
                                             <p class="icon" > <i class="fa fa-phone" aria-hidden="true"></i></p>
                                             <h3 class="h6 mb-0 text-white">
                                             Inbound Module</h3>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <a href="{{url('admin/customer-transaction-history')}}">
                              <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                 <div class="mainflip">
                                    <div class="frontside">
                                       <div class="card bg-gradient-x-primary pb-0">
                                          <div class="card-body text-center">
                                             <p class="icon" > <i class="fa fa-phone" aria-hidden="true"></i></p>
                                             <h3 class="h6 mb-0 text-white">
                                             Customer Transaction History</h3>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                           <a href="{{url('admin/booking-assign')}}">
                              <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                 <div class="mainflip">
                                    <div class="frontside">
                                       <div class="card bg-gradient-x-primary pb-0">
                                          <div class="card-body text-center">
                                             <p class="icon" > <i class="fa fa-phone" aria-hidden="true"></i></p>
                                             <h3 class="h6 mb-0 text-white">
                                             Booking Assign</h3>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </a>
                        </div>

        </div>
      </div>
    </div>
    <div class="d-sm-flex align-items-center justify-content-between  pt-4 pb-5">
      <h1 class="h3 mb-0 text-gray-800   h-100 px-1 text-capitalize">Booking Dashboard</h1>
    </div>
    <div class="row mb-5">
      <div class="col-xl-3 col-sm-6 py-2">
        <div class="card text-white  h-100">
          <div class="card-body card-totalvenue44">
            <h6 class="text-uppercase text-center">Total booking</h6>
            <h1 class="display-4 totalvenue">{{$totalBooking->count()}}</h1>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 py-2">
        <div class="card text-white h-100">
          <div class="card-body card-totalvenue11">
            <h6 class="text-uppercase text-center">Total Pending Booking</h6>
            <h1 class="display-4 totalvenue">{{$totalPendingBooking->count()}}</h1>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 py-2">
        <div class="card text-white  h-100">
          <div class="card-body card-totalvenue11">
            <h6 class="text-uppercase text-center">Total Confirmed Booking</h6>
            <h1 class="display-4 totalvenue">{{$totalConfirmedBooking->count()}}</h1>
          </div>
        </div>
      </div>
      </div>
      <div class="d-sm-flex align-items-center justify-content-between  pt-4 pb-5">
      <h1 class="h3 mb-0 text-gray-800   h-100 px-1 text-capitalize">Campaign Wise Booking Dashboard</h1>
      </div>
      <div class="row mb-5">
      
         @foreach($campaignWiseBooking as $booking)
         <div class="col-xl-3 col-sm-6 py-2">
        <div class="card text-white  h-100">
          <div class="card-body card-totalvenue44">
            <h6 class="text-uppercase text-center">Total booking {{$booking->campaign['campaign_name']}}</h6>
            <h1 class="display-4 totalvenue">{{$booking->campaign_count}}</h1>
          </div>
          </div>
          
        </div>
        @endforeach
        
      </div>
    </div>
  </div>
@stop