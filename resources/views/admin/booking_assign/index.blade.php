@extends('common.default')
@section('title', 'Booking Assign List')
@section('content')
<style type="text/css">
  .bttn{margin-top: 32px;height: 38px;font-weight: 800;}
  .row.border.p-3 {
    background-color: #f3f3f3;
    border-radius: 0px;
    padding: 2rem 20px !important;
  }

  .assign {
    color: #5d5858;
    font-weight: 600;
    letter-spacing: 0px;
    margin: 0px 0px !important;
    display: inline-block;
    letter-spacing: 0px;
  }

  .border-boking {
    border: 1px solid #ebebeb;
    margin-top: 13.2px;
    margin-bottom: 15px;
  }

  .border-boking::before {
    content: "";
    /* border: 2px solid red; */
    top: 36px;
    left: 0;
    width: 88px;
    position: absolute;
    height: 6px;
    background-image: linear-gradient(to right,#FF425C 0,#FFA8B4 100%);   background-color: #106db2;
    z-index: 999;
    border-radius: 10px;
  }

  .assign-book {
    position: relative;
    margin-top: 30px;
    margin-bottom: 30px;
  }

  h1.display-4.totalvenue {
    margin: 0px auto !important;
  }
</style>

<div id="content">
  <div class="container-fluid" style="margin-top: 20px;">
    <!-- Booking Assign and Back Button header starts here -->
    <div class="sub-headeing">
      <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Booking Assign</h3>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
          <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/product/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
              <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
          </span>
        </div>
      </div>
    </div>
    <!-- Booking Assign and Back Button header ends here -->
    <div class="card">
      <div class="card-body">
        <div id="message"></div>
        <!-- Table Listing -->
        <div class="card-body" >
          <div class="table-responsive">
            <div class="row mb-5">
              <div class="col-xl-3 col-sm-6 py-2">
                <div class="card text-white  h-100">
                  <div class="card-body card-totalvenue44">
                    <h6 class="text-uppercase text-center">Total Today Booking</h6>
                    <h1 class="display-4 totalvenue">{{$today_total_booking}}</h1>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 py-2">
                <div class="card text-white h-100">
                  <div class="card-body card-totalvenue11">
                    <h6 class="text-uppercase text-center">Total Yesterday Booking</h6>
                    <h1 class="display-4 totalvenue">{{$yesterday_total_booking}}</h1>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 py-2">
                <div class="card text-white  h-100">
                  <div class="card-body card-totalvenue11">
                    <h6 class="text-uppercase text-center">Total Week Booking</h6>
                    <h1 class="display-4 totalvenue">{{$week_total_booking}}</h1>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 py-2">
                <div class="card text-white  h-100">
                  <div class="card-body card-totalvenue11">
                    <h6 class="text-uppercase text-center">Total Month Booking</h6>
                    <h1 class="display-4 totalvenue">{{$month_total_booking}}</h1>
                  </div>
                </div>
              </div>
            </div>
            <!-- Booking Filters Starts Here -->
            <div class="assign-book">
              <h5 class="assign">Booking Filter</h5>
              <div class="border-boking"></div>
            </div>
          
            <div class="row border p-3">
              <div class="col-md-3">
                <label>From Date</label>
                <input type="date" name="from_date" id="from_date" class="form-control" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}">
              </div>
              <div class="col-md-3">
                <label>To Date</label>
                <input type="date" name="to_date" id="to_date" class="form-control" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}">
              </div>
              <div class="col-md-3">
                <label>Select Campaign</label>
                <select class="form-control select2" name="campaign_id" id="campaign_id" multiple>
                  <option value="0">Select Campaign</option>
                  @forelse($campaign as $camp)
                    <option value="{{$camp->id}}" {{(isset($_GET['campaign_id']) && $_GET['campaign_id'] == $camp->id) ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                  @empty
                    <p>No Product List</p>
                  @endforelse
                </select>
              </div>
              <div class="col-md-3">
                <label>Select Product</label>
                <select class="form-control select2" name="product_id" id="product_id" multiple>
                  <option value="0">Select Product</option>
                  @forelse($product as $pro)
                    <option value="{{$pro->id}}" {{(isset($_GET['product_id']) && $_GET['product_id'] == $pro->id) ? 'Selected' : ''}}>{{$pro->name}}</option>
                  @empty
                    <p>No Product List</p>
                  @endforelse
                </select>
              </div>
              <div class="col-md-1">
                <input type="button" class="btn btn-info btn-sm btn-block bttn" id="refresh" value="Refresh" >
              </div>
              <div class="col-md-1">
                <input type="button" class="btn btn-primary btn-sm btn-block bttn" id="filter" value="Filter" >
              </div>
            </div>
          <form method="post" id="assignBookingForm" action="{{ route('booking-assign.store') }}">
            @csrf
            <!-- Assign booking to Agent starts Here -->
            <div class="assign-book">
              <h5 class="assign">Assign Booking To Agent</h5>
              <div class="border-boking"></div>
            </div>
            
            <!-- Assign booking to Agent starts Here -->
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div><br />
            @endif
            <div class="" >
              @if(session()->get('success'))
                <div class="alert alert-success">
                  {{ session()->get('success') }}  
                </div>
              @endif
            </div>
            <div class="row border p-3">
              <div class="col-md-3">
                <label>Select Agent User
                <br>
                  <span class="btn btn-primary btn-sm select-all">Select all</span>
                  <span class="btn btn-primary btn-sm deselect-all">Deselect all</span>
                </label>
                <div class="error1" style="display: none;">This field is required.</div>
                <select class="form-control select2 required" name="user_id[]" multiple id="user_id">
                  @forelse($agentUser as $user)
                    <option value="{{$user->id}}" {{(isset($_GET['user_id']) && $_GET['user_id'] == $user->id) ? 'Selected' : ''}}>{{$user->name}}</option>
                  @empty
                    <p>No User List</p>
                  @endforelse
                </select>
              </div>
              <div class="col-md-3">
                <label>Select Assign Type</label>
                <div class="error2" style="display: none;">This field is required.</div>
                <select class="form-control required" name="assign_type_id" id="assign_type_id">
                  <option value="">Select Assign Type</option>
                  <option value="1" {{(isset($_GET['assign_type_id']) && $_GET['assign_type_id'] == '1') ? 'Selected' : ''}}>Distribute to all</option>
                  <option value="2" {{(isset($_GET['assign_type_id']) && $_GET['assign_type_id'] == '2') ? 'Selected' : ''}}>Agent Wise</option>
                </select>
              </div>
            
              <div class="col-md-1">
                <input type="button" onclick="validate();" class="btn btn-primary btn-sm btn-block bttn" value="Assign" style="margin-top: 43px;" >
              </div>
              <!-- <div class="col-md-1"></div> -->
              <!-- Assign booking to Agent ends Here ---->
            </div>
            <!-- Booking Filters Ends Here ---->
        
            <table class="table table-striped " id="" width="100%" cellspacing="0" style="margin-top: 35px !important;">
              <thead>
                <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e">
                  <th><input type="checkbox" id="select_all">&nbsp;Select All</th>
                  <th>SN</th>
                  <th>Campaign ID</th>
                  <th>Campaign Name</th>
                  <th>Product ID</th>
                  <th>Product Name</th>
                  <th>Date</th>
                  <!-- <th>Action</th>
                  <th>Status</th> -->
                </tr>
              </thead>
              <tbody></tbody>
            </table>
            {{ csrf_field() }}
          </form>
          <!-- Table Listing -->
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
<script>

  function validate(argument) 
    { 
      var isError = false;
      var user_id = $('#user_id').val();
      var assign_type_id = $('#assign_type_id').val();

       if($('tr').find('.chkbx').find(':checkbox:checked').length == 0){
        $('.error1').css('color','red').show();
        isError = true; 
      }

      if(user_id == ""){
        $('.error2').css('color','red').show();
        isError = true;
      } 

      if(assign_type_id == ""){
        $('.error3').css('color','red').show();
        isError = true;
      }

      if(isError == false){
        $('#assignBookingForm').submit();
      }
    }


  $(document).ready(function(){
    var _token = $('input[name="_token"]').val();
    fetch_data();
    function fetch_data(from_date = '', to_date = '', campaign_id = '', product_id = ''){
      $.ajax({
        url:"{{ route('booking-assign.fetch_data') }}",
        beforeSend: function(){
          // Show image container
          $("#loading").show();
        },
        method:"POST",
        data:{from_date:from_date, to_date:to_date, _token:_token, campaign_id:campaign_id, product_id:product_id},
        dataType:"json",
        success:function(data)
        {
          console.log(data);
          var output = '';
          var counter = 1;
          $('#total_records').text(data.length);
          
          for(var count = 0; count < data.length; count++)
          {
            output += '<div class="error3" style="display: none;">This field is required.</div><tr>';
            output += '<td class="chkbx"><input type="checkbox" name="booking_assign[' + count  + ']" value=' + data[count].id  + ' ></td>';
            output += '<td>' + counter++ + '</td>';
            output += '<td>' + data[count].campaign_id + '</td>';
            output += '<td>' + data[count].campaign.campaign_name + '</td>';
            output += '<td>' + data[count].product_id + '</td>';
            output += '<td>' + data[count].products.name + '</td>';
            output += '<td>' + data[count].created_on + '</td></tr>';
          }
          $('tbody').html(output);
        },
        complete: function(){
          $('#loading').hide();
        }
      })
    }

    $('#filter').click(function(){
      var from_date = $('#from_date').val();
      var to_date = $('#to_date').val();
      var campaign_id = $( "#campaign_id option:selected" ).val();
      var product_id = $( "#product_id option:selected" ).val();
      if((from_date != '' &&  to_date != '') || campaign_id != '' || product_id != ''){
        fetch_data(from_date, to_date, campaign_id, product_id);
        }/*else{
           alert('Both Date is required');
        }*/
    });
    
    $('#refresh').click(function(){
      $('#from_date').val('');
      $('#to_date').val('');
      $( "#campaign_id option:selected" ).val('');
      $( "#product_id option:selected" ).val('');
      fetch_data();
    });
    // Select all check - uncheck event trigger starts here
    $('#select_all').click(function() {
      var checked = this.checked;
      $('input[type="checkbox"]').each(function() {
        this.checked = checked;
      });
    });

    $('#assign_type_id').change(function(){
      var assign_type = $(this).val();
      if(assign_type == 1){
        $('input[type="checkbox"]').each(function() {
          $(this).attr('checked', 'checked');
        });     
      }else{
        $('input[type="checkbox"]').each(function() {
          $(this).removeAttr('checked');
        });
      }
    });
});
</script>

@stop



