

@extends('common.default')
@section('title', 'Winner Frequency List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
      <div class="container-fluid" style="margin-top:15px;">
           
           
       
      
           <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Winner Frequency</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                  
                     <a href="{{url('admin/set-winner-frequency/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Winner Frequency
                     </button></a>
                        
                        <a  href="{{url('admin/winner-campaign/dashboard')}}" class="btn btn-primary btn-sm   js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a> 
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
      
      
      
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <br>
      <div class="card">
         
         @if(session()->get('success'))
         <div class="alert alert-success">
            {{ session()->get('success') }}  
         </div>
         @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>Campaign Name</th>
                        <th>Winner Frequency</th>
                        <th>Backup Winner Frequency</th>
                        <th>Action</th>
                       
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($set_winner_frequency as $index=>$allocation)
                     <tr>
                        <td>{{++$index}}</td>
                        <td>{{$allocation->winner_campaign['campaign_name']}}</td>
                        <td>{{$allocation->winner_frequency}}</td>
                        <td>{{$allocation->backup_winner_frequency}}</td>
                        
                       
                        <td style="width: 200px;" class="text-center">
                           <a href="{{ route('set-winner-frequency.show',$allocation->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('set-winner-frequency.edit',$allocation->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                           
                           
                           
                        </td>
                        
                     </tr>
                     @empty
                     <p>No Product List</p>
                     @endforelse
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

