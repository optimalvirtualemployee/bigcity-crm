@extends('common.default')
@section('title', 'Product Create')
@section('content')
<!-- Main Content -->
<style>
input#exampleFormControlFile1 {
    border: 0px !important;
    padding: 1px;
}
</style>
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
         
        
        <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px"> 
        Target Assign</h3>
        
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/target-assign')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        
        </div>
        </div>
       
       
   <div class="container">
      <!-- Outer Row -->
      
      <div class="row justify-content-center mt-3">
         <div class="card target__assign__create">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <form class="user" action="{{ route('target-assign.store') }}" method="POST" enctype="multipart/form-data">
                           @csrf
                           
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Select Product</label>
                           <select   name="product_id" class="inputbox form-control js-example-basic-single" style="height: 45px;">
                              <option value="0">Select Product</option>
                              @forelse($products as $product)
                              <option value="{{$product->id}}" {{old('product_id') == $product->id ? 'Selected' : ''}}>{{$product->name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                           </select>
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Comment</label>
                              <textarea class="form-control" rows="5" placeholder="Enter Comment" id="comment" name="comment">{{ old('comment') }}</textarea>
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Target MRP</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Target MRP" name="target_mrp"value="{{ old('target_mrp') }}">
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Target BCP Price</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Target Proce" name="target_bcp_price"value="{{ old('target_bcp_price') }}">
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Select PD Lead</label>
                           <select   name="pd_lead_id" class="inputbox form-control js-example-basic-single" style="height: 45px;">
                              <option value="0">Select PD Lead</option>
                              @forelse($users as $user)
                              <option value="{{$user->id}}" {{old('pd_lead_id') == $user->id ? 'Selected' : ''}}>{{$user->name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                           </select>

                           
                            </div>
                           
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Target Number</label>
                              <input type="number" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Target No" name="target_no"value="{{ old('target_no') }}">
                           </div>
                           <div class="form-group">
                               <label for="name" class="mr-sm-2">Tatget Closure Date</label>
                              <input type="date" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Target Closure Date" name="target_closure_date"value="{{ old('target_closure_date') }}">
                           </div>

                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary  btn-block mt-4">Submit</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop









