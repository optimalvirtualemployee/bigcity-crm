@extends('common.default')
@section('title', 'Product List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 20px;">
       
        
        
        <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
                <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Target Assign</h3>
                
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                    
                     <a href="{{url('admin/target-assign/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Target Assign
                     </button></a>
                    
                    <a  href="{{url('admin/venue/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                        <i class="fas fa-chevron-left mr-1"></i> Back
                    </a>
                     
                </span>
            </div>
        </div>
        </div>
        </div>
       <br>
     
      <div class="card">
       
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th >Target Id</th>
                        <th>Product Name</th>
                        <th>PD Lead</th>
                        <th>Comment</th>
                        <th>Target MRP</th>
                        <th>Target BCP Price</th>
                        <th>Target No</th>
                        <th>Target Closure Date</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                       <!-- <th>Action</th>-->
                        <th>Status</th>
                        
                        
                        
                        
                        
                        
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($target_assign as $index=>$target)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$target->id}}</td>
                        <td>{{$target->product['name']}}</td>
                        <td>{{$target->user['name']}}</td>
                        <td>{{$target->comment}}</td>
                        <td>{{$target->target_mrp}}</td>
                        <td>{{$target->target_bcp_price}}</td>
                        <td>{{$target->target_no}}</td>
                        <td>{{$target->target_closure_date}}</td>
                        <td>{{isset($target->created_user['name']) ? $target->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($target->updated_user['name']) ? $target->updated_user['name'] : 'NA'}}</td>
                        <td>{{$target->created_at->format('d M Y')}}/{{$target->created_at->format('g:i A')}}</td>
                        <td>{{$target->updated_at->format('d M Y')}}/{{$target->updated_at->format('g:i A')}}</td>
                        
                        
                        
                        <!-- <td style="width: 200px;" class="text-center"> -->
                           <!-- <a href="{{ route('products.show',$target->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a> -->
                           
                           <!-- <a href="{{ route('products.edit',$target->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            <form action="{{ route('products.destroy', $target->id) }}" method="POST" id="delete-form-{{ $target->id }}" style="display: none;">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <input type="hidden" value="{{ $target->id }}" name="id">
                           </form> -->
                           <!-- <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                            event.preventDefault(); document.getElementById('delete-form-{{ $target->id }}').submit();">
                            <span style="color: red;padding:5px;"  ><i class="fas fa-trash fa-primary fa-md"></i></span></a> -->
                           <!--<span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                           <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span> -->
                        <!-- </td> -->
                        <td>
                            
                              <input data-id="{{$target->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $target->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="TargetAssign">  
                        </td> 
                        
                    </tr>
                    @empty
                    <p>No Product List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
