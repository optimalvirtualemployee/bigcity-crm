@extends('common.default')
@section('title', 'Product Edit')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
           <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Product</h1>
             <a href="{{url('admin/products')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center pt-3">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('products.update', $products->id) }}" enctype="multipart/form-data">
                           @method('PATCH') 
                           @csrf
                          
                          <div class="form-group">
                           <select class="form-control" name="category_id" style="height: 45px;">
                              <option value="0">Select Category</option>
                              @forelse($categories as $cat)
                              <option value="{{$cat->id}}" {{$cat->id == $products->category_id  ? 'selected' : ''}}>{{$cat->name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                           </select>
                           </div>
                           
                           
                           
                           <div class="form-group">
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Category Name" name="name" value="{{$products->name}}">
                           </div>
                           <div class="form-group">
                              <textarea class="form-control" rows="5" placeholder="Enter Description" id="comment" name="description">{{$products->description}}</textarea>
                           </div>
                           @if($products->image)
                           <div class="form-group">
                        
                               <img src="{{url('/assets/img/product-images/',$products->image)}}" height="60px" width="60px">
                    
                           </div> 
                           @endif
                           <div class="form-group">
                              <label for="exampleFormControlFile1">Image</label>
                              <input type="file" class="form-control" id="exampleFormControlFile1" name="image">
                            </div>
                             <div id="dvPreview">
                            </div>
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop





 