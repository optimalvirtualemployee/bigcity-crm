@extends('common.default')
@section('title', 'Show Product')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top: 0px;">
             <div class="d-sm-flex align-items-center justify-content-between pt-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize"></h1>
             <a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;  </a>
        </div>
     

      
      
      
      
           <table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                       <th>ID</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Image</th>
                        
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                     
                      
                    <tr>
                        <td>   {{ $products->id }} </td>
                        <td>   {{ $products->name }} </td>
                         <td>  {{ $products->category->name }}</td>
                        <td style="width:500px">  {{ $products->description }}</td>
                        <td>  <img src="{{url('/assets/img/product-images/',$products->image)}}" height="100px" width="100px"></td>
                 
                    
                  </tbody>
               </table>
               
               
               
   </div>
</div>
@stop


