@extends('common.default')
@section('title', 'Edit Winner Campaign')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
       
       
      
        
           
        <div class="sub-headeing">
        
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            
         <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px"> 
        Winner Campaign</h3>
        
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/winnercampaign')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        
        </div>
        </div>
        
        
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center"style="margin-top: 20px;">
         <div class="card login-card">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('winnercampaign.update', $winner_campaign->id) }}">
                           @method('PATCH') 
                           @csrf
                        
                           
                           <div class="form-group row mx-auto">
                               
                                <div class="col-md-12 col-lg-4">
                                  <label>Category Name</label>
                                  <input type="text" class="form-control form-control-user"
                                  id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Category Name " maxlength="20" name="campaign_name" value="{{ $winner_campaign->campaign_name }}">
                                </div>
                                <div class="col-md-12 col-lg-4">
                                 <label>Campaign Brand Id</label>
                                  <select class="form-control" name="campaign_brand_id">
                                  <option value="0">Select Brand</option>
                                  @forelse($companies as $comp)
            
                                    <option value="{{$comp->id}}" {{$winner_campaign->campaign_brand_id == $comp->id ? 'Selected' : ''}}>{{$comp->brand_name}}</option>
                                    @empty
                                      <p>No Brand List</p>
                                  @endforelse
                                  </select>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                    <label>BD Person Name Id</label>
                                   <select class="form-control" name="bd_person_name_id">
                                      <option value="0">Select BD Person Type</option>
                                      @forelse($bd_person_name as $bpn)
                                        <option value="{{$bpn->id}}" {{$winner_campaign->bd_person_name_id == $bpn->id ? 'Selected' : ''}}>{{$bpn->name}}</option>
                                        @empty
                                          <p>No BD Person Name List</p>
                                      @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                     <label>BD Person Type</label>
                                  ` <select class="form-control" name="cs_person_name_id">
                                      <option value="0">Select BD Person Type</option>
                                      @forelse($cs_person_name as $bpn)
                                        <option value="{{$bpn->id}}" {{$winner_campaign->cs_person_name_id == $bpn->id ? 'Selected' : ''}}>{{$bpn->name}}</option>
                                        @empty
                                          <p>No BD Person Name List</p>
                                      @endforelse
                                    </select>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                     <label>Auditor Required</label>
                                    <select class="form-control" name="is_auditor_required">
                                        <option value="0">Select Auditor Required</option>
                                      
                                        <option value="YES" {{$winner_campaign->is_auditor_required == 'YES' ? 'Selected' : ''}}>Yes</option>
                                        <option value="NO" {{$winner_campaign->is_auditor_required == 'NO' ? 'Selected' : ''}}>No</option>
                                        
                                          
                                    </select>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                     <label>Start Date</label>
                                   <input type="date" name="start_date" placeholder="Enter Start date" class="form-control" value="{{ $winner_campaign->start_date }}">
                                </div>
                            </div>
                            <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                     <label>End date</label>
                                   <input type="date" name="end_date" placeholder="Enter End date" class="form-control" value="{{ $winner_campaign->end_date }}">
                                </div>
                                <div class="col-md-12 col-lg-4">
                                     <label>Enter Total No. of winner </label>
                                   <input type="number" name="total_no_of_winners" placeholder="Enter Total No. of winner" class="form-control" value="{{ $winner_campaign->total_no_of_winners }}">
                                </div>
                                <div class="col-md-12 col-lg-4">
                                     <label>Gift Type Id </label>
                                    <select class="form-control" name="gift_type_id">
                                      <option value="0">Select Gift Type</option>
                                      @forelse($gift_type as $gt)
                                        <option value="{{$gt->id}}" {{$winner_campaign->gift_type_id == $gt->id ? 'Selected' : ''}}>{{$gt->name}}</option>
                                        @empty
                                          <p>No BD Person Name List</p>
                                      @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-4">
                                 <label>Auditor Required </label>
                               <select class="form-control" name="gift_tax_applicable">
                                    <option value="0">Select Auditor Required</option>
                                  
                                    <option value="YES" {{$winner_campaign->gift_tax_applicable == 'YES' ? 'Selected' : ''}}>Yes</option>
                                    <option value="NO" {{$winner_campaign->gift_tax_applicable == 'NO' ? 'Selected' : ''}}>No</option>
                                    
                                      
                                </select>
                            </div>
                            </div>
                            <div class="col-md-12 col-lg-12">
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-lg mt-4">Update</button>
                           </a>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop
 