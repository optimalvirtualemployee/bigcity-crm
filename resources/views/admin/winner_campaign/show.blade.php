@extends('common.default')
@section('title', 'Show Winner Campaign')
@section('content')

<style>
p.show-details{
    display: inline-block;
    width: 60%;
    font-weight:700;
}
p.show-details::after { 
    content: ' \25B6';
    color: #106db2;
    position: absolute;
    right: 0px;
    width: 45%;
    margin-top: 0px;
}
.card-body.col-md-12.col-lg-12 {
    padding: 0px 1rem !important;
}
</style>

<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
         
        
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Winner Campaign</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/winnercampaign')}}" class="btn btn-primary btn-sm  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
        
        
        
            <div class="container" style="margin-top:50px;">
            <!-- Outer Row -->
            <div class="row justify-content-center">
            <div class="card login-card" style="width: 70%;">
            <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
            <div class="col-lg-12">
            
            
            
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">ID </p>
            
            {{$winner_campaign['id']}}
            
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Name </p>
            
           {{$winner_campaign['name']}}
            
            </div>
            
            
             <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Brand Name </p>
            
           {{$winner_campaign->company['brand_name']}}
            
            </div>
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">BD Person Name </p>
            
           {{$winner_campaign->bd_user['name']}}
            
            </div>
            
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">CS Person Name </p>
            
            {{$winner_campaign->cs_user['name']}}
            
            </div>
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Gift Name </p>
            
            {{$winner_campaign->gift_type['name']}}
            
            </div>
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Auditor Required </p>
            
           {{$winner_campaign['is_auditor_required']}} 
            
            </div>
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Start Date </p>
            
             {{$winner_campaign['start_date']}}
            
            </div>
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">End Date </p>
            
            {{$winner_campaign['end_date']}}
            
            </div>
            
              <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Total no of Winners </p>
            
           {{$winner_campaign['total_no_of_winners']}}
            
            </div>
            
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Gift tax applicable </p>
            
           {{$winner_campaign['gift_tax_applicable']}}
            
            </div>
            
            
            
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Created By </p>
           {{Auth::user()->name}}
            </div>
            
            
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Created Date/Time</p>
           {{$winner_campaign->created_at->format('d M Y')}}/{{$winner_campaign->created_at->format('g:i A')}}
            </div>
            
            
            <div class="card-body col-md-12 col-lg-12">
            <p class="show-details">Status</p>
           {{$winner_campaign->status ? 'Active':'Inactive'}}
            </div>
            
            
            
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
       
       
       
       
       
     <!--   
  <div class="table-responsive">
             <table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>ID</th>
                        <th>Name</th>
                        <th>Brand Name</th>
                        <th>BD Person Name</th>
                        <th>CS Person Name</th>
                        <th>Gift Name</th>
                        <th>Auditor Required</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Total no of Winners</th>
                       
                        <th>gift_tax_applicable</th>
                        <th>Created By</th>
                        <th>Created Date/Time</th>
                        <th>Status</th>
                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    <tr>
                        <td>{{$winner_campaign['id']}} </td>
                        <td>{{$winner_campaign['name']}} </td>
                        <td>{{$winner_campaign->company['brand_name']}} </td>
                        <td>{{$winner_campaign->bd_user['name']}} </td>
                        <td>{{$winner_campaign->cs_user['name']}} </td>
                        <td>{{$winner_campaign->gift_type['name']}} </td>
                        
                        <td>{{$winner_campaign['is_auditor_required']}} </td>
                        <td>{{$winner_campaign['start_date']}} </td>
                        <td>{{$winner_campaign['end_date']}} </td>
                        <td>{{$winner_campaign['total_no_of_winners']}} </td>
                        <td>{{$winner_campaign['gift_tax_applicable']}} </td>
                        
                        
                        <td>{{Auth::user()->name}}</td>
                        <td>{{$winner_campaign->created_at->format('d M Y')}}/{{$winner_campaign->created_at->format('g:i A')}}</td>
                        <td>{{$winner_campaign->status ? 'Active':'Inactive'}}</td>
                        
                   
                   </tr>
                  </tbody>
             </table>
             </div>
             -->
    
   </div>
</div>
@stop