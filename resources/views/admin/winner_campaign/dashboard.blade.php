@extends('common.default')
@section('content')
<!-- For demo purpose -->
<style>
  
  .icon i{
      font-size: 50px;
      color: #FFF;
      }
      .icon i:hover{
      font-size: 50px;
      color: #FFF;
       
      transition: .5s; 
      
      }
 
</style>
 

 

<div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="dahboard-title">Winner Campaign Management</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-0   back_mbl_screen">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

        
    <div class="container">
         <div class="d-sm-flex align-items-center justify-content-between  winner_campaign">
            <h1 class="h3 mb-0    text-capitalize"></h1>
            
          </div>
          
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        <div class="col-6 col-sm-6 col-md-3 ">
                            <a href="{{url('admin/winnercampaign')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  pb-0 bg-gradient-x-primary">
                                       <div class="card-body text-center mobile_screen_dashboard">
                                          
                                             <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                          
                                          <h3 class="h6 mb-3  text-white">
                                          Add Winner Campaign</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/gifttype')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  pb-0 bg-gradient-x-primary">
                                       <div class="card-body text-center mobile_screen_dashboard">
                                          
                                             <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                          
                                          <h3 class="h6 mb-3   text-white">
                                          Add Gift Type</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/setgiftallocation')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card   pb-0 bg-gradient-x-primary">
                                       <div class="card-body text-center mobile_screen_dashboard">
                                          
                                             <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                          
                                          <h3 class="h6 mb-3   text-white">
                                          Set Gift Allocation</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>

                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/set-gift-attribute')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  pb-0 bg-gradient-x-primary">
                                       <div class="card-body text-center mobile_screen_dashboard">
                                          
                                             <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                          
                                          <h3 class="h6 mb-3  text-white">
                                          Set Gift Attribute</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/set-winner-frequency')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  pb-0 bg-gradient-x-primary">
                                       <div class="card-body text-center mobile_screen_dashboard">
                                          
                                             <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                          
                                          <h3 class="h6 mb-3  text-white">
                                          Set Winner Frequency</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/winner-data-upload')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  pb-0 bg-gradient-x-primary">
                                       <div class="card-body text-center mobile_screen_dashboard">
                                          
                                             <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                          
                                          <h3 class="h6 mb-3  text-white">
                                          Winner Data Upload</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/holiday-attribute')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  pb-0 bg-gradient-x-primary">
                                       <div class="card-body text-center mobile_screen_dashboard">
                                          
                                             <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                          
                                          <h3 class="h6 mb-3  text-white">
                                          Holiday Attribute</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                        <!--<div class="col-6 col-sm-6 col-md-3">
                            <a href="{{url('admin/winner-data-operation/create')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  pb-0 bg-gradient-x-primary">
                                       <div class="card-body text-center">
                                          
                                             <p class="icon" > <i class="fas fa-chart-line"></i></p>
                                          
                                          <h3 class="h6 mb-3  text-white">
                                          Winner Data Operation</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>-->
                         </div>
                  </div>
               </div>
                        
                       <!-- <div class="text-center   pt-4 pb-5">
                   <h1 class="h3 mb-0 text-gray-800   h-100 px-1 text-capitalize" style="letter-spacing: -1px;">Main Winners for Callouts Dashboard</h1>
             
               </div>
               <div class="row mb-5">
                   
                   
                   
                  
                  @foreach($winner_campaign as $winner)
                  <div class="col-xl-3 col-sm-6 py-2">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue22">
                            
                            <h6 class="text-uppercase text-center">Total Pending {{$winner->campaign_name}}</h6>
                            <h1 class="display-4 totalvenue">4</h1>
                              @can('venue-list')
                              <a href="{{url('admin/venues')}}" class="btn btn-primary btn-sm">Read More</a>
                              @endcan
                        </div>
                    </div>
                  </div>
                  @endforeach
                  </div>-->
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
               
               
       
          
               
           
@stop