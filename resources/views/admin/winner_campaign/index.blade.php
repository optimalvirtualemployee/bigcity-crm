@extends('common.default')
@section('title', 'Winner Campaign List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top: 15px;">
      <!-- Page Heading -->
      <!-- DataTales Example -->
        <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Winner Campaign</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                       <a href="{{url('admin/winnercampaign/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Winner Campaign
                     </button></a>
                        
                        <a  href="{{url('admin/winner-campaign/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
      
      <br>
     
      <div class="card ">
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                   
                   <!--
                  <li style="margin-right: 20px;">
                      <a href="{{url('admin/winnercampaign/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Winner Campaign
                     </button></a>
                  </li>
                  -->
                  
                  <!--<li style="margin-right: 20px;">  <button class="btn btn-primary btn-sm"     type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Import Category
                     </button>
                  </li>
                  <li style="margin-right: 20px;">  <button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-print"></i> &nbsp; Print Category
                     </button>
                  </li>
                  <li class="nav-item dropdown" ml-2>
                     <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-file-export"></i>&nbsp; Export Category
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton ">
                           <a class="dropdown-item font-weight-bold" href="{{route('export_category.excel')}}"><i class="fas fa-file-excel"></i> &nbsp; Export to Excel</a>
                           <a class="dropdown-item font-weight-bold" href="abc.docx"><i class="fas fa-file-word"></i> &nbsp; Export to Word</a>
                           <a class="dropdown-item font-weight-bold" href="abc.pdf"><i class="fas fa-file-pdf"></i> &nbsp;Export to PDF</a>
                        </div>
                     </div>
                  </li>-->
               </ul>
            </nav>
         </div>
       
         <div class="card-body" >
               @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Created By</th>
                        <th>Created Date/Time</th>
                        <th>Action</th>
                        
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($winner_campaign as $index=>$win_campaign)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$win_campaign->id}}</td>
                        <td>{{$win_campaign->campaign_name}}</td>
                        <td>{{Auth::user()->name}}</td>
                        <td>{{$win_campaign->created_at->format('d M Y')}}/{{$win_campaign->created_at->format('g:i A')}}</td>
                        
                        <td>
                           <a href="{{ route('winnercampaign.show',$win_campaign->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('winnercampaign.edit',$win_campaign->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                           
                        </td>
                    </tr>
                    @empty
                    <p>No Category List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      
 
        
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

