

@extends('common.default')
@section('title', 'Gift Allocation List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
      <div class="container-fluid" style="margin-top:15px;">
           
           
       
      
           <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Set Gift Allocation</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                  
                     <a href="{{url('admin/setgiftallocation/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Gift allocation
                     </button></a>
                        
                        <a  href="{{url('admin/winner-campaign/dashboard')}}" class="btn btn-primary btn-sm   js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a> 
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
      
      
      
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <br>
      <div class="card">
         
         @if(session()->get('success'))
         <div class="alert alert-success">
            {{ session()->get('success') }}  
         </div>
         @endif
         <div class="card-body" >
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>Campaign Name</th>
                        <th>Gift Type</th>
                        <th>Action</th>
                        <th>Status</th>
                       
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($set_gift_allocation as $index=>$allocation)
                     <tr>
                        <td>{{++$index}}</td>
                        <td>{{$allocation->winner_campaign['campaign_name']}}</td>
                        <td>{{$allocation->gift_type->name}}</td>
                        
                       
                        <td style="width: 200px;" class="text-center">
                           <a href="{{ route('setgiftallocation.show',$allocation->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('setgiftallocation.edit',$allocation->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                          
                           <!--<form action="{{ route('setgiftallocation.destroy', $allocation->id) }}" method="POST" id="delete-form-{{ $allocation->id }}" style="display: none;">
                              {{csrf_field()}}
                              {{ method_field('DELETE') }}
                              <input type="hidden" value="{{ $allocation->id }}" name="id">
                           </form>
                           
                           <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                              event.preventDefault(); document.getElementById('delete-form-{{ $allocation->id }}').submit();">
                           <span style="color: red;padding:5px;"  ><i class="fas fa-trash fa-primary fa-md"></i></span></a>-->
                           
                           <!--<span style="color: green;padding:5px;" ><i class="fas fa-comments fa-primary fa-md"></i></span>
                              <span style="color: grey;"><i class="fas fa-search fa-primary fa-md"></i></span> -->
                        </td>
                        <td>
                            
                              <input data-id="{{$allocation->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $allocation->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="SetGiftAllocation">  
                        </td> 
                        
                     </tr>
                     @empty
                     <p>No Product List</p>
                     @endforelse
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

