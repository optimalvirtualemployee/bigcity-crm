@extends('common.default')
@section('title', 'Set gift Allocation Edit')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top: 15px;">
         
        
         <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Set Gift Allocation</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/setgiftallocation')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fa fa-arrow-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center pt-3">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('setgiftallocation.update', $set_gift_allocation->id) }}" enctype="multipart/form-data">
                           @method('PATCH') 
                           @csrf
                          
                          <div class="form-group">
                           <label>Campaign Name</label>
                           <select   name="campaign_id" class="inputbox form-control" style="height: 45px;">
                              <option value="0">Select Campaign</option>
                              @forelse($winner_campaign as $campaign)
                              <option value="{{$campaign->id}}" {{$set_gift_allocation->campaign_id == $campaign->id ? 'Selected' : ''}}>{{$campaign->campaign_name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                           </select>
                           
                            </div>
                            <div class="form-group">
                            <label>Gift Type Id</label>
                           <select   name="gift_type_id" class="inputbox form-control" style="height: 45px;">
                              <option value="0">Select Gift Type</option>
                              @forelse($gift_type as $type)
                              <option value="{{$type->id}}" {{$set_gift_allocation->gift_type_id == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                           </select>
                           
                            </div>
                           <div class="form-group">
                              <label>Enter Quantity</label>
                              <input type="number" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Quantity" name="qty" value="{{ $set_gift_allocation->qty }}">
                           </div>
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop





 