@extends('common.default')
@section('title', 'Show Gift Allocation')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top: 15px;">
      
     
           <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Set Gift Allocation</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/setgiftallocation')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
      
      
      
            <div class="container" style="margin-top:50px;">
            <div class="row justify-content-center">
            <div class="card login-card" style="width: 50%;">
            <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
            <div class="col-lg-12">
                
            <div class="form-group">
            <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Qty </p>
            
            {{ $set_gift_allocation->qty }} 
            
            </div>
            
             <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Campaign ID </p>
            
             {{ $set_gift_allocation->campaign_id }}
            
            </div>
            
             <div class="card-body col-md-12 col-lg-12">
            
            <p class="show-details">Gift Type ID </p>
            
             {{ $set_gift_allocation->gift_type_id }} 
            
            </div>
            
            
            
      
    
            
            </div>
            
            
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
            
      
         
               
               
               
   </div>
</div>
@stop


