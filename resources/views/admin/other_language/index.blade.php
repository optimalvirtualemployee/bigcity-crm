@extends('common.default')
@section('title', 'Category List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      <!-- Page Heading -->
      <!-- DataTales Example -->
      
     
        
        
          <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Other Language</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            
             <a href="{{url('admin/otherlanguage/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Other Language
                     </button></a>
            
           <a href="{{route('export_otherlanguage.excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Other Language
                     </button></a>
            
            <a  href="{{url('admin/otherlanguage/dashboard')}}" class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fa fa-arrow-left mr-1"></i> Back
            </a>
            
            </span>
            </div>
            </div>
            </div>
            </div>
            </div>
      
     
      <div class="card ">
       
       
         <div class="card-body" >
               @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>ID</th>
                        <th>Other Lang Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                     @forelse($other_language as $index=>$other)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$other->id}}</td>
                        <td>{{$other->name}}</td>
                        <td>{{isset($other->created_user['name']) ? $other->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($other->updated_user['name']) ? $other->updated_user['name'] : 'NA'}}</td>
                        <td>{{$other->created_at->format('d M Y')}}/{{$other->created_at->format('g:i A')}}</td>
                        <td>{{$other->updated_at->format('d M Y')}}/{{$other->updated_at->format('g:i A')}}</td>
                        
                        <td>
                           <a href="{{ route('otherlanguage.show',$other->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('otherlanguage.edit',$other->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                           <td>
                              <input data-id="{{$other->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $other->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="OtherLanguage">
                              </div>
                        </td>
                    </tr>
                    @empty
                    <p>No Category List</p>
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      
 
        
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

