@extends('common.default')
@section('title', 'Venue List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
       
  <section class="top__bar mb-2">
    <div class="">
        <div class="top__inner">
            <div class="left__top ml-3">
                 <h3 class="dahboard-title">QC Rejected Venue List</h3>
            </div>
             <div class="right__top mr-3">
               
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/venue/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
              
            </div>
        </div>
    </div>
</section>


      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card">
         
         
         <div class="card-body" >
             @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th >Venue Id</th>
                        <th>Venue Name</th>
                        <th>Venue Type</th>
                        <th>PD Name</th>
                        <th>Product Catgeory Name</th>
                        <th>QC Comment</th>
                        <th>Rejected QC Comment</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>QC Status</th>
                        <th>Action</th>
                        
                        
                        
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                     @forelse($venues as $index=>$venue)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$venue->id}}</td>
                        <td>{{$venue->venue_name}}</td>
                        <td>{{($venue->venue_type == '1')? 'Chain' : 'Standalone'}}</td>
                        <td>{{$venue->user['name']}}</td>
                        <td>{{$venue->category['name']}}</td>
                        <td>{{isset($venue->qc_venue_comment) ? $venue->qc_venue_comment : 'NA' }}</td>
                        <td>{{isset($venue->rejected_venue_comment) ? $venue->rejected_venue_comment : 'NA'}}</td>
                        <td>{{isset($venue->created_user['name']) ? $venue->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($venue->updated_user['name']) ? $venue->updated_user['name'] : 'NA'}}</td>
                        <td>{{$venue->created_at->format('d M Y')}}/{{$venue->created_at->format('g:i A')}}</td>
                        <td>{{$venue->updated_at->format('d M Y')}}/{{$venue->updated_at->format('g:i A')}}</td>
                        <td>{{$venue->comment_status}}</td>
                        
                        
                        
                        
                        
                        <td>
                           <a href="{{ route('qc-rejected-venue.show',$venue->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <!--<a href="{{ route('venues.edit',$venue->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>-->
                            
                           
                        </td>
                        
                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->


@stop
