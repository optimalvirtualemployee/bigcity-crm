@extends('common.default')
@section('title', 'Primary Language')
@section('content')

<style>.card:hover{
     
  -webkit-box-shadow: none !important;  
	-moz-box-shadow: none ! important;
	box-shadow: unset !important;  
	border:1px solid red;
}
 </style>
<meta name="csrf-token" content="{!! csrf_token() !!}">
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      <!-- Page Heading -->
      <!-- DataTales Example -->
      
      
      
         <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Primary Language</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            
             <a href="{{route('primary_language.create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Primary Language
                     </button></a>
            
                 
            <a href="{{route('export_primarylanguage.excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Primary Language
                     </button></a>
            
            <a  href="{{url('admin/state/dashboard')}}" class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fa fa-arrow-left mr-1"></i> Back
            </a>
            
            </span>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
      <div class="card">
          <!--
         <div class="card-header d-none d-sm-block mt-4">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  <li style="margin-right: 20px;">
                      <a href="{{route('primary_language.create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Primary Language
                     </button></a>
                  </li>
                  <li style="margin-right: 20px;">
                      
                  <a href="{{route('export_primarylanguage.excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Primary Language
                     </button></a>
                     </li>
                 
               </ul>
            </nav>
         </div>
         -->
        
         <div class="card-body" >
               @if(session()->get('success'))
                <div class="alert alert-success">
                  {{ session()->get('success') }}  
                </div>
               @endif
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e;text-align: left"  >
                        <th>Sn</th>
                        <th>Id</th>
                        <th>Primary Lang Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i = 1;?>
                     @forelse($primarylanguage as $pl)
                      <tr>
                          <td>{{$i}}</td>
                          <td>{{$pl->id}}</td>
                          <td>{{$pl->name}}</td>
                          <td>{{isset($pl->created_user['name']) ? $pl->created_user['name'] : 'NA'}}</td>
                            <td>{{isset($pl->updated_user['name']) ? $pl->updated_user['name'] : 'NA'}}</td>
                            <td>{{$pl->created_at->format('d M Y')}}/{{$pl->created_at->format('g:i A')}}</td>
                            <td>{{$pl->updated_at->format('d M Y')}}/{{$pl->updated_at->format('g:i A')}}</td>
                          <td>
                             <a href="{{ route('primary_language.show',$pl->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                             
                             <a href="{{ route('primary_language.edit',$pl->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                             
                             <td>
                                <input data-id="{{$pl->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $pl->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="PrimaryLanguage">
                              </div>
                          </td>
                      </tr>
                      <?php $i++; ?>
                    @empty
                      <p>No Primary Languages</p>
                    @endforelse
                   </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

