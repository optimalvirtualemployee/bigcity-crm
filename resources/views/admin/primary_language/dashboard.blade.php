@extends('common.default')
@section('title', 'Language Dashboard')
@section('content')
<!-- For demo purpose -->
    <div class="container">
               <div class="d-sm-flex align-items-center justify-content-between pt-4">
                  <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Language Dashboard</h1>
                  <a href="{{url('admin/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back </a>
               </div>
               <div class="row" pb-4>
                  <div class="col-lg-12 mb-5 mt-5">
                     <div class="row">
                        
                        <div class="col-6 col-sm-6 col-md-2">
                            <a href="{{url('admin/primary_language')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          
                                             <p class="icon" > <i class="fas fa-language"></i></p>
                                          
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Primary Languages</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>

                        <div class="col-6 col-sm-6 col-md-2">
                            <a href="{{url('admin/otherlanguage')}}">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  shadow pb-0">
                                       <div class="card-body text-center">
                                          
                                             <p class="icon" > <i class="fas fa-language"></i></p>
                                          
                                          <h3 class="h6 mb-0 text-uppercase  text-gray-800">
                                          Other Languages</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </a>
                        </div>
                         
                     </div>
                  </div>
               </div>
            

            </div>
@stop