@extends('common.default')
@section('title', 'Primary Language Create')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
       
            <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px"> Add Primary Language</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/primary_language')}}" class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fa fa-arrow-left mr-1"></i> Back
            </a>
            
            </span>
            </div>
            </div>
            </div>
            </div>
            </div>
        
        
        
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center" style="margin-top:100px;">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                           <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                           </div><br />
                        @endif
                        <form class="user" action="{{ route('primary_language.store') }}" method="POST">
                           @csrf
                           <div class="form-group">
                                <label for="name" class="mr-sm-2">Primary Language</label>
                              <input type="text" class="form-control" placeholder="Enter Primary Language" name="name" value="{{ old('name') }}" autocomplete="off" maxlength="20">
                           </div>
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Submit</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop