@extends('common.default')
@section('title', 'Edit Category')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
      
        <div class="sub-headeing">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
        <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Mobile No. Usage Rules</h3>
        <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
        <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
        <a  href="{{url('admin/mobile-usage-rules')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
        <i class="fas fa-chevron-left mr-1"></i> Back
        </a>
        </span>
        </div>
        </div>
        </div>
        
        
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center"style="margin-top:80px;">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('mobile-usage-rules.update', $mobile_usage_rules->id) }}">
                           @method('PATCH') 
                           @csrf
                        
                           
                           <div class="form-group">
                              <label for="name" class="mr-sm-2">Edit Rule Name</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Rule Name" name="name" value="{{$mobile_usage_rules->name}}">
                           </div>
                           
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop
 