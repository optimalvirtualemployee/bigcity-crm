@extends('common.default')
@section('title', 'Category List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      <!-- Page Heading -->
      <!-- DataTales Example -->
      
      
     
        
        <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Mobile No. Usage Rules</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                       <a href="{{url('admin/mobile-usage-rules/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Mobile No. Usage Rules
                     </button></a>
                        
                        <a  href="{{url('admin/campaign/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fas fa-chevron-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
        
        
      <br>
     
      <div class="card ">
         <div class="card-body" >
               @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important; width: 100%;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th>Mobile Number Usage Rule ID</th>
                        <th>Mobile Number Usage Rule Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                        
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                     @forelse($mobile_usage_rules as $index=> $rules)
                      
                    <tr>
                        <td scope="row">{{++$index}}</td>
                        <td>{{$rules->id}}</td>
                        <td>{{$rules->name}}</td>
                        <td>{{isset($rules->created_user['name']) ? $rules->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($rules->updated_user['name']) ? $rules->updated_user['name'] : 'NA'}}</td>
                        <td>{{$rules->created_at->format('d M Y')}}/{{$rules->created_at->format('g:i A')}}</td>
                        <td>{{$rules->updated_at->format('d M Y')}}/{{$rules->updated_at->format('g:i A')}}</td>
                        
                        <td>
                           <a href="{{ route('mobile-usage-rules.show',$rules->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('mobile-usage-rules.edit',$rules->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                           
                        </td>
                        <td>
                            
                              <input data-id="{{$rules->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $rules->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="MobileUsageRule">  
                        </td>
                    </tr>
                    @empty
                
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      
 
        
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop

