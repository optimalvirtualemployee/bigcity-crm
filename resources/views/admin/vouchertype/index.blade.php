@extends('common.default')
@section('title', 'Voucher Type List')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      
        
    
        <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
                <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Voucher Type</h3>
                
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                    
                     
                     <a href="{{url('admin/vouchertype/create')}}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Voucher Type
                     </button></a>
                     
                     <a href="{{route('voucher-type.download-voucher-type-excel')}}"><button class="btn btn-primary btn-sm categoryExpor" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Voucher Type
                     </button></a>
                    
                    <a  href="{{url('admin/voucher-type/dashboard')}}" class="btn btn-primary btn-sm    js-click-ripple-enabled" data-toggle="click-ripple" >
                        <i class="fas fa-chevron-left mr-1"></i> Back
                    </a>
                     
                </span>
            </div>
        </div>
        </div>
        </div>
       <br>
        
        
      
      <div class="card">
         
       
         <div class="card-body" >
               @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
       @endif
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important; width: 100%;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th >SN</th>
                        <th >Voucher Type Id</th>
                        <th>Voucher Type Name</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($vouchers as $index=>$voucher)
                      
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$voucher->id}}</td>
                        <td>{{$voucher->name}}</td>
                        <td>{{isset($voucher->created_user['name']) ? $voucher->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($voucher->updated_user['name']) ? $voucher->updated_user['name'] : 'NA'}}</td>
                        <td>{{$voucher->created_at->format('d M Y')}}/{{$voucher->created_at->format('g:i A')}}</td>
                        <td>{{$voucher->updated_at->format('d M Y')}}/{{$voucher->updated_at->format('g:i A')}}</td>
                        <td>
                           <a href="{{ route('vouchertype.show',$voucher->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           
                           <a href="{{ route('vouchertype.edit',$voucher->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            
                        </td>
                        <td>
                            
                              <input data-id="{{$voucher->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $voucher->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="VoucherType">  
                        </td> 

                    </tr>
                    @empty
                    
                    @endforelse
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>
<script>
  $(function() {
    $('.toggle-class').change(function() {
        var status = $(this).prop('checked') == true ? '1' : '0'; 
        var category_id = $(this).data('id'); 
         
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{url('/admin/changeStatusCategory')}}",
            data: {'status': status, 'category_id': category_id},
            success: function(data){
              console.log(data.success)
            }
        });
    })
  })
</script>
@stop

