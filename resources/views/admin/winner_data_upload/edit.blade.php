@extends('common.default')
@section('title', 'Winner Data Edit')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
           <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Winner Data</h1>
             <a href="{{url('admin/winner-data-upload')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
   <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center pt-3">
         <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
               <!-- Nested Row within Card Body -->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="p-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                        <br /> 
                        @endif
                        <form method="post" action="{{ route('winner-data-upload.update', $winner_data_upload->id) }}" enctype="multipart/form-data">
                           @method('PATCH') 
                           @csrf
                          
                          <div class="form-group">
                           <label>Select Campaign</label>
                           <select   name="campaign_id" class="inputbox form-control" style="height: 45px;">
                              <option value="0">Select Campaign</option>
                              @forelse($winner_campaign as $campaign)
                              <option value="{{$campaign->id}}" {{$winner_data_upload->campaign_id == $campaign->id ? 'Selected' : ''}}>{{$campaign->campaign_name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                           </select>
                           
                            </div>
                            <div class="form-group">
                            <label>Gift Type Id</label>
                           <select   name="gift_type_id" class="inputbox form-control" style="height: 45px;">
                              <option value="0">Select Gift Type</option>
                              @forelse($gift_type as $type)
                              <option value="{{$type->id}}" {{$winner_data_upload->gift_type_id == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                           </select>
                           
                            </div>
                            <div class="form-group">
                            <label>Winner Type</label>
                           <select   name="winner_type" class="inputbox form-control" style="height: 45px;">
                              <option value="0">Select Winner Type</option>
                              
                              <option value="Main" {{$winner_data_upload->winner_type == 'Main' ? 'Selected' : ''}}>Main</option>
                              <option value="Backup" {{$winner_data_upload->winner_type == 'Backup'? 'Selected' : ''}}>Backup</option>
                              
                           </select>
                           
                            </div>
                           <div class="form-group">
                              <label>Enter Winner Data</label>
                              <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Winner Data" name="winner_data" value="{{ $winner_data_upload->winner_data }}">
                           </div>
                           <a href="#" class="">
                           <button type="submit" class="btn btn-primary btn-user btn-block mt-4">Update</button>
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </div>
</div>
</div>
<!-- End of Main Content -->
@stop





 