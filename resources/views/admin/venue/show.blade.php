@extends('common.default')
@section('title', 'Show Venue')
@section('content')
<style>.form-group {
    margin-bottom: 0rem !important;
}
span.venue-details-info {
    color: #d33f3e;
    font-size: 15px;
    font-weight: 800;
    margin-left:5px;
    text-transform: capitalize;
}
.reqedit{margin-top: 35px;float: right;}
</style>
<div id="content">
   <div class="container-fluid" style="margin-top:10px;">
 
    
<section class="top__bar mb-2">
    <div class="">
        <div class="top__inner">
            <div class="left__top ml-3">
                 <h3 class="dahboard-title">Venue Details</h3>
            </div>
             <div class="right__top mr-3">
               
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/venues')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
              
            </div>
        </div>
    </div>
</section>
        
        
     <div class="card shadow" style="margin-top: 30px;">
       <div class="card-body" >
           
           
      <div class="form-group row">
      <div class="card-body col-md-12 col-lg-3">
        <strong>Venue ID:</strong>
         <span class="venue-details-info"> {{ $venues->id }}</span>
      </div>
      <div class="card-body col-md-12 col-lg-3">
        <strong>Venue Name:</strong>
         <span class="venue-details-info"> {{ $venues->venue_name }} </span>
      </div>
      @if($venues->venue_type == '1')
      <div class="card-body col-md-12 col-lg-3">
        <strong>Chain Name:</strong>
         <span class="venue-details-info"> {{ $venues->chain_name }} </span>
      </div>
      @endif
      <div class="card-body col-md-12 col-lg-3">
        <strong>Venue Type:</strong>
         <span class="venue-details-info">  {{ ($venues->venue_type == '1' )? 'Chain' : 'Standalone'}} </span>
      </div>
      </div>
      
      <div class="form-group row">
      <div class="card-body col-md-12 col-lg-3">

           <strong>Product Category:</strong>
    
           <span class="venue-details-info">  {{ $venues->category['name'] }} </span>

       </div>
       <div class="card-body col-md-12 col-lg-3">

           <strong>PD Lead Name:</strong>
    
          <span class="venue-details-info">   {{ $venues->user['name'] }} </span>

       </div>
                  <div class="card-body col-md-12 col-lg-3">

           <strong>State:</strong>
    
          <span class="venue-details-info">   {{ $venues->state['name'] }}</span>

       </div>
       
       <div class="card-body col-md-12 col-lg-3">

           <strong>City:</strong>
    
           <span class="venue-details-info">  {{ $venues->city['name'] }}</span>

       </div>

       </div>
       
       <div class="form-group row">
           <div class="card-body col-md-12 col-lg-3">

           <strong>Area:</strong>
    
           <span class="venue-details-info">  {{ $venues->area['area_name'] }} </span>

       </div>
       
       <div class="card-body col-md-12 col-lg-3">

           <strong>Address:</strong>
    
           <span class="venue-details-info">   {{ $venues->address }} </span>

       </div>
       <div class="card-body col-md-12 col-lg-3">

           <strong>Latitude:</strong>
    
           <span class="venue-details-info">  {{ $venues->latitude }} </span>

       </div>
       
       <div class="card-body col-md-12 col-lg-3">

           <strong>Longitude:</strong>
    
           <span class="venue-details-info">  {{ $venues->longitude }} </span>

       </div>
       </div>
       <div class="form-group row">
           
       <div class="card-body col-md-12 col-lg-3">

           <strong>Rating:</strong>
    
           <span class="venue-details-info">  {{ $venues->rating }} </span>

       </div>
       <div class="card-body col-md-12 col-lg-3">

           

       </div>
       <div class="card-body col-md-12 col-lg-3">

           
       </div>
       <div class="card-body col-md-12 col-lg-3">

           <strong>Billing Term:</strong>
    
           @switch($venues->billing_terms)
                @case(1)
                    <span>Pre-Paid</span>
                    @break
            
                @case(2)
                    <span>Post-Paid</span>
                    @break
                @case(3)
                    <span>CC</span></span>
                    @break
                @case(4)
                    <span>Any Other</span>
                    @break
            @endswitch
       </div>
       
       
       </div>
       <div class="form-group row">
       <div class="card-body col-md-12 col-lg-3">

           <strong>Booking Alert Required:</strong>
    
           @switch($venues->booking_alert_required)
                @case(1)
                    <span>None</span>
                    @break
            
                @case(2)
                    <span>SMS</span>
                    @break
                @case(3)
                    <span>Both</span>
                    @break
                @case(4)
                    <span>Email</span>
                    @break
            @endswitch
       </div>
       @if($venues->billing_terms == '1' || $venues->billing_terms == '2')
       <div class="card-body col-md-12 col-lg-3">

           <strong>Bank Name:</strong>
    
           <span class="venue-details-info">  {{ $venues->bank_name }} </span>

       </div>
       <div class="card-body col-md-12 col-lg-3">

           <strong>Account Number:</strong>
    
           <span class="venue-details-info">  {{ $venues->account_number }} </span>

       </div>
       <div class="card-body col-md-12 col-lg-3">

           <strong>IFSC Code:</strong>
    
          <span class="venue-details-info">   {{ $venues->ifsc_code }} </span>

       </div>
       </div>
        <div class="form-group row">

       <div class="card-body col-md-12 col-lg-12">

           <strong>Name On Account:</strong>
    
           <span class="venue-details-info">  {{ $venues->name_on_account }} </span>

       </div>
       @endif
       @if($venues->other_value)
       <div class="card-body col-md-12 col-lg-3">

           <strong>Other Payment Value:</strong>
    
           <span class="venue-details-info">  {{ $venues->other_value }} </span>

       </div>
       @endif
       
       <br>
       <div class="card-body col-md-12 col-lg-6">

           

               <strong style=""><h5>Venue List:</h5></strong>
    
            <table class="table  table-bordered"   width="100%" cellspacing="0" style="margin-top:5px !important;">
                              <thead>
                                 <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                                    <th >Venue List Type Id</th>
                                    <th >Venue List Type</th>
                                    
            
                                     
                                 </tr>
                              </thead>
                              <tbody style="text-align: center">
                                @foreach($venues->list_type as $id => $list_type)
                                <tr>
                                    <td>{{$list_type->id}} </td>
                                    <td>{{$list_type->name}}</td>
                                    
                               
                               </tr>
                                @endforeach
                              </tbody>
                         </table>

       

       </div>
       
       
       <div class="card-body col-md-12 col-lg-6">

           

             <strong><h5>Black Out Date :</h5></strong>
    
            <table class="table  table-bordered"   width="100%" cellspacing="0" style="margin-top:5px !important;">
                              <thead>
                                 <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                                    <th >Black Out Date Id</th>
                                    <th >Black Out Date</th>
                                    
            
                                     
                                 </tr>
                              </thead>
                              <tbody style="text-align: center">
                                @foreach($venue_black_out_day as $id => $list_type)
                                <tr>
                                    <td>{{$list_type->id}} </td>
                                    <td>{{$list_type->date}}</td>
                                    
                               
                               </tr>
                                @endforeach
                              </tbody>
                         </table>

       

       </div>
       
       </div>
       
       
       
       
        <div class="card-body col-md-12 col-lg-12" style="overflow-x:auto;">

           <h5 style="margin-left: -11px;">Product Details :</h5>
    
            <table class="table  table-bordered"   width="100%" cellspacing="0" style="margin-top:5px !important;margin-left: -15px;">
                              <thead>
                                 <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                                     <th>Product Id</th>
                                    <th>Product Name</th>
                                    <th>Description</th>
                                    <th>Monday MRP of Prodcut</th>
                                    <th>Tuesday MRP of Prodcut</th>
                                    <th>Wednesday MRP of Prodcut</th>
                                    <th>Thursday MRP of Prodcut</th>
                                    <th>Friday MRP of Prodcut</th>
                                    <th>Saturday MRP of Prodcut</th>
                                    <th>Sunday MRP of Prodcut</th>
                                    <th>Monday cost of Prodcut</th>
                                    <th>Tuesday cost of Prodcut</th>
                                    <th>Wednesday cost of Prodcut</th>
                                    <th>Thursday cost of Prodcut</th>
                                    <th>Friday cost of Prodcut</th>
                                    <th>Saturday cost of Prodcut</th>
                                    <th>Sunday cost of Prodcut</th>
            
                                     
                                 </tr>
                              </thead>
                              <tbody style="text-align: center">
                                @foreach($venue_product as $venue_pro)
                                <tr>
                                    <td>{{$venue_pro->product_id}} </td>
                                    <td>{{$venue_pro->product['name']}} </td>
                                    <td>{{$venue_pro->description}}</td>
                                    <td>{{isset($venue_pro->mon_mrp) ? $venue_pro->mon_mrp : 'NA'}}</td>
                                    <td>{{isset($venue_pro->tue_mrp) ? $venue_pro->tue_mrp : 'NA'}}</td>
                                    <td>{{isset($venue_pro->wed_mrp) ? $venue_pro->wed_mrp : 'NA'}}</td>
                                    <td>{{isset($venue_pro->thus_mrp) ? $venue_pro->thus_mrp : 'NA'}}</td>
                                    <td>{{isset($venue_pro->fri_mrp) ? $venue_pro->fri_mrp : 'NA'}}</td>
                                    <td>{{isset($venue_pro->sat_mrp) ? $venue_pro->sat_mrp : 'NA'}}</td>
                                    <td>{{isset($venue_pro->sun_mrp) ? $venue_pro->sun_mrp : 'NA'}}</td>
                                    
                                    <td>{{isset($venue_pro->mon_cost) ? $venue_pro->mon_cost : 'NA'}}</td>
                                    <td>{{isset($venue_pro->tue_cost) ? $venue_pro->tue_cost : 'NA'}}</td>
                                    <td>{{isset($venue_pro->wed_cost) ? $venue_pro->wed_cost : 'NA'}}</td>
                                    <td>{{isset($venue_pro->thus_cost) ? $venue_pro->thus_cost : 'NA'}}</td>
                                    <td>{{isset($venue_pro->fri_cost) ? $venue_pro->fri_cost : 'NA'}}</td>
                                    <td>{{isset($venue_pro->sat_cost) ? $venue_pro->sat_cost : 'NA'}}</td>
                                    <td>{{isset($venue_pro->sun_cost) ? $venue_pro->sun_cost : 'NA'}}</td>
                                    
                               
                               </tr>
                               @endforeach
                              </tbody>
                         </table>

        
       </div>
       <div class="form-group row">
       <div class="card-body col-md-12 col-lg-6">

           <h5>Booking Contact :</h5>
    
<table class="table  table-bordered"   width="100%" cellspacing="0" style="margin-top:5px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Designation</th>

                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    @foreach($venue_billing as $venue_bill)
                    <tr>
                        <td>{{$venue_bill->id}}</td>
                        <td>{{$venue_bill->name}}</td>
                        <td>{{$venue_bill->email}}</td>
                        <td>{{$venue_bill->phone}}</td>
                        <td>{{$venue_bill->designation}}</td>
                   
                   </tr>
                   @endforeach
                  </tbody>
             </table>

       </div>
       <div class="card-body col-md-12 col-lg-6">

           <h5>Booking Contact :</h5>
    
            <table class="table  table-bordered"   width="100%" cellspacing="0" style="margin-top:5px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                         <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Designation</th>

                         
                     </tr>
                  </thead>
                  <tbody style="text-align: center">
                    @foreach($venue_booking as $venue_bok)
                    <tr>
                        <td>{{$venue_bok->id}}</td>
                        <td>{{$venue_bok->name}}</td>
                        <td>{{$venue_bok->email}}</td>
                        <td>{{$venue_bok->phone}}</td>
                        <td>{{$venue_bok->designation}}</td>
                   
                   </tr>
                   @endforeach
                  </tbody>
             </table>

       </div>
       </div>
       
      </div>
   </div>
</div>
</div>
</div>
@stop


