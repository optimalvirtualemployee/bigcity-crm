<!-- Thanks to Pieter B. for helping out with the logistics -->

@extends('common.default')
@section('title', 'Venue Create')
@section('content')
<!-- Main Content -->
<style>
.form-control {
    display: block;
    margin-top: 0px;
}
.select2-container .select2-selection--single {
    height: 35px;
}
#contact textarea{
   
    height:auto;
    
    resize: none; 
}

</style>
<div id="content">
  <div class="container-fluid" style="margin-top: 20px;">
       
  
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Edit Venues</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/venues')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            
    <div class="container">
        <!-- Outer Row -->
    <div class="row justify-content-center mt-5">
      <div class="" style="width: 100%;">
        <div class=""> 
         <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="">
                       
                      

                <div class="container">
                  <form id="contact" action="{{ route('venues.update',$venues->id) }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH') 
                           @csrf
                    <div>
                    <h3>Venue Detail</h3>
                      <section>
                        <div class="form-group row mx-auto">
                          <!--<div class="col-md-12 col-lg-4">
                            <label for="userName">Venue Type*</label>
                            <select class="form-control" name="venue_type" id="venue_type">
                            
                              <option value="2" {{$venues->venue_type == '2' ? 'Selected' : ''}}>Stand Alone</option>
                              <option value="1" {{$venues->venue_type == '1' ? 'Selected' : ''}}>Chain</option>
                            </select>
                          </div>-->
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm">Select PD Lead *</label>
                            <select class="form-control js-example-basic-single required" name="pd_lead_id">
                              <option value="">Select PD Lead</option>
                              @forelse($users as $user)
                              <option value="{{$user->id}}" {{($user->id == $venues->pd_lead_id) ? 'selected' : ''}}>{{$user->name}}</option>
                              @empty
                                <p>No User List</p>
                              @endforelse
                            </select>

                          </div>
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm">Venue Name *</label>
                            <input type="text" class="form-control required" placeholder="Enter Venue Name" name="venue_name" value="{{ $venues->venue_name }}">
                          </div>
                        </div>
                        
                        <div class="form-group row mx-auto">
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm">Select State *</label>
                            <select class="form-control js-example-basic-single state required" name="state_id">
                              <option value="">Select State</option>
                              
                              @forelse($states as $state)
                             
                              <option value="{{$state->id}}" {{ ($venues->state_id) == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                              @empty
                                <p>No State List</p>
                              @endforelse
                              
                              
                            </select>
                          </div>
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm">Select City Name *</label>
                            <select class="form-control js-example-basic-single city required" name="city_id" data-placeholder="First Select State" style="height: 45px;">
                              @forelse($cities as $city)
                              <option value="{{$city->id}}" {{($venues->city_id) == $city->id ? 'Selected' : ''}}>{{$city->name}}</option>
                              @empty
                              <p>No city List</p>
                              @endforelse
                            </select>
                          </div>
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm">Select Area *</label>
                            <select class="form-control js-example-basic-single area required" name="area_id" data-placeholder="First Select City" style="height: 45px;">
                              @forelse($areas as $area)
                              <option value="{{$area->id}}" {{($venues->area_id) == $area->id ? 'Selected' : ''}}>{{$area->area_name}}</option>
                              @empty
                              <p>No city List</p>
                              @endforelse
                             </select>
                           </div>
                        </div>
                        <div class="form-group row mx-auto">
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm">Latitude *</label>
                            <input type="text" name="latitude" class="form-control required" aria-describedby="emailHelp" placeholder="Enter Latitude" name="latitude" id="latitude" value="{{ $venues->latitude}}">
                          </div>
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm">Longitude *</label>
                            <input type="text" name="longitude" class="form-control required" aria-describedby="emailHelp" placeholder="Enter Longitude" name="longitude" value="{{ $venues->longitude }}">
                          </div>
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm">Select Rating *</label>
                            <select class="form-control required" name="rating">
                              <option value="">Select Rating</option>
                              
                              <option value="1" {{$venues->rating == '1' ? 'Selected' : ''}}>1</option>
                              <option value="2" {{$venues->rating == '2' ? 'Selected' : ''}}>2</option>
                              <option value="3" {{$venues->rating == '3' ? 'Selected' : ''}}>3</option>
                              <option value="4" {{$venues->rating == '4' ? 'Selected' : ''}}>4</option>
                              <option value="5" {{$venues->rating==  '5' ? 'Selected' : ''}}>5</option>
                               
                                
                            </select>
                          </div>
                        </div>
                        <div class="form-group row mx-auto">
                          <div class="col-md-12 col-lg-4">
                                  
                            <label for="confirm"> Venue List : </label>
                            <select name="list_type[]" id="list_type" class="form-control select2" multiple="multiple">
                            @foreach($list_type as $id=> $list_type)

                                  <option value="{{ $id }}" {{ (in_array($id, old('list_type', [])) || isset($venues) && $venues->list_type->contains($id)) ? 'selected' : '' }}>
                                      {{ $list_type }}
                                  </option>
                            @endforeach
                            
                              </select>
                            
                          </div>
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm"> Select Booking</label>
                            <select class="form-control booking_alert_required required" name="booking_alert_required">
                              <option value="">Select Booking Alert Required</option>
                              <option value="4" {{$venues->booking_alert_required == '4' ? 'Selected' : ''}}>None</option>
                              <option value="1" {{$venues->booking_alert_required == '1' ? 'Selected' : ''}}>SMS</option>
                              <option value="2" {{$venues->booking_alert_required == '2' ? 'Selected' : ''}}>Both</option>
                              <option value="3" {{$venues->booking_alert_required == '3' ? 'Selected' : ''}}>Email</option>
                            </select>
                          </div>
                          <div class="col-md-12 col-lg-4">
                            <label for="confirm"> Address</label>
                            <textarea class="form-control addressFeild required" aria-describedby="emailHelp" placeholder="Enter Address" name="address">{{ $venues->address }}</textarea>
                          </div>
                        </div>
                        </section>
                        <h3>Billing Datail</h3>
                        <section>
                        <div class="form-group row mx-auto">
                          <div class="col-md-12 col-lg-4">
                            <label for="name">Select Billing Term*</label>
                            
                            <select class="form-control required" name="billing_terms" id="billing_terms">
                              <option value="">Select Billing Term</option>
                              <option value="1" {{$venues->billing_terms == '1' ? 'Selected' : ''}}>Pre Paid</option>
                              <option value="2" {{$venues->billing_terms == '2' ? 'Selected' : ''}}>Post Paid</option>
                              <option value="3" {{$venues->billing_terms == '3' ? 'Selected' : ''}}>CC Form</option>
                              <option value="4" {{$venues->billing_terms == '4' ? 'Selected' : ''}}>Any Other</option>
                            </select>
                          </div>
                        </div>
                        
                        
                        <div class="form-group row mx-auto bank_detail">
                          
                          
                          
                          <div class="col-md-12 col-lg-3">
                            <label for="name">Bank Name*</label>  
                              <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter Bank Name" name="bank_name" value="{{ $venues->bank_name }}">
                          </div>
                          
                          <div class="col-md-12 col-lg-3">
                            <label for="name">Account Number*</label> 
                              <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter Account Number" name="account_number" value="{{ $venues->account_number }}">
                          </div>
                          
                          <div class="col-md-12 col-lg-3">
                            <label for="name">IFSC Code"*</label> 
                              <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter IFSC Code" name="ifsc_code" value="{{ $venues->ifsc_code }}">
                          </div>
                          
                          <div class="col-md-12 col-lg-3">
                            <label for="name">Name on Account*</label> 
                              <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter Name on Account" name="name_on_account" value="{{ $venues->name_on_account }}">
                          </div>

                                  
                        </div>

                        
                        <div class="col-md-12 col-lg-4 other_value">
                          <label for="name">Othre Value *</label> 
                          <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="This is to capture information like Paytm number etc" name="other_value" value="{{ $venues->other_value }}">
                        </div>
                        
                         <fieldset>
                          <legend>Partner Billing</legend>
                          
                        <div class="parent_clone_billing"> 
                        
                         
                          <?php $counter = count($billingContact); ?>  
                          @foreach($billingContact as $key => $value)
                          <div class="child_clone_billing"> 
                            <div class="row mx-auto">
                                <div class="col-md-12 col-lg-3">
                                  <label for="name">Name *</label> 
                                  <input type="text" name="billing_name[]" class="form-control  billing_name required" aria-describedby="emailHelp" placeholder="Enter Name" value="{{ $value['name']}}">
                                </div>
                                  
                                <div class="col-md-12 col-lg-3">
                                  <label for="email">Email*</label> 
                                  <input type="email" name="billing_email[]" class="form-control  billing_email required email" aria-describedby="emailHelp" placeholder="Enter Email" value="{{ $value['email']}}">
                                  <span id="venueEmailBillingErrorMsg" style="color:red;"></span>
                                </div>
                                <div class="col-md-12 col-lg-2">
                                  <label for="name">Phone*</label> 
                                  <input type="tel" name="billing_phone[]" class="form-control  billing_phone required number" aria-describedby="emailHelp" placeholder="Enter phone" value="{{ $value['phone']}}">
                                  <span id="venuePhoneBillingErrorMsg" style="color:red;"></span>
                                </div>
                                <div class="col-md-12 col-lg-3">
                                  <label for="name">Designation*</label> 
                                  <input type="text" name="billing_designation[]" class="form-control  billing_designation required" aria-describedby="emailHelp" placeholder="Enter Designation" value="{{ $value['designation']}}">
                                </div>
                                @if($key == 0)
                                <div class="col-md-12 col-lg-1 add">
                                  
                                  <button type="button" name="add" id="addMoreBillingButton" class="btn btn-success"> <i class="fa fa-plus" aria-hidden="true"></i>
                                  </button>
                                
                                </div>
                                @else
                                  <div class="col-md-12 col-lg-1"><button type="button" name="remove" class="btn btn-danger removeBilling"> <i class="fa fa-minus" aria-hidden="true"></i> </button >
                                  </div>
                                @endif
                            </div>
                          
                          </div>
                          @endforeach
                          
                        
                                   
                        </div>
                        
                          </fieldset>
                        
                        
                        
                            <fieldset>
                          <legend>Partner Booking</legend>
                        
                        <div class="parent_clone_booking"> 
                          <?php $counter = count($bookingContact); ?>  
            
                          @foreach($bookingContact as $key => $value)
                          <div class="child_clone_booking"> 
                         
                            <div class="row mx-auto">
                                
                                <div class="col-md-12 col-lg-3">
                                  <label for="name">Name *</label> 
                                  <input type="text" name="booking_name[]" class="form-control  booking_name required " aria-describedby="emailHelp" placeholder="Enter Name" value="{{ $value['name'] }}">
                                </div>
                                  
                                <div class="col-md-12 col-lg-3">
                                  <label for="email">Email *</label> 
                                  <input type="email" name="booking_email[]" class="form-control  booking_email required email" aria-describedby="emailHelp" placeholder="Enter Email" value="{{ $value['email'] }}">
                                  <span id="venueEmailBookingErrorMsg" style="color:red;"></span>
                                </div>
                                <div class="col-md-12 col-lg-2">
                                  <label for="name">Phone *</label> 
                                  <input type="tel" name="booking_phone[]" class="form-control  booking_phone required
                                  number" aria-describedby="emailHelp" placeholder="Enter phone" value="{{ $value['phone'] }}">
                                  <span id="venuePhoneBookingErrorMsg" style="color:red;"></span>
                                </div>
                                <div class="col-md-12 col-lg-3">
                                  <label for="name">Designation *</label> 
                                  <input type="text" name="booking_designation[]" class="form-control  booking_designation required" aria-describedby="emailHelp" placeholder="Enter Designation" value="{{ $value['designation'] }}">
                                </div>
                                @if($key == 0)
                                <div class="col-md-12 col-lg-1 add">

                                  <button type="button" name="add" id="addMoreBookingButton" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
                                  </button>
                                
                                </div>
                                @else
                                  <div class="col-md-12 col-lg-1"><button type="button" name="remove" class="btn btn-danger removeBooking"><i class="fa fa-minus" aria-hidden="true"></i></button >
                                  </div>
                                @endif
                            </div>
                          
                          </div>
                          @endforeach
                            
                        </div>
                            </fieldset>
                          
                        
                        
                        </section>
                        <h3>Product Detail</h3>
                        <section>
                        <div class="form-group row mx-auto"> 
                          <div class="col-md-12 col-lg-6" style="display: grid;">
                            <label for="name">Select Category *</label> 
                            <select class="form-control category js-example-basic-single required"  name="category_id">
                              <option value="">Select Category</option>
                              @forelse($categories as $cat)
                              <option value="{{$cat->id}}" {{$venues->category_id == $cat->id ? 'Selected' : ''}}>{{$cat->name}}</option>
                              @empty
                                <p>No Category List</p>
                              @endforelse
                              
                              
                            </select>
                          </div>
                          <div class="col-md-12 col-lg-6" style="display: grid;">
                            <label for="name">Select Product *</label> 
                            <select class="form-control product select2 required" multiple="multiple" name="product_id[]" data-placeholder="First Select Category" id="sel1">
                            <?php $counter = count($products); ?>  
            
                              @foreach($products as $id=> $product)

                                  <option value="{{ $id }}" {{ (in_array($id, old('product_id', [])) || isset($venues) && $venues->venue_product->contains($id)) ? 'selected' : '' }}>
                                      {{ $product }}
                                  </option>
                              @endforeach
                            </select>
     
                          </div>
                            
                                      
                        </div>
                        <div class="row mx-auto parent_clone"> 
                          
                              
                              @foreach($venues->venue_product as $id=> $venue_product) 
                              
                              <div class="col-md-12 col-lg-6">
                <textarea name="description[]" class="form-control form-control-user"  placeholder="Enter Product Description">{{$venue_product->pivot->description}}</textarea>
                              </div>
                              <div class="weekDays-selector col-md-12 col-lg-6">
                          
                                <div class="form-group row mx-auto">
                                  <div class="col-md-12 col-lg-4 weekday"> 
                                  <input type="checkbox" id="weekday-mon" class="weekday" {{(isset($venue_product->pivot->mon_mrp)) ? 'checked':'' }}/>
                                  <span for="weekday-mon">Monday</span>
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control required number" id="mrp_of_product" placeholder="Enter MRP Of Product" name="mon_mrp" value="{{$venue_product->pivot->mon_mrp}}">
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control required number" id="cost_of_product" placeholder="Enter Cost Of Product" name="mon_cost" value="{{$venue_product->pivot->mon_cost}}">
                                  </div>
                                </div>
                                <div class="form-group row mx-auto">
                                  <div class="col-md-12 col-lg-4 weekday"> 
                                  <input type="checkbox" id="weekday-tue" class="weekday" {{(isset($venue_product->pivot->tue_mrp)) ? 'checked':'' }}/>
                                  <span for="weekday-tue">Tuesday</span>
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="mrp_of_product" placeholder="Enter MRP Of Product" name="tue_mrp" value="{{$venue_product->pivot->tue_mrp}}">
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="cost_of_product" placeholder="Enter Cost Of Product" name="tue_cost" value="{{$venue_product->pivot->tue_cost}}">
                                  </div>
                                </div>
                                <div class="form-group row mx-auto">
                                  <div class="col-md-12 col-lg-4 weekday"> 
                                  <input type="checkbox" id="weekday-wed" class="weekday" {{(isset($venue_product->pivot->wed_mrp)) ? 'checked':'' }}/>
                                  <span for="weekday-wed">Wednesday</span>
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="mrp_of_product" placeholder="Enter MRP Of Product" name="wed_mrp" value="{{$venue_product->pivot->wed_mrp}}">
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="cost_of_product" placeholder="Enter Cost Of Product" name="wed_cost" value="{{$venue_product->pivot->wed_cost}}">
                                  </div>
                                </div>
                                <div class="form-group row mx-auto">
                                  <div class="col-md-12 col-lg-4 weekday"> 
                                  <input type="checkbox" id="weekday-thu" class="weekday" {{(isset($venue_product->pivot->thus_mrp)) ? 'checked':'' }}/>
                                  <span for="weekday-thu">Thursday</span>
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="mrp_of_product" placeholder="Enter MRP Of Product" name="thus_mrp" value="{{$venue_product->pivot->thus_mrp}}">
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="cost_of_product" placeholder="Enter Cost Of Product" name="thus_cost" value="{{$venue_product->pivot->thus_cost}}">
                                  </div>
                                </div>
                                <div class="form-group row mx-auto">
                                  <div class="col-md-12 col-lg-4 weekday"> 
                                  <input type="checkbox" id="weekday-fri" class="weekday" {{(isset($venue_product->pivot->fri_mrp)) ? 'checked':'' }}/>
                                  <span for="weekday-fri">Friday</span>
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="mrp_of_product" placeholder="Enter MRP Of Product" name="fri_mrp" value="{{$venue_product->pivot->fri_mrp}}">
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="cost_of_product" placeholder="Enter Cost Of Product" name="fri_cost" value="{{$venue_product->pivot->fri_cost}}">
                                  </div>
                                </div>
                                <div class="form-group row mx-auto">
                                  <div class="col-md-12 col-lg-4 weekday"> 
                                  <input type="checkbox" id="weekday-sat" class="weekday" {{(isset($venue_product->pivot->sat_mrp)) ? 'checked':'' }}/>
                                  <span for="weekday-sat">Saturdat</span>
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="mrp_of_product" placeholder="Enter MRP Of Product" name="sat_mrp" value="{{$venue_product->pivot->sat_mrp}}">
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="cost_of_product" placeholder="Enter Cost Of Product" name="sat_cost" value="{{$venue_product->pivot->sat_cost}}">
                                  </div>
                                </div>
                                <div class="form-group row mx-auto">
                                  <div class="col-md-12 col-lg-4 weekday"> 
                                  <input type="checkbox" id="weekday-sun" class="weekday" {{(isset($venue_product->pivot->sun_mrp)) ? 'checked':'' }}/>
                                  <span for="weekday-sun">Sunday</span>
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="mrp_of_product" placeholder="Enter MRP Of Product" name="sun_mrp" value="{{$venue_product->pivot->sun_mrp}}">
                                  </div>
                                  <div class="col-md-12 col-lg-4">
                                  <input type="text" class="form-control number" id="cost_of_product" placeholder="Enter Cost Of Product" name="sun_cost" value="{{$venue_product->pivot->sun_cost}}">
                                  </div>
                                </div>
                                 
                                    
                              </div>   
                              @endforeach
                              
                              
                                    
                                  
                        </div>

                        </section>
                        <h3>Black Out Dates</h3>
                        <section>
                        <div class="parent_clone_black_out_date"> 
                          <?php $counter = count($blackOutDates); ?>  
          
                          
                          @foreach($blackOutDates as $key => $value)
                          <div class="child_clone_black_out_dates"> 
                            
                            <div class="row justify-content-center">
                                <div class="col-md-12 col-lg-6">
                                  <label for="name">Black Out Date *</label> 
                                  <input type="date" name="black_out_dates[]" class="form-control required" aria-describedby="emailHelp" value="{{ $value['date'] }}">
                                </div>
                                
                                
                                
                                @if($key == 0)
                                <div class="col-md-12 col-lg-3 add">
                                  <button type="button" name="add" id="addMoreBlackOutDateButton" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
                                  </button>
                                
                                </div>
                                @else
                                
                                  <div class="col-md-12 col-lg-3">
                                      <button type="button" name="remove" class="btn btn-danger removeBlackOutDate"><i class="fa fa-minus" aria-hidden="true"></i></button >
                                      </div>
                                @endif
                            </div>
                          
                          </div>
                          @endforeach
                            
                        </div>
                        
                        
                        </section>
                      
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@stop
      