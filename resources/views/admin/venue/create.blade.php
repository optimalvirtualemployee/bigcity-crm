<!-- Thanks to Pieter B. for helping out with the logistics -->

@extends('common.default')
@section('title', 'Venue Create')
@section('content')
<!-- Main Content -->
<style>
    .select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    display: block;
}
</style>
<div id="content">
  <div class="container-fluid" style="margin-top:15px;">
       
     
            <div class="sub-headeing">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Venues</h3>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{url('admin/venues')}}" class="btn btn-primary btn-sm js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fas fa-chevron-left mr-1"></i> Back
            </a>
            </span>
            </div>
            </div>
            </div>
            
            
    <div class="container">
        <!-- Outer Row -->
    <div class="row justify-content-center mt-2">
      <div class="" style="width: 100%;">
        <div class=""> 
         <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
              <div class="">
                       
                <div class="mt-3">
                  
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#standalone">Standalone</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#chain">Chain</a>
                    </li>
                    
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div id="standalone" class="tab-pane active"><br>
                      <div class="">
                        <form id="contact" action="{{ route('venues.store') }}" autocomplete="on" method="POST" enctype="multipart/form-data">
                          @csrf
                          <div>
                            <h3>Venue Detail</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                <input type="hidden" name="venue_type" value="2">
                                <!-- <div class="col-md-12 col-lg-4 venueType">
                                  <label for="userName">Venue Type*</label>
                                  <select class="form-control" name="venue_type" id="venue_type">
                                  
                                    <option value="2" {{old('venue_type') == '2' ? 'Selected' : ''}}>Stand Alone</option>
                                    <option value="1" {{old('venue_type') == '1' ? 'Selected' : ''}}>Chain</option> 
                                  </select>
                                </div> -->
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm">Select PD Lead *</label>
                                  <select class="form-control js-example-basic-single required" name="pd_lead_id">
                                    <option value="">Select PD Lead</option>
                                    @forelse($users as $user)
                                    <option value="{{$user->id}}" {{old('user_id') == $user->id ? 'Selected' : ''}}>{{$user->name}}</option>
                                    @empty
                                      <p>No User List</p>
                                    @endforelse
                                  </select>

                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm">Venue Name *</label>
                                  <input type="text" class="form-control required" placeholder="Enter Venue Name" name="venue_name" value="{{ old('venue_name') }}">
                                </div>
                              </div>
                              <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm">Select State *</label>
                                  <select class="form-control js-example-basic-single state required" name="state_id">
                                    <option value="">Select State</option>
                                    @forelse($states as $state)
                                    <option value="{{$state->id}}" {{old('state_id') == $state->id ? 'Selected' : ''}}>{{$state->name}}</option>
                                    @empty
                                      <p>No State List</p>
                                    @endforelse
                                    
                                    
                                  </select>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm">Select City Name *</label>
                                  <select class="form-control js-example-basic-single city required" name="city_id" data-placeholder="First Select State" style="height: 45px;">
                                  </select>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm">Select Area *</label>
                                  <select class="form-control js-example-basic-single area required" name="area_id" data-placeholder="First Select City" style="height: 45px;">
                                   </select>
                                 </div>
                              </div>
                              <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm">Latitude *</label>
                                  <input type="text" name="latitude" class="form-control required" aria-describedby="emailHelp" placeholder="Enter Latitude" name="latitude" id="latitude" value="{{ old('latitude') }}">
                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm">Longitude *</label>
                                  <input type="text" name="longitude" class="form-control required" aria-describedby="emailHelp" placeholder="Enter Longitude" name="longitude" value="{{ old('longitude') }}">
                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm">Select Rating *</label>
                                  <select class="form-control required" name="rating">
                                    <option value="">Select Rating</option>
                                    
                                    <option value="1" {{old('rating') == '1' ? 'Selected' : ''}}>1</option>
                                    <option value="2" {{old('rating') == '2' ? 'Selected' : ''}}>2</option>
                                    <option value="3" {{old('rating') == '3' ? 'Selected' : ''}}>3</option>
                                    <option value="4" {{old('rating') == '4' ? 'Selected' : ''}}>4</option>
                                    <option value="5" {{old('rating') == '5' ? 'Selected' : ''}}>5</option>
                                     
                                      
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                        
                                  <label for="confirm"> Venue List : </label>
                                  <select name="list_type[]" id="list_type" class="form-control select2 required" multiple="multiple">
                                      @foreach($list_type as $id => $list_type)
                                          <option value="{{ $id }}" {{ (in_array($id, old('list_type', [])) || isset($venue) && $venue->list_type->contains($id)) ? 'selected' : '' }}>
                                              {{ $list_type }}
                                          </option>
                                      @endforeach
                                  </select>
                                    
                                  <!-- <div class="form-check">
                                    <input class="form-check-input required" type="checkbox" name="list_type[]" id="exampleRadios1" value="B List" id="acceptTerms">
                                    <label class="form-check-label" for="exampleRadios1">
                                      B List
                                    </label>
                                    &nbsp;
                                  </div>
                                    
                                  <div class="form-check">
                                    <input class="form-check-input required" type="checkbox" name="list_type[]" id="exampleRadios1" value="C List" id="acceptTerms"> 
                                    <label class="form-check-label" for="exampleRadios1">
                                      C List
                                    </label>
                                  </div>  -->
                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm"> Select Booking Alert Required</label>
                                  <select class="form-control booking_alert_required required" name="booking_alert_required">
                                    <option value="">Select Booking Alert Required</option>
                                    <option value="4" {{old('booking_alert_required') == '0' ? 'Selected' : ''}}>None</option>
                                    <option value="1" {{old('booking_alert_required') == '1' ? 'Selected' : ''}}>SMS</option>
                                    <option value="2" {{old('booking_alert_required') == '2' ? 'Selected' : ''}}>Both</option>
                                    <option value="3" {{old('booking_alert_required') == '3' ? 'Selected' : ''}}>Email</option>
                                  </select>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label for="confirm"> Address</label>
                                  <textarea class="form-control addressFeild required" aria-describedby="emailHelp" placeholder="Enter Address" name="address">{{ old('address') }}</textarea>
                                </div>
                              </div>
                            </section>
                            <h3>Billing Details</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                  <label for="name">Select Billing Term*</label>
                                  
                                  <select class="form-control required" name="billing_terms" id="billing_terms">
                                    <option value="">Select Billing Term</option>
                                    <option value="1" {{old('billing_terms') == '1' ? 'Selected' : ''}}>Pre Paid</option>
                                    <option value="2" {{old('billing_terms') == '2' ? 'Selected' : ''}}>Post Paid</option>
                                    <option value="3" {{old('billing_terms') == '2' ? 'Selected' : ''}}>CC Form</option>
                                    <option value="4" {{old('billing_terms') == '2' ? 'Selected' : ''}}>Any Other</option>
                                  </select>
                                </div>
                              </div>
                              
                              
                              <div class="form-group row mx-auto bank_detail">
                                
                                
                                
                                <div class="col-md-12 col-lg-3">
                                  <label for="name">Bank Name*</label>  
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter Bank Name" name="bank_name" value="{{ old('bank_name') }}">
                                </div>
                                
                                <div class="col-md-12 col-lg-3">
                                  <label for="name">Account Number*</label> 
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter Account Number" name="account_number" value="{{ old('account_number') }}">
                                </div>
                                
                                <div class="col-md-12 col-lg-3">
                                  <label for="name">IFSC Code"*</label> 
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter IFSC Code" name="ifsc_code" value="{{ old('ifsc_code') }}">
                                </div>
                                
                                <div class="col-md-12 col-lg-3">
                                  <label for="name">Name on Account*</label> 
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter Name on Account" name="name_on_account" value="{{ old('name_on_account') }}">
                                </div>

                                        
                              </div>

                              
                              <div class="col-md-12 col-lg-6 other_value">
                                <label for="name">Othre Value *</label> 
                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="This is to capture information like Paytm number etc" name="other_value" value="{{ old('other_value') }}">
                              </div>
                              <div class="parent_clone_billing"> 
                                <label style="margin-left:15px;font-weight:700;">Partner Billing:</label>
                                <div class="child_clone_billing"> 
                                  <div class="row mx-auto">
                                      <div class="col-md-12 col-lg-3">
                                        <label for="name">Name *</label> 
                                        <input type="text" name="billing_name[]" class="form-control  billing_name required" aria-describedby="emailHelp" placeholder="Enter Name" value="{{ old('billing_name') }}">
                                      </div>
                                        
                                      <div class="col-md-12 col-lg-3">
                                        <label for="email">Email*</label> 
                                        <input type="email" name="billing_email[]" class="form-control  billing_email required email" aria-describedby="emailHelp" placeholder="Enter Email" value="{{ old('billing_email') }}">
                                        <span id="venueEmailBillingErrorMsg" style="color:red;"></span>
                                      </div>
                                      <div class="col-md-12 col-lg-2">
                                        <label for="name">Phone*</label> 
                                        <input type="tel" name="billing_phone[]" class="form-control  billing_phone required number" aria-describedby="emailHelp" placeholder="Enter phone" value="{{ old('billing_phone') }}">
                                        <span id="venuePhoneBillingErrorMsg" style="color:red;"></span>
                                      </div>
                                      <div class="col-md-12 col-lg-3">
                                        <label for="name">Designation*</label> 
                                        <input type="text" name="billing_designation[]" class="form-control  billing_designation required" aria-describedby="emailHelp" placeholder="Enter Designation" value="{{ old('billing_designation') }}">
                                      </div>
                                      <div class="col-md-12 col-lg-1 add">
                                        
                                        <button type="button" name="add" id="addMoreBillingButton" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                      
                                      </div>
                                  </div>
                                
                                </div>
                                         
                              </div>
                              
                              <div class="parent_clone_booking"> 
                                  
                                <div class="child_clone_bookingg"> 
                                <label style="margin-left:15px;font-weight:700;">Partner Booking :</label>
                                  <div class="row mx-auto">
                                      
                                      <div class="col-md-12 col-lg-3">
                                        <label for="name">Name *</label> 
                                        <input type="text" name="booking_name[]" class="form-control  booking_name required " aria-describedby="emailHelp" placeholder="Enter Name" value="{{ old('booking_name') }}">
                                      </div>
                                        
                                      <div class="col-md-12 col-lg-3">
                                        <label for="email">Email *</label> 
                                        <input type="email" name="booking_email[]" class="form-control  booking_email required email" aria-describedby="emailHelp" placeholder="Enter Email" value="{{ old('booking_email') }}">
                                        <span id="venueEmailBookingErrorMsg" style="color:red;"></span>
                                      </div>
                                      <div class="col-md-12 col-lg-2">
                                        <label for="name">Phone *</label> 
                                        <input type="tel" name="booking_phone[]" class="form-control  booking_phone required
                                        number" aria-describedby="emailHelp" placeholder="Enter phone" value="{{ old('booking_phone') }}">
                                        <span id="venuePhoneBookingErrorMsg" style="color:red;"></span>
                                      </div>
                                      <div class="col-md-12 col-lg-3">
                                        <label for="name">Designation *</label> 
                                        <input type="text" name="booking_designation[]" class="form-control  booking_designation required" aria-describedby="emailHelp" placeholder="Enter Designation" value="{{ old('booking_designation') }}">
                                      </div>

                                      <div class="col-md-12 col-lg-1 add">

                                        <button type="button" name="add" id="addMoreBookingButton" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                      
                                      </div>
                                  </div>
                                
                                </div>
                                  
                              </div>
                            
                            </section>
                            <h3>Product Detail</h3>
                            <section>
                              <div class="form-group row mx-auto"> 
                                <div class="col-md-12 col-lg-6" style="display: grid;">
                                  <label for="name">Select Category *</label> 
                                  <select class="form-control category js-example-basic-single required"  name="category_id">
                                    <option value="">Select Category</option>
                                    @forelse($categories as $cat)
                                    <option value="{{$cat->id}}" {{old('category_id') == $cat->id ? 'Selected' : ''}}>{{$cat->name}}</option>
                                    @empty
                                      <p>No Category List</p>
                                    @endforelse
                                    
                                    
                                  </select>
                                </div>
                                <div class="col-md-12 col-lg-6" style="display: grid;">
                                  <label for="name">Select Product *</label> 
                                  <select class="form-control product select2 required" multiple="multiple" name="product_id[]" data-placeholder="First Select Category" id="sel1">

                                  </select>
           
                                </div>      
                              </div>
                              <div class="row mx-auto parent_clone"> 
                                       
                              </div>

                            </section>
                            <h3>Black Out Dates</h3>
                            <section>
                              <div class="parent_clone_black_out_date"> 
                                
                                <div class="child_clone_black_out_dates"> 
                                  <div class="row mx-auto">
                                      <div class="col-md-12 col-lg-6">
                                        <label for="name">Black Out Date *</label> 
                                        <input type="date" name="black_out_dates[]" class="form-control required" aria-describedby="emailHelp" value="{{ old('black_out_dates') }}">
                                      </div>
                                        
                                      
                                      <div class="col-md-12 col-lg-3 add">
                                        <button type="button" name="add" id="addMoreBlackOutDateButton" class="btn btn-success">+
                                        </button>
                                      
                                      </div>
                                  </div>
                                
                                </div>
                              </br>
                            
                            </section>
                              
                          </div>
                        </form>
                      
                      </div>
                    </div>
                    <div id="chain" class="container tab-pane fade col-md-12"><br>
                      <form id="contact" action="{{ route('venues.store') }}" autocomplete="on" method="POST" enctype="multipart/form-data">
                        @CSRF
                        <div class="row">
                            
                            <div class="col-md-12 col-lg-4">
                              <label>Select PD Lead *</label>
                              <select class="form-control js-example-basic-single required" name="pd_lead_id">
                                <option value="">Select PD Lead</option>
                                @forelse($users as $user)
                                <option value="{{$user->id}}" {{old('user_id') == $user->id ? 'Selected' : ''}}>{{$user->name}}</option>
                                @empty
                                  <p>No User List</p>
                                @endforelse
                              </select>

                            </div>
                            <div class="col-md-12 col-lg-4">
                              
                              <label for="confirm">Chain Name *</label>
                              <input type="text" class="form-control"  name="chain_name"  id="venue_file" value="{{ old('chain_name') }}" placeholder="Enter Chain Name" required>
                            </div>
                            
                            <div class="col-md-12 col-lg-4">
                              <input type="hidden" name="venue_type" value="1">
                              <label for="confirm">Venue Upload *</label>
                              <input type="file" class="form-control"  name="venue_file"  id="venue_file" value="{{ old('venue_file') }}" accept=".xls,.xlsx" required>
                            </div>
                        </div>
                        
                        <div class="form-group download_sample">
                               <label for="name" class="mr-sm-2">Download Sample File</label><br>
                           <a href="{{url('assets/file/venue1.xlsx')}}" download><button class="btn btn-primary btn-sm"    type="button" id="dropdownMenuButton">Download Sample File</button></a>
                        </div>

                        <a href="#" style="margin-left:10px">
                          <button type="" class="btn btn-primary mt-4">Submit</button>
                        </a>
                      </form>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
