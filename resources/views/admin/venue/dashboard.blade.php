@extends('common.default')
@section('content')
<style>
 
 .icon i{
      font-size: 50px;
      color: #FFF;
      }
     
     .icon i:hover{
      font-size: 50px;
      color: #FFF;
      transition: .5s; 
      }
 
</style>
@php 
            $qc_user =  App\User::select('users.*')->where('users.status','1')->where('users.id',Auth::user()->id)->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('model_type', '=', 'App\\User')->whereIn('model_has_roles.role_id', ['4','5'])->first()
            @endphp
            
<!--
<div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="dahboard-title">Venues</h3>
                    
                </div>
                @if($_SERVER['REQUEST_URI'] != '/admin/dashboard' && !$qc_user)
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

-->
<section class="top__bar">
    <div class="container">
        <div class="top__inner">
            <div class="left__top">
                 <h3 class="dahboard-title">Venues</h3>
            </div>
             <div class="right__top">
                  @if($_SERVER['REQUEST_URI'] != '/admin/dashboard' && !$qc_user)
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary" data-toggle="click-ripple"><i class="fas fa-arrow-alt-circle-left"></i></a>
                         
                    </span>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>


<!-- For demo purpose -->
    <div class="container-fluid mbl_view">
        
        <section id="test-part">
          <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="row mt-5">
                        
                        @can('venue-list')
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary  pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/venues')}}">
                                             <p class="icon" > <i class="fas fa-handshake"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0   text-white">
                                          Venues</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endcan
                        @can('venue-qc-list')
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/venue-qc')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0   text-white">
                                          Venue QC</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endcan
                        @can('target-assign-list')
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/target-assign')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0   text-white">
                                          Target Assign</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endcan
                        <div class="col-6 col-sm-6 col-md-3">
                           <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                              <div class="mainflip">
                                 <div class="frontside">
                                    <div class="card  bg-gradient-x-primary pb-0">
                                       <div class="card-body text-center">
                                          <a href="{{url('admin/qc-rejected-venue')}}">
                                             <p class="icon" > <i class="fas fa-ticket-alt"></i></p>
                                          </a>
                                          <h3 class="h6 mb-0   text-white">
                                          QC Rejected Venue</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        
                        
                     </div>
                  </div>
               </div>
               
               </div>
               
               </section>
               
               
               
              <section class="venue__qc">
               
              <div class="container">
               <div class="d-sm-flex align-items-center justify-content-between">
                   <h1 class="h3  text-gray-800  font-weight-bolder h-100 px-1 text-capitalize underline-small">Venue QC</h1>
             
               </div>
               <div class="row mt-2 mb-5 mbl__view">
                  <div class="col-xl-3 col-sm-6">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue44">
                            
                            <h6 class="text-uppercase text-center venue-card">Total Venue</h6>
                            <h1 class="display-4 totalvenue">{{$totalVenue->count()}}</h1>
                              @can('venue-list')
                              <a href="{{url('admin/venues')}}" class="btn btn-primary btn-sm">Read More</a>
                              @endcan
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-xl-3 col-sm-6">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue33">
                          
                            <h6 class="text-uppercase text-center">Total Confirmed Venue</h6>
                            <h1 class="display-4 totalvenue">{{$totalConfirmed->count()}}</h1>
                              <!--<a href="#" class="btn btn-primary btn-sm">Read More</a>-->
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-xl-3 col-sm-6">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue22">
                            
                            <h6 class="text-uppercase text-center">Total Rejected Venue</h6>
                            <h1 class="display-4 totalvenue">{{$totalRejected->count()}}</h1>
                               <a href="{{url('admin/qc-rejected-venue')}}" class="btn btn-primary btn-sm">Read More</a>
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-xl-3 col-sm-6">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue11">
                            
                            
                            <h6 class="text-uppercase text-center">Total Venue QC</h6>
                            <h1 class="display-4 totalvenue">{{$totalVenueQC->count()}}</h1>
                            <a href="{{url('/admin/venue-qc')}}" class="btn btn-primary btn-sm">Read More</a>
                        </div>
                    </div>
                  </div>
                  <div class="col-xl-3 col-sm-6">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue11">
                            
                            
                            <h6 class="text-uppercase text-center">Total Request Edit Venue</h6>
                            <h1 class="display-4 totalvenue">{{$totalRequestEditVenue->count()}}</h1>
                           
                        </div>
                    </div>
                  </div>
                  <div class="col-xl-3 col-sm-6">
                    <div class="card text-white  h-100">
                        <div class="card-body card-totalvenue22">
                            
                            <h6 class="text-uppercase text-center">Total Target Assign</h6>
                            <h1 class="display-4 totalvenue">{{$totalTargetAssign->count()}}</h1>
                            @can('target-assign-list')
                               <a href="{{url('admin/target-assign')}}" class="btn btn-primary btn-sm">Read More</a>
                            @endcan
                        </div>
                    </div>
                  </div>
                  
                  
                  
               </div>
               </div>
               
              </section>
               
               
            </div>
@stop