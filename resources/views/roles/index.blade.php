@extends('common.default')
@section('title', 'Product Create')
@section('content')


<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      <!--
      <div class="d-sm-flex align-items-center justify-content-between pb-4">
         <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Role Permission Management</h1>
         <a href="{{url('admin/user/dashboard')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
      </div>
      -->
      
      
        <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Role Permission Management</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                         @can('role-create')
                 
                      <a href="{{ route('roles.create') }}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add Role Permission
                     </button></a>
                
                  @endcan
                        
                        <a  href="{{url('admin/user/dashboard')}}" class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fa fa-arrow-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

      <!-- Page Heading -->
      <!-- DataTales Example -->
      <div class="card shadow ">
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
               
                  <!-- <li style="margin-right: 20px;">
                     <a href="{{url('admin/product/download-product-excel')}}"><button class="btn btn-primary btn-sm" type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download Product
                     </button></a>
                  </li> -->
                  
               </ul>
            </nav>
         </div>
         @if(session()->get('success'))
         <div class="alert alert-success">
            {{ session()->get('success') }}  
         </div>
         @endif
         <div class="card-body" >
            <div class="table-responsive">

                <table class="table  table-striped able-bordered" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                    <thead>
                        <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                   
                     
                            <th>Sn.</th>
                            <th>Role</th>
                            <th>Created By</th>
                            <th>Created Date/Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{Auth::user()->name}}</td>
                        <td>{{$role->created_at->format('d M Y')}}/{{$role->created_at->format('g:i A')}}</td>
                        <td style="width: 200px;" class="">
                            <a  href="{{ route('roles.show',$role->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                            @can('role-edit')
                                <a  href="{{ route('roles.edit',$role->id) }}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                            @endcan
                            <form action="{{ route('roles.destroy', $role->id) }}" method="POST" id="delete-form-{{ $role->id }}" style="display: none;">
                              {{csrf_field()}}
                              {{ method_field('DELETE') }}
                              <input type="hidden" value="{{ $role->id }}" name="id">
                           </form>
                           @can('role-delete')
                           <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                              event.preventDefault(); document.getElementById('delete-form-{{ $role->id }}').submit();">
                           <span style="color: red;padding:5px;"  ><i class="fas fa-trash fa-primary fa-md"></i></span></a>
                           @endcan
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

