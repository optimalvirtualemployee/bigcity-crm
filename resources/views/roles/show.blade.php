@extends('common.default')
@section('title', 'Product Create')
@section('content')
<div id="content">
   <div class="container-fluid" style="margin-top: 20px;">
         <!--
         <div class="d-sm-flex align-items-center justify-content-between pb-4">
            <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Role Permission</h1>
             <a href="{{ route('roles.index') }}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
        </div>
        -->
    
    <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size: 20px;">Role Permission</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a  href="{{ route('roles.index') }}" class="btn btn-sm  btn-primary px-4 py-2 js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fa fa-arrow-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
  
             <table class="table  table-striped"   width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        
                        <th>Name</th>
                        <th>Permission</th>
                     </tr>
                  </thead>
                  <tbody>
                    <tr>
                       <div class="row">
                   <td>{{ $role->name }} </td>
                   <td>@if(!empty($rolePermissions))
                @foreach($rolePermissions as $v)
                    <label class="label label-success">{{ $v->name }},</label>
                @endforeach
            @endif</td>
                   </tr>
                  </tbody>
             </table>
    
   </div>
</div>




@stop