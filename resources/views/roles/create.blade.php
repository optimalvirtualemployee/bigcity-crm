@extends('common.default')
@section('title', 'Product Create')
@section('content')
<style>
label{
        text-transform: capitalize;
    font-weight: 600;
    
}
.select2-container {
   
    margin-top: 15px;
}

</style>
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
      <!--
      <div class="d-sm-flex align-items-center justify-content-between pb-4">
         <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize"></h1>
         <a href="{{ route('roles.index') }}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
      </div>
      -->
    <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size: 20px;">Add Role Permission</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a  href="{{ route('users.index') }}" class="btn btn-sm  btn-primary px-4 py-2 js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fa fa-arrow-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
   </div>

      <div class="container">
         <!-- Outer Row -->
         <div class="row justify-content-center mt-5">
            <div class="card login-card" style="width:600px">
               <div class="card-body p-0">
                  <!-- Nested Row within Card Body -->
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="p-5">
                           @if ($errors->any())
                           <div class="alert alert-danger">
                              <ul>
                                 @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                           <br />
                           @endif
                           <form action="{{ route("roles.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="title">Title*</label>
                            <input type="text" id="title" name="name" class="form-control" value="{{ old('title', isset($role) ? $role->title : '') }}">
                            @if($errors->has('title'))
                                <p class="help-block">
                                    {{ $errors->first('title') }}
                                </p>
                            @endif
                            
                        </div>
                        <div class="form-group {{ $errors->has('permissions') ? 'has-error' : '' }}">
                            <label for="permissions">Permission*
                                <span class="btn btn-primary btn-xs select-all">Select all</span>
                                <span class="btn btn-primary btn-xs deselect-all">Deselect all</span></label>
                                <br>
                            <select name="permissions[]" id="permissions" class="form-control select2" multiple="multiple" style="width:100% !important">
                                @foreach($permissions as $id => $permissions)
                                    <option value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) && $role->permissions->contains($id)) ? 'selected' : '' }}>
                                        {{ $permissions }}
                                    </option>
                                @endforeach
                            </select>
                            @if($errors->has('permissions'))
                                <p class="help-block">
                                    {{ $errors->first('permissions') }}
                                </p>
                            @endif
                            
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="Submit">
                        </div>
                    </form>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- End of Main Content -->
@stop