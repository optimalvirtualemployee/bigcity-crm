@extends('common.default')
@section('title', 'Change Password')
@section('content')
<div class="breadcrumb-holder">
   <div class="container-fluid">
      <ul class="breadcrumb">
         <li class="breadcrumb-item"><a href="index.html">Home</a></li>
         <li class="breadcrumb-item active">User</li>
      </ul>
   </div>
</div>
<div class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <h5 class="title">Update Password</h5>
            </div>
            <div id="page-inner">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="card-action">
                        @if (session('error'))
                        <div class="alert alert-danger">
                           {{ session('error') }}
                        </div>
                        @endif
                        @if (session('success'))
                 <div class="alert alert-success" id="alert" style="background-color: #1b3a5d;">
                           {{ session('success') }}
                        </div>
                        @endif
                     </div>
                     <form class="form-horizontal" id="updatepassfrm" method="POST" action="{{ url('/admin/user/changePassword') }}/{{$user['id']}}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                           <label for="new-password-confirm" class="col-md-4 control-label">Password <span class="required-field">*</span></label>
                           <div class="col-md-6">
                              <input id="current-password" type="password" class="form-control" name="current-password">
                              @if ($errors->has('current-password'))
                              <span class="help-block">
                              <strong>{{ $errors->first('current-password') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                           <label for="new-password-confirm" class="col-md-4 control-label">New Password <span class="required-field">*</span></label>
                           <div class="col-md-6">
                              <input id="new-password" type="password" class="form-control" name="new-password">
                              @if ($errors->has('new-password'))
                              <span class="help-block">
                              <strong>{{ $errors->first('new-password') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="new-password-confirm" class="col-md-4 control-label">Confirm New Password <span class="required-field">*</span></label>
                           <div class="col-md-6">
                              <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation">
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-md-6 col-md-offset-4">
                              <button type="submit" onclick="test();" class="btn btn-success">Change Password</button>
                              <button class="btn btn-info" onclick="window.history.go(-1); return false;">Cancel</button>
                           </div>
                        </div>
                     </form>
                     <div class="clearBoth"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<script type="text/javascript">
   $(document).ready(function(){			
     $('#updatepassfrm').on('submit', function (e) {
       e.preventDefault();              
       var noError         = true;
   	var password     			= $("#current-password").val();
   	var new_pass     			= $("#new-password").val();
   	var new_pass_confirm     	= $("#new-password-confirm").val();
   
       if(password == ""){
         $('#current-password').css('border','1px solid red');
         noError = false;               
       }
       if(new_pass == ""){
         $('#new-password').css('border','1px solid red');
         noError = false;               
       }
   	if(new_pass_confirm == ""){
   	$('#new-password-confirm').css('border','1px solid red');
   	noError = false;               
   	}		    
       if(noError){
         $('#updatepassfrm')[0].submit();   
       }
     });
   });
   
   // check match password
   // function checkPasswordMatch() {
   // var password = $("#txtpassword").val();
   // var confirmPassword = $("#txtconfirmpass").val();
   
   // if (password != confirmPassword)
   // $("#CheckPassdMatch").html("Passwords do not match!").css('color','red');
   // else
   // $("#CheckPassdMatch").html("Passwords match.").css('color','green');
   // }
   
   // $(document).ready(function () {
   // $("#txtpassword, #txtconfirmpass").keyup(checkPasswordMatch);
   // });
</script>
@stop