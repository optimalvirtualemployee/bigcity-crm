@extends('common.default')
@section('title', 'User Create')
@section('content')
<!-- Main Content -->
<div id="content">
   <!-- Begin Page Content -->
   <div class="container-fluid" style="margin-top:15px;">
      <!-- Page Heading -->
      <!-- DataTales Example -->
      <!--
      <div class="d-sm-flex align-items-center justify-content-between pb-4">
         <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize"></h1>
         <a href="{{url('admin/user/dashboard')}}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
      </div>
      -->
        <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Users Management</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        
                       <a href="{{ route('users.create') }}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add User
                     </button></a>
                     
                        <a href="{{ route('user.download-user-excel') }}"><button class="btn btn-primary btn-sm"     type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download User
                     </button></a>
                        
                        <a  href="{{url('admin/user/dashboard')}}" class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fa fa-arrow-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


      <div class="card ">
          <!--
         <div class="card-header d-none d-sm-block">
            <nav class="navbar navbar-expand">
               <ul class="navbar-nav ml-auto">
                  <li style="margin-right: 20px;">
                      <a href="{{ route('users.create') }}">  <button class="btn btn-danger btn-sm"    type="button" id="dropdownMenuButton">
                     <i class="fas fa-plus"></i> &nbsp; Add User
                     </button></a>
                  </li>
                  <li style="margin-right: 20px;">  
                  <a href="{{ route('user.download-user-excel') }}"><button class="btn btn-primary btn-sm"     type="button" id="dropdownMenuButton">
                     <i class="fas fa-file-import"></i> &nbsp; Download User
                     </button></a>
                  </li>
                  
               </ul>
            </nav>
         </div>
         -->
         <div class="card-body" >
            @if(session()->get('success'))
            <div class="alert alert-success">
               {{ session()->get('success') }}  
            </div>
            @endif
            <div class="table-responsive">
               <table class="table  table-striped" id="dataTable" width="100%" cellspacing="0" style="margin-top: 35px !important;">
                  <thead>
                     <tr style="text-transform: capitalize;color: #FFF;background-color: #d33f3e"  >
                        <th>SN</th>
                        <th>Id</th>
                        <th>User Name</th>
                        <th>User Email</th>
                        <th>Roles</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     @forelse($data as $key => $user)
                     <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        
                        <td>

                          @if(!empty($user->getRoleNames()))

                            @foreach($user->getRoleNames() as $v)

                               <label class="badge badge-success">{{ $v }}</label>

                            @endforeach

                          @endif

                        </td>
                        <td>{{isset($user->created_user['name']) ? $user->created_user['name'] : 'NA'}}</td>
                        <td>{{isset($user->updated_user['name']) ? $user->updated_user['name'] : 'NA'}}</td>
                        <td>{{$user->created_at->format('d M Y')}}/{{$user->created_at->format('g:i A')}}</td>
                        <td>{{$user->updated_at->format('d M Y')}}/{{$user->updated_at->format('g:i A')}}</td>
                        <td>
                           <a href="{{ route('users.show',$user->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
                           <a href="{{ route('users.edit',$user->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                           
                        <td>
                            
                              <input data-id="{{$user->id}}" class="toggle-class categoryStatus" type="checkbox" 
                              data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                              data-off="InActive" {{ $user->status ==1 ? 'checked' : '' }}>  
                              <input type="hidden" name="mdoelName" id="mdoelName" value="User">  
                        </td> 
                        </td>
                     </tr>
                     @empty
                     <p>No Category List</p>
                     @endforelse
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@stop