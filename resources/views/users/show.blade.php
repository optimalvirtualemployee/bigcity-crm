@extends('common.default')
@section('title', 'Product Create')
@section('content')

<div id="content">
   <div class="container-fluid" style="margin-top: 15px;">        
            <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;margin-bottom:15px">
            <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left" style="padding: 0px 20px;">
            <div class="flex-sm-fill">
            <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size:20px">Show User</h3>
            
            </div>
            <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
            <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
            <a  href="{{ route('users.index') }}" class="btn btn-primary btn-sm px-4  js-click-ripple-enabled" data-toggle="click-ripple" >
            <i class="fa fa-arrow-left mr-1"></i> Back
            </a>
            
            </span>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            
            
            <div class="row justify-content-center"style="margin-top:50px;">
            <div class="card login-card" style="width: 500px;">
            <div class="card-body p-0">
            
            
            <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12">
            
            <div class="form-group">
            
            <strong>Name:</strong>
            
            {{ $user->name }}
            
            </div>
            
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
            
            <div class="form-group">
            
            <strong>Email:</strong>
            
            {{ $user->email }}
            
            </div>
            
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
            
            <div class="form-group">
            
            <strong>Roles:</strong>
            
            @if(!empty($user->getRoleNames()))
            
            @foreach($user->getRoleNames() as $v)
            
            <label class="badge badge-success">{{ $v }}</label>
            
            @endforeach
            
            @endif
            
            </div>
            
            </div>
            
            </div>
            
            </div>
            </div>
            </div>
            </div>
            </div>

@stop