@extends('common.default')
@section('title', 'Product Create')
@section('content')
<!-- Main Content -->
<div id="content">
   <div class="container-fluid" style="margin-top:15px;">
      
      <!--<div class="d-sm-flex align-items-center justify-content-between pb-4">
         <h1 class="h3 mb-0 text-gray-800 border-left-primary  h-100 px-1 text-capitalize">Add user</h1>
         <a href="{{ route('users.index') }}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
      </div>
      -->
      
       <div class="bg-image overflow-hidden" style="background-color: #f5f5f5;">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="container  d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center   text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h3 class="font-w600   js-appear-enabled animated fadeIn" data-toggle="appear" style="color:#575757;font-weight: 600!important;font-size: 20px;">Add user</h3>
                    
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a  href="{{ route('users.index') }}" class="btn btn-sm  btn-primary px-4 py-2 js-click-ripple-enabled" data-toggle="click-ripple" >
                            <i class="fa fa-arrow-left mr-1"></i> Back
                        </a>
                         
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

      <div class="container">
         <!-- Outer Row -->
         <div class="row justify-content-center" style="margin-top:50px;">
            <div class="card login-card" style="width: 500px;">
               <div class="card-body p-0">
                  <!-- Nested Row within Card Body -->
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="p-5">
                           @if ($errors->any())
                           <div class="alert alert-danger">
                              <ul>
                                 @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                           <br />
                           @endif
                           <form class="user" action="{{ route('users.store') }}" method="POST">
                              @csrf
                              <div class="row">

                                <div class="col-xs-12 col-sm-12 col-md-12">

                                    <div class="form-group">

                                        <strong>Name:</strong>

                                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}

                                    </div>

                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">

                                    <div class="form-group">

                                        <strong>Email:</strong>

                                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                    </div>

                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">

                                    <div class="form-group">

                                        <strong>Password:</strong>

                                        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                                    </div>

                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">

                                    <div class="form-group">

                                        <strong>Confirm Password:</strong>

                                        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                                    </div>

                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">

                                    <div class="form-group">

                                        <strong>Role:</strong>

                                        {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}

                                    </div>

                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                                    <button type="submit" class="btn btn-primary">Submit</button>

                                </div>
                              
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@stop