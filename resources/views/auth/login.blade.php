

@section('title', 'Login')


<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>@yield('title')</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="robots" content="all,follow">
      
      <!-- Custom fonts for this template-->
      <link href="{{url('assets/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="{{url('assets/css/style.css')}}" rel="stylesheet">
  </head>
  <body style="background-color:#FFF;">
    <div class="container div-vh">
      <!-- Outer Row -->
      <div class="row justify-content-center">
        <div class="card login-card-panel my-5" style="width: 500px;">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="mbl__login">
                    <span class="login100-form-logo">
<img class="logo" src="http://crm.developersoptimal.com/assets/img/Bigcity_logo.png" alt="" style="margin-top: -5px;
    margin-left: 5px;
    width: 80%;">
</span>
                  <div class="text-center">
                    <h2 class="h3  mb-4 text-uppercase font-weight-bold login-title">CRM - Login</h2>
                  </div>
                  <form method="POST" action="{{ url('/logins') }}" class="user">
                  @csrf
                    
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{$error}}
                            </div>
                        @endforeach
                    @endif
                    <div class="form-group">
                      <label>Enter Email</label>
                      <input type="email" name="email" autocomplete="off" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder=""
                      value="{{ old('email') }}" autofocus>
                      <!--@if($message = Session::get('error'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @endif-->
                    </div>
                    <div class="form-group">
                       <label>Enter Password</label>
                      <input type="password" autocomplete="off" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-user" id="exampleInputPassword" placeholder="">
                      <!--@if ($errors->has('password'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif-->
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck" {{ old('remember') ? 'checked' : '' }}>Remember Me</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
                    
                      
                    </a>
                    
                     </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
   </body>
</html>
