@section('title', 'Otp')
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>@yield('title')</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="robots" content="all,follow">
      <!-- Bootstrap CSS-->
      <link rel="stylesheet" href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
      <!-- Font Awesome CSS-->
      <link rel="stylesheet" href="{{url('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
      <!-- Fontastic Custom icon font-->
      <link rel="stylesheet" href="{{url('assets/css/fontastic.css')}}">
      <!-- Google fonts - Roboto -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
      <!-- theme stylesheet-->
      <link rel="stylesheet" href="{{url('assets/css/style.default.css')}}" id="theme-stylesheet">
      <!-- Custom stylesheet - for your changes-->
      <link rel="stylesheet" href="{{url('assets/css/custom.css')}}">
      <link rel="stylesheet" href="{{url('assets/css/main.css')}}">
   </head>
   <body>
      <div class="page login-page forget-password-widget otp-wrap">
      <div class="container">
      <div class="form-outer  d-flex align-items-center flex-wrap">
         <div class="col-sm-12 col-md-12 col-lg-12 text-center">
            <div class=" login-tab">
                @if(Session::has('success_msg'))
                <p style="color:red;">{{ Session::get('success_msg') }}</p><br>
                @endif
               <form id="otpfrm" action="{{url('admin/checkOtp')}}" method="POST">
                  @csrf
                  <div class="form-group">
                     <input type="text" name="otp" class="form-control" placeholder="Enter OTP">
                  </div>
                  <div class="otp-button-flex">
                  <button type="button" onclick="otpbtn();" class="btn btn-primary bigcity-button">Submit</button>
                  <button type="button" id="resendotp" class="btn btn-primary bigcity-button otp-button">Resend</button>
               </div>
               </form>
            </div>
         </div>
      </div>
      <script>
    function otpbtn(){
       var isError = false;
       $('#otpfrm').find('input[type=text]').each(function(){
         $(this).parent().find('.error').html('');
         $(this).css('border','')
         if($(this).val() == ""){
         $(this).css('border','1px solid red');
           isError = true;
           return isError;
           } 
       });
	    if(isError == false){
	    $('#otpfrm').submit();
	  }
	}
	
    $(document).ready(function(){
    $('#resendotp').submit(function(){
    // Call ajax for resend otp
    $.ajax({
    type: 'POST',
    url: '{{url("admin/resendOtp")}}',
    data: $(this).serialize() // getting filed value in serialize form
    })
    });
    });
      </script>
      <!-- JavaScript files-->
      <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
      <script src="{{url('assets/vendor/popper.js/umd/popper.min.js')}}"> </script>
      <script src="{{url('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
      <!-- Main File-->
      <script src="{{url('assets/js/front.js')}}"></script>
   </body>
</html>