<!DOCTYPE html>
<html>
  <head>
   @include('common.head')
  </head>
  <body>
    <!-- Side Navbar -->
   
     @include('common.sidemenu')
    <div class="page">
      <!-- navbar-->
    <header class="header">
     @include('common.nav')
    </header>
     <!-- Counts Section --> 
     @yield('content')
     @include('common.footer')
    </body>
</html>