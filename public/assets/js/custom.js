

$(document).ready(function () {
$('.select-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', 'selected')
    $select2.trigger('change')
  })
  $('.deselect-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', '')
    $select2.trigger('change')
  })

  $('.select2').select2()
  
  
  // Select all check - uncheck event trigger ends here

  
  // Disable datatable default search filter starts here
  var pathArray = window.location.pathname.split('/');
  if(pathArray[2] == 'booking'){
    $('#dataTable_filter,#dataTable_length').remove();
  }
  // Disable datatable default search filter ends here

});

$('#clear').click(function(){
  $('#from_date').val('');
  $('#to_date').val('');
  $('#search').val('');
});
//-----------Start JS For Campaign Module Form Validation-------------------//
    function campaignbtn(){
      var isError = false;
      $('#campaignfrm')
      .find('input[name=campaign_name],input[name=campaign_estimate_no],textarea,select:not(.v_spec)')
      .each(function()
        {
          var palceholder =  $(this).attr('placeholder');
          // alert(palceholder);
          $(this).parent().find('.error').html('');
          $(this).css('border','')
          if($(this).val() == "" || $(this).val() == 0){
            $(this).css('border','1px solid red');
            isError = true;'';   
          }
        });
//--------------------------------------------------------------------//
      if($('#mt').val() == 1 || $('#mt').val() == 3){
        if($('input[name=sms_keyword]').val() == ""){
          $('input[name=sms_keyword]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=sms_keyword]').css('border','');
            isError = false;'';
        }

        if($('input[name=sms_long_code]').val() == ""){
          $('input[name=sms_long_code]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=sms_long_code]').css('border','');
            isError = false;'';
        }

        if($('input[name=sms_keyword]').val() == "" && $('input[name=sms_long_code]').val() == ""){
          $('input[name=sms_keyword]').css('border','1px solid red');
          $('input[name=sms_long_code]').css('border','1px solid red');
          isError = true;'';
        }else{
          $('input[name=sms_keyword]').css('border','');
          $('input[name=sms_long_code]').css('border','');
            isError = false;'';
        }

      }

      if($('#mt').val() == 2 || $('#mt').val() == 3){
        if($('input[name=web_microsite_name]').val() == ""){
          $('input[name=web_microsite_name]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=web_microsite_name]').css('border','');
            isError = false;'';
        }
      }
//---------------------------------------------------------------//
      if($('#vt').val() == 1 || $('#vt').val() == 3 ){
        if($('.v_spec').val() == 0){
          $('.v_spec').css('border','1px solid red');
            isError = true;'';
        }else{
          $('.v_spec').css('border','');
            isError = false;'';
        }
      }
//---------------------------------------------------------------//
      if($('select[name=is_printing_bigcity]').val() == 1){
        if($('input[name=campaign_voucher_cost]').val() == ""){
          $('input[name=campaign_voucher_cost]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=campaign_voucher_cost]').css('border','');
            isError = false;'';
        }

        if($('input[name=campaign_cost_to_client]').val() == ""){
          $('input[name=campaign_cost_to_client]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=campaign_cost_to_client]').css('border','');
            isError = false;'';
        }

        if($('input[name=campaign_cost_to_bigcity]').val() == ""){
          $('input[name=campaign_cost_to_bigcity]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=campaign_cost_to_bigcity]').css('border','');
            isError = false;'';
        }
      }
//---------------------------------------------------------------//     
      if(isError == false){
        $('#campaignfrm').submit();
      }
    }

    function mt_function(id){
      if(id == 1){
        $('#sms').show();
        $('#web').hide();
      }else if(id == 2){
        $('#web').show();
        $('#sms').hide();
      }else if(id == 3){
        $('#sms').show();
        $('#web').show();
      }else{
        $('#sms').hide();
        $('#web').hide();
      }
    };

    function vt_function(id){
      if(id == 1 || id == 3){
        $('#voucher_spec').show();
      }else{
        $('#voucher_spec').hide();
      }
    };

    function printing(id){
      if(id == 1){
        $('#is_printing_done_by_bigcity_yes_show').show();
      }else{
        $('#is_printing_done_by_bigcity_yes_show').hide();
      }
    };
//-----------End JS For Campaign Module Form Validation-------------------//

//-----------Start JS For Campaign Module-------------------//
window.onload = function() {
   var mechanic_type_id = $('#mt').val();
   var voucher_type_id = $('#vt').val();
   var is_printing_bigcity = $('#is_printing_bigcity').val();
   

     if(mechanic_type_id == 1){
       $('#sms').show();
       $('#web').hide();
     }else if(mechanic_type_id == 2){
       $('#web').show();
       $('#sms').hide();
     }else if(mechanic_type_id == 3){
       $('#sms').show();
       $('#web').show();
     }else{
       $('#sms').hide();
       $('#web').hide();
     }

     if(voucher_type_id == 1 || voucher_type_id == 3){
       $('#voucher_spec').show();
     }else{
       $('#voucher_spec').hide();
     }

     if(is_printing_bigcity == 1){
      $('#is_printing_done_by_bigcity_yes_show').show();
    }else{
      $('#is_printing_done_by_bigcity_yes_show').hide();
    }
    

}
//-----------End JS For Campaign Input Value-------------------//

//-----------Start JS For Remove Alert Message-------------------//
setTimeout(function(){ $(".alert-danger").remove();

$(".alert-success").remove() }, 6000);
//-----------End JS For Remove Alert Message-------------------//

//-----------Start JS For File Upload Preview-------------------//
$(function () {
   
    $("#exampleFormControlFile1").change(function () {

     
        $("#dvPreview").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (regex.test($(this).val().toLowerCase())) {
            if ($.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                $("#dvPreview").show();
                $("#dvPreview")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
            }
            else {
                if (typeof (FileReader) != "undefined") {
                    $("#dvPreview").show();
                    $("#dvPreview").append("<img />");
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#dvPreview img").attr("src", e.target.result);
                    }
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            }
        } else {
            alert("Please upload a valid image file.");
            return false;
        }
    });
});
//-----------End JS For File Upload Preview-------------------//

//-----------End JS For Venue Module Form Validation-------------------//
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
    function venuebtn(){

        var isError = false;
        var emailReg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        
        if($("#venue_type").val() == 2){
            
                
                $('#venuefrm').find('textarea,input:not(.latFeild),select').each(function(){
                      var palceholder =  $(this).attr('placeholder');
                        
                      // alert(palceholder);
                      $(this).parent().find('.error').html('');
                      $(this).css('border','')
                      if($(this).val() == "" || $(this).val() == 0){
                          
                          $(this).css('border','1px solid red');
                          isError = true;'';   
                      }
                      
                      
                      
                  });
               /* if($('#rating').val() == 0){
                  $('#rating').css('border','1px solid red');
                    isError = true;'';
                }else{
        
                  $('#rating').css('border','');
                    isError = false;'';
                }*/
                if($('input[name=latitude]').val() == ""){
                  $('input[name=latitude]').css('border','1px solid red');
                  
                    isError = true;'';
                    
                }else{
        
                  $('input[name=latitude]').css('border','');
                  
                    isError = false;'';
                }
                if($('input[name=longitude]').val() == ""){
                  $('input[name=longitude]').css('border','1px solid red');
                  
                    isError = true;'';
                }else{
        
                  $('input[name=longitude]').css('border','');
                  
                    isError = false;'';
                }
                if($('.billing_phone').val() == ""){

                  $('.billing_phone').css('border','1px solid red');
                  
                    isError = true;'';
                    
                }else if(!$.isNumeric($('.billing_phone').val())){
                    
                    $('#venuePhoneBillingErrorMsg').text('The Phone field is not valid.');
                  
                    isError = true;'';
                }else{
        
                  $('.billing_phone').css('border','');
                  $('#venuePhoneBillingErrorMsg').text('');
                    isError = false;'';
                }
                if($('.booking_phone').val() == ""){

                  $('.booking_phone').css('border','1px solid red');
                  
                    isError = true;'';
                    
                }else if(!$.isNumeric($('.booking_phone').val())){
                    
                    $('#venuePhoneBookingErrorMsg').text('The Phone field is not valid.');
                  
                    isError = true;'';
                }else{
        
                  $('.booking_phone').css('border','');
                  $('#venuePhoneBookingErrorMsg').text('');
                    isError = false;'';
                }
                if($('.billing_email').val() == ""){

                  $('.billing_email').css('border','1px solid red');
                  
                    isError = true;'';
                    
                }else if(!emailReg.test($('.billing_email').val())){
                    
                    $('#venueEmailBillingErrorMsg').text('The Email field is not valid.');
                  
                    isError = true;'';
                }else{
                  
                  $('.billing_email').css('border','');
                  $('#venueEmailBillingErrorMsg').text('');
                    isError = false;'';

                }if($('.booking_email').val() == ""){

                  $('.booking_email').css('border','1px solid red');
                  
                    isError = true;'';
                    
                }else if(!emailReg.test($('.booking_email').val())){
                    
                    $('#venueEmailBookingErrorMsg').text('The Email field is not valid.');
                  
                    isError = true;'';
                }else{
                  
                  $('.booking_email').css('border','');
                  $('#venueEmailBillingErrorMsg').text('');
                    isError = false;'';
                }
                /*if($('input[type=checkbox]').val() == ""){
                  $('#venueListTypeErrorMsg').text('List type field is required.');
                  
                    isError = true;'';
                }else{
        
                  $('input[type=checkbox]').text('');
                    isError = false;'';
                }*/
                
            
            }
            if($("#venue_type").val() == 1){
            
                if($('input[name=venue_file]').val() == ""){
                  $('#venueFileErrorMsg').text('The file field is required.');
                  $('input[name=venue_file]').css('border','1px solid red');
                    isError = true;'';
                }else{
                  $('#venueFileErrorMsg').text('');
                  $('input[name=venue_file]').css('border','');
                    isError = false;'';
                }
                if($('input[name=chain_name]').val() == ""){
                  
                  $('input[name=chain_name]').css('border','1px solid red');
                    isError = true;'';
                }else{
                 
                  $('input[name=chain_name]').css('border','');
                    isError = false;'';
                }
                if($('#rating').val() == 0){
                  $('#rating').css('border','1px solid red');
                    isError = true;'';
                }else{
        
                  $('#rating').css('border','');
                    isError = false;'';
                }
                if($('#send_QC').val() == 0){
                  $('#send_QC').css('border','1px solid red');
                    isError = true;'';
                }else{
        
                  $('#send_QC').css('border','');
                    isError = false;'';
                }
            
            }
        /*if($("#venue_type").val() == 2){*/
            
          
        /*}*/
        
        if(isError == false){
            alert('are you sure you want to save venue.!!');
            $('#venuefrm').submit();
        }
    }
//-----------End JS For Venue Module Form Validation-------------------//

//-----------Strat JS For Venue Type On Change Value-------------------//
    $("#venue_type").change(function(){

        if($(this).val()  == '1'){

          $(".for-standalone").hide();
          /*$(".rating").hide();*/
          $(".venueUpload").show();
          /*$(".list_type_wrap").hide();*/
        }else{
          $(".for-standalone").show();
          /*$(".list_type_wrap").show();*/
          /*$(".rating").show();*/
          $(".venueUpload").hide();
        }
        
      }).change();
      $("#black_out_day").change(function(){

        if($(this).val()  == '2'){

          $(".day_name").show();
          
        }else{
            
          $(".day_name").hide();
        }
        
      }).change();
      $("#billing_terms").change(function(){

        if($(this).val()  == '1' || $(this).val() == '2'){

          $(".bank_detail").show();
          $(".other_value").hide();
          
        }else if($(this).val()  == '4'){

          $(".bank_detail").hide();
          $(".other_value").show();
          
        }else{
            
          $(".bank_detail").hide();
          $(".other_value").hide();
        }
        
      }).change();
      
//-----------End JS For Venue Type On Change Value-------------------//
//-----------Start JS For Product Rules ----------------------------//
      
      $(document).ready(function(){
        
        var count = 1;
        dynamic_field(count); // Div Append for very first time
        
        function dynamic_field(number){
          var html = '<tr id="trow1'+count+'">';
              html += '<td><div class="row mx-auto"><input type="text" class="form-control col-md-11 col-lg-10 prn" id="td'+count+'"  placeholder="Enter Product Rule Name" name="product_rule_name[]" value="" autocomplete="off"></div></td></tr>';
              html += '<tr id="trow2'+count+'">';
              html += '<td>';
              html += '<table>';
              html += '<tr>';
              html += '<td><label>Start Date</label><div class="input-group date datetimepicker1"><input type="text" class="form-control datetimepicker1 startdate" placeholder="yyyy-mm-dd 00:00" name="start_date[]" id="start_date'+count+'" value="" autocomplete="off" /><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></td>';
              html += '<td><label >End Date</label><div class="input-group date datetimepicker1"><input type="text" class="form-control datetimepicker1 enddate" placeholder="yyyy-mm-dd 00:00" name="end_date[]" id="end_date'+count+'" value="" autocomplete="off"/><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></td>';
              html += '</tr></table><hr></td></tr>';

          if(number > 1){
            var bttn = '<button type="button" name="remove" class="btn btn-danger col-md-1 col-lg-1 offset-md-1 offset-lg-1 remove">x</button>';
              if(number == 2){ 
                  $('#tr2').after(html); 
              }else{ 
                  $('#trow2'+(count-1)).after(html); 
              }
              $('#td'+count).after(bttn);
              $('.datetimepicker1').datetimepicker({dateFormat: "yy-mm-dd HH:ii:ss",datepicker:true,timpicker:true});
          }else{
            var bttn = '<button type="button" name="add" id="add" class="btn btn-success col-md-1 col-lg-1 col-lg-1 offset-md-1 offset-lg-1">+</button>'; 
             $('#tr1').after(html);
             $('#td'+count).after(bttn);
          }
    }

    $('#add').click(function(){ // plus button click to add new div
      count++;
      dynamic_field(count);
    });

    $(document).on('click', '.remove', function(){ // to remove newly added div
      $('#trow1'+count).remove();
      $('#trow2'+count).remove();
      count--;
    });

    $('.datetimepicker1').datetimepicker({dateFormat: "yy-mm-dd HH:ii:ss",datepicker:true,timpicker:true});
  });
  
 // Create page submit button action 
  function productrule_submitbtn(){
    var isError = false;
    $('#productrulefrm').find('input[type=text],select').each(function()
    {
      $(this).css('border','');
      if($(this).val() == "" || $(this).val() == 0){
        $(this).css('border','1px solid red');
        isError = true;'';   
      }
    });
   
     
    // Text box for rule name must be unique, checking duplicate values for rule name    
    var arr = [];
    $(".prn").each(function(){ 
      var value = $(this).val();
      if (arr.indexOf(value) == -1 || arr == ""){ // to duplicate check for rule name
        arr.push(value);
      }else{
        $('#error_msg').addClass('alert alert-danger').html('Rule names cannot be duplicate').show();
        $(this).css('border','1px solid red');
        isError= true;
      }
    });


    // Validating date range 
    var startdate = [];
    var enddate = [];
    var i  = 0;
    var j = 0;  
    $(".startdate").each(function(){
      var sd = $(this).attr('id');
      startdate[i++] = $('#'+sd).val();
    });
       
    $(".enddate").each(function(){
      var ed = $(this).attr('id');
      enddate[j++] = $('#'+ed).val();
    });

    if(startdate.length == enddate.length) 
    { 
      for(var i=0;i<startdate.length;i++){ 
        if (startdate[i] > enddate[i]) {
          $('#error_msg').addClass('alert alert-danger').html('Start Date cannot be greater than Ending date!').show();
          $('#start_date'+(i+1)).css('border','1px solid red');
          $('#end_date'+(i+1)).css('border','1px solid red');
          isError = true;
        } 
      }    
    } 
        
    if(isError == false){
      $('#productrulefrm').submit();
    }
  }

// Ajax call on product onchange from dropdown to check rule exists or not.
  function changingProductforCreate(id){
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "/admin/changingProduct",
      data: {'id': id},
      success: function(data){
        //console.log(data.success);
        if(data.success){
          $('#error_msg').addClass('alert alert-danger').html(data.success).show();
          $('.submit').attr('disabled',true).css('cursor','no-drop');
          $('#add').attr('disabled',true).css('cursor','no-drop');
          $('.remove').attr('disabled',true).css('cursor','no-drop');
          $('.datetimepicker1').attr('disabled',true).css('cursor','no-drop');
          $('input[type=text]').attr('disabled',true).css('cursor','no-drop');
        }
        if(data.failure){
          $('#error_msg').hide();
          $('.submit').attr('disabled',false).css('cursor','');
          $('#add').attr('disabled',false).css('cursor','');
          $('.remove').attr('disabled',false).css('cursor','');
          $('.datetimepicker1').attr('disabled',false).css('cursor','');
          $('input[type=text]').attr('disabled',false).css('cursor','');
        }
      }
    });
   }
      
//-----------End JS For Product Rules ----------------------------//

$(document).ready(function(){
    $('#campaignMapped_productList').append('<label id="selectLabel">Select Product</label><select class="form-control select_class select_box select2" name="product_id"><option value="0">First Select Campaign</option></select>');
    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".parent_clone"); //Fields wrapper
    var add_button      = $("#add"); //Add button ID    
    var x = 1; //initlal text box count
    var y = '{{ $counter }}';
    $(add_button).click(function(e) { //on add input button click
      e.preventDefault();
      if(x < max_fields) { //max input box allowed
        x++; //text box increment
        y++;
        $(wrapper).append('<div class="child_clone"><div class="row mx-auto "><input type="text" class="form-control col-md-12 col-lg-10 prn"  placeholder="Enter Product Rule Name" name="product_rule_name[]" value=""><button type="button" name="remove" class="btn btn-danger col-md-1 col-lg-1 col-lg-1 offset-md-1 offset-lg-1 remove">x</button></div><div class="form-group row "><div class="form-group col-md-12 col-lg-6"><label>Start Date</label><div class="input-group date datetimepicker1"><input type="text" class="form-control datetimepicker1 startdate" placeholder="mm/dd/yy 00:00 AM" name="start_date[]" id="start_date'+y+'" value="" autocomplete="off"/><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></div><div class="form-group col-md-12 col-lg-6"><label>End Date</label><div class="input-group date datetimepicker1"><input type="text" class="form-control datetimepicker1 enddate" placeholder="mm/dd/yy 00:00 AM" name="end_date[]" value="" id="end_date'+y+'" autocomplete="off"/><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></div></div></div>'); //add input box
      }
      $('.datetimepicker1').datetimepicker({dateFormat: "yy-mm-dd HH:ii:ss",datepicker:true,timpicker:true});
    });    
    $(wrapper).on("click",".remove", function(e){ //user click on remove text
      e.preventDefault(); 
      $(this).parent().parent('.child_clone').remove();
      x--;
    });


    $('.datetimepicker1').datetimepicker({dateFormat: "Y-m-d H:i:s",datepicker:true,timpicker:true});

  });
function changingProductforEdit(id){
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/admin/changingProduct",
    data: {'id': id},
    success: function(data){
      console.log(data.success);
      if(data.success){
        $('#error_msg').addClass('alert alert-danger').html(data.success).fadeIn('slow');
        setTimeout(function(){ location.reload(true); },1000);
      }
      if(data.failure){
        $('#error_msg').hide();
      }
    }
  });
}
//---------------------------- Generate Codes JS Starts Here ------------------------------------//
   function get_genproductList_fn(campaign_id){
  
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/admin/get_genproductList",
    data: {'campaign_id': campaign_id},
    success: function(data){
        
      
      if(data.length > 0){
        $('.select_box').remove();
        $('#selectLabel').remove();
        $('#campaignMapped_productList').append('<label id="selectLabel">Select Product</label><select class="form-control select_class select_box select2" name="product_id"><option value="0">Select Product</option></select>');
       
        $.each(data,function(key, value)
        {
             
          $.ajax({
            type: "GET",
            dataType: "json",
            url: "/admin/get_genproductName",
            beforeSend: function(){
                    $('#loading').show();

                },
            data: {'id': value['product_id'],'campaign_id': value['campaign_id']},
            success: function(response){
              //console.log(response[0]['id']);
              for(var i =0;i < response.length;i++){
                 
                 $('.select_box').append('<option value=' + response[i]['id'] + '>' + response[i]['name'] + '</option>'); // return empty
              };
            },
            complete: function(){
                    $('#loading').hide();

                }
          })  
        });
      }else{
        
        $('.select_box').remove();
        $('#selectLabel').remove();
        $('#campaignMapped_productList').append('<label id="selectLabel">Select Product</label><select class="form-control select_class select_box select2" name="product_id"><option value="0">Data not found</option></select>');
      } 
      
    } 
    
  });
}
  /*function gc_btn(){
  
    var isError = false;
    $('#generatecodefrm').find('input[type=text],select').each(function()
    {
      $(this).css('border','');
      if($(this).val() == "" || $(this).val() == 0){
        $(this).css('border','1px solid red');
        isError = true;'';   
      }
    });
     */
   /* if(isError == false){*/
      /*$(function(){*/
          /*$('#generatecodefrm').submit(function() {
            $('#loading').show(); 
            return true;
          });*/
       /* });*/
   /* }*/
  
//   }

  

  $(document).ready(function(){
      
    $('.datetimepicker2').datetimepicker({timepicker:false,dateFormat: "yy-mm-dd"});
  });

//---------------------------- Generate Codes JS Ends Here   ------------------------------------//

//---------------------------- City Module JS Starts Here   ------------------------------------//
$('.addCity').on('change', function() {
    
    var val = this.checked ? this.value : '';
    if(val){

      $('#addCityForm').show();
     
      $('#downloadCityForm').css('display','none');
      $('#addUploadForm').css('display','none');
    }
      
    
    }).change();
    $('.uploadCity').on('change', function() {

    var val = this.checked ? this.value : '';
    if(val){
      $('#addUploadForm').css('display','block');
      $('#addCityForm').hide();
      $('#downloadCityForm').css('display','none');
    }
    
    
    
    }).change();
    $('.downloadCity').on('change', function() {

    var val = this.checked ? this.value : '';
    if(val){
        $('#addCityForm').hide();
      $('#addUploadForm').css('display','none');
      
      $('#downloadCityForm').css('display','block');
    }
    
    
    
    }).change();
    function uploadCityBtn(){
  
    var isError = false;
    $('#addUploadForm').find('input[type=file],select').each(function()
    {
      $(this).css('border','');
      $('#imageErrorMsg').text('');
      if($(this).val() == "" || $(this).val() == 0){
        $(this).css('border','1px solid red');
        $('#imageErrorMsg').text('The file field is required.');
        isError = true;'';   
      }
    });

     
    if(isError == false){
      $('#addUploadForm').submit();
    }
  
  }
  function downloadCityBtn(){
  
    var isError = false;
    $('#downloadCityForm').find('select').each(function()
    {
      
      $(this).css('border','');
      if($(this).val() == "" || $(this).val() == 0){
        
        $(this).css('border','1px solid red');
        isError = true;'';   
      }
    });

     
    if(isError == false){
      /*$("a").attr("href", "http://crm.developersoptimal.com/admin/city/download-city-excel")*/
      $('#downloadCityForm').submit();
    }
  
  }
  
//---------------------------- City Module JS Ends Here   ------------------------------------//

//---------------------------- Area Module JS Starts Here   ------------------------------------//
  $('.addArea').on('change', function() {
    
    var val = this.checked ? this.value : '';
    if(val){

      $('#addAreaForm').show();
      $('#uploadAreaForm').css('display','none');
    }
      
    
    }).change();
    $('.uploadArea').on('change', function() {

    var val = this.checked ? this.value : '';
    if(val){
      $('#uploadAreaForm').css('display','block');
      $('#addAreaForm').hide();
    }
    
    
    
    }).change();
    function uploadAreaBtn(){
  
    var isError = false;
    $('#uploadAreaForm').find('input[type=file],select').each(function()
    {
      $(this).css('border','');
      $('#imageErrorMsg').text('');
      if($(this).val() == "" || $(this).val() == 0){
        $(this).css('border','1px solid red');
        $('#imageErrorMsg').text('The file field is required.');
        isError = true;'';   
      }
    });

     
    if(isError == false){
      $('#uploadAreaForm').submit();
    }
  
  }
//---------------------------- Area Module JS Ends Here   ------------------------------------//
//---------------------------- Campaign Details JS Starts Here ----------------------------------//

   /*function promo_type(id){ // adding an input field on select multi-vaucher option.
    $('#child_div').remove();
    $('.vouchers').remove();
    if(id == 2){
      $('#parent_div').after('<div id="child_div" class="col-md-6"><label for="" class="mr-sm-2">No of product</label><input class="form-control" id="select_grid" name="no_of_products"></div>')
    }else{
      $('#child_div').remove();
    }

    var maxVal = 15;
    var select = $('<select>');
    select.append($('<option selected ></option >').val("0").html("Enter No of Products"));

    for (var i = 1; i <= maxVal; i++) {
        select.append($('<option ></option>').val(i).html(i));
    }

    $('#select_grid').append(select.html());
  }*/

  //------------------

  function product_count(id){ // adding multiple inputs based on value passed in myTextbox
    $('.vouchers').remove();
    for(var i = 0; i < parseInt(id); i++){
      $('#child_div').after('<div class="form-group vouchers"><input type="text" name="voucher_names[]" class="form-control voucher_input" placeholder="Enter Voucher Name"/></div>');
    }
  }

//--------------------

  /*function product_fn(id){// for product rule ajax on behalf of product id
    if(id != 0 ){
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "/admin/get_ProductRuleList",
      data: {'id': id},
      success: function(data){
        console.log(data);
        if(data.length > 0){
          $('.select_class').remove();
          $('#error_msg').hide();
          $('#product_rule').append('<label for="" class="mr-sm-2" id="selectLabel">Select Product Rule</label><select id="select_box" class="form-control select_class" name="product_rule_id"><option value="0">Select Product Rules</option></select>');
          $.each(data,function(key, value)
            {
              $('#select_box').append('<option value=' + value['id'] + '>' + value['title'] + '</option>'); // return empty
            });
        }else{
          $('#select_box').remove();
          $('#selectLabel').remove();
          $('#error_msg').addClass('alert alert-danger').html('No Rules defined for this product').show();
        }
    }
  })
  }else{
    $('#selectLabel').remove();
    $('#select_box').remove();
    $('#product_rule_msg').hide();
  }
}*/


  /*function cd_btn(){
  
    var isError = false;
    $('#campaignDetail_frm').find('input:not(.latFeild),select').each(function()
    {
      $(this).css('border','');
      if($(this).val() == "" || $(this).val() == 0){
        $(this).css('border','1px solid red');
        isError = true;'';   
      }
    });


    var arr = [];
    $(".voucher_input").each(function(){ 
      var value = $(this).val();
      if (arr.indexOf(value) == -1 || arr == ""){ // to duplicate check for rule name
        arr.push(value);
      }else{
        $('#error_msg').addClass('alert alert-danger').html('Names cannot be duplicate !').show();
        $(this).css('border','1px solid red');
        isError= true;
      }
    });
     
    if(isError == false){
      $('#campaignDetail_frm').submit();
    }
  
  }*/



//---------------------------- Campaign Details JS Ends Here ------------------------------------//

//---------------------------- Venue module JS Start Here ------------------------------------//
$(document).ready(function() {
    var max_fields = 100; //maximum input boxes allowed
    
    var billing_wrapper = $(".parent_clone_billing"); //Fields wrapper
    var booking_wrapper = $(".parent_clone_booking"); //Fields wrapper
    var black_out_date_wrapper = $(".parent_clone_black_out_date"); //Fields wrapper
    var add_button_booking = $("#addMoreBookingButton"); //Add button ID
    var add_button_billing = $("#addMoreBillingButton"); //Add button ID    
    var add_button_black_out_date = $("#addMoreBlackOutDateButton"); //Add button ID    
    
    var x = 1; //initlal text box count
    
    $(wrapper).on("click", ".removeProduct", function(e) { //user click on remove text
    
        e.preventDefault();
        $(this).parent().parent().parent('.child_clone').remove();
        x--;
    });
    $(add_button_billing).click(function(e) { //on add input button click

    e.preventDefault();
    if (x < max_fields) { //max input box allowed
        x++; //text box increment

        $(billing_wrapper).append('<div class="child_clone_billing"><div class="row mx-auto"><div class="col-md-12 col-lg-3"><input type="text" name="billing_name[]" class="form-control  billing_name" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Name" value=""></div><div class="col-md-12 col-lg-3"><input type="email" name="billing_email[]" class="form-control   billing_email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email" value=""></div><div class="col-md-12 col-lg-2"><input type="tel" name="billing_phone[]" class="form-control   billing_phone" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter phone" value=""></div><div class="col-md-12 col-lg-3"><input type="text" name="billing_designation[]" class="form-control   billing_designation" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Designation"></div><span class="add ml-2"><button type="button" name="remove" class="btn btn-danger removeBilling"><i class="fa fa-minus" aria-hidden="true"></i></button ></span></div></div>');
        //add input box
    }

    });
    $(billing_wrapper).on("click", ".removeBilling", function(e) { //user click on remove text
    
        e.preventDefault();
        $(this).parent().parent().parent('.child_clone_billing').remove();
        x--;
    });
    $(add_button_booking).click(function(e) { //on add input button click

    e.preventDefault();
    if (x < max_fields) { //max input box allowed
        x++; //text box increment

        $(booking_wrapper).append('<div class="child_clone_booking"><div class="row mx-auto"><div class="col-md-12 col-lg-3"><input type="text" name="booking_name[]" class="form-control  booking_name" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Name" value=""></div><div class="col-md-12 col-lg-3"><input type="email" name="booking_email[]" class="form-control   booking_email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email" value=""></div><div class="col-md-12 col-lg-2"><input type="tel" name="booking_phone[]" class="form-control   booking_phone" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter phone" value=""></div><div class="col-md-12 col-lg-3"><input type="text" name="booking_designation[]" class="form-control   billing_designation" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Designation"></div><div class="col-md-12 col-lg-1"><button type="button" name="remove" class="btn btn-danger  removeBooking"><i class="fa fa-minus" aria-hidden="true"></i></button ></div></div></div>');
        //add input box
    }

    });
    $(booking_wrapper).on("click", ".removeBooking", function(e) { //user click on remove text
    
        e.preventDefault();
        $(this).parent().parent().parent('.child_clone_booking').remove();
        x--;
    });
    $(add_button_black_out_date).click(function(e) { //on add input button click

    e.preventDefault();
    if (x < max_fields) { //max input box allowed
        x++; //text box increment

        $(black_out_date_wrapper).append('<div class="child_clone_black_out_dates"><div class="row mx-auto" style="padding-top: 10px;"><div class="col-md-12 col-lg-6"><input type="date" name="black_out_dates[]" class="form-control  booking_name" id="exampleInputEmail" aria-describedby="emailHelp" value=""></div><div class="col-md-12 col-lg-3"><button type="button" name="remove" class="btn btn-danger removeBlackOutDate">x</button ></div></div></div>');
        //add input box
    }

    });
    $(black_out_date_wrapper).on("click", ".removeBlackOutDate", function(e) { //user click on remove text
    
        e.preventDefault();
        $(this).parent().parent().parent('.child_clone_black_out_dates').remove();
        x--;
    });
    

});
//---------------------------- Tag Campaign Venue JS Starts Here --------------------------------//

function get_productList_fn(campaign_id){
  
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/admin/get_productList",
    data: {'campaign_id': campaign_id},
    success: function(data){
        
      
      if(data.length > 0){
        $('.select_box').remove();
       $('#selectLabel').remove();
        $('#campaignMapped_productList').append('<label id="selectLabel">Select Product</label><select class="form-control select_class select_box select2" name="product_id"><option value="0">Select Product</option></select>');
       
        $.each(data,function(key, value)
        {
             
          $.ajax({
            type: "GET",
            dataType: "json",
            url: "/admin/get_productName",
            beforeSend: function(){
                    $('#loading').show();

                },
            data: {'id': value['product_id'],'campaign_id': value['campaign_id']},
            success: function(response){
              //console.log(response[0]['id']);
              for(var i =0;i < response.length;i++){
                 
                 $('.select_box').append('<option value=' + response[i]['id'] + '>' + response[i]['name'] + '</option>'); // return empty
              };
            },
            complete: function(){
                    $('#loading').hide();

                }
          })  
        });
      }else{
        $('.select_box').remove();
        $('#selectLabel').remove();
      } 
      
    } 
    
  });
}


 $(document).ready(function() {
    $('#lstview').multiselect();
  });

 $('button').click(function(){

    var btnID = $(this).attr('id');
  
    if(btnID == 'lstview_rightAll'){
      var values = $('#lstview option').map(function() 
        { return $(this).val(); }).get();
      venue_ajax(values);
  
    }else if(btnID == 'lstview_rightSelected'){
      var values = $('#lstview').val();
      venue_ajax(values);
  
    }else if(btnID == 'lstview_leftSelected'){
      var values = $('#lstview_to').val();
      $('option[data-id='+ values+']').remove();
      // $('#checkbox_list').has('tbody:empty').hide();
    }else if(btnID == 'lstview_leftAll'){
      $('#checkbox_list_wrap').hide();
      $('.list_tr').remove();
    }
 })

 function venue_ajax(values){

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/admin/cityTag_venueList",
        beforeSend: function(){
            $('#loading').show();

        },
        data: {'id': values},
        success: function(data){
          if(data){
            $('#checkbox_list_wrap').show(); 
            $.each(data,function(key,value){
                var venue_checked = [];
                $('.venue_checked').each(function(i,j) {
                    venue_checked[i] = parseInt($(this).val());
                });
                condition = ( $.inArray(value.id, venue_checked) != -1 ? "Selected=Selected" : "" );// check if venue exists select the checkbox 
                $('#checkbox_list').append('<option  class="list_tr" data-id = '+value['city_id']+' name="venue_id[]" value="'+value['id']+','+value['city_id']+'" '+condition+'>'+value['venue_name']+'</option>');
            });
          }  
        },
        complete: function(){
            $('#loading').hide();

        }
      });

 }


 $("#ckbCheckAll").click(function () {
     alert('ssa');
    $(".checkBoxClass").prop('checked', $(this).prop('checked'));
 });
 $(document).ready(function(){
    $('.hide').remove();// hide all unwanted values of checklist from "selected cities"

    
    $.each($('.venue_checklist'),function(i,j){ // getting all venues according to value of column "selected cities" 
      if($(this).attr("type")=="hidden"){
        var valueOfHidFiled = $(this).val();
        var values = [valueOfHidFiled];
        venue_ajax(values);
      }
    });

  });
//---------------------------- Tag Campaign Venue JS Ends Here ----------------------------------//
//---------------------------- Voucher Specification JS Starts Here ----------------------------------//

  function voucher_type(id){//voucher type onchange event
    
    var url = window.location.pathname;

    var edit_page = url.split('/')[4];

    if(edit_page == 'edit'){
    
    $.ajax({// check value first if exists, will work on update case only. (only for edit page)
        type: "POST",
        dataType: "json",
        url: "/admin/getVoucher_specification",
        beforeSend: function(){
            $('#loading').show();

        },
        
        data: {"_token": $('meta[name="csrf-token"]').attr('content'),'id': id},
        success: function(data){
          if(data){
          var size_name   = (data['size_name'] ? data['size_name'] : "");
          var length      = (data['length'] ? data['length'] : "");
          var breadth     = (data['breadth'] ? data['breadth'] : "");
          var thickness   = (data['thickness'] ? data['thickness'] : "");
          

          var physical_elements  = '<div class="physical_elements"><div class="form-group"><label for="name" class="mr-sm-2">Size Name</label>';
              physical_elements += '<input type="text" class="form-control" id="size_name" placeholder="Enter Size Name" name="size_name" value="'+size_name+'" maxlength="10">'; 
              physical_elements += '</div><div class="form-group"><label for="name" class="mr-sm-2">Length</label><input type="text" id="length" class="form-control" placeholder="Enter Length" name="length" value="'+length+'" maxlength="10"></div>';
              physical_elements += '<div class="form-group"><label for="name" class="mr-sm-2">Breath</label><input type="text" id="breadth" class="form-control" placeholder="Enter Breadth" name="breadth" value="'+breadth+'" maxlength="10">';
              physical_elements += '</div><div class="form-group"><label for="name" class="mr-sm-2">Thickness</label><input type="text" id="thickness" class="form-control" placeholder="Enter Thickness" name="thickness" value="'+thickness+'" maxlength="10"></div></div>';
          
          var E_voucher  = '<div class="form-group e_voucher_elements"><label for="name" class="mr-sm-2">Select E-Voucher</label>';
              E_voucher += '<select class="form-control" name="evoucher_id" style="height: 45px;">';
              E_voucher += '<option value="0">Select E-Voucher Type</option>';
              E_voucher += '<option value="1" '+(data['evoucher_id'] == 1 ? "selected" : "")+'>SMS</option>';
              E_voucher += '<option value="2" '+(data['evoucher_id'] == 2 ? "selected" : "")+'>E-mail</option>';
              E_voucher += '</select></div>';
          

          if(id == 1 || id == 3){// if physical / sticker 
            $('.physical_elements').remove();
            $('#physical').append(physical_elements);
          } 

        //-------------------------

          if(id == 4 ){// if E-voucher 
            $('.physical_elements').remove();
            $('.e_voucher_elements').remove();
            $('#E_voucher').append(E_voucher);
          }else{  
            $('.e_voucher_elements').remove();
          }

          if(id == 2){// if Physical + E_voucher
            if ($('#physical').length > 0){
              $('.physical_elements').remove();
              $('.e_voucher_elements').remove();
              $('#physical').append(physical_elements);
              $('#E_voucher').append(E_voucher);
            }else{
              $('#E_voucher').append(E_voucher);
            }
          }
          var arr = ['1','2','3','4'];
          if(($.inArray(id,arr)) == -1){ // if none of the above option is in dropdown
              $('.physical_elements').remove();
              $('.e_voucher_elements').remove();
          }
        }
      },
        complete: function(){
            $('#loading').hide();

        }
        
      });
    }else{

      var physical_elements  = '<div class="physical_elements"><div class="form-group"><label for="name" class="mr-sm-2">Size Name</label>';
        physical_elements += '<input type="text" class="form-control" id="size_name" placeholder="Enter Size Name" name="size_name" value="" maxlength="10">'; 
        physical_elements += '</div><div class="form-group"><label for="name" class="mr-sm-2">Length</label><input type="text" id="length" class="form-control" placeholder="Enter Length" name="length" value="" maxlength="10"></div>';
        physical_elements += '<div class="form-group"><label for="name" class="mr-sm-2">Breadth</label><input type="text" id="breadth" class="form-control" placeholder="Enter Breadth" name="breadth" value="" maxlength="10">';
        physical_elements += '</div><div class="form-group"><label for="name" class="mr-sm-2">Thickness</label><input type="text" id="thickness" class="form-control" placeholder="Enter Thickness" name="thickness" value="" maxlength="10"></div></div>';
    
    var E_voucher  = '<div class="form-group e_voucher_elements"><label for="name" class="mr-sm-2">Select E-voucher</label>';
        E_voucher += '<select class="form-control" name="evoucher_id" style="height: 45px;">';
        E_voucher += '<option value="0">Select E-Voucher Type</option>';
        E_voucher += '<option value="1">SMS</option>';
        E_voucher += '<option value="2">E-mail</option>';
        E_voucher += '</select></div>';
    

    if(id == 1 || id == 3){// if physical / sticke 
      $('.physical_elements').remove();
      $('#physical').append(physical_elements);
    } 

  //-------------------------

    if(id == 4 ){// if E-voucher 
      $('.physical_elements').remove();
      $('.e_voucher_elements').remove();
      $('#E_voucher').append(E_voucher);
    }else{  
      $('.e_voucher_elements').remove();
    }

    if(id == 2){// if Physical + E_voucher
      if ($('#physical').length > 0){
        $('.physical_elements').remove();
        $('.e_voucher_elements').remove();
        $('#physical').append(physical_elements);
        $('#E_voucher').append(E_voucher);
      }else{
        $('#E_voucher').append(E_voucher);
      }
    }

      var arr = ['1','2','3','4'];
      if(($.inArray(id,arr)) == -1){// if none of the above option is in dropdown
          $('.physical_elements').remove();
          $('.e_voucher_elements').remove();
      }
  }

}

//-------------------------------------------------------------

  function submit_voucher(){// form submittion
    var isError = false;
    $('#frmvoucher_spec').find('input[type=text],select').each(function()//checking blank values
    {
      $(this).css('border','');
      if($(this).val() == "" || $(this).val() == 0){// checks for empty value or value == 0
        $(this).css('border','1px solid red');// sets border colour to red
        isError = true;   
      }
    });

    var length = $('#length').val();
   /* if(length){
    if( ( /^[0-9]*$/.test(length) == false ) && length != ""){
        $('#length').css('border','1px solid red');
        $('#error_msg').addClass('alert alert-danger').html('Invalid Input').fadeOut(8000);
        isError = true;
    }}

    var breadth = $('#breadth').val();
    if(breadth){
    if( /^[0-9]*$/.test(breadth) == false){
        $('#breadth').css('border','1px solid red');
        $('#error_msg').addClass('alert alert-danger').html('Invalid Input').fadeOut(8000);
        isError = true;
    }}

    var thickness = $('#thickness').val();
    if(thickness){
      if( /^[0-9]*$/.test(thickness) == false){
        $('#thickness').css('border','1px solid red');
        $('#error_msg').addClass('alert alert-danger').html('Invalid Input').fadeOut(8000);
        isError = true;
      }
    }
*/
    /*var str = $('#size_name').val();
    if(str){
    if(/^[a-zA-Z ]*$/.test(str) == false) {
        $('#size_name').css('border','1px solid red');
        $('#error_msg2').addClass('alert alert-danger').html('Invalid Input').fadeOut(8000);
        isError = true;
    }}*/

    if(isError == false){ // submitting form if all checks are clear
      $('#frmvoucher_spec').submit();
    }
  }
 


//---------------------------- Voucher Specification JS Ends Here -----------------------------------//



$("#gift_tax_required").change(function(){

        if($(this).val()  == 'YES'){

          $(".gift_tax_amount").show();
          
          
        }else{
          $(".gift_tax_amount").hide();
          
        }
        
}).change();
      
$("#gc_codes_type").change(function(){

        if($(this).val()  == '1'){

          $(".length_of_code_required").show();
          $(".num_letter").hide();
          $('option[value="4"]').hide();
          $('option[value="5"]').hide();
          $('option[value="6"]').hide();
          $('option[value="7"]').hide();
          $('option[value="8"]').hide();
          
          
          
        }else if($(this).val()  == '2'){
            
          $(".length_of_code_required").show();
          $(".num_letter").show();
          $('option[value="4"]').show();
          $('option[value="5"]').show();
          $('option[value="6"]').show();
          $('option[value="7"]').show();
          $('option[value="8"]').show();
          
        }else {
            $(".length_of_code_required").hide();
            $(".num_letter").hide();
        }
        
      }).change();
      $("#whole_batch").change(function(){

        if($(this).val()  == 'NO'){

          $(".voucher_code").show();
          
          
        }else {
            $(".voucher_code").hide();
            
        }
        
      }).change();
    $(document).ready(function(){
        $(".batch_name").find('input').attr('readonly', 'readonly');
        $('#product_readonly').prop("disabled", true); 
        $('#camp_readonly').prop("disabled", true); 
       
     });
     
//--------------------- Inbound Module popup window code -----------------------//

function popup(){ // Pop up wizard action here.
  
  var voucher_code = $('#voucher_code').val(); // get voucher code in to a variable.
  var mobile_no    = $('#mobile_no').val(); // get mobile no in to a variable.
  var view_button  = $('#view_btn').attr('data-id',voucher_code);

  $('.error').remove();

    if(voucher_code == '' && mobile_no == ''){ // checks if voucher code and mobile no fields is blank.
      $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>Voucher Code & Mobile no fields are required</b></div>').show();
    }else if(voucher_code != '' && mobile_no == ''){ //checks if only mobile no field is blank.
      $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>Mobile No field is required</b></div>').show();
    }else if(voucher_code == '' && mobile_no != '' ){ // checks if only voucher code field is blank.
      if(mobile_no.length < 10 ){ // checks mobile no length is not less than 10 digits.
        $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>Voucher code field is required and Mob no must be 10 digits</b></div>').show(); 
      }else{
        $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>Voucher code field is required</b></div>').show();
      }
    }else if(voucher_code != '' && mobile_no != ''){ // checks here if voucher code and mobile no is given than pop up display.
    
      if(mobile_no.length < 10 ){
          $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>Mob no must be 10 digits</b></div>').show(); 
      }else{
        $.ajax({
          type: "POST",
          dataType: "json",
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          url: '/admin/inbound_module/popup',
          data:{ 'voucher_code':voucher_code, 'mobile_no':mobile_no },
          beforeSend: function(){
            $('#loading').show();
          },
          success: function(data){
            // console.log(data);
            $('#tbl_div').remove();
            $('.custQuery').remove();
            $('.cq_div').remove();
            if(data.data == 1){ // checks, if voucher code is valid
              // console.log(data);
              var $Updated_date = date_value_format(data.get_voucher_code.gc_offer_valid_till_date);
              
              // Pop up window content.
              var modal_body = '<table id="tbl_div" class="table  table-striped table-bordered bg-gradient-light" width="100%" cellspacing="0">';
                  /*modal_body +=  '<tr><th>Microsite Name : </th><td><strong>Microsite Type 1</strong></td></tr>';*/
                  
                  modal_body +=  '<tr><th>Compaign Name : </th><td>'+data.campaign_name+'</td></tr>';
                  modal_body +=  '<tr><th>Offer Name : </th><td>'+data.product_name+'</td></tr>';
                  modal_body +=  '<tr><th>Voucher Code : </th><td><strong>'+data.get_voucher_code.voucher_code+'</strong></td></tr>';
                  modal_body +=  '<tr><th>Mobile No : </th><td>'+mobile_no+'</td></tr>';
                  modal_body +=  '<tr><th>Voucher Code Validity : </th><td>'+Updated_date+'</td></tr>';
                  modal_body +=  '<tr><th>Voucher Status : </th><td>'+data.VoucherStatus+'</td></tr>';

                  if(data.VoucherStatus == 'Booked'){

                    modal_body +=  '<tr><th>Voucher Registation Date & Time: </th><td>'+date_value_format(data.get_booking_data.prefereddate1)+'&nbsp'+time_value_format(data.get_booking_data.preferedTime1)+'</td></tr>';
                    
                    modal_body +=  '<tr><th>Voucher Registation Email Id : </th><td>'+data.get_booking_data.customer_email_id+'</td></tr>'; 
                    modal_body +=  '<tr><th>Voucher Registation Mobile number : </th><td>'+data.get_booking_data.customer_mob_no+'</td></tr>';
                    modal_body +=  '<tr><th>Booking Done Date & Time : </th><td>'+date_value_format(data.get_booking_data.created_on)+'&nbsp'+time_value_format(data.get_booking_data.created_on)+'</td></tr>'; 

                  }
                  modal_body +='</table>';
                  modal_body += '<div class="form-group custQuery">';
                  modal_body +=    '<label for="custQuery" class="mr-sm-2">Select Customer Query</label>';
                  modal_body +=    '<select id="custQuery" name="custQuery" class="inputbox form-control bg-gradient-light text-dark" onchange="checkCustomerQuery(this.value,'+data.get_voucher_code.voucher_code+','+mobile_no+',\''+data.product_name+'\');">';
                  modal_body +=       '<option value="0">Select Customer Query</option>';
                                      $.each(data.CustomerQuery,function(index,value){
                                        modal_body += '<option value="'+value.id+'">'+value.name+'</option>';  
                                      });
                  modal_body +=    '</select>';
                  modal_body +=  '</div>';
                  modal_body +=  '<div class="cq_div"></div>';



              if(mobile_no.length < 10 ){
                $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>Mob no must be 10 digits</b></div>').show(); 
              }else{
                $('#myModal').modal({ backdrop: 'static', keyboard: false });
                $('#myModal').find('.modal-body').append(modal_body)
                $('#myModal').modal('show');
              }
            /*}else if(data.data == 4){ // checks, if voucher code is not valid.
              $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>This Voucher Code is not in booking</b></div>').show();
            */
            }/*else if(data.data == 2){ // checks, if voucher code is not valid.
              $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>Invalid Voucher Code and Mobile No</b></div>').show();
            }*/else if(data.data == 3){ // checks, if voucher code is not valid.
              $('.error_msg').append('<div class="alert alert-danger text-gradient-success error"><b>Invalid Voucher Code </b></div>').show();
            }
          },
          complete: function(){
            $('#loading').hide();
          },
        });
      }
    }
  }

  // ============================================================================================== // 

  function checkCustomerQuery(id,voucher_code,mobile_no,product_name){ // customer query dropdown onchange event.
    
    $('#register').remove();                  // remove div for unable to register / booking div if appended already.
    $('.cancelBooking').remove();             // remove cancle booking div if already exists.
    $('#booking_detail').remove();            // remove booking details div on every event onchange if already present.
    $('#contest_related').remove();           // remove contest related div if already exists. 
    $('.microsite_issue').remove();           // remove microsite issue div if already exists.
    $('#validity_related').remove();          // remove validity related div if already exists.
    $('.venue_related_issue').remove();       // remove venue related div if already exists.
    $('#voucher_offer_code_related').remove();// remove vocuher / offer code related div if already exists.

    if(id == 1){ // Cancel Booking Event fired here...

      $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details', // get booking details for send voucher code
        data: {'voucher_code':voucher_code },
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          if(data.d.status == '1' || data.d.status == '2'){ // 1 => confirmed, 2 => pending
            var booking_date   = date_value_format(data.d.prefereddate1);// Date format like : 01-Sept-2020 
            var booking_time   = time_value_format(data.d.preferedTime1);// Time format like : 5:29PM 
            var booking_venue  = data.d1;// Booking Venue
            var button_name    = 'Cancel Booking'; 
            var btn_id         = 'cancel_booking';
            var btn_class      = 'class="btn btn-danger btn-sm col-md-2 offset-md-5 bg-gradient-danger text-white"';
            
            // dynamic div append with booking details against provided voucher code.
            var booking_detail = booking_details(product_name,booking_date,booking_time,booking_venue,btn_id,btn_class,button_name);
          
            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');

            $('#cancel_booking').on('click', function(){
              $('#confirm').modal('show');
              // var cnfrm =  confirm('Are you sure, want to cancel booking!');
              $('#cancel_yes').click(function(){// check confirmation before processing request for cancellation
                $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: '/admin/inbound_module/cancel_booking',// Cancel booking ajax request
                  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                  data: {'id':3,'voucher_code':voucher_code,'mobile_no':mobile_no},
                  beforeSend: function(){
                    $('#booking_detail').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
                  },
                  success: function(data){
                    if(data.data != '0'){
                      $('.cancelBooking').remove();
                      $('#booking_detail').html('booking cancelled');
                    }else if(data.data == '0'){
                      $('.cancelBooking').remove();
                      $('#booking_detail').addClass('alert alert-danger text-gradient-danger').html('booking cannot be cancelled due to request time out');
                    }
                  },
                });
              })
            });
          }else if(data.d == 'Not Booked'){

            var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
                booking_detail +=   'No booking for this voucher';
                booking_detail += '</div>';
            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');

            
          }else{

            var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
                booking_detail +=   'No booking request open for cancellation';
                booking_detail += '</div>';

            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');
          }
        }
      });
    }else if(id == 2){ // Re-Scheduling Event fired here... 

      $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details',
        data: {'voucher_code':voucher_code},
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          if(data.d.prefereddate1){
            var booking_date   = date_value_format(data.d.prefereddate1);// Date format like : 01-Sept-2020   
          }

          if(data.d.preferedTime1){
            var booking_time   = time_value_format(data.d.preferedTime1);// Time format like : 5:29PM   
          }
            
          if(data.d.status == '1' || data.d.status == '2'){

            var booking_venue  = data.d1;// Booking Venue
            var button_name    = 'Rescheduled';
            var btn_id         = 'can_reschedule';
            var btn_class      = 'class="btn btn-info btn-sm col-md-2 offset-md-5 bg-gradient-info text-white"';
            var booking_detail = booking_details(product_name,booking_date,booking_time,booking_venue,btn_id,btn_class,button_name);
      
            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');
            $('#can_reschedule').on('click', function(){
            $('#can_reschedule_form').remove();
            $.ajax({
              type: "GET",
              dataType: "json",
              url:"/admin/inbound_module/check_request",
              data:
                {
                  'voucher_code':voucher_code,
                  'mobile_no':mobile_no,
                  'title': 'Booking Rescheduled'
                },
                beforeSend: function(){
                  $('.cancelBooking').addClass('alert alert-success').html('Please Wait...');
                },
                success: function(response){
                  if(response.data == 0){
                    $('.cancelBooking').remove();
                    $('#booking_detail')
                    .removeClass('bg-gradient-light')
                    .addClass('alert alert-danger text-gradient-danger text-center font-weight-bold')
                    .html('Cannot process for reschedule, as the request time out.');
                  }else if(response.data == 1){
                    
                    var reschedule_booking  = '<div style="margin-top:0px!important;display:none;" id="can_reschedule_form">';
                        reschedule_booking +=    '<div class="row">';
                        reschedule_booking +=       '<div class="col-md-6">';
                        reschedule_booking +=          '<label>Booking Date :</label>';
                        reschedule_booking +=          '<input type="date" step="1" class="form-control" name="txtDate" id="date" value="'+((data.d.booking_time).split(' ')[0])+'" required>';
                        reschedule_booking +=       '</div>';
                        reschedule_booking +=       '<div class="col-md-6">';
                        reschedule_booking +=          '<label>Booking Time :</label>';
                        reschedule_booking +=          '<input type="time" class="form-control" id="time" value="'+((data.d.booking_time).split(' ')[1])+'" required>';
                        reschedule_booking +=       '</div>';
                        reschedule_booking +=   '</div><br>';  
                        reschedule_booking +=   '<div class="row pl-3">';
                        reschedule_booking +=       '<button type="button" class="btn btn-sm btn-primary bg-gradient-primary text-white" id="save_date_time">Save</button>&nbsp;&nbsp;';
                        reschedule_booking +=       '<button type="button" class="btn btn-sm btn-danger bg-gradient-danger text-white" id="cancel_date_time">Cancel</button>';
                        reschedule_booking +=   '</div>'
                        reschedule_booking += '</div>';

                    $('.cancelBooking').removeClass('alert alert-success text-center font-weight-bold').html('').append(reschedule_booking);
                    $('.cancelBooking').find('#can_reschedule_form:last').slideDown("fast");
                    
                    // Restrict date from old date to be visible in calander and set minimum date as today
                    var today = new Date().toISOString().split('T')[0];
                    document.getElementsByName('txtDate')[0].setAttribute('min', today);

                    // Cancel button click to remove booking detail box
                    $('#cancel_date_time').click(function(){
                      $('.cancelBooking').slideUp("slow", function() { $('.cancelBooking').remove();});
                    });
                    
                    $('#save_date_time').click(function(){
                      var date = $('#date').val();
                      var time = $('#time').val();
                      if(date != ''&& time != ''){
                        $.ajax({
                          type: "POST",
                          dataType: "json",
                          url: '/admin/inbound_module/can_rescheduled',
                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                          data: {'voucher_code':voucher_code,'date':date,'time':time,'email':response.email_id,'mobile_no':mobile_no},
                          beforeSend: function(){
                            $('#booking_detail')
                            .removeClass('bg-gradient-light')
                            .addClass('alert alert-success text-gradient-success')
                            .html('Please Wait...');
                          },
                          success : function(data){
                            console.log(data);
                            $('#booking_detail').html(data.message);
                          }
                        });
                      }else{
                        $('#date').css('border','1px solid red');
                        $('#time').css('border','1px solid red');
                      }
                    });
                  }
                }
              });
            });
          }else if(data.d == 'Not Booked'){

            var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
                booking_detail +=   'No booking for this voucher';
                booking_detail += '</div>';
            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');

            
          }else{

            var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
                booking_detail +=   'No booking request open for Reschedule';
                booking_detail += '</div>';

            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');
          }
        }
      });
    }else if(id == 3){ // Booking Confirmation not recieved
      
        $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details',
        data: {'voucher_code':voucher_code},
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          if(data.d.prefereddate1){
            var booking_date   = date_value_format(data.d.prefereddate1);// Date format like : 01-Sept-2020   
          }

          if(data.d.preferedTime1){
            var booking_time   = time_value_format(data.d.preferedTime1);// Time format like : 5:29PM   
          }
          
          if(data.d.status == '1' || data.d.status == '2'){

            var booking_venue  = data.d1;// Booking Venue
            var button_name    = 'Booking Confirmation';
            var btn_id         = 'booking_confirmation';
            var btn_class      = 'class="btn btn-primary btn-sm col-md-3 offset-md-5 bg-gradient-success text-white"';
            var booking_detail = booking_details(product_name,booking_date,booking_time,booking_venue,btn_id,btn_class,button_name);

            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');
            $('#booking_confirmation').click(function(){
              $.ajax({
                type: "GET",
                dataType: "json",
                url:"/admin/inbound_module/check_request",
                data:{'voucher_code':voucher_code,'mobile_no':mobile_no,'title': 'Booking Confirmation'},
                beforeSend: function(){
                    $('#booking_detail').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success')
                    .html('Please Wait...');
                  },
                success : function(response){
                  if(response.data == 0){
                    $('.cancelBooking').remove();
                    
                    $('#booking_detail').addClass('alert alert-danger text-gradient-danger text-center font-weight-bold')
                    .html('Booking confirmation can\'t be sent , as the request time out.');
                  
                  }else if(response.data == 1){
                    var booking_date = date_value_format(response.booking_time);
                    var booking_time = time_value_format(response.booking_time); 
                    
                    $.ajax({
                      type: "POST",
                      dataType: "json",
                      url: '/admin/inbound_module/booking_confirmation',
                      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                      data: 
                      {
                        'voucher_code':voucher_code,
                        'email':response.email_id,
                        'mobile_no':mobile_no,
                        'booking_date':booking_date,
                        'booking_time':booking_time 
                      },
                      beforeSend: function(){
                        $('#booking_detail').html('Please Wait...');
                      },
                      success : function(data){
                        $('#booking_detail').html(data.message);
                      }
                    });
                  }
                }
              });
            })  
          }else if(data.d == 'Not Booked'){

            var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
                booking_detail +=   'No booking for this voucher';
                booking_detail += '</div>';
            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');

            
          }else{

            var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
                booking_detail +=   'Booking confirmation can\'t be sent, as request is already cancelled';
                booking_detail += '</div>';

            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');
          }
        }
      }); 
       
    }else if(id == 4){ // ( Not Active from backend )
    }else if(id == 5){ // Venue Related Issue

      $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details',
        data: {'voucher_code':voucher_code},
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          console.log(data);
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          if(data.d != 'Not Booked'){
          
           var venue_output  = '<div class="row border p-3 venue_related_issue bg-gradient-light m-1 rounded" style="display:none;" class="bg-gradient-light">';
          venue_output +=   '<label>Message :</label>';
          venue_output +=   '<textarea class="form-control" id="venue_related_issue_message" placeholder="Enter message here..." rows="3"></textarea>';
          venue_output +=   '<input type="button" class="btn btn-primary btn-sm mt-2 bg-gradient-primary text-white" id="send_email_venue_related_issue" value="Send Email">&nbsp;&nbsp;';
          venue_output +=   '<button type="button" class="btn btn-sm btn-primary btn-sm mt-2 bg-gradient-danger text-white" id="cancel_venue_related_issue">Cancel</button>';
          venue_output += '</div>';
          
      $('.cq_div').append(venue_output);
      $('.cq_div').find('.venue_related_issue:last').slideDown('fast');

      // Cancel button click to remove booking detail box
      $('#cancel_venue_related_issue').click(function(){
        $('.venue_related_issue').slideUp("slow", function() { $('.venue_related_issue').remove();});
      });

      $('#send_email_venue_related_issue').click(function(){
        var venue_related_issue_message = $('#venue_related_issue_message').val();
        if(!venue_related_issue_message){
          $('#venue_related_issue_message').css('border','1px solid red');
        }else{
          $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/inbound_module/send_email_venue_related_issue',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {'message':venue_related_issue_message,'voucher_code':voucher_code,'mobile_no':mobile_no},
            beforeSend: function(){
              $('.venue_related_issue')
              .removeClass('bg-gradient-light row border p-3')
              .addClass('alert alert-success text-gradient-success')
              .css('margin','1px')
              .html('Please Wait... <span id="timer"></span>');

              var myVar = setInterval(myTimer, 1000);
              timer = 0; function myTimer() { timer = ++timer; $("#timer").html(timer); }
            },
            success : function(data){
              console.log(data);
              
              if(data.data == 1){
              
                $('.venue_related_issue')
                .addClass('alert alert-success text-gradient-success')
                .css('margin','1px')
                .html(data.message);
              
              }else if(data.data == 0){
                
                $('.venue_related_issue')
                .addClass('alert alert-danger text-gradient-danger')
                .css('margin','1px')
                .html(data.message);
              }
            }
          });
        }
      });
    }else if(data.d == 'Not Booked'){
      var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
      booking_detail +=   'No booking for this voucher';
      booking_detail += '</div>';
      $('.cq_div').append(booking_detail);
      $('.cq_div').find('#booking_detail:last').slideDown('fast');
    }else{
      var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
      booking_detail +=   'Booking confirmation can\'t be sent, as request is already cancelled';
      booking_detail += '</div>';

      $('.cq_div').append(booking_detail);
      $('.cq_div').find('#booking_detail:last').slideDown('fast');
    }
  }
});   

    }else if(id == 6){ // ( Not Active from backend )
    }else if(id == 7){ // OTP Not Received (Put On Hold)
    }else if(id == 8){ // Unable to register / Booking
      
      $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details',
        data: {'voucher_code':voucher_code},
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          console.log(data);
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          if(data.d != 'Not Booked'){
            var voucher_status = 1;
          }else{
            var voucher_status = 0;
          }
          $('#register').remove();
          $('.cq_div').append('<div style="margin-top:0px!important;display:none;" id="register" class="bg-gradient-light p-2"><label>Put Your Remarks Here:</label>&nbsp;&nbsp;<textarea class="form-control" rows="3" placeholder="Enter remarks here..." id="remarks" required></textarea><br><button type="button" class="btn btn-sm btn-primary bg-gradient-primary text-white" id="save_remarks">Save</button>&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger bg-gradient-danger text-white" id="cancel_remark">Cancel</button></div>');
          $('.cq_div').find('#register:last').slideDown('fast');
                  
          // Cancel button click to remove booking detail box
          $('#cancel_remark').click(function(){
            $('#register').slideUp("slow", function() { $('#register').remove();});
          });
              
        $('#save_remarks').click(function(){
          var remarks = $('#remarks').val();
          if(remarks != ''){
            $.ajax({
              type: "POST",
              dataType: "json",
              url: '/admin/inbound_module/unable_to_register',
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
              data: {'query_id':id,'voucher_code':voucher_code,'mobile_no':mobile_no,'remarks':remarks,'voucher_status':voucher_status},
              success : function(data){
                $('#register').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html(data.message);
              }
            });
          }else{
            $('#remarks').css('border','1px solid red');
          }
        });
        }
      })  
      
    }else if(id == 9){ // Microsite Issue

      $('.cq_div').append('<div class="row border p-3 microsite_issue bg-gradient-light" style="display:none;"><label>Message :</label><textarea rows="3" class="form-control" id="microsite_issue_message" placeholder="Enter message here..."></textarea><input type="button" class="btn btn-primary btn-sm mt-2 bg-gradient-primary text-white" id="send_email_microsite_issue" value="Send Email">&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger btn-sm mt-2 bg-gradient-danger text-white" id="cancel_microsite_issue">Cancel</button></div>');
      $('.cq_div').find('.microsite_issue:last').slideDown('fast');

      // Cancel button click to remove booking detail box
      $('#cancel_microsite_issue').click(function(){
        $('.microsite_issue').slideUp("slow", function() { $('.microsite_issue').remove();});
      });

      $('#send_email_microsite_issue').click(function(){
        var microsite_issue_message = $('#microsite_issue_message').val();
        if(!microsite_issue_message){
          $('#microsite_issue_message').css('border','1px solid red');
        }else{
          $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/inbound_module/send_email_microsite_issue',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {'message':microsite_issue_message},
            beforeSend: function(){
              $('.microsite_issue').removeClass('row p-3 border bg-gradient-light').addClass('alert alert-success text-gradient-success').css('margin','1px').html('Please Wait...');
            },
            success : function(data){
              console.log(data);
              if(data.data == 1){
                $('.microsite_issue').html(data.message);
              }else if(data.data == 0){
                $('.microsite_issue').removeClass('row p-3 border bg-gradient-light').addClass('alert alert-danger text-gradient-danger').html(data.message);
              }
            }
          });
        }
      });

    }else if(id == 10){ // Validity Related
      $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details',
        data: {'voucher_code':voucher_code},
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          console.log(data);
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          if(data.d != 'Not Booked'){
            var voucher_status = 1;
          }else{
            var voucher_status = 0;
          }
          
          $('#validity_related').remove();
          $('.cq_div').append('<div style="margin-top:0px!important;display:none;" id="validity_related" class="bg-gradient-light p-2"><label>Put Your Remarks Here:</label>&nbsp;&nbsp;<textarea class="form-control" rows="3" placeholder="Enter remarks here..." id="remarks" required></textarea><br><button type="button" class="btn btn-sm btn-primary bg-gradient-primary text-white" id="save_remarks">Save</button>&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger bg-gradient-danger text-white" id="cancel_remark">Cancel</button></div>');
          $('.cq_div').find('#validity_related:last').slideDown('fast');
                  
            // Cancel button click to remove booking detail box
            $('#cancel_remark').click(function(){
              $('#validity_related').slideUp("slow", function() { $('#validity_related').remove();});
            });
                  
            $('#save_remarks').click(function(){
              var remarks = $('#remarks').val();
              if(remarks != ''){
                $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: '/admin/inbound_module/validity_related',
                  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                  data: {'query_id':id,'voucher_code':voucher_code,'mobile_no':mobile_no,'remarks':remarks,'voucher_status':voucher_status},
                  success : function(data){
                    $('#validity_related').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html(data.message);
                  }
                });
              }else{
                $('#remarks').css('border','1px solid red');
              }
            });
          }
        });    

    }else if(id == 11){ // Contest Related

      $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details',
        data: {'voucher_code':voucher_code},
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          console.log(data);
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          if(data.d != 'Not Booked'){
            var voucher_status = 1;
          }else{
            var voucher_status = 0;
          }

          $('#contest_related').remove();
          $('.cq_div').append('<div style="margin-top:0px!important;display:none;" id="contest_related" class="bg-gradient-light p-2"><label>Put Your Remarks Here:</label>&nbsp;&nbsp;<textarea class="form-control" rows="3" placeholder="Enter remarks here..." id="remarks" required></textarea><br><button type="button" class="btn btn-sm btn-primary bg-gradient-primary text-white" id="save_remarks">Save</button>&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger bg-gradient-danger text-white" id="cancel_remark">Cancel</button></div>');
          $('.cq_div').find('#contest_related:last').slideDown('fast');
                  
            // Cancel button click to remove booking detail box
            $('#cancel_remark').click(function(){
              $('#contest_related').slideUp("slow", function() { $('#contest_related').remove();});
            });
                  
            $('#save_remarks').click(function(){
              var remarks = $('#remarks').val();
              if(remarks != ''){
                $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: '/admin/inbound_module/contest_related',
                  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                  data: {'query_id':id,'voucher_code':voucher_code,'mobile_no':mobile_no,'remarks':remarks,'voucher_status':voucher_status},
                  beforeSend:function(){
                    $('#contest_related').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please wait...');
                  },
                  success : function(data){
                    $('#contest_related').html(data.message);
                  }
                });
              }else{
                $('#remarks').css('border','1px solid red');
              }
            });
          }
        });    

      }else if(id == 12){ // Voucher / Offer Code Related

         $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details',
        data: {'voucher_code':voucher_code},
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          console.log(data);
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          if(data.d != 'Not Booked'){
            var voucher_status = 1;
          }else{
            var voucher_status = 0;
          }

          $('#voucher_offer_code_related').remove();
          $('.cq_div').append('<div style="margin-top:0px!important;display:none;" id="voucher_offer_code_related" class="bg-gradient-light p-2"><label>Put Your Remarks Here:</label>&nbsp;&nbsp;<textarea class="form-control" rows="3" placeholder="Enter remarks here..." id="remarks" required></textarea><br><button type="button" class="btn btn-sm btn-primary bg-gradient-primary text-white" id="save_remarks">Save</button>&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger bg-gradient-danger text-white" id="cancel_remark">Cancel</button></div>');
          $('.cq_div').find('#voucher_offer_code_related:last').slideDown('fast');
                  
            // Cancel button click to remove booking detail box
            $('#cancel_remark').click(function(){
              $('#voucher_offer_code_related').slideUp("slow", function() { $('#voucher_offer_code_related').remove();});
            });
                  
            $('#save_remarks').click(function(){
              var remarks = $('#remarks').val();
              if(remarks != ''){
                $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: '/admin/inbound_module/voucher_offer_code_related',
                  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                  data: {'query_id':id,'voucher_code':voucher_code,'mobile_no':mobile_no,'remarks':remarks,'voucher_status':voucher_status},
                  beforeSend:function(){
                    $('#voucher_offer_code_related').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please wait...');
                  },
                  success : function(data){
                    $('#voucher_offer_code_related').html(data.message);
                  }
                });
              }else{
                $('#remarks').css('border','1px solid red');
              }
            });
        }
      })

    }else if(id == 13){ // Reward / Offer code Resend

      $.ajax({
        type: "GET",
        dataType: "json",
        url: '/admin/inbound_module/booking_details',
        data: {'voucher_code':voucher_code},
        beforeSend: function(){
          $('.cq_div').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...'); // text loader before ajax request gets completed.
        },
        success: function(data){
          $('.cq_div').removeClass('bg-gradient-light alert alert-success text-gradient-success').html(''); // text loader before ajax request gets completed.
          console.log(data);
          if(data.d.prefereddate1){
            var booking_date   = date_value_format(data.d.prefereddate1);// Date format like : 01-Sept-2020   
          }

          if(data.d.preferedTime1){
            var booking_time   = time_value_format(data.d.preferedTime1);// Time format like : 5:29PM   
          }
          
          if(data.d.status == '1' || data.d.status == '2'){

            var booking_venue  = data.d.booking_venue;
            var button_name    = 'Reward/Offer Code';
            var btn_id         = 'reward_offer_code_resend';
            var btn_class      = 'class="btn btn-success btn-sm col-md-3 offset-md-5 bg-gradient-success text-white"';
            var booking_detail = booking_details(product_name,booking_date,booking_time,booking_venue,btn_id,btn_class,button_name);

            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');
            $('#reward_offer_code_resend').click(function(){
              $.ajax({
                type: "GET",
                dataType: "json",
                url:"/admin/inbound_module/check_request",
                data:{'voucher_code':voucher_code,'mobile_no':mobile_no,'title': 'Reward / Offer Code'},
                beforeSend: function(){
                    $('#booking_detail').removeClass('bg-gradient-light').addClass('alert alert-success text-gradient-success').html('Please Wait...');
                  },
                success : function(response){
                  if(response.data == 0){
                    $('.cancelBooking').remove();
                    $('#booking_detail').addClass('alert alert-danger text-gradient-danger text-center font-weight-bold').html('Resend request time out');
                  }else if(response.data == 1){
                    var booking_date = date_value_format(response.booking_time);
                    var booking_time = time_value_format(response.booking_time); 
                    $.ajax({
                      type: "POST",
                      dataType: "json",
                      url: '/admin/inbound_module/reward_offer_code_resend',
                      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                      data: 
                        {
                          'voucher_code':voucher_code,
                          'email':response.email_id,
                          'mobile_no':mobile_no,
                          'booking_date':booking_date,
                          'booking_time':booking_time
                        },
                      beforeSend: function(){
                        $('#booking_detail').addClass('alert alert-success text-gradient-success').html('Please Wait...');
                      },
                      success : function(data){
                        $('#booking_detail').addClass('alert alert-success text-gradient-success').html(data.message);
                      }
                    });
                  }
                }
              });
            })  
          }else{

            var booking_detail = '<div id="booking_detail" style="display:none;text-align:center;font-weight:800;" class="alert alert-danger text-gradient-danger">';
                booking_detail +=   'No booking for this voucher';
                booking_detail += '</div>';

            $('.cq_div').append(booking_detail);
            $('.cq_div').find('#booking_detail:last').slideDown('fast');
          }
        }
      });

    }else{

      $('#register').remove();                  // remove div for unable to register / booking div if appended already.
      $('.cancelBooking').remove();             // remove cancle booking div if already exists.
      $('#booking_detail').remove();            // remove booking details div on every event onchange if already present.
      $('#contest_related').remove();           // remove contest related div if already exists. 
      $('.microsite_issue').remove();           // remove microsite issue div if already exists.
      $('#validity_related').remove();          // remove validity related div if already exists.
      $('.venue_related_issue').remove();       // remove venue related div if already exists.
      $('#voucher_offer_code_related').remove();// remove vocuher / offer code related div if already exists.
    }
  }

  // Append dynamic div for booking details...
  function booking_details(product_name,booking_date,booking_time,booking_venue,btn_id,btn_class,button_name){
    var booking_detail  = '<div id="booking_detail" style="display:none;" class="bg-gradient-light">';
        booking_detail +=   '<div class="cancelBooking border p-2" style="padding-top:10px!important;margin-top:0px!important">';
        booking_detail +=     '<strong>BOOKING DETAILS:</strong>';
        booking_detail +=     '<label>Product Name : <strong>'+product_name+'</strong></label>';
        booking_detail +=     '<div class="row">';
        booking_detail +=       '<div class="col-md-4">';
        booking_detail +=         '<label><strong>Booking Date : </strong></label>';
        booking_detail +=         '<label class="bg-gradient-primary text-white p-1 rounded">'+booking_date+'</label>';
        booking_detail +=       '</div>';
        booking_detail +=       '<div class="col-md-4">';
        booking_detail +=         '<label><strong>Booking Time : </strong></label>';
        booking_detail +=         '<label class="bg-gradient-primary text-white p-1 rounded">'+booking_time+' </label>';
        booking_detail +=       '</div>';
        booking_detail +=       '<div class="col-md-4">'; 
        booking_detail +=         '<label><strong>Booking Venue : </strong></label>';
        booking_detail +=         '<label class="bg-gradient-primary text-white p-1 rounded">'+booking_venue+'</label>';
        booking_detail +=       '</div>';
        booking_detail +=     '</div>';
        booking_detail +=     '<div class="row">';
        booking_detail +=       '<input type="button" id="'+btn_id+'" '+btn_class+' value="'+button_name+'">';
        booking_detail +=     '</div>';
        booking_detail +=   '</div>';
        booking_detail += '</div>';

     return booking_detail;   
  }

//--------------------- Inbound Module popup window code -----------------------//


//=============== Custom Date and Time Format function Starts here =============//
function date_value_format(date_value){ // create custom date format like : 20-Aug-2020
  // DATE STARTS HERE -------------------------------------------------
    var formattedDate = new Date((date_value).split(' ')[0]);
    var days = formattedDate.getDate();
    var months = formattedDate.getMonth();
        months += 1;  // JavaScript months are 0-11
    var years = formattedDate.getFullYear();
        days = (days.toString().length < 2) ? "0"+ days : days;
        months_arr = {0:"Jan",1:"Feb",3:"March",4:"April",5:"May",6:"June",7:"July",8:"Aug",9:"Sept",10:"Oct",11:"Nov",12:"Dec"};
        Updated_date = days + "-" + months_arr[months] + "-" + years ;
    return Updated_date;    
  //DATE ENDS HERE -----------------------------------------------------
}

function time_value_format(time_value){ // create custom time format like : 09:16 AM
  // TIME STARTS HERE --------------------------------------------------
    var timeString = (time_value).split(' ')[1];
    var hourEnd = timeString.indexOf(":");
    var H = +timeString.substr(0, hourEnd);
    var h = H % 12 || 12;
    var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        timeString = h + timeString.substr(hourEnd, 3) + ampm;
    return timeString;    
  // TIME ENDS HERE ---------------------------------------------------
}
//=============== Custom Date and Time Format function Ends here =============// 

function viewQueryDetails(e){
  var voucherCode = $(e).attr('data-id');
  $('#tbl_div_query').remove();
  $.ajax({
      type: "POST",
      dataType: "json",
      url: '/admin/inbound_module/queryDetails',
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      data: { 'voucher_code':voucherCode },
      success : function(data){
        console.log(data);
          $('#myModal').css({'filter':'blur(2px)'});
          if(data != ""){
          // Pop up window content.
            var modal_body   = '<table id="tbl_div_query" class="table  table-striped table-bordered bg-gradient-light" width="100%" cellspacing="0">';
                modal_body  +=  '<tr><th>Voucher Code : </th><td colspan="3" class="text-center"><b>'+data[0].voucher_code+'</b></td></tr>';
                modal_body  +=  '<tr>';
                modal_body  +=  '<th>Custmer\'s Mobile No : </th>';
                modal_body  +=  '<th>Query : </th>';
                modal_body  +=  '<th>Remarks : </th>';
                modal_body  +=  '</tr>';
                modal_body  +=  '<tr>';
                modal_body  +=  '<td><table cellspacing="0" cellpadding="0" width="100%" style="border:none;text-align:center;">';  
                $.each(data,function(index,value){
                  modal_body  +=  '<tr><td>'+value.customer_mob_no+'</td></tr>';
                });
                modal_body  +=  '</table></td><td><table cellspacing="0" cellpadding="0" width="100%" style="border:none;text-align:center;">';
                $.each(data,function(index,value){
                  modal_body  += '<tr><td>'+value.cust_query.name+'</td></tr>';              
                });
                modal_body  +=  '</table></td><td><table cellspacing="0" cellpadding="0" width="100%" style="border:none;text-align:center;">';
                $.each(data,function(index,value){
                  modal_body  += '<tr><td>'+value.remarks+'</td></tr>';
                });
                modal_body  +=  '</table></td>';
                modal_body  +=  '</tr>';
                modal_body  += '</table>';
                

          $('#view_queryDetails').find('.modal-body').append(modal_body);
          $('#view_queryDetails').modal('show').css({'padding-right':'43px','z-index':'1050'});
        }else{
          var modal_body   = '<table id="tbl_div_query" class="table  table-striped table-bordered bg-gradient-light" width="100%" cellspacing="0">';
              modal_body  +=  '<tr><th>Voucher Code : </th><td colspan="3" class="text-center"><b>'+voucherCode+'</b></td></tr>';
              modal_body  +=  '<tr><td colspan="3" style="text-align:center;color:red"><strong>There is no query exists for this voucher code.</strong></td></tr>';  
              modal_body  += '</table>';
          $('#view_queryDetails').find('.modal-body').append(modal_body);
          $('#view_queryDetails').modal('show').css({'padding-right':'43px','z-index':'1050'});
        }
      },
    });

    $('#view_queryDetails_btn').click(function(){
        $('#myModal').css({'filter':''});
    });
}

   
     
     