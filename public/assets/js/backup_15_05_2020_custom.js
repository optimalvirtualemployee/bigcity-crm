//-----------Start JS For Campaign Module Form Validation-------------------//
    function campaignbtn(){
      var isError = false;
      $('#campaignfrm')
      .find('input[name=campaign_name],input[name=campaign_estimate_no],textarea,select:not(.v_spec)')
      .each(function()
        {
          var palceholder =  $(this).attr('placeholder');
          // alert(palceholder);
          $(this).parent().find('.error').html('');
          $(this).css('border','')
          if($(this).val() == "" || $(this).val() == 0){
            $(this).css('border','1px solid red');
            isError = true;'';   
          }
        });
//--------------------------------------------------------------------//
      if($('#mt').val() == 1 || $('#mt').val() == 3){
        if($('input[name=sms_keyword]').val() == ""){
          $('input[name=sms_keyword]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=sms_keyword]').css('border','');
            isError = false;'';
        }

        if($('input[name=sms_long_code]').val() == ""){
          $('input[name=sms_long_code]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=sms_long_code]').css('border','');
            isError = false;'';
        }

        if($('input[name=sms_keyword]').val() == "" && $('input[name=sms_long_code]').val() == ""){
          $('input[name=sms_keyword]').css('border','1px solid red');
          $('input[name=sms_long_code]').css('border','1px solid red');
          isError = true;'';
        }else{
          $('input[name=sms_keyword]').css('border','');
          $('input[name=sms_long_code]').css('border','');
            isError = false;'';
        }

      }

      if($('#mt').val() == 2 || $('#mt').val() == 3){
        if($('input[name=web_microsite_name]').val() == ""){
          $('input[name=web_microsite_name]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=web_microsite_name]').css('border','');
            isError = false;'';
        }
      }
//---------------------------------------------------------------//
      if($('#vt').val() == 1 || $('#vt').val() == 3 ){
        if($('.v_spec').val() == 0){
          $('.v_spec').css('border','1px solid red');
            isError = true;'';
        }else{
          $('.v_spec').css('border','');
            isError = false;'';
        }
      }
//---------------------------------------------------------------//
      if($('select[name=is_printing_bigcity]').val() == 1){
        if($('input[name=campaign_voucher_cost]').val() == ""){
          $('input[name=campaign_voucher_cost]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=campaign_voucher_cost]').css('border','');
            isError = false;'';
        }

        if($('input[name=campaign_cost_to_client]').val() == ""){
          $('input[name=campaign_cost_to_client]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=campaign_cost_to_client]').css('border','');
            isError = false;'';
        }

        if($('input[name=campaign_cost_to_bigcity]').val() == ""){
          $('input[name=campaign_cost_to_bigcity]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=campaign_cost_to_bigcity]').css('border','');
            isError = false;'';
        }
      }
//---------------------------------------------------------------//     
      if(isError == false){
        $('#campaignfrm').submit();
      }
    }

    function mt_function(id){
      if(id == 1){
        $('#sms').show();
        $('#web').hide();
      }else if(id == 2){
        $('#web').show();
        $('#sms').hide();
      }else if(id == 3){
        $('#sms').show();
        $('#web').show();
      }else{
        $('#sms').hide();
        $('#web').hide();
      }
    };

    function vt_function(id){
      if(id == 1 || id == 3){
        $('#voucher_spec').show();
      }else{
        $('#voucher_spec').hide();
      }
    };

    function printing(id){
      if(id == 1){
        $('#is_printing_done_by_bigcity_yes_show').show();
      }else{
        $('#is_printing_done_by_bigcity_yes_show').hide();
      }
    };
    
//-----------End JS For Campaign Module Form Validation-------------------//

//-----------Start JS For Campaign Module-------------------//

window.onload = function() {
   var mechanic_type_id = $('#mt').val();
   var voucher_type_id = $('#vt').val();
   var is_printing_bigcity = $('#is_printing_bigcity').val();
   

     if(mechanic_type_id == 1){
       $('#sms').show();
       $('#web').hide();
     }else if(mechanic_type_id == 2){
       $('#web').show();
       $('#sms').hide();
     }else if(mechanic_type_id == 3){
       $('#sms').show();
       $('#web').show();
     }else{
       $('#sms').hide();
       $('#web').hide();
     }

     if(voucher_type_id == 1 || voucher_type_id == 3){
       $('#voucher_spec').show();
     }else{
       $('#voucher_spec').hide();
     }

     if(is_printing_bigcity == 1){
      $('#is_printing_done_by_bigcity_yes_show').show();
    }else{
      $('#is_printing_done_by_bigcity_yes_show').hide();
    }
    

}
//-----------End JS For Campaign Input Value-------------------//

//-----------Start JS For Remove Alert Message-------------------//

setTimeout(function(){ $(".alert-danger").remove();
$(".alert-success").remove() }, 3000);

//-----------End JS For Remove Alert Message-------------------//

//-----------Start JS For File Upload Preview-------------------//

$(function () {
   
    $("#exampleFormControlFile1").change(function () {

     
        $("#dvPreview").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (regex.test($(this).val().toLowerCase())) {
            if ($.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                $("#dvPreview").show();
                $("#dvPreview")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
            }
            else {
                if (typeof (FileReader) != "undefined") {
                    $("#dvPreview").show();
                    $("#dvPreview").append("<img />");
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#dvPreview img").attr("src", e.target.result);
                    }
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            }
        } else {
            alert("Please upload a valid image file.");
            return false;
        }
    });
});

//-----------End JS For File Upload Preview-------------------//
//-----------End JS For Venue Module Form Validation-------------------//

function venuebtn(){

      var isError = false;
      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      $('#venuefrm')
      .find('input[name=venue_name],textarea,select:not(#rating)')
      .each(function()
        {
          var palceholder =  $(this).attr('placeholder');
          
          // alert(palceholder);
          $(this).parent().find('.error').html('');
          $(this).css('border','')
          if($(this).val() == "" || $(this).val() == 0){
            $(this).css('border','1px solid red');
            isError = true;'';   
          }
        });
        if($('.mrp_of_product').val() == ""){
            $('.mrp_of_product').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.cost_of_product').val() == ""){
            $('.cost_of_product').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.billing_name').val() == ""){
            $('.billing_name').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.billing_phone').val() == ""){
            $('.billing_phone').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.billing_email').val() == ""){
            $('.billing_email').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.billing_designation').val() == ""){
            $('.billing_designation').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.booking_name').val() == ""){
            $('.booking_name').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.booking_phone').val() == ""){
            $('.booking_phone').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.booking_email').val() == ""){
            $('.booking_email').css('border','1px solid red');
            isError = true;'';   
          }
          if($('.booking_designation').val() == ""){
            $('.booking_designation').css('border','1px solid red');
            isError = true;'';   
          }
//--------------------------------------------------------------------//
      if($("#venue_type").val() == 2){
        if($('input[name=address]').val() == ""){
          $('input[name=address]').css('border','1px solid red');
            isError = true;'';
        }else{
          $('input[name=address]').css('border','');
            isError = false;'';
        }
        
        if($('#rating').val() == 0){
          $('#rating').css('border','1px solid red');
            isError = true;'';
        }else{

          $('#rating').css('border','');
            isError = false;'';
        }

        

      }
      if(isError == false){
        $('#venuefrm').submit();
      }
    }
    //-----------End JS For Venue Module Form Validation-------------------//
    //-----------Strat JS For Venue Type On Change Value-------------------//
    $("#venue_type").change(function(){

        if($(this).val()  == '1'){

          $(".address").hide();
          $(".rating").hide();
          $(".venueUpload").show();
        }else{
          $(".address").show();
          $(".rating").show();
          $(".venueUpload").hide();
        }
        
      }).change();
      
      //-----------End JS For Venue Type On Change Value-------------------//
      
      